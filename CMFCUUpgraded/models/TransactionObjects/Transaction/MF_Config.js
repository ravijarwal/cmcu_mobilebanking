define([],function(){
	var mappings = {
		"Transaction_id" : "Transaction_id",
		"dbpErrCode" : "dbpErrCode",
		"dbpErrMsg" : "dbpErrMsg",
		"opstatus" : "opstatus",
		"httpStatusCode" : "httpStatusCode",
	};
	Object.freeze(mappings);
	
	var typings = {
		"Transaction_id" : "string",
		"dbpErrCode" : "string",
		"dbpErrMsg" : "string",
		"opstatus" : "string",
		"httpStatusCode" : "string",
	}
	Object.freeze(typings);
	
	var primaryKeys = [
					"Transaction_id",
	];
	Object.freeze(primaryKeys);
	
	var config = {
		mappings : mappings,
		typings : typings,
		primaryKeys : primaryKeys,
		serviceName : "TransactionObjects",
		tableName : "Transaction"
	};
	Object.freeze(config);
	
	return config;
})
