define([],function(){
	var BaseRepository = kony.mvc.Data.BaseRepository;
	
	//Create the Repository Class
	function subAccountsRepository(modelDefinition, config, defaultAppMode, dataSourceFactory, injectedDataSource) {
		BaseRepository.call(this, modelDefinition, config, defaultAppMode, dataSourceFactory, injectedDataSource);
	};
	
	//Setting BaseRepository as Parent to this Repository
	subAccountsRepository.prototype = Object.create(BaseRepository.prototype);
	subAccountsRepository.prototype.constructor = subAccountsRepository;

	//For Operation 'createNewSubAccounts' with service id 'CreateSubAccount2387'
	subAccountsRepository.prototype.createNewSubAccounts = function(params,onCompletion){
		return subAccountsRepository.prototype.customVerb('createNewSubAccounts',params, onCompletion);
	};
	
	
	return subAccountsRepository;
})