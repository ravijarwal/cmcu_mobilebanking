define([],function(){
	var BaseModel = kony.mvc.Data.BaseModel;
	var preProcessorCallback;
    var postProcessorCallback;
    var objectMetadata;
    var context = {"object" : "subAccounts", "objectService" : "RBObjects"};
	
	var setterFunctions = {
		accountNumber : function(val, state){
			context["field"]  = "accountNumber";
			context["metadata"] = (objectMetadata ? objectMetadata["accountNumber"] : null);
			state['accountNumber'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
		},
		idType : function(val, state){
			context["field"]  = "idType";
			context["metadata"] = (objectMetadata ? objectMetadata["idType"] : null);
			state['idType'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
		},
		recordType : function(val, state){
			context["field"]  = "recordType";
			context["metadata"] = (objectMetadata ? objectMetadata["recordType"] : null);
			state['recordType'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
		},
		id : function(val, state){
			context["field"]  = "id";
			context["metadata"] = (objectMetadata ? objectMetadata["id"] : null);
			state['id'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
		},
	};
	
	
	//Create the Model Class
	function subAccounts(defaultValues){
		var privateState = {};
			context["field"]  = "accountNumber";
			context["metadata"] = (objectMetadata ? objectMetadata["accountNumber"] : null);
			privateState.accountNumber = defaultValues?(defaultValues["accountNumber"]?kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["accountNumber"], context):null):null;
			context["field"]  = "idType";
			context["metadata"] = (objectMetadata ? objectMetadata["idType"] : null);
			privateState.idType = defaultValues?(defaultValues["idType"]?kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["idType"], context):null):null;
			context["field"]  = "recordType";
			context["metadata"] = (objectMetadata ? objectMetadata["recordType"] : null);
			privateState.recordType = defaultValues?(defaultValues["recordType"]?kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["recordType"], context):null):null;
			context["field"]  = "id";
			context["metadata"] = (objectMetadata ? objectMetadata["id"] : null);
			privateState.id = defaultValues?(defaultValues["id"]?kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["id"], context):null):null;
		//Using parent contructor to create other properties req. to kony sdk	
			BaseModel.call(this);

		//Defining Getter/Setters
			Object.defineProperties(this,{
				"accountNumber" : {
					get : function(){
						context["field"]  = "accountNumber";
			        	context["metadata"] = (objectMetadata ? objectMetadata["accountNumber"] : null);
						return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.accountNumber, context);},
					set : function(val){
						setterFunctions['accountNumber'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"idType" : {
					get : function(){
						context["field"]  = "idType";
			        	context["metadata"] = (objectMetadata ? objectMetadata["idType"] : null);
						return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.idType, context);},
					set : function(val){
						setterFunctions['idType'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"recordType" : {
					get : function(){
						context["field"]  = "recordType";
			        	context["metadata"] = (objectMetadata ? objectMetadata["recordType"] : null);
						return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.recordType, context);},
					set : function(val){
						setterFunctions['recordType'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"id" : {
					get : function(){
						context["field"]  = "id";
			        	context["metadata"] = (objectMetadata ? objectMetadata["id"] : null);
						return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.id, context);},
					set : function(val){
						setterFunctions['id'].call(this,val,privateState);
					},
					enumerable : true,
				},
			});
			
			//converts model object to json object.
			this.toJsonInternal = function() {
				return Object.assign({}, privateState);
			};

			//overwrites object state with provided json value in argument.
			this.fromJsonInternal = function(value) {
									privateState.accountNumber = value?(value["accountNumber"]?value["accountNumber"]:null):null;
					privateState.idType = value?(value["idType"]?value["idType"]:null):null;
					privateState.recordType = value?(value["recordType"]?value["recordType"]:null):null;
					privateState.id = value?(value["id"]?value["id"]:null):null;
			};

	}
	
	//Setting BaseModel as Parent to this Model
	BaseModel.isParentOf(subAccounts);
	
	//Create new class level validator object
	BaseModel.Validator.call(subAccounts);
	
	var registerValidatorBackup = subAccounts.registerValidator;
	
	subAccounts.registerValidator = function(){
		var propName = arguments[0];
		if(!setterFunctions[propName].changed){
			var setterBackup = setterFunctions[propName];
			setterFunctions[arguments[0]] = function(){
				if( subAccounts.isValid(this, propName, val) ){
					return setterBackup.apply(null, arguments);
				}else{
					throw Error("Validation failed for "+ propName +" : "+val);
				}
			}
			setterFunctions[arguments[0]].changed = true;
		}
		return registerValidatorBackup.apply(null, arguments);
	}
	
	//Extending Model for custom operations
	
	var relations = [
	];
	
	subAccounts.relations = relations;
	
	subAccounts.prototype.isValid = function(){
		return subAccounts.isValid(this);
	};
	
	subAccounts.prototype.objModelName = "subAccounts";
	
	/*This API allows registration of preprocessors and postprocessors for model.
	 *It also fetches object metadata for object. 
	 *Options Supported
	 *preProcessor  - preprocessor function for use with setters.
	 *postProcessor - post processor callback for use with getters.
	 *getFromServer - value set to true will fetch metadata from network else from cache.
	 */
	subAccounts.registerProcessors = function(options, successCallback, failureCallback) {
	
		if(!options) {
			options = {};
		}
			
		if(options && ((options["preProcessor"] && typeof(options["preProcessor"]) === "function") || !options["preProcessor"])) {
			preProcessorCallback = options["preProcessor"];
		}
		
		if(options && ((options["postProcessor"] && typeof(options["postProcessor"]) === "function") || !options["postProcessor"])){
			postProcessorCallback = options["postProcessor"];
		}
		
		function metaDataSuccess(res) {
			objectMetadata = kony.mvc.util.ProcessorUtils.convertObjectMetadataToFieldMetadataMap(res);
			successCallback();
		}
		
		function metaDataFailure(err) {
			failureCallback(err);
		}
		
		kony.mvc.util.ProcessorUtils.getMetadataForObject("RBObjects", "subAccounts", options, metaDataSuccess, metaDataFailure);
	};
	
	//clone the object provided in argument.
	subAccounts.clone = function(objectToClone) {
		var clonedObj = new subAccounts();
		clonedObj.fromJsonInternal(objectToClone.toJsonInternal());
		return clonedObj;
	};
	
	return subAccounts;
});