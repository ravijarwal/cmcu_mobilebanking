define({
  makeATransfer : function(description,amount) {
    var transactionManager = applicationManager.getTransactionManager();
    var navMan=applicationManager.getNavigationManager();
    transactionManager.setTransactionAttribute("transactionsNotes",description);
    transactionManager.setTransactionAttribute("amount",amount);
    var toaccntData=navMan.getCustomInfo("frmTransfersFromAccount");
    var tranAmt=navMan.getCustomInfo("frmTransferAmount");
    var transferAmount=amount;
    var amount=transferAmount.substring(1,transferAmount.length);
   
    var recType=transactionManager.getTransactionObject().toAccountType.toUpperCase();
    var donType=transactionManager.getTransactionObject().fromAccountType.toUpperCase();
    var params=
        {
          "donorAccountNumber":transactionManager.getTransactionObject().fromAccountNumber,
          "recipientAccountNumber":transactionManager.getTransactionObject().toAccountNumber,
          "donorId":  transactionManager.getTransactionObject().fromAccountID,
          "recipientId":transactionManager.getTransactionObject().toAccountID,
          "donorType":donType,
          "recipientType":recType,
          "transferAmount":amount,
          "comment":transactionManager.getTransactionObject().transactionsNotes

        }

    if (transactionManager.getTransactionObject().transactionId !== "" && transactionManager.getTransactionObject().transactionId !== null && transactionManager.getTransactionObject().transactionId != null) {
     
      var transactionManager = applicationManager.getTransactionManager();
      transactionManager.updateTransaction(transactionManager.getTransactionObject(), this.presentationMakeATransferSuccess, this.presentationMakeATransferError);
    } else {
       var authMode = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
    	authMode.presentationController.showDownTimeMessage();

      var transactionManager = applicationManager.getTransactionManager();
      transactionManager.createTransaction(params, this.presentationMakeATransferSuccess, this.presentationMakeATransferError);
    }

  },
  presentationMakeATransferSuccess : function(resp) {
    var navManager = applicationManager.getNavigationManager();
    var navigateToForm = navManager.getEntryPoint("makeatransfer");
    var transactionManager = applicationManager.getTransactionManager(); 

    var toAccountID = transactionManager.getTransactionObject().toAccountNumber;
    scope_TransfersPresentationController.clearBuilderNonGeneratedAttributes();
    transactionManager.clearTransferObject();
    if (navigateToForm !== "frmAccountDetails") {

      var transMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransactionModule");
      transMod.presentationController.getTransactions();
      var navMan = applicationManager.getNavigationManager();
      var data = {};
      data.type = "success";
      data.typeOfTransaction = "create";
      data.res = resp;
      data.popup="InternalTransfer";
      data.referenceId=resp["referenceId"];
      navMan.setCustomInfo("frmTransfersSuccess", data);
      var accountMod = applicationManager.getAccountManager();
      var transMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransactionModule");
      transMod.presentationController.getTransactions();
      
      //kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountManager");
      // var respse=   scope_AuthPresenter.getAllAccounts();
      // var accountManager = applicationManager.getAccountManager();
      // accountManager.fetchInternalAccounts(scope_Acc_Pres.presentationAccountsSucc, scope_Acc_Pres.presentationAccountsErr);

      //alert("respse"+respse);
      navMan.navigateTo("frmTransfers");
    } else {
      var navMan = applicationManager.getNavigationManager();
      var data = {};
      data.type = "success";
      data.typeOfTransaction = "create";
      data.res = resp;

      navMan.setCustomInfo("frmAccountDetails", data);
      var accountMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountModule");
      accountMod.presentationController.fetchAccountTransactions(toAccountID);

    }

  },
  presentationMakeATransferError :function(err) 
  {
    // alert("error msg"+err["errorMessage"]);
    if (err["isServerUnreachable"]) {
      applicationManager.getPresentationUtility().dismissLoadingScreen();
      applicationManager.getPresentationInterruptHandler().showErrorMessage("postLogin", err);
    } else {
      var navMan = applicationManager.getNavigationManager();
      var navigateToForm = navMan.getEntryPoint("makeatransfer");
      var transactionManager = applicationManager.getTransactionManager();
      var toAccountID = transactionManager.getTransactionObject().toAccountNumber;
      scope_TransfersPresentationController.clearBuilderNonGeneratedAttributes();
      transactionManager.clearTransferObject();
      if (navigateToForm !== "frmAccountDetails") {

        var transMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransactionModule");
        transMod.presentationController.getTransactions();
        var navMan = applicationManager.getNavigationManager();
        var data = {};
        data.type = "error";
        data.typeOfTransaction = "create";
        data.res = err["errorMessage"];
        navMan.setCustomInfo("frmTransfers", data);
      } else {
        var navMan = applicationManager.getNavigationManager();
        var data = {};
        data.type = "error";
        data.typeOfTransaction = "create";
        data.res = err["errorMessage"];
        navMan.setCustomInfo("frmAccountDetails", data);
        var accountMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountModule");
        accountMod.presentationController.fetchAccountTransactions(toAccountID);
      }
    }

  },

  /**

  This Method is used to call the Businessctrller Method to
  call customverb for updating Share Scheduled transfer
  */
  deleteTransaction : function(data) {
    // alert("data in PC extsn"+data[0]);
    var accNum=data[0].acctNum;
    var shareId=data[0].shareId;
    //shareId=parseInt(shareId);
    var locator=data[0].locator;
    var currDate = new Date();
    var curr_date = currDate.getDate();
    var curr_month = currDate.getMonth() + 1; //Months are zero based
    if(curr_month<10)
    {
      curr_month="0"+curr_month;
    }
    if(curr_date<10)
    {
      curr_date="0"+curr_date;
    }
    var curr_year = currDate.getFullYear();
    currDate=curr_year+"-"+curr_month+"-"+curr_date;
    //updateShareTransfer i/p params
    var criteria = {
      "accountNumber":accNum,    
      "STEffectiveDate":currDate,
      "ExpirationDate":currDate,
      "shareId":shareId,
      "ShareTransferLocator":locator
    }
     var authMode = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
    authMode.presentationController.showDownTimeMessage();    
    var transactionObj = applicationManager.getTransactionManager();
    transactionObj.deleteTransaction(criteria, this.deleteScheduledSuccess, this.deleteScheduledError);

  },
  /**

  This Method is used to call the Businessctrller Method to
  call cusomverb for updating Loan Scheduled transfer
  */
  deleteLoanScheduledTransfer : function(data) {
    // alert("data in PC extsn"+data);
    var accNum=data[0].acctNum;
    var loanId=data[0].loanId;
    var locator=data[0].locator;
    var currDate = new Date();
    var curr_date = currDate.getDate();
    var curr_month = currDate.getMonth() + 1; //Months are zero based
    if(curr_month<10)
    {
      curr_month="0"+curr_month;
    }
    if(curr_date<10)
    {
      curr_date="0"+curr_date;
    }
    var curr_year = currDate.getFullYear();
    
    currDate=curr_year+"-"+curr_month+"-"+curr_date;
//updateLoanScheduleTransfer i/p params
    var criteria = {
      "accountNumber":accNum,
      "STEffectiveDate":currDate,
      "ExpirationDate":currDate,
      "loanId":loanId,
      "LTLocator":locator
    }
 		var authMode = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
    	authMode.presentationController.showDownTimeMessage();

    var transactionObj = applicationManager.getTransactionManager();
    // transactionObj.deleteTransaction(criteria, this.deleteScheduledSuccess, this.deleteScheduledError);
    transactionObj.deleteLoanScheduledTransaction(criteria, this.deleteScheduledSuccess, this.deleteScheduledError);
  },

  updateEftScheduledTransaction: function(data) {
    // alert("data in PC extsn"+data);
    var accNum=data[0].acctNum;

    var locator=data[0].locator;
    var currDate = new Date();
    var curr_date = currDate.getDate();
    var curr_month = currDate.getMonth() + 1; //Months are zero based
    if(curr_month<10)
    {
      curr_month="0"+curr_month;
    }
    if(curr_date<10)
    {
      curr_date="0"+curr_date;
    }
    var curr_year = currDate.getFullYear();
    //var d1 = new Date();
    // d1.toString('yyyy-MM-dd'); 
    currDate=curr_year+"-"+curr_month+"-"+curr_date;
    var criteria = {
      "accountNumber":accNum,
      "EffectiveDate":currDate,
      "ExpirationDate":currDate,     
      "eftLocator":locator
    }
     var authMode = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
     authMode.presentationController.showDownTimeMessage();

    var transactionObj = applicationManager.getTransactionManager();
    // transactionObj.deleteTransaction(criteria, this.deleteScheduledSuccess, this.deleteScheduledError);
    transactionObj.updateEftScheduledTransaction(criteria, this.deleteScheduledSuccess, this.deleteScheduledError);
  },

 deleteEftScheduledTransaction: function(data) {
    //alert("data in PC extsn"+data);
    var accNum=data[0].acctNum;

    var locator=data[0].locator;
      var criteria = {
      "accountNumber":accNum,         
      "eftLocator":locator
    }
      var authMode = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
    	authMode.presentationController.showDownTimeMessage();
    var transactionObj = applicationManager.getTransactionManager();
    // transactionObj.deleteTransaction(criteria, this.deleteScheduledSuccess, this.deleteScheduledError);
    transactionObj.deleteEftScheduledTransaction(criteria, this.deleteScheduledSuccess, this.deleteScheduledError);
  },


  deleteScheduledSuccess : function(res) {
    // alert("delete scussess****"+JSON.stringify(res));
    applicationManager.getPresentationUtility().dismissLoadingScreen();
    var transMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransactionModule");
    transMod.presentationController.getTransactions();
    var navMan = applicationManager.getNavigationManager();
    var data = {};
    data.type = "success";
    data.typeOfTransaction = "delete";
    data.res = res;

    //navMan.setCustomInfo("frmTransfers", data);
    navMan.navigateTo("frmTransfers");
  },
  deleteScheduledError : function(err) {
    //alert("delete failure"+JSON.stringify(err));
    if (err["isServerUnreachable"]) {
      applicationManager.getPresentationUtility().dismissLoadingScreen();
      applicationManager.getPresentationInterruptHandler().showErrorMessage("postLogin", err);
    } else {
      var navMan = applicationManager.getNavigationManager();
      var transMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransactionModule");
      transMod.presentationController.getTransactions();
      var data = {};

      data.type = "error";
      data.res = err["errorMessage"];
      navMan.setCustomInfo("frmTransfers", data);
    }
  }
});