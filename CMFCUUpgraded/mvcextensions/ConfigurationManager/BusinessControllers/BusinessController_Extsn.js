define({
 
  getHamburgerMenuItems : function() {
      this.hamburgerMenuItems=[
        {
          "img": "accounts.png",
          "text": this.constants.MENUACCOUNTS,
          "info":null
        },   
        {
          "img": "transfer.png",
          "text": this.constants.MENUTRANSFERS,
          "info":null
        },
        {
          "img": "billpay.png",
          "text": this.constants.MENUBILLPAY,
          "info":null
        },
        {
          "img": "checkdepositimg.png",
          "text": this.constants.MENUCHECKDEPOSIT,
          "info":null
        },
        {
          "img": "cardlesscash.png",
          "text": this.constants.MENUCARDLESS,
          "info":null
        }, 
        {
          "img": "cardmange.png",
          "text": this.constants.MENUCARDMANAGEMENT,
          "info":null
        },
        {
          "img": "mangeothrbank.png",
          "text": this.constants.MENUMANAGEOTHERBANKACCOUNTS,
          "info": null
        },

        {
          "img": "opennew.png",
          "text": this.constants.MENUOPENACOUNT,
          "info":null
        },
        {
          "img": "billpay.png",
          "text": this.constants.MENUPFMMYMONEY,
          "info":null
        },
        {
          "img": "message.png",
          "text": this.constants.MENUMESSAGES,
          "info":null
        },
        {
          "img": "settings.png",
          "text": this.constants.MENUSETTINGS,
          "info":null
        },

        {
          "img": "locateus.png",
          "text": this.constants.MENULOCATE,
          "info":null
        },
        {
          "img": "contactus.png",
          "text": this.constants.MENUCONTACT,
          "info":null
        } ];
   this.masterHamburgerData=(JSON.parse(JSON.stringify(this.hamburgerMenuItems)));
   return this.hamburgerMenuItems;
  },
   ConfigurationManager:function() {

    //START OLB Configurations
	this.appLaunchedMode = "";
    this.billPayOneTimePayment = "true";
    this.canViewPastEBills = "true";
    this.addExternalAccount = "true";
    this.verifyByCredentials = "true";
    this.fundTransferHistory = "true";
    this.enrolSecurityQuestionsAvailable = "true";
    this.editUsername = "false";
    this.editPassword = "true";
    this.idleTimeOut = "5";
    this.loanPaymentEnabled = "true";
    this.showLoanUpdateDisclaimer = "true";
    this.loanPaymentAfterDueDateEnabled = "true";
    this.modifyLoanPaymentAmount = "true";
    this.editNickNameAccountSettings = "true";
    this.limitLoanTransfersEnabled = "true";
    this.payOffLoanPaymentEnabled = "true";
    this.billPaySearch = "true";
    this.canSearchTransfers = "true";
    this.isInteractiveNotificationEnabled = "true";
    this.editDisputeATransaction = "true";
    this.printingTransactionDetails = "true";
    this.enableDefaultAccounts = "true";
    this.reOrderAccountPreferences = "true";
    this.enableProfileSettings = "true";
    this.enablePhoneSettings = "true";
    this.enableEmailSettings = "true";
    this.enableAddressSettings = "true";
    this.enableUsernameAndPasswordSettings = "true";
    this.enableSecurityQuestionsSettings = "true";
    this.enableSecureAccessCodeSettings = "true";
    this.enableAccountPreferences = "true";
    this.enableAlertSettings = "true";
    this.enableAlertsIcon = "true";
    this.serviceFeeFlag = "true";
    this.p2pServiceFee = "0.1";
    this.frontendDateFormat = "mm/dd/yyyy";
    this.canSearchP2PPersons = "true";
    this.payApersonOneTimePayment = "true";
    this.backendDateFormat = "yyyy-mm-dd";
    this.additionalAddressAllowed = "true";
    this.additionalPhoneAllowed = "true";
    this.getDashboardMessageCount = "3";
    this.isPFMWidgetEnabled = "true";
    this.enableEstatements = "true";
    this.eStatementsFormat = "pdf,csv";
    this.pfmMaxYears = "5";
    this.wireTranferFees = "10";
    this.enableStopPayments = "true";
    this.enalbeStopPaymentServiceFeesAndValidity = "true";
    this.checkServiceFee = "30";
    this.checkServiceVality = "6";
    this.isAggregatedAccountsEnabled = "false";
    this.isMFAEnabledForP2P = "true";
    this.minimumAmountForMFAP2P = "100";
    this.isMFAEnabledForWireTransfer = "true";
    this.minimumAmountForMFAWireTransfer = "100";
    this.isMFAEnabledForBillPay = "true";
    this.minimumAmountForMFABillPay = "50";
    this.numberOfLocations=5;
    //END OLB Configurations

    //OLB START Entitlements
    this.isBillPayEnabled = "false";
    this.minBillPayLimit = "1";
    this.maxBillPayLimit = "100000";
    this.isPayAPersonEnabled = "false";
    this.minP2PLimit = "1";
    this.maxP2PLimit = "100000";
    this.isRDCEnabled = "false";
    this.minRDCLimit = "1";
    this.maxRDCLimit = "100000";
    this.isTransfersEnabled = "false";
    this.minTransferLimit = "1";
    this.maxTransferLimit = "100000";
    this.isInternationalWireTransferEnabled = "false";
    this.isDomesticWireTransferEnabled = "false";
    this.minInternationalWireTransferLimit = "1";
    this.maxInternationalWireTransferLimit = "100000";
    this.minDomesticWireTransferLimit = "1";
    this.maxDomesticWireTransferLimit = "100000";
    this.isKonyBankAccountsTransfer = "false";
    this.minKonyBankAccountsTransferLimit = "1";
    this.maxKonyBankAccountsTransferLimit = "100000";
    this.isOtherKonyAccountsTransfer = "false";
    this.minOtherKonyAccountsTransferLimit = "1";
    this.maxOtherKonyAccountsTransferLimit = "100000";
    this.isOtherBankAccountsTransfer = "false";
    this.minOtherBankAccountsTransferLimit = "1";
    this.maxOtherBankAccountsTransferLimit = "100000";
    this.isInternationalAccountsTransfer = "false";
    this.minInternationalAccountsTransferLimit = "1";
    this.maxInternationalAccountsTransferLimit = "100000";
    this.isSecurityQuestionConfigured = "false";
    this.payeeBillsLimit = "12";

    //END OLB Configurations

    this.androidPhoneNativeAppLink = "https://play.google.com/store/apps/details?id=com.kony.RetailBanking";
    this.iphoneNativeAppLink = "https://itunes.apple.com/us/app/kony-retail-banking/id1172171955?mt=8";
    this.androidTabletNativeAppLink = "https://play.google.com/store/apps/details?id=com.kony.RetailBanking";
    this.ipadNativeAppLink = "https://itunes.apple.com/us/app/kony-retail-banking/id1172171955?mt=8";

    /**@member {Array} outageMessages holds the outage messages*/
    this.outageMessages = [];
    this.servicesListForUser=[];
	/**@member {boolean} AggregatedExternalAccountEnabled used for tracking the Aggregated External Account Enabled status*/
    this.AggregatedExternalAccountEnabled = false;
	/**@member {string} isStartupCompleted used for tracking the Startup status*/
    this.isStartupCompleted ="";
    this.DebugMode = false; // This parameter is for enabling Logging in release mode.
    var HashTable = require("HashTable");
	/**@member {object} configurations contains instance of HashTable*/
    this.configurations = new HashTable();
	 /**@member {OBJECT}  contains all currency codes*/
    this.currencyCode = {
        'USD': '$', // US Dollar
        'EUR': '€', // Euro
        'CRC': '₡', // Costa Rican Colón
        'GBP': '£', // British Pound Sterling
        'ILS': '₪', // Israeli New Sheqel
        'INR': '₹', // Indian Rupee
        'JPY': '¥', // Japanese Yen	
        'KRW': '₩', // South Korean Won
        'NGN': '₦', // Nigerian Naira
        'PHP': '₱', // Philippine Peso
        'PLN': 'zł', // Polish Zloty
        'PYG': '₲', // Paraguayan Guarani
        'THB': '฿', // Thai Baht
        'UAH': '₴', // Ukrainian Hryvnia
        'VND': '₫', // Vietnamese Dong
    };
	/**@member {OBJECT}  Contains all constants*/
    this.constants = {
		IDLE_TIMEOUT : 10,
      	TRAVELPLANS_DESTINATION_LIMIT : 5,
		IDENTITYSERVICENAME: "CustomLogin",
        WEEKLY: "Weekly",
        DAILY: "Daily",
        MONTHLY: "Monthly",
        BIWEEKLY: "Biweekely",
        SERVICE_NAME: "RBSERVICES",
        LOADING_SCREEN_MESSAGE: "Loading ...",
        CHECKING: "Checking",
        SAVINGS: "Savings",
        CREDITCARD: "CreditCard",
        DEPOSIT: "Deposit",
        MORTGAGE: "Mortgage",
        LOAN: "Loan",
		EXTERNAL_BANK_ACCOUNT: "ExternalBankAccount",
        HEADERFAQ: kony.i18n.getLocalizedString("kony.mb.Support.HeaderFAQ"),
        HEADERPRIVACYPOLICY: kony.i18n.getLocalizedString("kony.mb.Support.HeaderPrivacyPolicy"),
        HEADERABOUTUS: kony.i18n.getLocalizedString("kony.mb.Support.HeaderAbout"),
        HEADERTERMSANDCONDITIONS: kony.i18n.getLocalizedString("kony.mb.Support.HeaderTC"),
        FAQ: kony.i18n.getLocalizedString("kony.mb.Support.FAQ"),
        PRIVACY: kony.i18n.getLocalizedString("kony.mb.Support.Privacy"),
        TERMS: kony.i18n.getLocalizedString("kony.mb.Support.Terms"),
        ABOUT: kony.i18n.getLocalizedString("kony.mb.Support.Aboutus"),
        Last7days: kony.i18n.getLocalizedString("kony.mb.AdvanceSearch.Last7days"),
        Last30days: kony.i18n.getLocalizedString("kony.mb.AdvanceSearch.Last30days"),
        Last60days: kony.i18n.getLocalizedString("kony.mb.AdvanceSearch.Last60days"),
        CustomRange: kony.i18n.getLocalizedString("kony.mb.AdvanceSearch.CustomRange"),
        Withdrawals: kony.i18n.getLocalizedString("kony.mb.AdvanceSearch.Withdrawals"),
        Deposits: kony.i18n.getLocalizedString("kony.mb.AdvanceSearch.Deposits"),
        P2PDebits: kony.i18n.getLocalizedString("kony.mb.AdvanceSearch.P2PDebits"),
        BillPay: kony.i18n.getLocalizedString("kony.mb.AdvanceSearch.BillPay"),
        Transfers: kony.i18n.getLocalizedString("kony.mb.AdvanceSearch.Transfers"),
        Checks: kony.i18n.getLocalizedString("kony.mb.AdvanceSearch.Checks"),
        P2PCredits: kony.i18n.getLocalizedString("kony.mb.AdvanceSearch.P2PCredits"),
        MENUACCOUNTS: kony.i18n.getLocalizedString("kony.mb.Hamburger.Accounts"),
        MENUTRANSFERS: kony.i18n.getLocalizedString("kony.mb.Hamburger.Transfers"),
        MENUOPENACOUNT: kony.i18n.getLocalizedString("kony.mb.Hamburger.OpenAccount"),
        MENUMESSAGES: kony.i18n.getLocalizedString("kony.mb.Hamburger.Messages"),
        MENUSETTINGS: kony.i18n.getLocalizedString("kony.mb.Hamburger.Settings"),
        MENUCHATBOT: kony.i18n.getLocalizedString("kony.mb.Hamburger.chatbot"),
        MENULOCATE: kony.i18n.getLocalizedString("kony.mb.Hamburger.Locateus"),
        MENUCONTACT: kony.i18n.getLocalizedString("kony.mb.Hamburger.Contactus"),
        MENUMYWALLET: kony.i18n.getLocalizedString("kony.mb.Hamburger.Mywallet"),
        MENUBILLPAY: kony.i18n.getLocalizedString("kony.mb.Hamburger.BillPay"),
        MENUCHECKDEPOSIT: kony.i18n.getLocalizedString("kony.mb.Hamburger.CheckDeposit"),
        MENUCARDLESS: kony.i18n.getLocalizedString("kony.mb.Hamburger.CardLessCash"),
        MENUCARDMANAGEMENT: kony.i18n.getLocalizedString("kony.mb.Hamburger.CardManagement"),
        MENUMANAGEOTHERBANKACCOUNTS: kony.i18n.getLocalizedString("kony.mb.ExternalAccounts.ManageOtherBankAccounts"),
      	MENUPFMMYMONEY : kony.i18n.getLocalizedString("kony.mb.PFMMyMoney")
    };
	/**@member {Array} quickActionItems holds the list of action items required for transaction*/
    this.quickActionItems = [
    {
      "id" : "ATM finder",
      "title" : "Find an ATM",
      "subtitle" : "",
      "icon" : kony.forcetouch.QUICK_ACTION_ICON_TYPE_LOCATION,
      "info" : null

    }, 
    {
      "id" : "Pay a Bill",
      "title" : "Pay a Bill",
      "subtitle" : "",
      "icon" : kony.forcetouch.QUICK_ACTION_ICON_TYPE_DATE,
      "info" : null
    }, 
    {
      "id" : "Transfer Money",
      "title" : "Transfer Money",
      "subtitle" : "",
      "icon" : kony.forcetouch.QUICK_ACTION_ICON_TYPE_SHUFFLE,
      "info" : null
    },
    {
      "id" : "New Check Deposit",
      "title" : "Deposit Check",
      "subtitle" : "",
      "icon" : kony.forcetouch.QUICK_ACTION_ICON_TYPE_CAPTUREPHOTO,
      "info" : null
    } ];

   /**@member {Array} unreadMessageCount holds the count of unread message count */
    this.unreadMessageCount = 0;


   /**@member {Array} maritalStatus holds the list of different marital status*/
  this.maritalStatus = [['Single','Single'],['Married','Married'],['Widowed','Widowed'],['Divorced','Divorced']];
  /**@member {Array} numberOfDependents holds the list of different dependent numbers*/
  this.numberOfDependents = [['0','00'],['1','01'],['2','02'],['3','03'],['4','04'],['5','05'],['6','06'],['7','07'],['8','08']];
/**@member {Array} gender holds the list of different genders*/
  this.gender = [['Male','Male'],['Female','Female']];
  /**@member {Array} employmentInfoYears holds the list of different employement information years*/
  this.employmentInfoYears=[['0 year','1 year','0'],['1 year','2 years','1'],['2 years','3 years','2'],['3 years','4 years','3'],['4 years','5 years','4'],['5 years','6 years','5'],['6 years','7 years','6'],['7 years','8 years','7'],['8 years','9 years','8'],['9 years','10 years','9'],['10 years','11 years','10']];
  /**@member {Array} annualIncome holds the list of different annual Incomes*/
  this.annualIncome=[['0','25000','0'],['25001','50000','1'],['50001','100000','2'],['100001','200000','3'],['200001','300000','4'],['300001','500000','5'],['500001','1200000','6'],['1200001','9999999','7']];
  /**@member {Array} assets holds the list of different assets*/
  this.assets=[['0','25000','0'],['25001','50000','1'],['50001','100000','2'],['100001','200000','3'],['200001','300000','4'],['300001','500000','5'],['500001','1200000','6'],['1200001','9999999','7']];
  /**@member {Array} monthlyExpenditure holds the list of different monthly expenditures*/
  this.monthlyExpenditure=[['0','25000','0'],['25001','50000','1'],['50001','100000','2'],['100001','200000','3'],['200001','300000','4'],['300001','500000','5'],['500001','1200000','6'],['1200001','9999999','7']];
  /**@member {Array} employmentTypeValues holds the list of different employment type values*/
  this.employmentTypeValues = ['Employed','Unemployed','Retired','Student'];
  /**@member {Array} hamburgerMenuItems holds the list of HamburgerMenu Items*/
  this.hamburgerMenuItems=[
    					  {
         					 "img": "accounts.png",
          					 "text": this.constants.MENUACCOUNTS,
          					  "info":null
        					},   
						    {
							 "img": "transfer.png",
							 "text": this.constants.MENUTRANSFERS,
							 "info":null
						   },
                           {
                             "img": "billpay.png",
                             "text": this.constants.MENUBILLPAY,
                             "info":null
                           },
                           {
      					   	  "img": "checkdepositimg.png",
      						  "text": this.constants.MENUCHECKDEPOSIT,
      					      "info":null
    					   },
    						{
							 "img": "cardlesscash.png",
                             "text": this.constants.MENUCARDLESS,
                             "info":null
                           }, 
    						{
		  					  "img": "cardmange.png",
		  					  "text": this.constants.MENUCARDMANAGEMENT,
							  "info":null
							},
    						{
                              "img": "mangeothrbank.png",
                              "text": this.constants.MENUMANAGEOTHERBANKACCOUNTS,
                              "info": null
	  					  },
    
                           {
                             "img": "opennew.png",
                             "text": this.constants.MENUOPENACOUNT,
                             "info":null
                           },
    						{
		  					  "img": "billpay.png",
		  					  "text": this.constants.MENUPFMMYMONEY,
							  "info":null
							},
                           {
                             "img": "message.png",
                             "text": this.constants.MENUMESSAGES,
                             "info":null
                           },
                           {
                             "img": "settings.png",
                             "text": this.constants.MENUSETTINGS,
                             "info":null
                           },
                            
                           {
                             "img": "locateus.png",
                             "text": this.constants.MENULOCATE,
                             "info":null
                           },
                           {
                             "img": "contactus.png",
                             "text": this.constants.MENUCONTACT,
                             "info":null
                           }
                           	
                          ];
  this.masterHamburgerData=(JSON.parse(JSON.stringify(this.hamburgerMenuItems)));
     
    /**@member {Array} moreMenuItems holds the list of more MenuItems*/
    this.moreMenuItems=[
    {
      "img": "checkdepositimg.png",
      "text": this.constants.MENUCHECKDEPOSIT,
      "info":null
    },
    {
      "img": "cardlesscash.png",
      "text": this.constants.MENUCARDLESS,
      "info":null
    },
    {
      "img": "cardmange.png",
      "text": this.constants.MENUCARDMANAGEMENT,
      "info":null
    },
    {
       "img": "mangeothrbank.png",
       "text": this.constants.MENUMANAGEOTHERBANKACCOUNTS,
       "info": null
    },
    {
      "img": "opennew.png",
      "text": this.constants.MENUOPENACOUNT,
      "info":null
    },
      {
		"img": "billpay.png",
       "text": this.constants.MENUPFMMYMONEY,
       "info":null
     },
    {
      "img": "message.png",
      "text": this.constants.MENUMESSAGES,
      "info":null
    },
    {
      "img": "settings.png",
      "text": this.constants.MENUSETTINGS,
      "info":null
    },
    
    {
      "img": "locateus.png",
      "text": this.constants.MENULOCATE,
      "info":null
    },
      
    {
      "img": "contactus.png",
      "text": this.constants.MENUCONTACT,
      "info":null
    }
     
  ];
  this.mastermoreMenuItems = (JSON.parse(JSON.stringify(this.moreMenuItems)));
    
   /**@member {Array} iPhoneMenuItems holds the list of iPhone MenuItems*/
   this.iPhoneMenuItems=[{
    "img": "accounts.png",
    "text": this.constants.MENUACCOUNTS,
    "info":null
  }, {
    "img": "transfer.png",
    "text": this.constants.MENUTRANSFERS,
    "info":null
  },{
    "img": "billpay.png",
    "text": this.constants.MENUBILLPAY,
    "info": null
  }];
  /**@member {Array} denominationAmountValues holds the list of different denomination amount values*/
  this.denominationAmountValues = ['50','100','150','300','500','1000'];
  /**@member {Boolean} value used in custom metrics*/
  this.CustomMetricsEnabled = true;
/**@member {object} value Olb specific constants*/
  this.OLBConstants = {
    DEFAULT_OFFSET : 0,
    PAGING_ROWS_LIMIT : 10,
    ACCOUNT_LIST_NAME_MAX_LENGTH : 32,
    WIRE_ACTIVITY_LIMIT : 12,
    ASCENDING_KEY : 'asc',
    DESCENDING_KEY : 'desc',
    NOTES_LENGTH:50,
    CALENDAR_ALLOWED_FUTURE_YEARS: 3,
    ALL: 'All',
    PENDING: 'pending',
    SUCCESSFUL: 'successful',
    BOTH: 'Both',
    CURRENCY_NAME: 'Dollar',
    ANY_DATE: 'ANY_DATE',
    CUSTOM_DATE_RANGE: 'CUSTOM_DATE_RANGE',
    IBAN_MINIMUM_LENGTH: 22,
    IBAN_MAXIMUM_LENGTH: 34,
    CHECK_SERIES_SEPARATOR: "-",
    CHECK_REQUEST_TYPES: {
        SINGLE: 'Single',
        SERIES: 'Series'
    },
    DISPUTED_CHECKS: "DisputesChecks",
    DISPUTED_TRANSACTIONS: "DisputedTransactions",
    NOTES_MAX_LENGTH: 120,
    Channel : 'Online',
    MAX_CHECKS_COUNT: 50,
    OTPLength: 6,
    ACCOUNT_TYPE : {
        SAVING : 'Savings',
        CHECKING: 'Checking',
        CREDITCARD: 'CreditCard',
        LOAN : 'Loan',
        MORTGAGE : 'Mortgage',
        DEPOSIT : 'Deposit',
        OTHER : 'Other',
        EXTERNAL: 'External',
        CURRENT: 'Current',
        LINE_OF_CREDIT: 'Line of Credit'
    },
    TRANSACTION_TYPE : {
        CHECKS: 'Checks',
        DEPOSITS:  'Deposits',
        TRANSFERS: 'Transfers',
        WITHDRAWLS: 'Withdrawals',
        PAYMENTS: 'Payments',
        PURCHASES: 'Purchases',
        INTEREST: 'Interest',
        EXTERNALTRANSFER: 'ExternalTransfer',
        INTERNALTRANSFER: 'InternalTransfer',
        BILLPAY: 'BillPay',
        P2P: 'P2P',
        FEES: "Fees",
        INTERESTDEBIT: 'InterestDebit',
        INTERESTCREDIT: 'InterestCredit',
        LOAN : 'Loan',
        STOPCHECKPAYMENTREQUEST: 'StopCheckPaymentRequest',
        DISPUTEDTRANSACTIONSREQUEST: 'DisputedTransactionRequest',
        WIRE: 'Wire',
        DEPOSIT : 'Deposit',
        CARDLESS: 'Cardless',
        CHECKWITHDRAWAL : 'CheckWithdrawal',
        WITHDRAWL: 'Withdrawal',
        RECEIVEDP2P: 'ReceivedP2P',
        RECEIVEDREQUEST : 'ReceivedRequest'
    },
    TRANSACTION_STATUS: {
        INPROGRESS: "In-Progress",
        SUCCESSFUL: "Successful",
        CLEARED:"Cleared",
        REQUESTEXPIRED:"Request Expired",
        FAILED:"Failed",
        PENDING: "Pending",
        STOPPED: "Stopped"
    },
    TRANSACTION_RECURRENCE:{
        ONCE: "Once",
        DAILY: "Daily",
        WEEKLY: "Weekly",
        BIWEEKLY: "BiWeekly",
        MONTHLY: "Monthly",
        YEARLY: "Yearly",
        HALFYEARLY: "Half Yearly",
        QUARTERLY: "Quarterly",
        EVERYTWOWEEKS: "Every Two Weeks"
    },
    ACTION: {
        ACCOUNT_PREFERENCES: 'Account Preferences',
        EDIT_ACCOUNT: 'Edit Account',
        TRANSFER_MONEY: 'Transfer Money',
        VIEW_STATEMENTS: 'View Statements',
        UPDATE_ACCOUNT_SETTINGS: 'Update Account Settings',
        ORDER_CHECKS: 'Order Checks',
        REQUEST_OR_REPLACE_CARD: 'Request Card/Replace Card',
        GET_ASSISTANCE: 'Get Assistance',
        ECHECK_OR_ROUTING_DETAILS: 'eCheck/Routing Details',
        REWARDS_POINTS: 'Reward Points',
        PAY_A_PERSON_OR_SEND_MONEY: 'Send Money',
        PAY_DUE_AMOUNT: 'Pay Due Amount',
        PAY_A_BILL: 'Pay a Bill',
        MANAGE_CARD_OR_CARD_CONTROLS: 'Manage Card/Card Controls',
        REPORT_LOST_OR_STOLEN: 'Report Lost/Stolen',
        SETUP_NEW_PIN: 'Set up New PIN',
        LOCK_OR_DECACTICATE_CARD: 'Lock Card/Deactivate Card (Temporary)',
        TRAVEL_NOTIFICATION: 'Travel Notification',
        REMOVE_ACCOUNT: 'Remove Account',
        DOWNLOAD_STATEMENTS: 'Download Statements',
        ACCOUNT_SETTINGS: 'Account Settings',
        ACCOUNT_SERVICES: 'Account Services',
        PAYOFF_LOAN : 'Payoff Loan',
        EDIT_ACCOUNTS : 'Edit Accounts',
        SCHEDULED_TRANSACTIONS : 'Scheduled Transactions',
        MAKE_A_TRANSFER : 'Make A Transfer',
        VIEW_BILL :  'View Bill',
        SHOW_DISPUTE_TRANSACTION_FORM: 'ShowDisputeTransactionForm',
        SHOW_STOPCHECKS_FORM: 'ShowStopChecksForm',
        STOPCHECKS_PAYMENT: 'Stop Check Payment'
    },
    TRANSFER_TYPES: {
        OWN_INTERNAL_ACCOUNTS: 'OWN_INTERNAL_ACCOUNTS',
        OTHER_INTERNAL_MEMBER: 'OTHER_INTERNAL_MEMBER',
        OTHER_EXTERNAL_ACCOUNT: 'OTHER_EXTERNAL_ACCOUNT',
        INTERNATIONAL_ACCOUNT: 'INTERNATIONAL_ACCOUNT',
        WIRE_TRANSFER: 'WIRE_TRANSFER'
    },
    
    MONTHS_FULL: {
      January: "January",
      February: "February",
      March: "March",
      April: "April",
      May: "May",
      June: "June",
      July: "July",
      August: "August",
      September: "September",
      October: "October",
      November: "November",
      December: "December"
    },  
    CONFIG : {
        ACCOUNTS_QUICK_ACTIONS: {
            "Savings": [
                "Transfer Money",
                "Stop Check Payment",
                "View Statements"
            ],
            "Checking": [
                "Transfer Money",
                "Send Money",
                "Pay a Bill",
                "Stop Check Payment",
                "View Statements"
            ],
            "CreditCard": [
                "Pay a Bill",
                "View Statements"
                //"Pay Due Amount" Not in scope
            ],
            "Loan": [
                "Pay Due Amount",
                "View Statements",
                "Update Account Settings"
            ],
            "Line of Credit": [
                //"Pay Due Amount",
                "View Statements",
                "Update Account Settings"
            ],
            "Mortgage": [
                //"Pay Due Amount",
                "View Statements",
                "Update Account Settings"
            ],
            "Deposit": [
                "View Statements",
                "Update Account Settings"
            ]
        },
        EXTERNAL_ACCOUNT_QUICK_ACTIONS:[
            "Remove Account",
            "Account Preferences",
            "Edit Account",
        ],
        ACCOUNTS_RIGHTSIDE_ACTIONS : {
            "Savings" : [
                "Scheduled Transactions",
                "Make A Transfer"
            ],
            "Checking" : [
                "Scheduled Transactions",
                "Make A Transfer",
                "Pay a Bill"
            ],
            "CreditCard" : [
                "Scheduled Transactions",
                "View Statements"
                //"Pay Due Amount" Not in scope
            ],
            "Loan" : [
                "Pay Due Amount",
                "View Statements",
                "Update Account Settings"
            ],
            "Line of Credit" : [
                //"Pay Due Amount", Not in scope
                "View Statements",
                "Update Account Settings"
            ],
            "Mortgage" : [
                //"Pay Due Amount", Not in scope
                "View Statements",
                "Update Account Settings"
            ],
            "Deposit" : [
                "View Statements",
                "Update Account Settings"
            ]
        },
        ACCOUNTS_SECONDARY_ACTIONS :   {
            "Savings" : [
                "View Statements",
                "Stop Check Payment",
                "Update Account Settings"
                //"Order Checks", //Post R4
                //"Manage Card" //Post R4
            ],
            "Checking" : [
                "Send Money",
                "View Statements",
                "Stop Check Payment",
                "Update Account Settings",
                //"Order Checks", //Post R4
                //"Manage Card" //Post R4
            ],
            "CreditCard" : [
                "Pay a Bill",
                "Update Account Settings",
                //"Manage Card", //Post R4
                //"Report Lost/Stolen", //Post R4
                //"Lock Card/Deactivate Card", //Post R4
            ],
            "Loan" : [
                "Payoff Loan",
                "Update Account Settings"
            ],
            "Line of Credit" : [
                //"Payoff Loan", Not in Scope
                "Update Account Settings"
            ],
            "Mortgage" : [
                "Update Account Settings"
            ],
            "Deposit" : [
                "Update Account Settings"
            ]
        }
    },
    CARD_TYPE:{
        'Debit': 'Debit',
        'Credit': 'Credit'
    },
    CARD_ACTION:{
        'Lock':'Lock Card',
        'Unlock':'Unlock Card',
        'Replace':'Replace Card',
        'Report_Lost': 'Report Lost',
        'Cancel': 'Cancel Card',
        'Change_Pin': 'Change Pin'
    },
    CARD_STATUS:{
        'Active': 'Active',
        'Locked': 'Locked',
        'ReportedLost': 'Reported Lost',
        'ReplaceRequestSent': 'Replace Request Sent',
        'CancelRequestSent': 'Cancel Request Sent',
        'Cancelled': 'Cancelled'
    },
    CARD_PRODUCT:{
        'PlatinumCredit': 'My Platinum Credit Card',
        'GoldDebit': 'Gold Debit Card',
        'PremiumCredit': 'Premium Club Credit Card',
        'ShoppingCard': 'Shopping Card',
        'PetroCard': 'Petro Card',
        'FoodCard': 'Eazee Food Card'
    },
    MFA_OPTIONS:{
        'SECURE_ACCESS_CODE': 'Secure Access Code',
        'SECURITY_QUESTIONS': 'Security Questions'
    },
    CHANGE_PIN_OFFLINE_OPTION:{
        'EMAIL': 'E-mail ID',
        'PHONE': 'Phone No',
        'POSTAL_ADDRESS': 'Postal Address'
    },
    CARD_CHANGE_PIN_REASON:{
        'PIN_COMPROMISED': 'PIN Compromised',
        'FORGOT_PIN': 'Lost PIN'
    },
    CARD_REPORTLOST_REASON:{
        'LOST': 'Lost',
        'STOLEN': 'Stolen'
    },
    WireTransferConstants: {
        RECIPIENT_INDIVIDUAL: 'Individual',
        RECIPIENT_BUSINESS: 'Business',
        ACCOUNT_DOMESTIC: 'Domestic',
        ACCOUNT_INTERNATIONAL: 'International',
        DOMESTIC_COUNTRY: 'USA',
        DOMESTIC_COUNTRY_NAME: 'United States of America',
        CURRENCIES: [{
            symbol: "£",
            name: "Pound"
        }, {
            symbol: "€",
            name: "Euro"
        }, {
            symbol: "₹",
            name: "Rupee"
        }, {
            symbol: "$",
            name: "Dollar"
        }]
    }
};

  this.PFM_CATEGORIES_COLORS =
        {
            "Home": "#FEDB64",
            "Transport": "#3645A7",
            "Financial ": "#6753EC",
            "Food": "#D6B9EA",
            "Utilities": "#E87C5E",
            "Health": "#04B6DF",
            "Education": "#E8A75E",
            "Other": "#B160DC",
            "Travel" : "#8ED174"
        };
}

});