define({
  enrollFlow : true,
  invldOtpCntr : 0,
  cantLogin : "forgotPassword",
  enroll : "enrollment",
  //code which navigates to frmEnrollDOB after setting enroll attributes
  navigateToFrmEnrollDOB : function(lastName,firstName){
    scope=this;
    var navManager = applicationManager.getNavigationManager();
    var newUserManager = applicationManager.getNewUserBusinessManager();
    newUserManager.setEnrollAttribute("userlastname",lastName);
    newUserManager.setEnrollAttribute("userfirstname",firstName);
    navManager.navigateTo("frmEnrollDOB");

  },
  getEnrollFirstName :function(){
    var newUserManager = applicationManager.getNewUserBusinessManager();
    return newUserManager.getEnrollObject().userfirstname;
  },
  //code for calling SSN service that returns entire data of user and validates member records
  verifySsnEnrolled : function(SSN,flag){
    scope=this;
    scope.enrollFlow = flag;
    var newUserManager = applicationManager.getNewUserBusinessManager();
    newUserManager.setEnrollAttribute("ssn",SSN);
    var authMode = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
    authMode.presentationController.showDownTimeMessage();
    var params = {"ssn" : SSN};
    newUserManager.SsnEnrolled(params,scope.verifySsnEnrolledSuccess,scope.verifySsnEnrolledFailure);
  },
  verifySsnEnrolledSuccess : function(response){
    applicationManager.getPresentationUtility().dismissLoadingScreen();
    var navManager = applicationManager.getNavigationManager();
    if(response.LoopDataset && response.LoopDataset.length===0){
      navManager.setCustomInfo("isValidUser",false);
      if(!this.enrollFlow){
        navManager.navigateTo("frmForgotEnterSSN");
      }else
        navManager.navigateTo("frmEnrollSSn");     
    }
    else{
      this.verifyUserSsnDetails(response);
    }
  },
  verifySsnEnrolledFailure : function(response){
    applicationManager.getPresentationUtility().dismissLoadingScreen();
    this.showServiceResponseFailureError();  
  },
  verifyUserSsnDetails :function(response){
    var validUserFound = false;
    var newUserManager = applicationManager.getNewUserBusinessManager();
    var dob =  newUserManager.getEnrollObject().dateOfBirth;
    var lastName=newUserManager.getEnrollObject().userlastname;
    var firstName=newUserManager.getEnrollObject().userfirstname;
    lastName= lastName.toUpperCase().trim();
    firstName=firstName.toUpperCase().trim();

    var navManager = applicationManager.getNavigationManager();
    navManager.setCustomInfo("frmEnrollSSn", response);

    for(var loopDataSet of response.LoopDataset){
      for( var user of loopDataSet.User )
      {
        if((user.ssn!==null&&user.ssn!==undefined) && 
           (user.dateOfBirth!==null&&user.dateOfBirth!==undefined) &&
           (user.userfirstname!==null||user.userfirstname!==undefined) &&
           (user.userlastname!==null||user.userlastname!==undefined)) {
          if(user.ssn === SSN && dob === user.dateOfBirth) {
            if((user.userlastname===lastName)&&(user.userfirstname===firstName)){
              validUserFound = true;
            }
          }
        }
        if(validUserFound){
          var navMgr = applicationManager.getNavigationManager();
          if((user.ssn!==null&&user.ssn!==undefined)&&(user.nameEmail!==null&&user.nameEmail!==undefined)){  
            if(user.ssn === SSN){
              navMgr.setCustomInfo("frmEnrollSecurityCheck",user);
              var mainUser = navMgr.getCustomInfo("frmEnrollSecurityCheck");
              var accNum = loopDataSet.accountNumber;
              mainUser.mainAccountNum = accNum;
              navMgr.setCustomInfo("frmEnrollSecurityCheck",mainUser);
            }
          }   
          break;
        }
      }
    }
    //if the valid user is found and all details match on backend the navigate to security check 
    if(validUserFound){ 
      if(!this.enrollFlow){
        navManager.navigateTo("frmForgotSelectMethod");
      }
      else
        navManager.navigateTo("frmEnrollSecurityCheck");
    }
    //if the user is not enrolled then showing member not found error
    else{
      navManager.setCustomInfo("isValidUser",false);
      navManager.navigateTo("frmEnrollSSn"); 
    }

  },
  alertCallback: function(){
    var enrollMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("EnrollModule");
    enrollMod.presentationController.resetEnrollObj();

  },

  //invoking custom identity service of CMCU
  invokeIdentity : function(){
    scope=this;
    var authMode = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
    authMode.presentationController.showDownTimeMessage();
    var authManger = applicationManager.getAuthManager();
    authManger.invokeIdentityService(scope.identitySuccess, scope.identityFailure);
  },
  identitySuccess: function(resSuccess){
    scope=this;
    scope.sendOTP();
  },
  identityFailure : function(resError){

    applicationManager.getPresentationUtility().dismissLoadingScreen();
    var controller = applicationManager.getPresentationUtility().getController('frmEnrollSecurityCheck', true);
  },
  //code for fetching otp from service when user requests otp through email,call,text,etc
  sendOTP : function(){
    scope=this;
    var newUserManager = applicationManager.getNewUserBusinessManager();
    var destination =  newUserManager.getEnrollObject().destination;
    var type =  newUserManager.getEnrollObject().type;
    var params = {
      "destination":destination,
      "userName": "JYOTHIVIJU",
      "type":type,
      "deviceInfo":""
    };
    if(!this.enrollFlow){
      params.screenFlow = this.cantLogin;
    }else
      params.screenFlow = this.enroll; 
    var authMode = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
    authMode.presentationController.showDownTimeMessage();
    var authManager = applicationManager.getAuthManager();
    authManager.sendOTP(params,scope.sendOTPSuccess, scope.sendOTPFailure);

  },
  //navigating to security code screen to enter otp
  sendOTPSuccess : function(response){

    applicationManager.getPresentationUtility().dismissLoadingScreen();
    var navManager = applicationManager.getNavigationManager();
    if(!this.enrollFlow){
      //alert("frmForgotEnterSecurityCode");
      navManager.navigateTo("frmForgotEnterSecurityCode");
    }
    else
      navManager.navigateTo("frmEnrollSecurity");
  },

  sendOTPFailure : function(error){
    applicationManager.getPresentationUtility().dismissLoadingScreen();
    if (error["isServerUnreachable"])
      applicationManager.getPresentationInterruptHandler().showErrorMessage("preLogin",err);
    else{
      var controller = applicationManager.getPresentationUtility().getController('frmEnrollSecurityCheck', true);
      var errorMsg = applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.enroll.SomethingWrong");
      controller.bindViewError(errorMsg);  
    }
  },

  //code for calling resend otp service
  resendOTP : function(type){
    scope=this;
    var newUserManager = applicationManager.getNewUserBusinessManager();
    var params = {
      "userName": "JYOTHIVIJU",
      "deviceInfo":""
    };
    if(!this.enrollFlow){
      params.screenFlow = this.cantLogin;
    }else
      params.screenFlow = this.enroll;
    var authMode = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
    authMode.presentationController.showDownTimeMessage();
    var authManager = applicationManager.getAuthManager();
    authManager.resendOTP(params,scope.resendOTPSuccess, scope.resendOTPFailure);
  },

  resendOTPSuccess :function(response){
    applicationManager.getPresentationUtility().dismissLoadingScreen();
    var controller = applicationManager.getPresentationUtility().getController('frmEnrollSecurity', true);
    var forgotController = applicationManager.getPresentationUtility().getController('frmForgotEnterSecurityCode', true);
    controller.showToast();
    forgotController.showToast();
  },
  
  //code for validating otp after user clicks verify btn in otp screen
  validateOTP : function(otp){
    scope=this;
    var validationUtilManager = applicationManager.getValidationUtilManager();
    if(validationUtilManager.isValidOTP(otp))
    {
      var newUserManager = applicationManager.getNewUserBusinessManager();
      //var username = "";
      var verifyOTPJSON = {
        "otp":otp,
        "userName": "JYOTHIVIJU",
        "deviceInfo":""
      };
      if(!this.enrollFlow){
        verifyOTPJSON.screenFlow = this.cantLogin;
      }else
        verifyOTPJSON.screenFlow = this.enroll;
      var authMode = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
      authMode.presentationController.showDownTimeMessage();
      var authManager = applicationManager.getAuthManager();
      authManager.verifyOTP(verifyOTPJSON,scope.validateOTPSuccess,scope.validateOTPFailure);
    }
    else
    {
      applicationManager.getPresentationUtility().dismissLoadingScreen();
      var controller = applicationManager.getPresentationUtility().getController('frmEnrollSecurity', true);
      var forgotController = applicationManager.getPresentationUtility().getController('frmForgotEnterSecurityCode', true);
      var errormsg =  applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.common.enterSecurityCode");
      controller.bindGenericError(errormsg);
      forgotController.bindGenericError(errormsg);
    } 

  },
  alertCB: function(response){
    if(response){
      var enrollModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("EnrollModule");
      enrollModule.presentationController.resetEnrollObj();
    }
  },
  ContactHandler:function(response){
    if (response===true){
      var number = "7043750183";
      kony.phone.dial(number);
    }

  },
  //validates otp,if its success then it populates account list and user list and navigate to account selection list screen
  validateOTPSuccess : function(response){
    scope=this;
    var navManager = applicationManager.getNavigationManager();
    // enrollPresentationScope.logger.log("####  OTP validation success call back ####");
    applicationManager.getPresentationUtility().dismissLoadingScreen();
    if(response.isSuccessful && response.isSuccessful!== "") {
      if(response.isSuccessful==="true"){
        scope.populateAccountList();
        if(!this.enrollFlow){
          var userList = navManager.getCustomInfo("userList");
		  if(userList === undefined){
            navManager.setCustomInfo("isValidUser",false);
            navManager.navigateTo("frmForgotEnterSecurityCode");
          }
          else if(userList.length === 1){
            if(userList[0].locked === 1){
              var i18nString = applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.login.accountFrozen");
              kony.ui.Alert(i18nString, this.ContactHandler, constants.ALERT_TYPE_CONFIRMATION, "Contact", "Ok", ""); 
            }
            else{
              var data = {
                "accNumHidden" : userList[0].accountNumber,
                "lblAccNameValue" : userList[0].hbUsername,
              };
              navManager.setCustomInfo("accPrimLinkData",data);
              navManager.navigateTo("frmForgotMain");
            }
		  }
          else     
            navManager.navigateTo("frmForgotAccountPrimaryLinking");
        }else
          navManager.navigateTo("frmAccountPrimaryLinking");
      } else {
        this.invldOtpCntr = this.invldOtpCntr + 1;
        if(this.invldOtpCntr === 2){
          kony.ui.Alert("Verification failed. Due to security reasons you may have to begin enrollment again.", this.alertCB, constants.ALERT_TYPE_INFO, "Ok", "");
        }
        else{
          if(!this.enrollFlow){
            var frgtController = applicationManager.getPresentationUtility().getController('frmForgotEnterSecurityCode', true);
            var frgtErrorMsg = applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.common.invalidSecurityCode");
            frgtController.bindGenericError(frgtErrorMsg);
          }
          else{
            var controller = applicationManager.getPresentationUtility().getController('frmEnrollSecurity', true);
            var errorMsg = applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.common.invalidSecurityCode");
            controller.bindGenericError(errorMsg);
          }
        }
      }
    }
  },
   validateOTPFailure : function(error){
    enrollPresentationScope.logger.log("####  OTP validation error call back ####");
    applicationManager.getPresentationUtility().dismissLoadingScreen();
    if (err["isServerUnreachable"]){
      applicationManager.getPresentationInterruptHandler().showErrorMessage("preLogin", err);
    }
    else{
      if(!this.enrollFlow){
        var frgtController = applicationManager.getPresentationUtility().getController('frmForgotEnterSecurityCode', true);
      	var frgtErrorMsg = applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.enroll.enterValidOTP");
      	frgtController.bindGenericError(frgtErrorMsg);
      }
      else{
        //alert("inside else");
        var controller = applicationManager.getPresentationUtility().getController('frmEnrollSecurity', true);
      	var errorMsg = applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.enroll.enterValidOTP");
      	controller.bindGenericError(errorMsg);
      }
    }
  },
  //populates account list and user list for primary account screen
  populateAccountList : function(){
    var navManager = applicationManager.getNavigationManager();
    var response =  navManager.getCustomInfo("frmEnrollSSn");
    accountList=[];userList=[]; accTemp={}; userTemp={}; 
    var newUserManager = applicationManager.getNewUserBusinessManager();
    var ssn =  newUserManager.getEnrollObject().ssn;
    for(var loopDataSet of response.LoopDataset){
      navManager.setCustomInfo("loopset",loopDataSet);
      accNum=loopDataSet.accountNumber;
      accountType = loopDataSet.accountType;
      frozenMode = loopDataSet.FrozenMode;
      for( var user of loopDataSet.User )
      { 
        if((user.hbUsername!==null&&typeof user.hbUsername!==undefined &&user.hbUsername!=="") &&
           (user.homeFax!==null&&user.homeFax!==undefined&&user.homeFax!=="")) {
          var homeFax = user.homeFax.replace(/-/g,"");
          homeFax=homeFax.replace(/^0+/, '');
          if(ssn===homeFax){
            var userTemp ={
              "accountNumber":accNum,
              "accountType":accountType,
              "hbUsername":user.hbUsername,
              "locked":user.Locked,
            };
            userList.push(userTemp);
          }
          navManager.setCustomInfo("userList",userList);
        }

        if(ssn === user.ssn){
          if(user.shareId!==null && user.shareId!==""&&typeof user.shareId!=="undefined"){
            accTemp.shareId = user.shareId;
          }  

          if(user.loanId!==null && user.loanId!==""&&typeof user.loanId!=="undefined"){
            accTemp.loanId = user.loanId;
          } 
        }

        if((user.nameType!==null&&user.nameType!==undefined)){
          if(user.nameType===0||user.nameType===1||user.nameType===6||
             user.nameType===8||user.nameType===9||user.nameType===14) {
            var accTemp ={
              "accountNumber":accNum,
              "nameType":user.nameType,
              "name":user.longName, 
              "accountType":accountType,
              "frozenMode":frozenMode
            };
            accountList.push(accTemp);
          }
        }
        navManager.setCustomInfo("accountList",accountList);
      }
    }   
  },

  //create user preference for creating user in enrollment
  createUser : function(userName,password){
    var data = [];
    var temp = {};
    var subAccNum,id,idType;
    this.userName = userName;
    this.password = password;
    var newUserManager = applicationManager.getNewUserBusinessManager();
    var navManager = applicationManager.getNavigationManager();

    var ssn =  newUserManager.getEnrollObject().ssn;    
    newUserManager.setPrimarykeyAttribute({"userName":userName});
    newUserManager.setEnrollAttribute("password", password);

    var accPrimLinkData = navManager.getCustomInfo("frmAccountPrimaryLinking");
    var accountNumber = accPrimLinkData[0].accNumHidden;

    var accLinkingData = navManager.getCustomInfo("frmAccountLinking");
    if(accLinkingData !== undefined && accLinkingData !== null && accLinkingData !== ""){
      for(var i=0;i<accLinkingData.length;i++){
        subAccNum = accLinkingData[i].accNumHidden;
        var keys = Object.keys(accLinkingData[i]);
        if(accLinkingData[i].nameType===0){
          id="";
          idType=2;
        }else{
          for(var j=0;j<keys.length;j++){
            if (keys[j] === "loanId") {
              if(typeof accLinkingData[i].loanId !== "undefined" && accLinkingData[i].loanId !== null){
                id = accLinkingData[i].loanId;
                idType = "1";
              }
            }if (keys[j] === "shareId") {
              if(typeof accLinkingData[i].shareId !== "undefined" && accLinkingData[i].shareId !== null){
                id = accLinkingData[i].shareId;
                idType = "0";
              }
            }
          }
          temp ={
            "recordType": "2",
            "idType": "1",
            "accountNumber": subAccNum,
            "id": "01" 
          };
        }
        data.push(temp);
      }
    }

    var params = {
      "hbUserName": userName,
      "hbPassword":password,
      "homeFax":ssn,
      "accountNumber":accountNumber,
      "subAccounts": data,
    };
    var authMode = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
    authMode.presentationController.showDownTimeMessage();
    newUserManager.createUserPreference(params,scope.createUserSuccess, scope.createUserFailure);
	//newUserManager.createUser(params,scope.createUserSuccess, scope.createUserFailure);
  },

  /**
  * createUserForEnroll Success callback
  * @member of Enroll_PresentationController
  */
  //if user is created successfully if he doesnot have email id navigate to update email screen
  createUserSuccess :function(response){
    applicationManager.getPresentationUtility().dismissLoadingScreen();
    var navManager = applicationManager.getNavigationManager();
    navManager.setCustomInfo("frmEnrollSignUp",{
      "userName" : scope.userName,
      "password" : scope.password,
      "isEnrollSuccess" : true
    });
    var custominfo= navManager.getCustomInfo("frmEnrollSecurity");
    var emailFlag=custominfo.isEmailPresent;
    if(!emailFlag){
      navManager.navigateTo("frmEmailCapture");
    }
    else{
      var controller = applicationManager.getPresentationUtility().getController('frmEnrollSignUp', true);
      controller.navigatetoDevReg();
    }
  },

  /**
  * createUserForEnroll Failure callback
  * @member of Enroll_PresentationController
  */
  createUserFailure :function(error){
    applicationManager.getPresentationUtility().dismissLoadingScreen();
    var controller = applicationManager.getPresentationUtility().getController('frmEnrollSignUp', true);
    if(error.errmsg.includes("UserName already exists!")){
      controller.bindUserNameUnAvailable();
    }
    else if (error["isServerUnreachable"])
    { 
      applicationManager.getPresentationInterruptHandler().showErrorMessage(handleMode, err);
    }
    else{
      // var contro = applicationManager.getPresentationUtility().getController('frmEnrollSignUp', true);
      var errorMsg = applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.enroll.enrollFailed");
      controller.bindViewError(errorMsg);
    }
  },

  checkAvailabilityOfUserName :function(userName){
    scope=this;
    var newUserManager = applicationManager.getNewUserBusinessManager();
    var params = {"hbUsername" : userName};
    var authMode = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
    authMode.presentationController.showDownTimeMessage();
    newUserManager.checkUserName(params,scope.checkAvailabilityOfUserNameSuccess,scope.checkAvailabilityOfUserNameFailure);
  },

  checkAvailabilityOfUserNameSuccess : function(response){
    //enrollPresentationScope.logger.log("#### checkAvailabilityOfUserNameSuccess in Enroll_PresentationController ####");
    var controller = applicationManager.getPresentationUtility().getController('frmEnrollSignUp', true);
    if(response.accountNumber && response.accountNumber!== "")
      controller.bindUserNameIsNotAvailable();
    else
      controller.bindUserNameIsAvailable();
  },

  /**
  * CheckUserName Failure Callback
  */
  checkAvailabilityOfUserNameFailure :function(){
    //enrollPresentationScope.logger.log("#### checkAvailabilityOfUserNameFailure in Enroll_PresentationController ####");
    var controller = applicationManager.getPresentationUtility().getController('frmEnrollSignUp', true);
    controller.bindUserNameIsNotAvailable();
  },

  saveEmail : function(email){
    scope=this;
    var newUserManager = applicationManager.getNewUserBusinessManager();
    var accountNumber=newUserManager.getEnrollObject().accountNumber;
    var params = {
      "email": email,
      "accountNumber":accountNumber
    };
    var authMode = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
    authMode.presentationController.showDownTimeMessage();
    newUserManager.updateUserEmail(params,scope.updateEmailSuccess, scope.updateEmailFailure);

  },
  updateEmailSuccess : function(response){
    //enrollPresentationScope.logger.log("#### checkAvailabilityOfUserNameSuccess in Enroll_PresentationController ####");
    // alert("success");
    var controller = applicationManager.getPresentationUtility().getController('frmEmailCapture', true);
    controller.navigatetoDevReg();

  },

  updateEmailFailure :function(){
    //enrollPresentationScope.logger.log("#### checkAvailabilityOfUserNameFailure in Enroll_PresentationController ####");
    var controller = applicationManager.getPresentationUtility().getController('frmEmailCapture', true);
    var errorMsg = applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.OnBoarding.InvalidEmail");
    controller.bindViewError(errorMsg);
  },

  showServiceResponseFailureError :function(){
    applicationManager.getPresentationUtility().dismissLoadingScreen();
    // enrollPresentationScope.logger.log("####  checkUserEnrolled failure callback ####");
    if (error["isServerUnreachable"])
      applicationManager.getPresentationInterruptHandler().showErrorMessage("preLogin", err);
    else{
      var controller = applicationManager.getPresentationUtility().getController('frmEnrollSSn', true);
      var errorMsg = applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.enroll.SomethingWrong");
      controller.bindViewError(errorMsg);
    }

  }

});