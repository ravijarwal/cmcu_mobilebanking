define({

AccountManager: function(){
      /**@member {Array} internalAccounts Contains data of all internal accounts*/
    this.internalAccounts = [];
    /**@member {Array} externalAccounts Contains data of all external accounts*/
    this.externalAccounts = [];
    /**@member {Array} fromTransferSupportedAccounts Contains data of all internal accounts which supports Transfers from them */
    this.fromTransferSupportedAccounts = [];
    /**@member {Array} toTransferSupportedAccounts Contains data of all internal accounts which supports Transfers to them*/
    this.toTransferSupportedAccounts = [];
    /**@member {Array} billPaySupportedAccounts Contains data of all internal accounts which supports BillPay from them */
    this.billPaySupportedAccounts = [];
    /**@member {Array} depositSupportedAccounts Contains data of all internal accounts which supports deposits to them*/
    this.depositSupportedAccounts = [];
    /**@member {Array} cardLessWithdrawlSupportedAccounts Contains data of all internal accounts which supports cardless cash withdrawal  from them*/
    this.cardLessWithdrawlSupportedAccounts = [];
    /**@member {Array} myMoneySupportedAccounts Contains data of all internal accounts which supports MyMoney */
    this.myMoneySupportedAccounts = [];
    /**@member {Array} shareAccounts Contains data of all internal accounts having share code */
    this.shareAccounts = [];
    /**@member {Array} loanAccounts Contains data of all internal accounts having loan code */
    this.loanAccounts = [];
},

  /**
 * Fetches the Internal Accounts using a service call.
 * @param {Function} presentationSuccessCallback - will be called when call is successfull.
 * @param {Function} presentationErrorCallback - will be called when call is not successfull.
 */
  fetchInternalAccounts : function(presentationSuccessCallback, presentationErrorCallback) {
    var self = this;
    this.AccountManager();
    var navManager = applicationManager.getNavigationManager();
    var tempLoginData = navManager.getCustomInfo("frmLogin");
    var userParam = tempLoginData.userName.toUpperCase();
    var pwdParam = tempLoginData.password;
    var params = {"username":userParam,"password":pwdParam};
    var accountsRepo = kony.mvc.MDAApplication.getSharedInstance().getRepoManager().getRepository("Accounts");
    accountsRepo.customVerb('getAccountsPostLogin', params, getAllCompletionCallback);

    function getAllCompletionCallback(status, data, error) {
      var srh = applicationManager.getServiceResponseHandler();
      var obj = srh.manageResponse(status, data, error);
      if (obj["status"] === true) {
        self.clearInternalAccounts();
        self.createLoanAndShareAccounts(obj["data"]);
        self.splitInternalAccounts(obj["data"]);
        presentationSuccessCallback(obj["data"]);
      } else {
        presentationErrorCallback(obj["errmsg"]);
      }
    }

  },

    createLoanAndShareAccounts : function(internalAccounts) {
    this.internalAccounts = internalAccounts;
    for (var i = 0; i < internalAccounts.length; i++) {
      if (internalAccounts[i].loanCode!==null && internalAccounts[i].loanCode!==undefined)
      {
        this.loanAccounts.push(internalAccounts[i]);
      }

      if (internalAccounts[i].shareCode!==null && internalAccounts[i].shareCode!==undefined)
      {
        this.shareAccounts.push(internalAccounts[i]);
      }

    }
  },

    getInternalShareAccountByID : function(accoundID) {
    for (var i = 0; i < this.shareAccounts.length; i++) {
      if (this.shareAccounts[i].accountID == accoundID) {
        return this.shareAccounts[i];
      }
    }
    return "";
  },
  
  
    getInternalLoanAccountByID : function(accoundID) {
    for (var i = 0; i < this.loanAccounts.length; i++) {
      if (this.loanAccounts[i].accountID == accoundID) {
        return this.loanAccounts[i];
      }
    }
    return "";
  },
  
  //Update NickName for Shares
  updateShareNickName : function(criteria,presentationSuccessCallback, presentationErrorCallback){
    var accRepo = kony.mvc.MDAApplication.getSharedInstance().getRepoManager().getRepository("Accounts");
    accRepo.customVerb("updateShareNickName", criteria, getAllCompletionCallback);
    function getAllCompletionCallback(status, data, error) {
      var srh = applicationManager.getServiceResponseHandler();
      var obj =  srh.manageResponse(status, data, error);
      if(obj["status"] === true){
        presentationSuccessCallback(obj["data"]);
      }
      else {
        presentationErrorCallback(obj["errmsg"]);
      }
    }
  },
  
  //Update NickName for Loans
  updateLoanNickName : function(criteria,presentationSuccessCallback, presentationErrorCallback){
    var accRepo = kony.mvc.MDAApplication.getSharedInstance().getRepoManager().getRepository("Accounts");
    accRepo.customVerb("updateLoanNickName", criteria, getAllCompletionCallback);
    function getAllCompletionCallback(status, data, error) {
      var srh = applicationManager.getServiceResponseHandler();
      var obj =  srh.manageResponse(status, data, error);
      if(obj["status"] === true){
        presentationSuccessCallback(obj["data"]);
      }
      else {
        presentationErrorCallback(obj["errmsg"]);
      }
    }
  },
  
});