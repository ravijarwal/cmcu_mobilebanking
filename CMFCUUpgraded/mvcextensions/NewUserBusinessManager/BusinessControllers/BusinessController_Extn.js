define({
  //code for calling actual SSN service 
  SsnEnrolled : function(params,presentationSuccessCallback,presentationErrorCallback) {          
	//alert("In NewUserBusiness Manager Extn For validate Account");       
	var self = this;          
	var accEnrolled=  kony.mvc.MDAApplication.getSharedInstance().getRepoManager().getRepository("User");          
	accEnrolled.customVerb('verifySsnEnrollment', params, SsnEnrolledCompletionCallback);    	    
   function  SsnEnrolledCompletionCallback(status,  data,  error) {              
		var srh = applicationManager.getServiceResponseHandler();              
		var obj = srh.manageResponse(status, data, error);              
		kony.print("Data from ssn Enrollment... "+JSON.stringify(obj));              
		if (obj["status"] === true) {                  
			presentationSuccessCallback(obj["data"]);             
		} else {                  
			presentationErrorCallback(obj["errmsg"]);              
		}          
	}      
},
  
  //code for creating new user
  createUserPreference : function(params,successCallback,errorCallback){
    var userRepo = kony.mvc.MDAApplication.getSharedInstance().getRepoManager().getRepository("User");
    userRepo.customVerb('createNewUser', params, completionCallback);

    function completionCallback(status,data,error){
      var srh = applicationManager.getServiceResponseHandler();
      var obj = srh.manageResponse(status, data, error);
      if (obj["status"] === true) {
        successCallback(obj["data"]);
      } else {
        errorCallback(obj["errmsg"]);
      }
    }
  },
  
  // calling integration service to create new user
  createUser : function (userObj,presentationSuccessCallback,presentationErrorCallback){
    var serviceName = "CMCUUserService";
    // Get an instance of SDK
    var client = kony.sdk.getCurrentInstance();
    var integrationSvc = client.getIntegrationService(serviceName);
    var operationName = "Create";
    var options = {};
    var headers = {};
    var userName = userObj.hbUserName;
    var password = userObj.hbPassword;
    var ssn = userObj.homeFax;
    var accountNumber = userObj.accountNumber;
    var subAccounts = userObj.subAccounts;
    var params = {
      "hbUserName": userName,
      "hbPassword":password,
      "homeFax":ssn,
      "accountNumber":accountNumber,
      "subAccounts": subAccounts,
    };
    integrationSvc.invokeOperation(operationName, headers, params, successCallback, errorCallback);

    function successCallback(response) {
      if (response.errorCode == "0") {
        presentationSuccessCallback(response);
      } else {
        presentationErrorCallback(response);
      }
    }

    function errorCallback(resError) {
      presentationErrorCallback(resError);
    }
  },

  /**
  * Check wheather username  is already registered by making a service call.
  * @param {object} params - username which has to be verified.
  * @param {function} presentationSuccessCallback - invoke the call back with success response.
  * @param {function} presentationErrorCallback - invoke the call back with error response.
  */
  checkUserName :function(params, presentationSuccessCallback, presentationErrorCallback) {
    var self = this;
    var checkUserName  =  kony.mvc.MDAApplication.getSharedInstance().getRepoManager().getRepository("NewUser");
    checkUserName.customVerb('verifyEnrolledUser', params,  getAllCompletionCallback);

    function  getAllCompletionCallback(status,  data,  error) {
      var srh = applicationManager.getServiceResponseHandler();
      var obj = srh.manageResponse(status, data, error);
      if (obj["status"] === true) {
        presentationSuccessCallback(obj["data"]);
      } else {
        presentationErrorCallback(obj["errmsg"]);
      }
    }
  },
  updateUserEmail: function(params, presentationSuccessCallback, presentationErrorCallback) {
    var self = this;
    var updateEmail  =  kony.mvc.MDAApplication.getSharedInstance().getRepoManager().getRepository("User");
    updateEmail.customVerb('updateShareNameId', params,  updateEmailCompletionCallback);

    function  updateEmailCompletionCallback(status,  data,  error) {
      var srh = applicationManager.getServiceResponseHandler();
      var obj = srh.manageResponse(status, data, error);
      if (obj["status"] === true) {
        presentationSuccessCallback(obj["data"]);
      } else {
        presentationErrorCallback(obj["errmsg"]);
      }
    }
  }

});