define({
  invokeIdentity : function(destination,type){
    scope=this;
    var authManger = applicationManager.getAuthManager();
    authManger.invokeIdentityService(scope.identitySuccess, scope.identityFailure);
  },
  identitySuccess: function(resSuccess){
    scope=this;

    scope.sendOTP(destination,type);
  },
  identityFailure : function(resError){

    applicationManager.getPresentationUtility().dismissLoadingScreen();
    var controller = applicationManager.getPresentationUtility().getController('frmEnrollSecurityCheck', true);
  },
  //code for fetching otp from service when user requests otp through email,call,text,etc
  sendOTP : function(destination,type){
    scope=this;
    var userObj = applicationManager.getUserPreferencesManager();
    var userName = userObj.getUserName();

    var params = {
      "destination":"ravijarwal10@gmail.com",
      "userName": userName,
      "type":"m",
      "deviceInfo":"",
      "screenFlow":"settings module"
    };
    var authManager = applicationManager.getAuthManager();
    authManager.sendOTP(params,scope.sendOTPSuccess, scope.sendOTPFailure);
  },
  sendOTPSuccess : function(response){

    applicationManager.getPresentationUtility().dismissLoadingScreen();
    var navManager = applicationManager.getNavigationManager();

    navManager.navigateTo("frmProfileSecurityCode");
  },

  sendOTPFailure : function(error){
    //enrollPresentationScope.logger.log("#### request OTP service call failed ####");
    applicationManager.getPresentationUtility().dismissLoadingScreen();
    if (error["isServerUnreachable"])
      applicationManager.getPresentationInterruptHandler().showErrorMessage("preLogin",err);
    else{
      var controller = applicationManager.getPresentationUtility().getController('frmProfileSecurityCode', true);
      var errorMsg = applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.enroll.SomethingWrong");
      controller.bindViewError(errorMsg);  
      //generic error callback
    }
  },

  //code for calling resend otp service
  resendOTP : function(type){
    scope=this;
    var newUserManager = applicationManager.getNewUserBusinessManager();

    var params = {
      "userName": "SWAPZ",
    };
    var authManager = applicationManager.getAuthManager();
    authManager.resendOTP(params,scope.resendOTPSuccess, scope.resendOTPFailure);
    // newUserManager.sendOTP(params,scope.sendOTPSuccess, scope.sendOTPFailure);

  },


  resendOTPSuccess :function(){

    applicationManager.getPresentationUtility().dismissLoadingScreen();

  },
  resendOTPFailure :function(){

    applicationManager.getPresentationUtility().dismissLoadingScreen();

  },
  //code for validating otp after user clicks verify btn in otp screen
  verifyOTP : function(otp){
    scope=this;
    var validationUtilManager = applicationManager.getValidationUtilManager();
    var userObj = applicationManager.getUserPreferencesManager();

    if(validationUtilManager.isValidOTP(otp))
    {
      var newUserManager = applicationManager.getNewUserBusinessManager();
      var userName = userObj.getUserName();
      var verifyOTPJSON = {
        "otp":otp,
        "userName": userName,
        "deviceInfo":"",
        "screenFlow":"settings module"
      };
      var authManager = applicationManager.getAuthManager();
      authManager.verifyOTP(verifyOTPJSON,scope.validateOTPSuccess,scope.validateOTPFailure);
    }
    else
    {
      applicationManager.getPresentationUtility().dismissLoadingScreen();
      var controller = applicationManager.getPresentationUtility().getController('frmProfileSecurityCode', true);
      var errormsg =  applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.common.enterSecurityCode");
      controller.bindGenericError(errormsg);
    } 

  },
  validateOTPSuccess : function(response){
    //enrollPresentationScope.logger.log("####  OTP validation success call back ####");
    applicationManager.getPresentationUtility().dismissLoadingScreen();
    if(response.isSuccessful && response.isSuccessful!== "")
    {
      if(response.isSuccessful==="true"){
        var navManager = applicationManager.getNavigationManager();
        navManager.navigateTo("frmProfileChangeAndUpdatePassword");
      }
      else{
        var controller = applicationManager.getPresentationUtility().getController('frmProfileSecurityCode', true);
        var errorMsg = applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.enroll.enterValidOTP");
        controller.bindGenericError(errorMsg);
      }
    }

  },

  updateUserNameWithNewname : function(newUserName){
    var userObj = applicationManager.getUserPreferencesManager();
    var oldUserName = userObj.getUserName();
    if(oldUserName === newUserName){
      var controller = applicationManager.getPresentationUtility().getController('frmProfileUsername',true);
      var i18nMsg = applicationManager.getPresentationUtility().getStringFromi18n('kony.mb.profile.usernameSame',"New username can't be same as current username");
      controller.bindViewError(i18nMsg);
      return;
    }
    applicationManager.getPresentationUtility().showLoadingScreen();
    var accountMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountModule");
    var accountNumber = accountMod.presentationController.primaryAccntNumber;
    var params ={"currentUserName":oldUserName,"newUserName":newUserName,"accountNumber":accountNumber};
    userObj.updateUserName(params,scope_SettingsPresenter.updateUserNameSuccess,scope_SettingsPresenter.updateUserNameFailure);
  },

  invokeIdentityForUsername: function(newUserName) {
    scope = this;
    this.changeUsername = newUserName;
    var authManger = applicationManager.getAuthManager();
    authManger.invokeIdentityService(scope.identitySuccessForUsername, scope.identityFailureForUsername);
  },
  identitySuccessForUsername: function(resSuccess) {
    scope = this;
    scope.updateUserNameWithNewname(this.changeUsername);
  },
  identityFailureForUsername: function(resError) {
    applicationManager.getPresentationUtility().dismissLoadingScreen();
    var controller = applicationManager.getPresentationUtility().getController('frmEnrollSecurityCheck', true);
  },

  Settings_PresentationController:function() {
    scope_SettingsPresenter = this;
    var defaultAcc;
    kony.mvc.Presentation.BasePresenter.call(this);
    this.flowType="";
    this.eStatementPopup="";
    this.estatementData={};
    this.currLatitude ="";
    this.currLongitude ="";
    scope_SettingsPresenter.userAddressFlowtype = null;
    this.changeUsername = "";

  },

  checkAndUpdatePassword : function(oldPassword,newPassword,reEnteredPassword){
    var userObj = applicationManager.getUserPreferencesManager();
    var password = userObj.getPassword();
    password = password.toUpperCase();
    oldPassword = oldPassword.toUpperCase();
    var formController = applicationManager.getPresentationUtility().getController('frmProfileChangeAndUpdatePassword',true);
    if(password !== oldPassword){
      var i18nV = applicationManager.getPresentationUtility().getStringFromi18n('kony.profile.currentpasswordWrong');
      formController.bindViewError(i18nV);
      return;
    }
    var validationUtility = applicationManager.getValidationUtilManager();
    if(validationUtility.isValidPassword(newPassword) === false){
      var i18nV1 = applicationManager.getPresentationUtility().getStringFromi18n('kony.mb.common.invalidPassword');
      formController.bindViewError(i18nV1);
      return;
    }
    //var newUserPassword = newPassword.toUpperCase();
    if(newUserPassword !== reEnteredPassword){
      var i18nP = applicationManager.getPresentationUtility().getStringFromi18n('kony.mb.enroll.passwordNotMatch');
      formController.bindViewError(i18nP);
      return;
    }
    applicationManager.getPresentationUtility().showLoadingScreen();
    var userManager = applicationManager.getUserPreferencesManager();
    var accountMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountModule");
    var accountNumber = accountMod.presentationController.primaryAccntNumber;
    var userName = userObj.getUserName();
    var params = {
      "userName": userName,
      "newUserPassword": newPassword,
      "accountNumber": accountNumber
    };
    userManager.updateUserPassword(params,scope_SettingsPresenter.updatePasswordSuccess,scope_SettingsPresenter.updatePasswordFailure);
  }

});