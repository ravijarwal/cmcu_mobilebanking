define({
  getTransactions : function(){
     scope_Trans_Pre = this;
    applicationManager.getPresentationUtility().showLoadingScreen();
   // scope_Trans_Pre.asyncManager.initiateAsyncProcess(scope_Trans_Pre.numberOfAsyncForTransactions);
    kony.print("delay");
    scope_Trans_Pre.getUserScheduledTransactions();
  },
  getUserScheduledTransactions : function(){
    var authMode = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
    authMode.presentationController.showDownTimeMessage();
    var transactionManager=applicationManager.getTransactionManager();
    transactionManager.fetchUserScheduledTransactions(this.getUserScheduledTransactionsSuccessCallback,this.getUserScheduledTransactionsErrorCallback);
  },
 getUserScheduledTransactionsErrorCallback : function(resErr){
    scope_Trans_Pre.asyncManager.setErrorStatus(1,resErr);
    applicationManager.getPresentationUtility().dismissLoadingScreen();
    if(resErr["isServerUnreachable"])
      applicationManager.getPresentationInterruptHandler().showErrorMessage("postLogin", resErr);
  },
 
  getUserScheduledTransactionsSuccessCallback : function(resSuccess){
   // scope_Trans_Pre.asyncManager.setSuccessStatus(1,resSuccess);
     applicationManager.getPresentationUtility().dismissLoadingScreen();
   scope_Trans_Pre.navToTransferLanding(resSuccess);
  },

  navToTransferLanding : function(response){
    kony.print("navToTransferLanding response: "+JSON.stringify(response));
    var navMan = applicationManager.getNavigationManager();
    //this.filterAccountData();
    var formatUtil=applicationManager.getFormatUtilManager();
    var tranDate,tranAmt,reference2,matchId3,amtCode,type,amtData,sTType,sTAmount,sTEfftDate,lnType,amount,efftDate,freq,sTFrequency,lnFrequency,sTAccountNumber,sTId,sTIdType,acctNum,lnId,lnIdType,id,sTLocator,locator,eftLocator,expDate,sTExpDate,lTExpDate,shareId,loanId;
    var temp = {},shareResponse = {},loanResponse = {};
    var transactions = [],eftList=[],shareList=[],loanList=[];
    var i,j,length;
    eftList = response.EftList;
    shareList = response.ShareList;
    loanList = response.LoanList;
	if(eftList !== undefined){
		length = eftList.length;
    if(length !== 0){
      for(i=0 ; i<length ; i++){
        if(eftList[i]['Type'] >= "0" && eftList[i]['Type'] <= "5"){
          tranDate = eftList[i]['transactionDate'];
          tranAmt = eftList[i]['TranAmount'];
          reference2 = eftList[i]['reference2'];
          matchId3 = eftList[i]['matchId3'];
          amtCode = eftList[i]['amountCode'];
          freq = eftList[i]['frequencyType'];
          eftLocator = eftList[i]['eftLocator'];
          expDate = eftList[i]['ExpirationDate'];
          id = eftList[i]['Id'];
          amtData = "";
          var amtText = "";
          if(expDate !== ""){
            if(tranDate !== null && tranAmt !== null && reference2 !== null && matchId3 !== null && amtCode !== null && tranDate !== undefined && tranAmt !== undefined && reference2 !== undefined && matchId3 !== undefined && amtCode !== undefined && id !== null && id !== undefined && eftLocator !== null && eftLocator !== undefined){
              if(amtCode == "22" || amtCode == "27"){
                amtText = "Checking";
              }
              else if(amtCode == "32" || amtCode == "37"){
                amtText = "Saving";
              }
              else if(amtCode == "52"){
                amtText = "Loan";
              }
              amtData = matchId3 + " " + amtText;
              if(amtCode == "27" || amtCode == "37"){
                tranAmt = "-$"+tranAmt;
              }
              else{
                tranAmt = "$"+tranAmt;
              }
              temp = {"listType":"eftList","tranDate":tranDate,"tranAmt":tranAmt,"amtData":amtData,"reference2":reference2,"acctType":amtText,"acctText":matchId3,"freq":freq,"id":id,"amtCode":amtCode,"eftLocator":eftLocator};
              transactions.push(temp);
            }  
          }
        }
      }
    }
	}
    if(shareList !== undefined){
		length = shareList.length;
    if(length !== 0){
        for(i=0 ; i<length ; i++){
          shareId = shareList[i]["shareId"];
          if(shareId !== null && shareId !== undefined && shareList[i]["ShareTransferList"] !== undefined && shareList[i]["ShareTransferList"] !== null){
            shareResponse = shareList[i]["ShareTransferList"];
            for(j=0 ; j<shareResponse.length ; j++){
              sTType = shareResponse[j]["STType"];
              sTExpDate = shareResponse[j]["STExpDate"];
              if(sTExpDate !== ""){
                if(sTType !== null && sTType !== undefined && (sTType == "3" || sTType == "9")){
                  sTAmount = "$"+shareResponse[j]["STAmount"];
                  sTEfftDate = shareResponse[j]["STEffectiveDate"];
                  sTFrequency = shareResponse[j]["STFrequency"];
                  sTAccountNumber = shareResponse[j]["STAccountNumber"];
                  sTId = shareResponse[j]["STId"];
                  sTIdType = shareResponse[j]["STIdType"];
                  sTLocator = shareResponse[j]["STLocator"];
                  if(sTAmount !== null && sTAmount !== undefined && sTEfftDate !== null && sTEfftDate !== undefined && sTFrequency != null && sTFrequency != undefined && sTLocator !== null && sTLocator !== undefined){
                    temp = {"listType":"shareList","sTAmount":sTAmount,"sTEfftDate":sTEfftDate,"sTFrequency":sTFrequency,"sTAccountNumber":sTAccountNumber,"sTId":sTId,"sTIdType":sTIdType,"sTLocator":sTLocator,"shareId":shareId};
					transactions.push(temp);
				  }
                }
              }
            }
          }
        }
    }
	}
    if(loanList !== undefined){	
		length = loanList.length;
    if(length !== 0){
        for(i=0 ; i<length ; i++){
          loanId = loanList[i]["loanId"];
          if(loanId !== undefined && loanId !== null && loanList[i]["LoanTransferList"] !== undefined && loanList[i]["LoanTransferList"] !== null){
            loanResponse = loanList[i]["LoanTransferList"];
            for(j=0 ; j<loanResponse.length ; j++){
              lnType = loanResponse[j]["LnType"];
              lTExpDate = loanResponse[j]["LTExpDate"];
              if(lTExpDate !== ""){
                if(lnType !== null && lnType !== undefined || (lnType === "3" || lnType === "9" )){
                  amount = "$"+loanResponse[j]["amount"];
                  efftDate = loanResponse[j]["effectiveDate"];
                  lnFrequency = loanResponse[j]["LnFrequency"];
                  acctNum = loanResponse[j]["accountNumber"];
                  lnId = loanResponse[j]["LnId"];
                  lnIdType = loanResponse[j]["LnIdType"];
                  locator = loanResponse[j]["Locator"];
                  if(amount != null && amount != undefined && efftDate != null && efftDate != undefined && lnFrequency != null && lnFrequency != undefined && acctNum != null && acctNum != undefined && lnId != null && lnId != undefined && lnIdType != null && lnIdType != undefined && locator !== null && locator !== undefined && loanId !== null && loanId !== undefined){
                    temp = {"listType":"loanList","amount":amount,"efftDate":efftDate,"lnFrequency":lnFrequency,"acctNum":acctNum,"lnId":lnId,"lnIdType":lnIdType,"locator":locator,"loanId":loanId};
					transactions.push(temp);
				  }
                }
              }
            }
          }
        }  
      }
	}
    
    //alert("transactions--- "+JSON.stringify(transactions));
    navMan.setCustomInfo("frmTransfers",transactions);   
    navMan.navigateTo("frmTransfers");
  },


  showAccounts:function(type)
  {
    var navMan = applicationManager.getNavigationManager();
    var trasMan = applicationManager.getTransactionManager();
    var authMode = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
    authMode.presentationController.showDownTimeMessage();

    trasMan.getDonorReceipientAccountsList(this.PSCBFordonorRecipient,this.PECBFordonorRecipient);

    applicationManager.getPresentationUtility().dismissLoadingScreen();
    //navMan.setCustomInfo("frmTransfersToAccount",{});
    //var accountList = navMan.getCustomInfo("frmTransfersToAccount");
    var accountList = navMan.getCustomInfo("frmToFromAccount");
    if (accountList && accountList !== null) {
      accountList.type = type;
    } else {
      accountList = {
        "type": type
      };
    }
    navMan.setCustomInfo("frmTransfersToAccount", accountList);
    if (type == applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.transfer.MyCMCUAccounts")) {
      scope_TransfersPresentationController.setFlowType("MyCMCUAccounts");
      trasMan.setTransactionAttribute("transactionType","InternalTransfer");
      scope_TransfersPresentationController.showFromAccounts(scope_TransfersPresentationController.showInternalAccountsPresentationSuccessCallBack);

    }

  },



  PSCBFordonorRecipient:function(resp)
  {
     //alert("response for accounts"+JSON.stringify(resp));
    // var data={};
    var donorList=resp.Donors;
    var recpList=resp.Recipients;
    var navMan = applicationManager.getNavigationManager();
    // this.data.donorList=donorList;
    //this.data.receipientList=recpList;
    navMan.setCustomInfo("frmToFromAccounts",resp);  

    navMan.navigateTo("frmTransfersToAccount");
  },

  PECBFordonorRecipient:function(resErr)
  {
    scope_Trans_Pre.asyncManager.setErrorStatus(0,resErr);
    applicationManager.getPresentationUtility().dismissLoadingScreen();
    if(resErr["isServerUnreachable"])
      applicationManager.getPresentationInterruptHandler().showErrorMessage("postLogin", resErr);

  },

  processAccountsData :function(data) {
    var accProcessedData = [];
    //alert("entered recp list"+JSON.stringify(data));
    for (var i = 0; i < data.length; i++) {
      accProcessedData[i] = {};
      // alert("data.list"+data.list);
      if( data.list!=="DONORLIST")      

      {

        if((data[i].balance!==null && data[i].balance!==" ")||
           ( data[i].toAccountNumber!==null &&  data[i].toAccountNumber!==" ") || (data[i].toAccountType!==null && data[i].toAccountType!==" "))
        {
          accProcessedData[i].accountName = data[i].description;
          accProcessedData[i].availableBalance = this.getAvailableBalanceCurrencyString(data[i]);
          accProcessedData[i].accountBalanceType = this.getAvailableBalanceType(data[i]);
          accProcessedData[i].accountType = data[i].toAccountType;
          accProcessedData[i].accountID = data[i].accountID;
          accProcessedData[i].accountNumber = data[i].toAccountNumber;
          //accProcessedData[i].bankName = data[i].bankName;
        }
      }
      else
      {


        if((data[i].availableBalance!==null && data[i].availableBalance!==" ")||
           ( data[i].fromAccountName!==null &&  data[i].fromAccountName!==" ") || (data[i].fromAccountType!==null && data[i].fromAccountType!==" "))
        {
          accProcessedData[i].accountName = data[i].description;
          accProcessedData[i].availableBalance = this.getAvailableBalanceCurrencyStringForFromAccounts(data[i]);
          accProcessedData[i].accountBalanceType = this.getAvailableBalanceType(data[i]);
          accProcessedData[i].accountType = data[i].fromAccountType;
          accProcessedData[i].accountID = data[i].accountID;
          accProcessedData[i].accountNumber = data[i].fromAccountNumber;
          //accProcessedData[i].bankName = data[i].bankName;
        }


      }




      //accProcessedData[i].availableBalance = this.getAvailableBalanceCurrencyString(data[i]);



    }
    return accProcessedData;
  },

  getAvailableBalanceCurrencyString :function(data) {

    var forUtility = applicationManager.getFormatUtilManager();
    var configManager = applicationManager.getConfigurationManager();
    switch (data.accountType) {
      case configManager.constants.SAVINGS:
        return forUtility.formatAmountandAppendCurrencySymbol(data["availableBalance"]);
      case configManager.constants.CHECKING:
        return forUtility.formatAmountandAppendCurrencySymbol(data["availableBalance"]);
      case configManager.constants.CREDITCARD:
        return forUtility.formatAmountandAppendCurrencySymbol(data["availableBalance"]);
      case configManager.constants.DEPOSIT:
        return forUtility.formatAmountandAppendCurrencySymbol(data["currentBalance"]);
      case configManager.constants.MORTGAGE:
        return forUtility.formatAmountandAppendCurrencySymbol(data["outstandingBalance"]);
      case configManager.constants.LOAN:
        return forUtility.formatAmountandAppendCurrencySymbol(data["outstandingBalance"]);
      default:
        return forUtility.formatAmountandAppendCurrencySymbol(data["balance"]);
    }
  },



  getAvailableBalanceCurrencyStringForFromAccounts :function(data) {

    var forUtility = applicationManager.getFormatUtilManager();
    var configManager = applicationManager.getConfigurationManager();
    switch (data.accountType) {
      case configManager.constants.SAVINGS:
        return forUtility.formatAmountandAppendCurrencySymbol(data["availableBalance"]);
      case configManager.constants.CHECKING:
        return forUtility.formatAmountandAppendCurrencySymbol(data["availableBalance"]);
      case configManager.constants.CREDITCARD:
        return forUtility.formatAmountandAppendCurrencySymbol(data["availableBalance"]);
      case configManager.constants.DEPOSIT:
        return forUtility.formatAmountandAppendCurrencySymbol(data["currentBalance"]);
      case configManager.constants.MORTGAGE:
        return forUtility.formatAmountandAppendCurrencySymbol(data["outstandingBalance"]);
      case configManager.constants.LOAN:
        return forUtility.formatAmountandAppendCurrencySymbol(data["outstandingBalance"]);
      default:
        return forUtility.formatAmountandAppendCurrencySymbol(data["availableBalance"]);
    }
  },

  getAvailableBalanceType :function(data) {
    var configManager = applicationManager.getConfigurationManager();
    switch (data.accountType) {
      case configManager.constants.SAVINGS:
        return kony.i18n.getLocalizedString("kony.mb.accdetails.availBal");
      case configManager.constants.CHECKING:
        return kony.i18n.getLocalizedString("kony.mb.accdetails.availBal");
      case configManager.constants.CREDITCARD:
        return kony.i18n.getLocalizedString("kony.mb.accdetails.availBal");
      case configManager.constants.DEPOSIT:
        return kony.i18n.getLocalizedString("kony.mb.accdetails.currBal");
      case configManager.constants.MORTGAGE:
        return kony.i18n.getLocalizedString("kony.mb.accdetails.outstandingBal");
      case configManager.constants.LOAN:
        return kony.i18n.getLocalizedString("kony.mb.accdetails.outstandingBal");
      default:
        return kony.i18n.getLocalizedString("kony.mb.accdetails.availBal");
    }
  },
  getPendingPostedLoanTransactions:function(searchOptions){
    scope_Trans_Pre.getPostedLoanTransactions(searchOptions);
  },
  getPostedLoanTransactions :function(searchOptions){
    searchOptions.isScheduled ="0";
    var transactionManager=applicationManager.getTransactionManager();
    transactionManager.fetchPostedLoanTransactions(searchOptions,this.getPostedTransactionsPresentationSuccessCallback,this.getPostedTransactionsPresentationErrorCallback);
  },
  
  getPendingPostedTransactions :function(searchOptions) {
    //calling only posted tran
    scope_Trans_Pre.getPostedTransactions(searchOptions);
  },
  getPostedTransactions :function(searchOptions){
    searchOptions.isScheduled ="0";
    var transactionManager=applicationManager.getTransactionManager();
    transactionManager.fetchPostedTransactions(searchOptions,this.getPostedTransactionsPresentationSuccessCallback,this.getPostedTransactionsPresentationErrorCallback);
  },
  getPostedTransactionsPresentationSuccessCallback :function(resSuccess){
    scope_Trans_Pre.navigateTofrmAdvanceSearchResults(resSuccess);
  },

  navigateTofrmAdvanceSearchResults :function(resSuccess)
  {
      var navMan = applicationManager.getNavigationManager();
      var transactionDetails = {};
      transactionDetails.postedTransactions = resSuccess;
      navMan.setCustomInfo("frmAdvanceSearch",transactionDetails);
      scope_Trans_Pre.setPendPostTransactions(transactionDetails);
      var controller = applicationManager.getPresentationUtility().getController('frmAdvanceSearchResults', true);
      controller.setTransactionData();   
      navMan.navigateTo("frmAdvanceSearchResults");
    }
});



