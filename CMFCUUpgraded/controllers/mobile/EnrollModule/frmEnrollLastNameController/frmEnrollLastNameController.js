define({
  timerCounter:0,
  init : function(){
    var navManager = applicationManager.getNavigationManager();
    var currentForm=navManager.getCurrentForm();
    applicationManager.getPresentationFormUtility().initCommonActions(this,"YES",currentForm);
  },
  frmEnrollLAstNamePreShow : function(){
    this.setFlowAction();
    this.setPreShowData();
    this.view.tbxLastName.text = "";
    this.view.tbxFirstName.text = "";
    this.view.btnContinue.skin = "sknBtnOnBoardingInactive";
    this.view.btnContinue.setEnabled(false);
    applicationManager.getPresentationUtility().dismissLoadingScreen();
    var navManager = applicationManager.getNavigationManager();
    var currentForm=navManager.getCurrentForm();
    applicationManager.getPresentationFormUtility().logFormName(currentForm);
  },
  setFlowAction  : function(){
    var scopeObj = this;
    this.view.customHeader.flxBack.onClick = function(){
      scopeObj.navBack();
    };
    this.view.customHeader.btnRight.onClick = function(){
      scopeObj.onClickCancel();
    };
    this.view.tbxFirstName.onTextChange = this.onFirstNameChange;
    this.view.tbxLastName.onTextChange = this.onLastNameChange;
    this.view.btnContinue.onClick = function(){
      scopeObj.validateLastName();
    };
  },
  onFirstNameChange: function(){
    if(this.view.tbxLastName.text!==null||this.view.tbxLastName.text!=="")
    {
      this.changeSkinBtnContinue();
    }
  },
  onLastNameChange: function(){
    if(this.view.tbxFirstName.text!==null||this.view.tbxFirstName.text!=="")
    {
      this.changeSkinBtnContinue();
    }
  },
  setPreShowData  : function(){
    this.view.lblText.skin = "sknLbl424242SSPLight36px";
    this.view.lblFirstName.text = kony.i18n.getLocalizedString("kony.mb.enroll.enterFirstName");
    this.view.lblLastName.text =  kony.i18n.getLocalizedString("kony.mb.enroll.enterLastName");
    this.view.tbxLastName.placeholder = kony.i18n.getLocalizedString("kony.mb.enroll.input");
    this.view.tbxLastName.skin = "sknTbx424242SSPRegular28px";
    this.view.tbxLastName.setFocus(true);
    this.view.tbxFirstName.placeholder = kony.i18n.getLocalizedString("kony.mb.enroll.input");
    this.view.tbxFirstName.skin = "sknTbx424242SSPRegular28px";
    this.view.tbxFirstName.setFocus(true);
    var scope = this;
    this.view.flxHeader.customHeader.lblLocateUs.text = kony.i18n.getLocalizedString("kony.mb.enroll.enroll");
    this.view.flxHeader.customHeader.btnRight.text="";
    /*if(kony.os.deviceInfo().name !== "iPhone"){
      this.view.flxHeader.isVisible = true;
    }
    else{
      this.view.flxHeader.isVisible = false;
    }*/
    this.view.flxHeader.isVisible = true;
    var enrollMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("EnrollModule");
    var userlastname = enrollMod.presentationController.getEnrollLastName();
    if(userlastname !== null && userlastname !== "" && userlastname !== undefined){
      this.view.tbxLastName.text = userlastname;
      this.view.btnContinue.skin = "sknBtn0095e426pxEnabled";
      this.view.btnContinue.focusSkin = "sknBtn0095e426pxEnabled";
      this.view.btnContinue.setEnabled(true);
    }
    else{
      this.view.tbxLastName.text = "";
      this.view.btnContinue.skin = "sknBtnOnBoardingInactive";
      this.view.btnContinue.setEnabled(false);
    }
    this.view.btnContinue.skin = "sknBtnOnBoardingInactive";
    this.view.btnContinue.setEnabled(false);
  },
  changeSkinBtnContinue : function(){
    if(this.isValidInputs()){
      this.view.btnContinue.skin = "sknBtn0095e426pxEnabled";
      this.view.btnContinue.focusSkin = "sknBtn0095e426pxEnabled";
      this.view.btnContinue.setEnabled(true);
    }else{
      this.view.btnContinue.skin = "sknBtnOnBoardingInactive";
      this.view.btnContinue.setEnabled(false);
    }
  },
  isValidInputs :function(){
    return this.view.tbxFirstName.text.length>0 && this.view.tbxLastName.text.length>0;
  },
  navToSecurityCheck : function(){
    var enrollMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("EnrollModule");
    enrollMod.presentationController.commonFunctionForNavigation("frmEnrollSecurityCheck");
  },
  onClickCancel : function(){
    var enrollMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("EnrollModule");
    enrollMod.presentationController.resetEnrollObj();
  },
  navToDOB : function(){
    var enrollMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("EnrollModule");
    enrollMod.presentationController.commonFunctionForNavigation("frmEnrollDOB");
  },
  navBack : function(){
    var enrollPC = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("EnrollModule");
    enrollPC.presentationController.commonFunctionForNavigation("frmLogin");
  },

  //development
  /**
  * validates Last Name
  */
  validateLastName : function(){
    var lastName = this.view.tbxLastName.text;
    var firstName = this.view.tbxFirstName.text;
    if(lastName === '' && firstName===''|| lastName === null && firstName===null || lastName === undefined&&firstName===undefined){
      this.bindViewError(applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.common.invalidLastName"));      
    }
    else{
      var enrollMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("EnrollModule");
      var navManager = applicationManager.getNavigationManager();
      navManager.setCustomInfo("dateOfBirth", "");
      enrollMod.presentationController.navigateToFrmEnrollDOB(lastName,firstName);
    }
  },
  /**
  *Shows Toast Message with red skin
  */
  bindViewError : function(msg)
  {
    applicationManager.getPresentationUtility().dismissLoadingScreen();
    applicationManager.getDataProcessorUtility().showToastMessageError(this,msg);
  },
});