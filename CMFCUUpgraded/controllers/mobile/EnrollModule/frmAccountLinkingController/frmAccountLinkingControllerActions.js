define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** init defined for frmAccountLinking **/
    AS_Form_a8647c9d49ea4d3481d32cfad2b0a894: function AS_Form_a8647c9d49ea4d3481d32cfad2b0a894(eventobject) {
        var self = this;
        return self.init.call(this);
    },
    /** preShow defined for frmAccountLinking **/
    AS_Form_b3984f45ac7d444a898b7264c7e3ce8b: function AS_Form_b3984f45ac7d444a898b7264c7e3ce8b(eventobject) {
        var self = this;
        return self.setPreshowData.call(this);
    }
});