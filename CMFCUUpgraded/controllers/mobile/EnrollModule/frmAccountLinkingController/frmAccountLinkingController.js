define({
  prevSelIdx : -1,
  clickedStatus : [],
  init: function() {
    var navManager = applicationManager.getNavigationManager();
    var currentForm = navManager.getCurrentForm();
    applicationManager.getPresentationFormUtility().initCommonActions(this, "YES", currentForm);
  },
  frmAccLinkPreshow: function() {
    applicationManager.getPresentationUtility().dismissLoadingScreen();
    this.setPreshowData();
    var navManager = applicationManager.getNavigationManager();
    var currentForm = navManager.getCurrentForm();
    applicationManager.getPresentationFormUtility().logFormName(currentForm);
    //     if(kony.os.deviceInfo().name !== "iPhone"){
    //       //this.view.flxHeader.isVisible = true;
    //       this.view.flxHeader.setVisibility(true);
    //     }
    //     else{
    //       //this.view.flxHeader.isVisible = false;
    //       this.view.flxHeader.setVisibility(false);
    //     }
    this.view.flxHeader.setVisibility(true);
  },
  setPreshowData: function() {
    var scopeObj = this;
    this.view.customHeader.flxBack.isVisible = true;
    this.view.flxHeader.customHeader.lblLocateUs.text = kony.i18n.getLocalizedString("kony.mb.enroll.enroll");
    this.view.lblText.text = kony.i18n.getLocalizedString("kony.mb.enroll.linkAcc");
    this.view.lblTextdesc.text = kony.i18n.getLocalizedString("kony.mb.enroll.slctPrimAcc");
    this.view.flxHeader.customHeader.btnRight.skin = "sknLblb8dcffSSP32px";
    this.view.customHeader.flxHeader.btnRight.focusSkin="sknLblb8dcffSSP32px";
    this.view.customHeader.btnRight.text=kony.i18n.getLocalizedString("kony.mb.common.CancelSmall");
    this.view.segAccLink.rowFocusSkin="sknSegf9f9f9";
    this.view.flxAccLink.skin = "sknFlxWhiteBorder1";
    this.view.btnContinue.skin = "skinButtonDisabled";
    this.view.btnContinue.setEnabled(false);
    this.view.btnSkip.setVisibility(true);
    this.view.btnSkip.skin = "sknBtn0095e426pxEnabled";
    this.view.btnSkip.focusSkin = "sknBtn0095e426pxEnabled";
    this.setAccLinks();
    this.view.segAccLink.onRowClick = this.segAccLinkOnRowClick;
    this.view.btnContinue.onClick = this.btnContinueOnClick;
    this.view.btnSkip.onClick = this.btnContinueOnClick;

    this.view.customHeader.flxBack.onClick = function() {
      scopeObj.navBack();
    };
    this.view.customHeader.btnRight.onClick = function() {
      scopeObj.onClickCancel();
    };
  },

  navBack: function() {
    var navManager = applicationManager.getNavigationManager();
    navManager.goBack();
  },

  onClickCancel: function() {
    var enrollMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("EnrollModule");
    enrollMod.presentationController.resetEnrollObj();
  },

  setAccLinks: function() {
    applicationManager.getPresentationUtility().dismissLoadingScreen();
    var navMan = applicationManager.getNavigationManager();
    var accPrimLinkData = navMan.getCustomInfo("frmAccountPrimaryLinking");
    var accountList = navMan.getCustomInfo("accountList");
    var data = [];
    var temp = {};
    var lblAccNum, img, lblAccName,lblAccNameValue, lblAccType, lblAccTypeValue,accNumHidden,shareId,loanId ;
    img = {
      src: "checkbox_unactive_light.png"
    };
    lblAccName = kony.i18n.getLocalizedString("kony.mb.enroll.primary");
    lblAccType = kony.i18n.getLocalizedString("kony.mb.enroll.accType");
    var primLinkAccNum = accPrimLinkData[0].accNumHidden;
    for (var i = 0; i < accountList.length; i++) {
      if(accountList[i].accountNumber!==primLinkAccNum){
        if(accountList[i].shareId!==null && accountList[i].shareId!==""&&typeof accountList[i].shareId!=="undefined")
          shareId = accountList[i].shareId;
        if(accountList[i].loanId!==null && accountList[i].loanId!==""&&typeof accountList[i].loanId!=="undefined")
          loanId = accountList[i].loanId;
        accNumHidden = accountList[i].accountNumber;
        lblAccNum = accountList[i].accountNumber.replace(/\d(?=\d{4})/g, "X");
        lblAccNameValue = accountList[i].name;
        lblAccTypeValue = this.accTypeValue(accountList[i].accountType);
        temp = {
          "lblAccNum": lblAccNum,
          "imgIndicator": img,
          "lblAccName": lblAccName,
          "lblAccNameValue": lblAccNameValue,
          "lblAccType": lblAccType,
          "lblAccTypeValue": lblAccTypeValue,
          "accNumHidden":accNumHidden,
          "shareId":shareId,
          "loanId":loanId
        };
        data.push(temp);
        this.view.segAccLink.setData(data);
      }
    }
    var segItems = this.view.segAccLink.data;
    for (i = 0; i < segItems.length; i++)
      this.clickedStatus[i] = false;
  },

  accTypeValue : function(response){
    var typeValue;
    if(response!==null && response!=="" && typeof response!=="undefined"){
      if(response === "0" || response ==="2"||response==="5"||response==="7"||response==="8")
        typeValue = "Personal";
      else if(response === "10")
        typeValue = "Business";
      else if(response === "11")
        typeValue = "Personal Agency";
      else if(response === "12")
        typeValue = "Irrevocable Trust";
      else if(response === "13")
        typeValue = "Revocable Trust";
      else if(response === "20")
        typeValue = "Minor";
      else if(response === "21")
        typeValue = "NC UTMA";
      else
        typeValue = "Other";
    }
    return typeValue;
  },

  segAccLinkOnRowClick: function() {
    var navManager = applicationManager.getNavigationManager();
    var i,selItems,selIndex,imgActive;
    var data = [];
    selItems = this.view.segAccLink.selectedRowItems[0];
    selIndex = this.view.segAccLink.selectedRowIndex[1];
    if (this.clickedStatus[selIndex] === true) {
      var count = 0;
	  this.clickedStatus[selIndex] = false;
      imgActive = {
        src: "checkbox_unactive_light.png"
      };
      selItems.imgIndicator = imgActive;
      this.view.segAccLink.setDataAt(selItems, selIndex);
      var segItems = this.view.segAccLink.data;
      for (i = 0; i < segItems.length; i++) {
        if (segItems[i].imgIndicator.src === "checkbox_unactive_light.png") {
          count++;
        }
      }
      if (count === segItems.length) {
        this.view.btnContinue.setEnabled(false);
        this.view.btnContinue.skin = "skinButtonDisabled";
      } else {
        this.view.btnContinue.skin = "sknBtn0095e426pxEnabled";
        this.view.btnContinue.focusSkin = "sknBtn0095e426pxEnabled";
        this.view.btnContinue.setEnabled(true);
      }
      for(i=0;i<segItems.length;i++){
        if(segItems[i].imgIndicator.src === "checkbox_active.png"){
          data.push(segItems[i]);
          navManager.setCustomInfo("frmAccountLinking",data);
        }
      }
    } else {
	  this.clickedStatus[selIndex] = true;
      imgActive = {
        src: "checkbox_active.png"
      };
      selItems.imgIndicator = imgActive;
      this.view.segAccLink.setDataAt(selItems, selIndex);
      var slctdData = this.view.segAccLink.data;
      for(i=0;i<slctdData.length;i++){
        if(slctdData[i].imgIndicator.src === "checkbox_active.png"){
          data.push(slctdData[i]);
          navManager.setCustomInfo("frmAccountLinking",data);
        }
      }
      this.view.btnContinue.skin = "sknBtn0095e426pxEnabled";
      this.view.btnContinue.focusSkin = "sknBtn0095e426pxEnabled";
      this.view.btnContinue.setEnabled(true);
    }
  },

  btnContinueOnClick : function(){
    applicationManager.getPresentationUtility().showLoadingScreen();
    var enrollMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("EnrollModule");
    enrollMod.presentationController.commonFunctionForNavigation("frmEnrollSignUp");
  }
});