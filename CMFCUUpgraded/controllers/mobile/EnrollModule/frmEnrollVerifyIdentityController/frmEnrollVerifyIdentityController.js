define({
  timerCounter:0,
  init : function(){
    var navManager = applicationManager.getNavigationManager();
    var currentForm=navManager.getCurrentForm();
    applicationManager.getPresentationFormUtility().initCommonActions(this,"YES",currentForm);
  },
  frmEnrollVerifyIdentityPreShow : function(){
    this.setFlowAction();
    this.setPreShowData();
    this.view.verifyIdentity.customAccountNumtext="";
    this.view.verifyIdentity.ssntext="";
    this.view.verifyIdentity.updateInputBulletsSSN(this.view.verifyIdentity.ssntext);
    this.view.verifyIdentity.dobtext="";
    this.view.verifyIdentity.updateInputBulletsDOB(this.view.verifyIdentity.dobtext);
    this.view.verifyIdentity.imgsrc="checkbox_empty.png";
    this.view.verifyIdentity.continueskin="skinButtonDisabled";
    this.view.verifyIdentity.acceptTerms=false;
    this.view.verifyIdentity.btnsetEnabled(false);
    applicationManager.getPresentationUtility().dismissLoadingScreen();
    var navManager = applicationManager.getNavigationManager();
    var currentForm=navManager.getCurrentForm();
    applicationManager.getPresentationFormUtility().logFormName(currentForm);
  },
  setFlowAction  : function(){
     var scopeObj = this;
    this.view.customHeader.flxBack.onClick = function(){
      scopeObj.navBack();
    };
    this.view.customHeader.btnRight.onClick = function(){
      scopeObj.onClickCancel();
    };
 
    this.view.verifyIdentity.setCustomNavigateTo(this.navToSecurityCheck);
  },  
  
  setPreShowData  : function(){
    var scope = this;
    this.view.customHeader.lblLocateUs.text = "Verify Your Identity";
    if(kony.os.deviceInfo().name !== "iPhone"){
      this.view.flxHeader.isVisible = true;
    }
    else{
      this.view.flxHeader.isVisible = false;
    }
   // var enrollMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("EnrollModule");
   // var userlastname = enrollMod.presentationController.getEnrollLastName();

  },
  navToSecurityCheck : function(){
    var enrollMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("EnrollModule");
    enrollMod.presentationController.commonFunctionForNavigation("frmEnrollSecurityCheck");
  },
  onClickCancel : function(){
    var enrollMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("EnrollModule");
    enrollMod.presentationController.resetEnrollObj();
    
  },
  navToDOB : function(){
    var enrollMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("EnrollModule");
    enrollMod.presentationController.commonFunctionForNavigation("frmEnrollDOB");
  },
  navBack : function(){
     var enrollPC = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("EnrollModule");
      enrollPC.presentationController.commonFunctionForNavigation("frmLogin");
   
  },

 
  /**
  *Shows Toast Message with red skin
  */
  bindViewError : function(msg)
  {
    applicationManager.getPresentationUtility().dismissLoadingScreen();
    applicationManager.getDataProcessorUtility().showToastMessageError(this,msg);
  },
});