define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for btnRight **/
    AS_Button_f545f7cf18cb40bcb0acffcde049c152: function AS_Button_f545f7cf18cb40bcb0acffcde049c152(eventobject) {
        var self = this;
        var enrollMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("EnrollModule");
        enrollMod.presentationController.resetEnrollObj();
        enrollMod.presentationController.commonFunctionForNavigation("frmLogin");
    },
    /** onClick defined for undefined **/
    AS_BarButtonItem_i27894f237264816b2432fb472285ed3: function AS_BarButtonItem_i27894f237264816b2432fb472285ed3(eventobject) {
        var self = this;
        this.navBack();
    },
    /** onClick defined for undefined **/
    AS_BarButtonItem_jba3a45783df456ca115dab716d128a6: function AS_BarButtonItem_jba3a45783df456ca115dab716d128a6(eventobject) {
        var self = this;
        this.onClickCancel();
    },
    /** init defined for frmEnrollVerifyIdentity **/
    AS_Form_ff8658d39750423baf897406102a3082: function AS_Form_ff8658d39750423baf897406102a3082(eventobject) {
        var self = this;
        this.init();
    },
    /** preShow defined for frmEnrollVerifyIdentity **/
    AS_Form_e031d3ff65ed4894b7953acf4fb53361: function AS_Form_e031d3ff65ed4894b7953acf4fb53361(eventobject) {
        var self = this;
        this.frmEnrollVerifyIdentityPreShow();
    }
});