define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for btnRight **/
    AS_Button_f545f7cf18cb40bcb0acffcde049c152: function AS_Button_f545f7cf18cb40bcb0acffcde049c152(eventobject) {
        var self = this;
        var enrollMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("EnrollModule");
        enrollMod.presentationController.resetEnrollObj();
        enrollMod.presentationController.commonFunctionForNavigation("frmLogin");
    },
    /** onEndEditing defined for txtboxEmail **/
    AS_TextField_e1ab6df807974dbf92077048536e42cd: function AS_TextField_e1ab6df807974dbf92077048536e42cd(eventobject, changedtext) {
        var self = this;
        this.verifyEmail();
    },
    /** init defined for frmEmailCapture **/
    AS_Form_e29a1039672d4473aa1428c94fd396e6: function AS_Form_e29a1039672d4473aa1428c94fd396e6(eventobject) {
        var self = this;
        this.init();
    },
    /** preShow defined for frmEmailCapture **/
    AS_Form_fee308713d344731a5bd1ed17bbd9b2c: function AS_Form_fee308713d344731a5bd1ed17bbd9b2c(eventobject) {
        var self = this;
        this.frmEmailCapturePreShow();
    },
    /** onDeviceBack defined for frmEmailCapture **/
    AS_Form_gd1019304e864142b2707c9dc7e61fba: function AS_Form_gd1019304e864142b2707c9dc7e61fba(eventobject) {
        var self = this;
        this.frmEmailCapturePreShow();
    }
});