define({ 

  //Type your controller code here 
  timerCounter:0,
  init : function(){
    var navManager = applicationManager.getNavigationManager();
    var currentForm=navManager.getCurrentForm();
    applicationManager.getPresentationFormUtility().initCommonActions(this,"YES",currentForm);
  },
  frmEmailCapturePreShow : function(){
    this.setFlowAction();
    this.setPreShowData();
    this.view.txtboxEmail.text="";
    this.view.btnContinue.skin = "skinButtonDisabled";
    this.view.btnContinue.focusSkin = "skinButtonDisabled";
    this.view.btnContinue.setEnabled(false);
    this.view.customHeader.btnRight.skin="sknLblffffffSSP30px";
    applicationManager.getPresentationUtility().dismissLoadingScreen();
    var navManager = applicationManager.getNavigationManager();
    var currentForm=navManager.getCurrentForm();
    applicationManager.getPresentationFormUtility().logFormName(currentForm);
  },
  
  setFlowAction  : function(){
    var scopeObj = this;
    this.view.customHeader.flxHeader.btnRight.skin="sknLblb8dcffSSP32px";
    this.view.customHeader.flxHeader.btnRight.focusSkin="sknLblb8dcffSSP32px";
    this.view.customHeader.flxBack.isVisible=false;
    this.view.customHeader.imgBack.isVisible=false;
    this.view.customHeader.btnRight.onClick = function(){
      scopeObj.onClickCancel();
    };
    this.view.txtboxEmail.onTextChange = this.onChangeEmail;
    this.view.btnContinue.onClick = this.onClickContinue;    
  },
  
  onClickContinue : function(){
    this.saveEmail(this.view.txtboxEmail.text);
  },
  
  saveEmail: function(email){
    var enrollModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("EnrollModule");
    enrollModule.presentationController.saveEmail(email);
  },
  
  onChangeEmail: function(){
    var email = this.view.txtboxEmail.text;

    if(this.isValidEmail(email)){    
      this.view.btnContinue.skin = "sknBtn0095e426pxEnabled";
      this.view.btnContinue.focusSkin = "sknBtn0095e426pxEnabled";
      this.view.btnContinue.setEnabled(true);
    }else{

      this.view.btnContinue.skin = "skinButtonDisabled";
      this.view.btnContinue.focusSkin = "skinButtonDisabled";
      this.view.btnContinue.setEnabled(false);
    }
  },
  
  verifyEmail: function(){
    if(!(this.isValidEmail(this.view.txtboxEmail.text))){
      this.showError();
    }
  },
  
  showError : function(){
    var controller = applicationManager.getPresentationUtility().getController('frmEmailCapture', true);
    var errorMsg = applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.OnBoarding.InvalidEmail");
    controller.bindViewError(errorMsg);

  },
  
  isValidEmail: function(email){
    return kony.string.isValidEmail(email);
  },
  
  navigatetoDevReg : function(){
    var navManager = applicationManager.getNavigationManager();
    var customInfo =  navManager.getCustomInfo("frmEnrollSignUp");
    var enteredUserName = customInfo.userName;
    var enteredPassword =customInfo.password ;
    var authMode = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
    authMode.presentationController.currentAuthMode = "password";
    authMode.presentationController.onLogin({"username":enteredUserName,"password":enteredPassword}, this);     
  },

  onClickCancel : function(){
    var enrollMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("EnrollModule");
    enrollMod.presentationController.resetEnrollObj();
  },
  
  setPreShowData  : function(){
    var scope = this;
    this.view.flxHeader.customHeader.lblLocateUs.text = kony.i18n.getLocalizedString("kony.mb.enroll.enroll");
    this.view.flxHeader.customHeader.btnRight.text="";
    this.view.flxHeader.setVisibility(true);
  },

  bindViewError : function(msg)
  {
    applicationManager.getPresentationUtility().dismissLoadingScreen();
    applicationManager.getDataProcessorUtility().showToastMessageError(this,msg);
  }

});