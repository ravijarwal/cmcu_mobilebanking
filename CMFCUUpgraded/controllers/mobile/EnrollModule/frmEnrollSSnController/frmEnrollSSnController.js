define({
  keypadString: '',
  timerCounter: 0,
  init: function() {
    var navManager = applicationManager.getNavigationManager();
    var currentForm = navManager.getCurrentForm();
    navManager.setCustomInfo("isValidUser",true);
    applicationManager.getPresentationFormUtility().initCommonActions(this, "YES", currentForm);
  },
  showEnterSSN: function() {
    this.setActions();
    var scope = this;
    var navMan = applicationManager.getNavigationManager();
    var isValidUser = navMan.getCustomInfo("isValidUser");
    if(!isValidUser){
      navMan.setCustomInfo("isValidUser",true);
      this.showErrorPopup();
    } 
    this.keypadString = '';
    this.updateInputBullets("flxInputSSN");
    this.incompleteSSNoView();
    this.updateInputBullets("flxInputSSN");
    /*if (kony.os.deviceInfo().name !== "iPhone") {
      this.view.flxHeader.isVisible = true;
      this.view.flxMainContainer.top = "56dp";
    } else {
      this.view.flxHeader.isVisible = false;
      this.view.flxMainContainer.top = "0dp";
    }*/
    this.view.flxHeader.isVisible = true;
    scope.clearSSN();
    applicationManager.getPresentationUtility().dismissLoadingScreen();
    var currentForm = navMan.getCurrentForm();
    applicationManager.getPresentationFormUtility().logFormName(currentForm);
  },
  setActions: function() {
    var scope = this;
    this.view.flxHeader.customHeader.lblLocateUs.text = kony.i18n.getLocalizedString("kony.mb.enroll.enroll");
    this.view.lblEnterSSN.skin =  "sknLbl424242SSPLight36px";
    this.view.lblCallUs.text = kony.i18n.getLocalizedString("kony.mb.Support.CallBranch");
    this.view.lblEmailUs.text = kony.i18n.getLocalizedString("kony.mb.enroll.emailUs");
    this.view.lblVisitBranch.text = kony.i18n.getLocalizedString("kony.mb.enroll.visitBranch");
    this.view.lblOk.text = kony.i18n.getLocalizedString("kony.mb.enroll.ok");
    this.view.imgSSNView.src = "viewicon.png";
    this.view.flxHeader.customHeader.btnRight.text = kony.i18n.getLocalizedString("kony.mb.common.CancelSmall");
    this.view.flxCustomPopup.setVisibility(false);
    this.view.customHeader.flxHeader.btnRight.skin="sknLblb8dcffSSP32px";
    this.view.customHeader.flxHeader.btnRight.focusSkin = "sknLblb8dcffSSP32px";
    this.view.btnVerifySSN.onClick = function() {
      scope.verifyAndNavigate();
    };
    this.view.flxCancel.onClick = function(){
      scope.onClickClosePopup();
    };
    this.view.customHeader.flxBack.onClick = function() {
      var navManager = applicationManager.getNavigationManager();
      navManager.setCustomInfo("isValidUser",true);
      navManager.setCustomInfo("SSNtext", "");
      scope.navToDOB();
    };
    this.view.customHeader.btnRight.onClick = function() {
      scope.onClickCancel();
    };
    this.view.flxView.onClick = function(){
      scope.imgSSNViewOnClick();
    };
    this.view.flxCallUs.onClick = function(){
      kony.phone.dial("7043750183");
    };
    this.view.flxEmailUs.onClick = function(){
      var cc = [""];
      var bcc = [""];
      var sub = " ";
      var msgbody = "";
      var to = ["info@cmcu.org"];
      kony.phone.openEmail(to, cc, bcc, sub, msgbody);
    };
    this.view.flxVisitBranch.onClick = function(){
      var loginCntrlr = applicationManager.getPresentationUtility().getController('frmLogin', true);
      loginCntrlr.onLocateUSClick();
    };
    this.view.flxOk.onClick = function(){
      scope.onClickClosePopup();
    };
  },
  onClickClosePopup : function(){
    var enrollMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("EnrollModule");
    enrollMod.presentationController.resetEnrollObj();
  },
  showErrorPopup : function(){
    this.view.flxCustomPopup.setVisibility(true);
  },
  //to mask and unmask the SSN
  imgSSNViewOnClick: function() {
    var navManager = applicationManager.getNavigationManager();
    var sSNtext = navManager.getCustomInfo("SSNtext");
    if(sSNtext !== "" && sSNtext !== null && typeof sSNtext !== "undefined" && sSNtext.length === 9){
      if (this.view.imgSSNView.src === "viewicon.png") {
        this.view.imgSSNView.src = "viewactive.png";
        this.view.lblSSNoDigit1.text = sSNtext.charAt(0);
        this.view.lblSSNoDigit2.text = sSNtext.charAt(1);
        this.view.lblSSNoDigit3.text = sSNtext.charAt(2);
        this.view.lblSSNoDigit4.text = sSNtext.charAt(3);
        this.view.lblSSNoDigit5.text = sSNtext.charAt(4);
        this.view.lblSSNoDigit6.text = sSNtext.charAt(5);
        this.view.lblSSNoDigit7.text = sSNtext.charAt(6);
        this.view.lblSSNoDigit8.text = sSNtext.charAt(7);
        this.view.lblSSNoDigit9.text = sSNtext.charAt(8);
        this.view.flxInputSSN.forceLayout();
      } else {
        this.view.imgSSNView.src = "viewicon.png";
        this.view.lblSSNoDigit1.text = "•";
        this.view.lblSSNoDigit2.text = "•";
        this.view.lblSSNoDigit3.text = "•";
        this.view.lblSSNoDigit4.text = "•";
        this.view.lblSSNoDigit5.text = "•";
        this.view.lblSSNoDigit6.text = "•";
        this.view.lblSSNoDigit7.text = "•";
        this.view.lblSSNoDigit8.text = "•";
        this.view.lblSSNoDigit9.text = "•";
        this.view.flxInputSSN.forceLayout();
      }
    }else{
      this.view.imgSSNView.src = "viewicon.png";
      this.view.flxInputSSN.forceLayout();
    }
  },
  /**
     * Code to verify the SSN is valid or not 
     */
  verifyAndNavigate: function() {
    var scope = this;
    var temp = scope.keypadString;
    SSN = temp.replace(/[-() ]/g, "");
    if (SSN === null || SSN.length === 0) {
      scope.bindViewError(applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.enroll.enterSSN"));
    } else {
      if (this.isValidSSN(SSN)) {
        var enrollModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("EnrollModule");
        //var params = {
        // "dateOfBirth":enrollModule.presentationController.getEnrollDOB(),
        // "ssn":SSN,
        // "userlastname":enrollModule.presentationController.getEnrollLastName()
        //  };
        // enrollModule.presentationController.checkUserEnrolled(params);
        var navManager = applicationManager.getNavigationManager();
        navManager.setCustomInfo("dateOfBirth", "");
        navManager.setCustomInfo("SSNtext", "");
        enrollModule.presentationController.verifySsnEnrolled(SSN,true);
      } else {
        scope.bindViewError(applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.enroll.validSsn"));
      }
    }
  },
  //method to check whether SSN is valid or not 
  isValidSSN: function(SSN) {
    var validssn = true;
    var invalidSsn = ["000000000", "111111111", "222222222", "333333333", "444444444", "555555555", "666666666", "777777777", "888888888", "999999999", "123456789", "987654320", "987654321", "987654322", "987654323", "987654324", "987654325", "987654326", "987654327", "987654328", "987654329"];
    for (var i = 0; i < invalidSsn.length; i++) {
      if (SSN == invalidSsn[i]) {
        validssn = false;
        break;
      }
    }
    if (validssn) return true;
    else return false;
  },
  userNotEnrolled: function() {
    var scope = this;
    var temp = scope.keypadString;
    var SSN = temp.replace(/[-() ]/g, "");
    var enrollModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("EnrollModule");
    enrollModule.presentationController.validateEnrollSSN(SSN);
  },
  navToSecurityCheck: function() {
    var  enrollMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("EnrollModule");
    enrollMod.presentationController.commonFunctionForNavigation("frmEnrollSecurityCheck");
  },
  navToDOB: function() {
    var navManager = applicationManager.getNavigationManager();
    navManager.goBack();
  },

  onClickCancel: function() {
   /* kony.ui.Alert({
      "alertType": constants.ALERT_TYPE_CONFIRMATION,
      "alertTitle": null,
      "yesLabel": kony.i18n.getLocalizedString("kony.mb.enroll.dontCancel"),
      "noLabel": kony.i18n.getLocalizedString("kony.mb.common.AlertYes"),
      "message": kony.i18n.getLocalizedString("kony.mb.enroll.cancelPopup"),
      "alertHandler": this.alertCallback
    }, {
      "iconPosition": constants.ALERT_CONTENT_ALIGN_CENTER,
      "contentAlignment": constants.ALERT_ICON_POSITION_LEFT
    });*/
    var i18n = applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.enroll.cancelPopup");
    kony.ui.Alert(i18n, this.alertCallback, constants.ALERT_TYPE_CONFIRMATION, "Yes", "No", "");
  },
  alertCallback: function(response) {
    if (response === true) {
      var  enrollMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("EnrollModule");
      enrollMod.presentationController.resetEnrollObj();
    }
  },
  navToAlreadyEnrolled: function() {
    var  enrollMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("EnrollModule");
    enrollMod.presentationController.commonFunctionForNavigation("frmAlreadyEnrolled");
  },
  setKeypadChar: function(char) {
    this.keypadString = this.keypadString + char;
    if (this.keypadString.length === 12) {
      var navManager = applicationManager.getNavigationManager();
      var SSN = this.keypadString.replace(/[-() ]/g, "");
      navManager.setCustomInfo("SSNtext", SSN);
      this.enterSSNPostAction();
    } else if (this.keypadString.length < 12) {
      this.incompleteSSNoView();
    } else if (this.keypadString.length > 12) {
      this.keypadString = this.keypadString.slice(0, 12);
      return;
    }
    this.updateInputBullets("flxInputSSN");
  },
  clearKeypadChar: function() {
    var navManager = applicationManager.getNavigationManager();
    navManager.setCustomInfo("SSNtext", "");
    this.keypadString = this.keypadString.replace(/[) ]/g, "");
    if (this.keypadString.length === 1) {
      this.keypadString = '';
      this.updateInputBullets("flxInputSSN");
    }
    if (this.keypadString.length !== 0) {
      if (this.keypadString[this.keypadString.length-1] == '-') {
        this.keypadString = this.keypadString.substr(0, this.keypadString.length - 1);
      }
      this.keypadString = this.keypadString.substr(0, this.keypadString.length - 1);
      if (this.keypadString.length < 11) {
        this.incompleteSSNoView();
      }
      this.updateInputBullets("flxInputSSN");
    }
  },
  updateInputBullets: function(inputFlx) {
    var dummyString = '(___-__-____)';
    if (this.keypadString.length === 0) {
      this.keypadString = this.keypadString + '(';
    }
    else if (this.keypadString.length === 12) {
      this.keypadString = this.keypadString + ')';
    }
    else if (this.keypadString.length === 4 || this.keypadString.length === 7) {
      this.keypadString = this.keypadString + '-';
    }
    var widgets = this.view[inputFlx].widgets();
    for (var i = 0; i < this.keypadString.length; i++) {
      if (this.keypadString[i] === '-') {
        widgets[i].text = this.keypadString[i];
      } 
      else if(this.keypadString[i] === '('){
        widgets[i].text = this.keypadString[i];
      }
      else if(this.keypadString[i] === ')'){
        widgets[i].text = this.keypadString[i];
      }else {
        widgets[i].text = "•";
      }
    }
    for (var j = this.keypadString.length; j < widgets.length; j++) {
      //widgets[i].skin = "sknLble3e3e3SSP60px";
      widgets[j].text = dummyString[j];
      this.view.imgSSNView.src = "viewicon.png";
    }
    this.view.forceLayout();
  },
  enterSSNPostAction: function() {
    this.view.btnVerifySSN.setEnabled(true);
    this.view.btnVerifySSN.skin = "sknBtn0095e426pxEnabled";
    this.view.btnVerifySSN.focusSkin = "sknBtn0095e426pxEnabled";
    this.view.flxMainContainer.forceLayout();
  },
  incompleteSSNoView: function() {
    this.view.btnVerifySSN.skin = "sknBtnOnBoardingInactive";
    this.view.flxMainContainer.forceLayout();
    this.view.btnVerifySSN.setEnabled(false);
  },
  /*
     *Code to show error message
     */
  bindViewError: function(msg) {
    var scope = this;
    // applicationManager.getDataProcessorUtility().showToastMessageError(scope, msg);
    this.showToastMessageError(msg);
  },
  showToastMessageError : function(msg){
    this.showToastMessage("sknFlxf54b5e", "errormessage.png",msg);
  },
  showToastMessage : function(skin,img,msg){
    var scope = this;
    this.view.flxMainContainer.top = "60dp";
    this.view.flxPopup.isVisible = true;
    if (this.timerCounter === undefined || this.timerCounter === null)
      this.timerCounter = 0;
    this.timerCounter=parseInt(this.timerCounter)+1;
    var timerId="timerPopupSuccess"+this.timerCounter;
    this.view.flxPopup.skin = "" + skin;
    this.view.customPopup.imgPopup.src = "" + img;
    this.view.customPopup.lblPopup.text = msg;
    try{
      kony.print(timerId);
      kony.timer.schedule(timerId, function() {
        scope.view.flxPopup.setVisibility(false);
        scope.view.flxPopup.isVisible = false;
        scope.view.flxMainContainer.top = "56dp";
      }, 3, false);
    }
    catch(e)
    {
      kony.print(timerId);
      kony.print(e);
    }
  },
  clearSSN: function() {
    var widgets = this.view["flxInputSSN"].widgets();
    for (var i = 0; i < 13; i++) {
      if (i === 0) {
        widgets[i].text = '(';
      }
      else if (i == 12) {
        widgets[i].text = ')';
      }
      else if (i == 4 || i == 7) {
        widgets[i].text = '-';
      } else {
        widgets[i].text = '_';
      }
    }
    this.view.forceLayout();
  }
});