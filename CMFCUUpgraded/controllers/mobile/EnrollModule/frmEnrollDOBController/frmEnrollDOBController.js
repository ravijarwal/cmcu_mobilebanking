define({
  timerCounter: 0,
  keypadString: '',
  locale : kony.i18n.getCurrentLocale(),
 // locale : "sv",
  init : function(){
    var FormValidator = require("FormValidatorManager")
	this.fv = new FormValidator(1);
    var navManager = applicationManager.getNavigationManager();
    var currentForm = navManager.getCurrentForm();
    applicationManager.getPresentationFormUtility().initCommonActions(this, "YES", currentForm);
  },
  preShow: function() {
    this.view.flxHeaderPersonalInfo.customHeaderPersonalInfo.lblLocateUs.text = kony.i18n.getLocalizedString("kony.mb.enroll.enroll");
	this.locale = kony.i18n.getCurrentLocale();
    this.setDummyText();
	var dateOfBirthInLocaleFormat = "";
    this.view.flxHeaderPersonalInfo.customHeaderPersonalInfo.flxHeader.btnRight.text = kony.i18n.getLocalizedString("kony.mb.common.CancelSmall");
    var  enrollMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("EnrollModule");
    var dateOfBirth = enrollMod.presentationController.getEnrollDOB();
    if (dateOfBirth !== null && dateOfBirth !== "" && dateOfBirth !== undefined) {
      this.view.btnVerifyDOB.skin = "sknBtn0095e426pxEnabled";
      this.view.btnVerifyDOB.focusSkin = "sknBtn0095e426pxEnabled";
      this.view.btnVerifyDOB.setEnabled(true);
      this.keypadString = '';
      this.updateInputBullets();
    } else {
      this.view.btnVerifyDOB.skin = "sknBtnOnBoardingInactive";
      this.view.btnVerifyDOB.setEnabled(false);
      this.keypadString = '';
      this.updateInputBullets();
    }
    this.view.btnVerifyDOB.skin = "sknBtnOnBoardingInactive";
    this.view.btnVerifyDOB.setEnabled(false);
    this.setFlowActions();
    /*if(kony.os.deviceInfo().name !== "iPhone"){
          this.view.flxHeaderPersonalInfo.isVisible = true;
        }
        else{
          this.view.flxHeaderPersonalInfo.isVisible = false;
        }*/
    this.view.flxHeaderPersonalInfo.isVisible = true;
    this.fv.submissionView(this.view.btnVerifyDOB);
    this.fv.checkDOBLength(this.keypadString);
    applicationManager.getPresentationUtility().dismissLoadingScreen();
    var navManager = applicationManager.getNavigationManager();
    var currentForm = navManager.getCurrentForm();
    applicationManager.getPresentationFormUtility().logFormName(currentForm);
  },
   setDummyText : function(){
    var configManager = applicationManager.getConfigurationManager();
    var dummy = configManager.getCalendarDateFormat();
    var widgets = this.view["flxDOB"].widgets();
    for (var i = 0; i < this.keypadString.length; i++) {
      widgets[i].skin = "sknLbl979797SSP60px";
      widgets[i].text = dummy[i];
    }
  },
  setFlowActions : function(){
    var scope = this;
    this.view.imgDOBView.src = "viewicon.png";
    this.view.lblEnterPhoneNumberHeader.skin = "sknLbl424242SSPLight36px";
    this.view.customHeaderPersonalInfo.btnRight.skin = "sknLblb8dcffSSP32px";
    this.view.customHeaderPersonalInfo.btnRight.focusSkin = "sknLblb8dcffSSP32px";
    
    this.view.btnVerifyDOB.onClick = function() {
      scope.validateDOB();
    };
    this.view.customHeaderPersonalInfo.flxBack.onClick = function() {
      scope.navToLastName();
    };
    this.view.customHeaderPersonalInfo.btnRight.onClick = function() {
      scope.onClickCancel();
    };
    this.view.flxView.onClick = function() {
      scope.imgDOBViewOnClick();
    };
  },
  //to view the masked DOB 
  imgDOBViewOnClick: function() {
    var navManager = applicationManager.getNavigationManager();
    var dateOfBirth = navManager.getCustomInfo("dateOfBirth");
    if (dateOfBirth !== "" && dateOfBirth !== null && typeof dateOfBirth !== "undefined" && dateOfBirth.length === 10) {
      if (this.view.imgDOBView.src === "viewicon.png") {
        this.view.imgDOBView.src = "viewactive.png";
        this.view.lblMonthOne.text = dateOfBirth.charAt(0);
        this.view.lblMonthTwo.text = dateOfBirth.charAt(1);
        this.view.lblDayOne.text = dateOfBirth.charAt(3);
        this.view.lblDayTwo.text = dateOfBirth.charAt(4);
        this.view.lblYearOne.text = dateOfBirth.charAt(6);
        this.view.lblYearTwo.text = dateOfBirth.charAt(7);
        this.view.lblYearThree.text = dateOfBirth.charAt(8);
        this.view.lblYearFour.text = dateOfBirth.charAt(9);
        this.view.flxDOB.forceLayout();
      } else {
        this.view.imgDOBView.src = "viewicon.png";
        this.view.lblMonthOne.text = "•";
        this.view.lblMonthTwo.text = "•";
        this.view.lblDayOne.text = "•";
        this.view.lblDayTwo.text = "•";
        this.view.lblYearOne.text = "•";
        this.view.lblYearTwo.text = "•";
        this.view.lblYearThree.text = "•";
        this.view.lblYearFour.text = "•";
        this.view.flxDOB.forceLayout();
      }
    } else {
      this.view.imgDOBView.src = "viewicon.png";
      this.view.flxDOB.forceLayout();
    }
  },
  navToSSN: function() {
    var  enrollMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("EnrollModule");
    enrollMod.presentationController.commonFunctionForNavigation("frmEnrollSSn");
  },
  navToLastName: function() {
    var navManager = applicationManager.getNavigationManager();
    navManager.setCustomInfo("dateOfBirth", "");
    navManager.setCustomInfo("SSNtext", "");
    navManager.goBack();
  },
  onClickCancel: function() {
    /* kony.ui.Alert({
      "alertType": constants.ALERT_TYPE_CONFIRMATION,
      "alertTitle": null,
      "yesLabel": kony.i18n.getLocalizedString("kony.mb.enroll.dontCancel"),
      "noLabel": kony.i18n.getLocalizedString("kony.mb.common.AlertYes"),
      "message": kony.i18n.getLocalizedString("kony.mb.enroll.cancelPopup"),
      "alertHandler": this.alertCallback
    }, {
      "iconPosition": constants.ALERT_CONTENT_ALIGN_CENTER,
      "contentAlignment": constants.ALERT_ICON_POSITION_LEFT
    });*/
    var i18n = applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.enroll.cancelPopup");
    kony.ui.Alert(i18n, this.alertCallback, constants.ALERT_TYPE_CONFIRMATION, "Yes", "No", "");
  },
  alertCallback: function(response) {
    if (response === true) {
      var  enrollMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("EnrollModule");
      enrollMod.presentationController.resetEnrollObj();
    }
  },
  setKeypadChar: function(char) {
    if (this.keypadString.length === 10) return;
    this.keypadString = this.keypadString + char;
    if (this.keypadString.length === 2 || this.keypadString.length === 5) {
      this.keypadString = this.keypadString + '/';
    }
    this.updateInputBullets();
    this.fv.checkDOBLength(this.keypadString);
  },
  clearKeypadChar: function() {
    var navManager = applicationManager.getNavigationManager();
    navManager.setCustomInfo("dateOfBirth", "");
    if (this.keypadString.length === 1) {
      this.keypadString = '';
      this.updateInputBullets();
    }
    if (this.keypadString.length !== 0) {
      if (this.keypadString[this.keypadString.length - 1] === '/') {
        this.keypadString = this.keypadString.substr(0, this.keypadString.length - 1);
      }
      this.keypadString = this.keypadString.substr(0, this.keypadString.length - 1);
      this.updateInputBullets();
    }
    this.fv.checkDOBLength(this.keypadString);
  },
  updateInputBullets: function() {
    var scope = this;
    var dummyString = 'MM/DD/YYYY';
    var widgets = this.view["flxDOB"].widgets();
    for (var i = 0; i < this.keypadString.length; i++) {
      var navManager = applicationManager.getNavigationManager();
      navManager.setCustomInfo("dateOfBirth", this.keypadString);
      widgets[i].skin = "sknLbl979797SSP60px";
      if(i===2 || i===5){
        widgets[i].text = "/";
      }else{
        widgets[i].text = "•"; 
      }           
    }
    for (var j = this.keypadString.length; j < widgets.length; j++) {
      widgets[j].skin = "sknLble3e3e3SSP60px";
      widgets[j].text = dummyString[j];
      this.view.imgDOBView.src = "viewicon.png";
    }
    this.view.forceLayout();
  },
  validateAndNavigate: function() {
    var date = this.keypadString;
    if (date.length === 10) {
      var  NUOMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("NewUserModule");
      NUOMod.presentationController.validateDOBAndNavigate(date);
    } else {
      this.bindViewError(applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.common.validDOB"));
    }
  },
  assignDataToForm: function(newUserJSON) {
    var scope = this;
    var dob = (newUserJSON.dateOfBirth && newUserJSON.dateOfBirth !== "" && newUserJSON.dateOfBirth !== null) ? newUserJSON.dateOfBirth : "";
    if (dob !== "") {
      dob = dob.substr(0, 10);
      dob = dob.split("-");
      var dobText = dob[1] + dob[2] + dob[0];
      for (var i = 0; i < dobText.length; i++) {
        this.setKeypadChar(dobText.charAt(i));
      }
    } else {
      this.keypadString = "";
      this.updateInputBullets();
    }
    this.view.forceLayout();
  },
  //Development
  /**
     * validates Date of Birth
     */
  validateDOB: function() {
    var dob = this.keypadString;
    if (dob.length < 10) {
      this.bindViewError(applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.common.validDOB"));
    } else {
      var  enrollMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("EnrollModule");
      var navManager = applicationManager.getNavigationManager();
      navManager.setCustomInfo("isValidUser",true);
      navManager.setCustomInfo("dateOfBirth", "");
      navManager.setCustomInfo("SSNtext", "");
      enrollMod.presentationController.validateDOB(dob);
    }
  },
  /**
     * Shows Toast Message with red skin
     */
  bindViewError: function(msg) {
    applicationManager.getPresentationUtility().dismissLoadingScreen();
    //applicationManager.getDataProcessorUtility().showToastMessageError(this, msg);
    this.showToastMessageError(msg);
  },
  showToastMessageError : function(msg){
    this.showToastMessage("sknFlxf54b5e", "errormessage.png",msg);
  },
  showToastMessage : function(skin,img,msg){
    var scope = this;
    this.view.flxMainContainer.top = "60dp";
    this.view.flxPopup.isVisible = true;
    if (this.timerCounter === undefined || this.timerCounter === null)
      this.timerCounter = 0;
    this.timerCounter=parseInt(this.timerCounter)+1;
    var timerId="timerPopupSuccess"+this.timerCounter;
    this.view.flxPopup.skin = "" + skin;
    this.view.customPopup.imgPopup.src = "" + img;
    this.view.customPopup.lblPopup.text = msg;
    try{
      kony.print(timerId);
      kony.timer.schedule(timerId, function() {
        scope.view.flxPopup.setVisibility(false);
        scope.view.flxPopup.isVisible = false;
        scope.view.flxMainContainer.top = "56dp";
      }, 3, false);
    }
    catch(e)
    {
      kony.print(timerId);
      kony.print(e);
    }
  },
});