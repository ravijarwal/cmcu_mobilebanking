define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** init defined for frmAccountPrimaryLinking **/
    AS_Form_h38f7a3499db4f9a9bb84d34f4c0a6f4: function AS_Form_h38f7a3499db4f9a9bb84d34f4c0a6f4(eventobject) {
        var self = this;
        return self.init.call(this);
    },
    /** preShow defined for frmAccountPrimaryLinking **/
    AS_Form_a8dc647dc0bb430ca00aeda24f539b4a: function AS_Form_a8dc647dc0bb430ca00aeda24f539b4a(eventobject) {
        var self = this;
        return self.frmAccPrimaryLinkPreshow.call(this);
    }
});