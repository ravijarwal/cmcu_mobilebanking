define({ 
  onNavigate: function (obj) {

  },
  init : function(){
    var navManager = applicationManager.getNavigationManager();
    var currentForm=navManager.getCurrentForm();
    applicationManager.getPresentationFormUtility().initCommonActions(this,"YES",currentForm);
 },
  preShow: function () {
    this.view.flxPopup.setVisibility(false);
    this.initActions();
    this.bindData();
    this.setUserName();
    this.renderTitleBar();
    applicationManager.getPresentationUtility().dismissLoadingScreen();
    var navManager = applicationManager.getNavigationManager();
    var currentForm=navManager.getCurrentForm();
    applicationManager.getPresentationFormUtility().logFormName(currentForm);
  },
  renderTitleBar :function(){
    var deviceUtilManager = applicationManager.getDeviceUtilManager();
    var isIphone = deviceUtilManager.isIPhone();
//     if(!isIphone){
//       this.view.flxHeader.isVisible = true;
//     }
//     else{
//       this.view.flxHeader.isVisible = false;
//     }
    this.view.flxHeader.isVisible = true;
  },
  bindData : function(){
    this.view.customHeader.lblLocateUs.text = kony.i18n.getLocalizedString("kony.mb.cantLogin.cantLogin");
    this.view.customHeader.btnRight.text = kony.i18n.getLocalizedString("kony.mb.common.CancelSmall");
    this.view.customHeader.btnRight.skin = "sknRtx424242SSP26px";
    this.view.customHeader.btnRight.focusSkin = "sknRtx424242SSP26px";
    this.view.customHeader.lblLocateUs.skin = "sknRtx424242SSP26px";
    this.view.customHeader.flxBack.setVisibility(false);
  },
  initActions: function(){
    this.view.customHeader.btnRight.onClick = this.onCancel;
    this.view.customHeader.flxBack.onClick = this.goBack;
    this.view.flxSelectCVV.onClick = this.loginWithUsername;
    this.view.flxSelectSecurityCode.onClick = this.navigateToResetPassword; 
  },
  setUserName:function()
  {
    var navManager = applicationManager.getNavigationManager();
    var userData = navManager.getCustomInfo("accPrimLinkData");
    var userName = userData[0].lblAccNameValue;
    //this.view.rtxForgotInfo.text=applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.forgot.wefoundyouwith")+"</br> "+applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.common.username")+" "+userName+"</b>";
    //this.view.rtxSelectCVV.text=applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.forgot.loginas")+" "+userName;
	this.view.rtxSelectCVV.text = "Login as "+userName;
  },
  goBack:function()
  {
    var navManager = applicationManager.getNavigationManager();
    navManager.goBack();
  },
    onCancel : function()
  {
    var navManager = applicationManager.getNavigationManager();
    navManager.clearStack();
    var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
      authModule.presentationController.navigateToLogin();
  },
  loginWithUsername : function(){
    var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
    var navManager = applicationManager.getNavigationManager();
    var userData = navManager.getCustomInfo("accPrimLinkData");
    var userName = userData[0].lblAccNameValue;
    authModule.presentationController.navigateToLogin(userName);    
  },
  navigateToResetPassword : function(){ 
    var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
    //authModule.presentationController.commonFunctionForNavigation("frmForgotSelectMethod");
    authModule.presentationController.commonFunctionForNavigation("frmForgotCreatePassword");
  }
  
  
});