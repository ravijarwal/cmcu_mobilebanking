define({
  timerCounter: 0,
  onNavigate: function (obj) {

  },
 init : function(){
    var navManager = applicationManager.getNavigationManager();
    var currentForm=navManager.getCurrentForm();
    applicationManager.getPresentationFormUtility().initCommonActions(this,"YES",currentForm);
 },
  preShow: function () {
    this.view.flxPopup.setVisibility(false);
    this.setUserName();
    this.initActions();
    this.renderTitleBar();
    this.handleData();
    applicationManager.getPresentationUtility().dismissLoadingScreen();
    var navManager = applicationManager.getNavigationManager();
    var currentForm=navManager.getCurrentForm();
    applicationManager.getPresentationFormUtility().logFormName(currentForm);
  },
  renderTitleBar :function(){
    var deviceUtilManager = applicationManager.getDeviceUtilManager();
    var isIphone = deviceUtilManager.isIPhone();
//     if(!isIphone){
//       this.view.flxHeader.isVisible = true;
//     }
//     else{
//       this.view.flxHeader.isVisible = false;
//     }
    this.view.flxHeader.isVisible = true;
  },
  initActions: function(){
    var scopeObj = this;
    this.view.flxSeparator.setVisibility(false);
    this.view.customHeader.flxBack.onClick = function(){
      scopeObj.navBack();
    };
    this.view.customHeader.btnRight.onClick = scopeObj.onCancel;
    //     this.view.flxCVV.onClick = function(){
    //       scopeObj.getAllCards();
    //       //       scopeObj.navToCVV();
    //     };
    //     this.view.flxSecurityCode.onClick = function(){
    //       scopeObj.triggerOTP();
    //     };
    // this.view.SecurityCheckOptions.onClickCVV=function(){
    //     scopeObj.getAllCards();
    //    scopeObj.navToCVV();
    // };
    this.view.SecurityCheckOptions.onClickEmailSetData();
    this.view.SecurityCheckOptions.onRowClicktextMessage=function(){
      scopeObj.SelectedMobileText();
      destination=textNumber;
      type="t";
      scopeObj.sendOTP(destination,type);
    };
    this.view.SecurityCheckOptions.onRowClickPhoneCall=function(){
      scopeObj.selectedMobileNum();
      destination=mobNumber;
      type="v";
      scopeObj.sendOTP(destination,type);
    };
    this.view.SecurityCheckOptions.onRowClickEmailCode=function(){
      scopeObj.selectedEmail();
      destination=email;
      type="m";
      scopeObj.sendOTP(destination,type);
    };
  },
  handleData : function(){
    this.view.customHeader.lblLocateUs.text = kony.i18n.getLocalizedString("kony.mb.enroll.securityMethod");
    this.view.customHeader.btnRight.skin = "sknLblGrey98";
    this.view.customHeader.btnRight.isVisible=true;
    this.view.customHeader.flxHeader.btnRight.text=kony.i18n.getLocalizedString("kony.mb.common.CancelSmall");
    this.view.customHeader.flxHeader.btnRight.skin="sknLblb8dcffSSP32px";
    this.view.customHeader.flxHeader.btnRight.focusSkin="sknLblb8dcffSSP32px";
    this.view.customHeader.flxBack.isVisible = false;
    this.view.lblSecurityCheck.text =kony.i18n.getLocalizedString("kony.mb.EnrollSecurityCheck.SecurityCheck");
    this.view.lblDescription.text = kony.i18n.getLocalizedString("kony.mb.enroll.verification");
  },
  onSelectCVV : function(){
    applicationManager.getPresentationUtility().showLoadingScreen();
    var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
    authModule.presentationController.navigateToCVV();
  },
  onSelectSecurityCode : function() {
    applicationManager.getPresentationUtility().showLoadingScreen();
    var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
    authModule.presentationController.requestOTP();
  },
  goBack:function()
  {
    var navManager = applicationManager.getNavigationManager();
    navManager.goBack();
  },

   onCancel : function()
  {
    var navManager = applicationManager.getNavigationManager();
    navManager.clearStack();
   var i18n = applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.enroll.cancelPopup");
    kony.ui.Alert(i18n, this.alertCB, constants.ALERT_TYPE_CONFIRMATION, "Yes", "No", "");
  },
 
  alertCB: function(response){
    if(response){
      var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
      authModule.presentationController.navigateToLogin();
    }
  },
  sendOTP : function(destination,type){
    // type="m";
    var newUserManager = applicationManager.getNewUserBusinessManager();
    newUserManager.setEnrollAttribute("destination",destination);
    newUserManager.setEnrollAttribute("type",type);
    applicationManager.getPresentationUtility().showLoadingScreen();
    var enrollModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("EnrollModule");
    //enrollModule.presentationController.sendOTP();
    enrollModule.presentationController.invokeIdentity();
  },
  selectedEmail: function(){
    email= this.view.SecurityCheckOptions.selectedEmailRowIndex();
  },
  selectedMobileNum: function(){
    mobNumber= this.view.SecurityCheckOptions.selectedMobileNumRowIndex();
  },
  SelectedMobileText: function(){
    textNumber=this.view.SecurityCheckOptions.selectedMobileForTextIndex();
  },
  setUserName:function()
  {
    var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
    var forgotObj = authModule.presentationController.getForgotObjectForView();
    var userName = forgotObj.UserName;
    this.view.lblUserName.text="Hi! "+userName;
  },
   bindGenericError: function (errorMsg) {
        applicationManager.getPresentationUtility().dismissLoadingScreen();
        var scopeObj = this;
        applicationManager.getDataProcessorUtility().showToastMessageError(scopeObj, errorMsg);
  },
  doNothing : function(){
    
  }

});