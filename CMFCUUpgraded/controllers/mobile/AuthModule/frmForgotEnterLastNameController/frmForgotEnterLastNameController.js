define({
  timerCounter: 0,
  init : function(){
    var navManager = applicationManager.getNavigationManager();
    var currentForm=navManager.getCurrentForm();
    applicationManager.getPresentationFormUtility().initCommonActions(this,"YES",currentForm);
 },
  preShow: function () {
    this.view.txtFirstName.setFocus(true);
    this.view.txtLastName.setFocus(false);
    this.view.flxPopup.setVisibility(false);
    this.initActions();
    this.renderTitleBar();
    this.handleData();
    applicationManager.getPresentationUtility().dismissLoadingScreen();
    var navManager = applicationManager.getNavigationManager();
    var currentForm=navManager.getCurrentForm();
    applicationManager.getPresentationFormUtility().logFormName(currentForm);
  },
  renderTitleBar :function(){
    var deviceUtilManager = applicationManager.getDeviceUtilManager();
    var isIphone = deviceUtilManager.isIPhone();
    /*if(!isIphone){
      this.view.flxHeader.isVisible = true;
    }
    else{
      this.view.flxHeader.isVisible = false;
    }*/
	this.view.flxHeader.isVisible = true;
  },
  handleData : function(){
    var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
	var forgotObj = authModule.presentationController.getForgotObjectForView();
	this.view.customHeader.btnRight.text = kony.i18n.getLocalizedString("kony.mb.common.CancelSmall");
    this.view.customHeader.btnRight.skin = "sknRtx424242SSP26px";
    this.view.customHeader.lblLocateUs.skin = "sknRtx424242SSP26px";
    this.view.customHeader.lblLocateUs.text = kony.i18n.getLocalizedString("kony.mb.cantLogin.cantLogin");
    this.view.txtLastName.text = "";
    this.view.txtFirstName.text = "";
    this.view.btnUpdatePassword.skin = "sknBtnOnBoardingInactive";
    this.view.btnUpdatePassword.setEnabled(false);
  },
    initActions:function()
  {  
     this.view.btnUpdatePassword.onClick=this.validateUserName;
     this.view.customHeader.flxBack.onClick=this.goBack;
     this.view.customHeader.btnRight.onClick = this.onCancel;
     this.view.txtFirstName.onTextChange = this.onFirstNameChange;
    this.view.txtLastName.onTextChange = this.onLastNameChange;
  },
   
 onFirstNameChange: function(){
    if(this.view.txtLastName.text!==null||this.view.txtLastName.text!=="")
  {
    this.changeSkinbtnUpdatePassword();
  }
  },
  onLastNameChange: function(){
    if(this.view.txtFirstName.text!==null||this.view.txtFirstName.text!=="")
  {
    this.changeSkinbtnUpdatePassword();
  }
  },
  changeSkinbtnUpdatePassword : function(){
           if(this.isValidInputs()){
             	this.view.btnUpdatePassword.skin = "sknBtn0095e426pxEnabled";
      	        this.view.btnUpdatePassword.setEnabled(true);
           }else{
             this.view.btnUpdatePassword.skin = "skinButtonDisabled";
               this.view.btnUpdatePassword.focusSkin = "skinButtonDisabled";
              this.view.btnUpdatePassword.setEnabled(false);
           }
   },
  isValidInputs :function(){
    return this.view.txtFirstName.text.length>0 && this.view.txtLastName.text.length>0;
  },
 validateUserName:function()
  {
    var lastName = this.view.txtLastName.text;
     var firstName = this.view.txtFirstName.text;
    if(lastName === '' && firstName===''|| lastName === null && firstName===null || lastName === undefined&&firstName===undefined){
      this.bindViewError(applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.common.invalidLastName"));      
    }
    else{
       
      var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
	authModule.presentationController.naviagteToDOB(firstName,lastName);   
    }  
  },
  bindViewError : function(msg)
  {
    applicationManager.getPresentationUtility().dismissLoadingScreen();
    applicationManager.getDataProcessorUtility().showToastMessageError(this,msg);
  },
   goBack:function()
  {
     var navManager = applicationManager.getNavigationManager();
    navManager.goBack();
  },
  onCancel : function()
  {
   var navManager = applicationManager.getNavigationManager();
    navManager.clearStack();
    var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
    authModule.presentationController.navigateToLogin();    
  }
  
});
