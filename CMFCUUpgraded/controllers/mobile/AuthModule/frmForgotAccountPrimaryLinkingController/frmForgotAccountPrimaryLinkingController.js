define({
  flag:0,
  prevSelItems:[],
  prevSelIndex:-1,
  init: function() {
    var navManager = applicationManager.getNavigationManager();
    var currentForm = navManager.getCurrentForm();
    applicationManager.getPresentationFormUtility().initCommonActions(this, "YES", currentForm);
  },
  frmAccPrimaryLinkPreshow: function() {
    this.setPreshowData();
    applicationManager.getPresentationUtility().dismissLoadingScreen();
    var navManager = applicationManager.getNavigationManager();
    var currentForm = navManager.getCurrentForm();
    applicationManager.getPresentationFormUtility().logFormName(currentForm);
    //     if(kony.os.deviceInfo().name !== "iPhone"){
    //       this.view.flxHeader.setVisibility(true);
    //     }
    //     else{
    //       this.view.flxHeader.setVisibility(false);
    //   }
    this.view.flxHeader.setVisibility(true);
  },
  setPreshowData: function() {
    var scopeObj = this;
    this.flag = 0;
    this.view.customHeader.flxBack.isVisible = false;
    this.view.flxHeader.customHeader.lblLocateUs.text = kony.i18n.getLocalizedString("kony.mb.cantLogin.cantLogin");
    //     this.view.lblText.text = kony.i18n.getLocalizedString("kony.mb.enroll.primAcc");
    //     this.view.lblTextdesc.text = kony.i18n.getLocalizedString("kony.mb.enroll.slctPrimAcc");
    //     this.view.lblAccTextDesc.text = kony.i18n.getLocalizedString("kony.mb.enroll.slctAnthrAccDesc");
    this.view.flxHeader.customHeader.btnRight.skin = "sknLblb8dcffSSP32px";
    this.view.customHeader.btnRight.focusSkin = "sknRtx424242SSP26px";
    this.view.flxAccLink.skin = "sknFlxWhiteBorder1";
    this.view.btnContinue.skin = "skinButtonDisabled";
    this.view.btnContinue.setEnabled(false);
    this.setAccLinks();
    this.view.btnContinue.onClick = this.btnContinueOnClick;
    this.view.segAccLink.onRowClick = this.segAccLinkOnRowClick;
    this.view.customHeader.btnRight.onClick = function() {
      scopeObj.onClickCancel();
    };
  },

  ContactHandler:function(response){
    if (response===true){
      var number = "7043750183";
      kony.phone.dial(number);
    }

  },
  btnContinueOnClick: function() {
    applicationManager.getPresentationUtility().showLoadingScreen();
    var navMan = applicationManager.getNavigationManager();
    var user = navMan.getCustomInfo("accPrimLinkData");
    if(user[0].locked === 1){
      var i18nString = applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.login.accountFrozen");
      kony.ui.Alert(i18nString, this.ContactHandler, constants.ALERT_TYPE_CONFIRMATION, "Contact", "Ok", "");     
    }
    else{
      var authMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
      authMod.presentationController.commonFunctionForNavigation("frmForgotMain");
    }
  },

  setAccLinks: function() {
    var navMan = applicationManager.getNavigationManager();
    var userList = navMan.getCustomInfo("userList");
    var mainUser = navMan.getCustomInfo("frmEnrollSecurityCheck");
    var data = [];
    var temp = {};
    var lblAccNum, img, lblAccName,lblAccNameValue,lblAccType, lblAccTypeValue,accNumHidden ;
    img = {
      src: "radio_inactive.png"
    };
    lblAccName = kony.i18n.getLocalizedString("kony.mb.enroll.primary");
    lblAccType = kony.i18n.getLocalizedString("kony.mb.enroll.accType");
    for (var i = 0; i < userList.length; i++) {
      if(mainUser.mainAccountNum === userList[i].accountNumber){
        lblAccNum = userList[i].accountNumber.replace(/\d(?=\d{4})/g, "X");
        lblAccNameValue = userList[i].hbUsername;
        accNumHidden = userList[i].accountNumber;
        lblAccTypeValue = this.accTypeValue(userList[i].accountType);
        temp = {
          "lblAccNum": lblAccNum,
          "imgIndicator": img,
          "lblAccName": lblAccName,
          "lblAccNameValue": lblAccNameValue,
          "lblAccType": lblAccType,
          "lblAccTypeValue": lblAccTypeValue,
          "accNumHidden":accNumHidden,
          "locked": userList[i].locked
        };
        data.push(temp);
      }
    }
    this.view.segAccLink.setData(data);
  },

  accTypeValue : function(response){
    var typeValue;
    if(response!==null && response!=="" && typeof response!=="undefined"){
      if(response === "0" || response ==="2"||response==="5"||response==="7"||response==="8")
        typeValue = "Personal";
      else if(response === "10")
        typeValue = "Business";
      else if(response === "11")
        typeValue = "Personal Agency";
      else if(response === "12")
        typeValue = "Irrevocable Trust";
      else if(response === "13")
        typeValue = "Revocable Trust";
      else if(response === "20")
        typeValue = "Minor";
      else if(response === "21")
        typeValue = "NC UTMA";
      else
        typeValue = "Other";
    }
    return typeValue;
  },

  segAccLinkOnRowClick: function() {
    var navManager = applicationManager.getNavigationManager();
    var selItems,selIndex,imgActive;
    var data = [];
	selIndex = this.view.segAccLink.selectedRowIndex[1];
    if(this.prevSelIndex >= 0 && selIndex === this.prevSelIndex){
         return;
    }
    if (this.flag) {
      this.flag = 0;
      var imgInActive = {
        src: "radio_inactive.png"
      };
      this.prevSelItems.imgIndicator = imgInActive;
      this.view.segAccLink.setDataAt(this.prevSelItems, this.prevSelIndex);
    }
    if (!this.flag) {
	  try{
		selItems = this.view.segAccLink.selectedRowItems[0];
		selIndex = this.view.segAccLink.selectedRowIndex[1];
		this.prevSelItems = selItems;
		this.prevSelIndex = selIndex;
		this.flag = 1;
		imgActive = {
			src: "radio_active.png"
		};
		selItems.imgIndicator = imgActive;
		this.view.segAccLink.setDataAt(selItems, selIndex);
		data.push(selItems);
		navManager.setCustomInfo("accPrimLinkData",data);
	  }catch(e){
        kony.print("Exception is "+e);
      }
    }
    this.view.btnContinue.skin = "sknBtn0095e426pxEnabled";
    this.view.btnContinue.setEnabled(true);
  },
  onClickCancel: function() {
    applicationManager.getPresentationUtility().dismissLoadingScreen();
    var enrollMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("EnrollModule");
    enrollMod.presentationController.resetEnrollObj();
  },
});