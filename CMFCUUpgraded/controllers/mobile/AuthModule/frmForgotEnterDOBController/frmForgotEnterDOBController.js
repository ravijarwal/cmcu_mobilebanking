define({
    keypadString: '',
    timerCounter: 0,
    init : function(){
    var navManager = applicationManager.getNavigationManager();
    var currentForm=navManager.getCurrentForm();
    applicationManager.getPresentationFormUtility().initCommonActions(this,"YES",currentForm);
 },
  preShow: function () {
        this.view.flxPopup.setVisibility(false);
        this.initActions();
        this.renderTitleBar();
        this.handleData();
        applicationManager.getPresentationUtility().dismissLoadingScreen();
        var navManager = applicationManager.getNavigationManager();
   	    var currentForm=navManager.getCurrentForm();
   		applicationManager.getPresentationFormUtility().logFormName(currentForm);
    },
  renderTitleBar :function(){
    var deviceUtilManager = applicationManager.getDeviceUtilManager();
    var isIphone = deviceUtilManager.isIPhone();
    /*if(!isIphone){
      this.view.flxHeader.isVisible = true;
    }
    else{
      this.view.flxHeader.isVisible = false;
    }*/
	this.view.flxHeader.isVisible = true;
  },
  handleData : function(){
    var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
    var forgotObj = authModule.presentationController.getForgotObjectForView();
    this.view.customHeader.btnRight.text = kony.i18n.getLocalizedString("kony.mb.common.CancelSmall");
    this.view.customHeader.btnRight.skin = "sknRtx424242SSP26px";
    this.view.customHeader.btnRight.focusSkin = "sknRtx424242SSP26px";
    this.view.customHeader.lblLocateUs.skin = "sknRtx424242SSP26px";
	var navManager = applicationManager.getNavigationManager();
	navManager.setCustomInfo("dateOfBirth", "");
    this.keypadString = "";
    this.updateInputBullets();
  },
  initActions:function()
  {
    this.view.btnVerify.onClick=this.validateDOB;
    this.view.customHeader.flxBack.onClick=this.goBack;
    this.view.customHeader.btnRight.onClick = this.onCancel;
    this.view.flxView.onClick = this.imgDOBViewOnClick;
  },
  imgDOBViewOnClick: function() {
    var navManager = applicationManager.getNavigationManager();
    var dateOfBirth = navManager.getCustomInfo("dateOfBirth");
    if (dateOfBirth !== "" && dateOfBirth !== null && typeof dateOfBirth !== "undefined" && dateOfBirth.length === 10) {
      if (this.view.imgSSNView.src === "viewicon.png") {
        this.view.imgSSNView.src = "viewactive.png";
        this.view.lblMonthOne.text = dateOfBirth.charAt(0);
        this.view.lblMonthTwo.text = dateOfBirth.charAt(1);
        this.view.lblDayOne.text = dateOfBirth.charAt(3);
        this.view.lblDayTwo.text = dateOfBirth.charAt(4);
        this.view.lblYearOne.text = dateOfBirth.charAt(6);
        this.view.lblYearTwo.text = dateOfBirth.charAt(7);
        this.view.lblYearThree.text = dateOfBirth.charAt(8);
        this.view.lblYearFour.text = dateOfBirth.charAt(9);
        this.view.flxInputDOB.forceLayout();
      } else {
        this.view.imgSSNView.src = "viewicon.png";
        this.view.lblMonthOne.text = "•";
        this.view.lblMonthTwo.text = "•";
        this.view.lblDayOne.text = "•";
        this.view.lblDayTwo.text = "•";
        this.view.lblYearOne.text = "•";
        this.view.lblYearTwo.text = "•";
        this.view.lblYearThree.text = "•";
        this.view.lblYearFour.text = "•";
        this.view.flxInputDOB.forceLayout();
      }
    } else {
      this.view.imgSSNView.src = "viewicon.png";
      this.view.flxInputDOB.forceLayout();
    }
  },
    //KEYPAD OPS:
  updateInputBullets: function () {
    var scope = this;
    var dummyString = 'MM/DD/YYYY';
    var navManager = applicationManager.getNavigationManager();
    var widgets = this.view["flxInputDOB"].widgets();
    for (var i = 0; i < this.keypadString.length; i++) {
      navManager.setCustomInfo("dateOfBirth", this.keypadString);
      widgets[i].skin = "sknLbl979797SSP60px";
      //widgets[i].text = this.keypadString[i];
      if(i===2 || i===5){
        widgets[i].text = "/";
      }else{
        widgets[i].text = "•"; 
      }  
    }
    for (var i = this.keypadString.length; i < widgets.length; i++) {
      widgets[i].skin = "sknLble3e3e3SSP60px";
      widgets[i].text = dummyString[i];
	  this.view.imgSSNView.src = "viewicon.png";
    }

    if(this.keypadString.length !== 10)
    {
      this.view.btnVerify.skin = "sknBtnOnBoardingInactive";
      this.view.btnVerify.setEnabled(false);
    }
    else
    {
      this.view.btnVerify.skin = "sknBtn0095e426pxEnabled";
      this.view.btnVerify.setEnabled(true);
    }
    this.view.forceLayout();
  },
    setKeypadChar: function (char) {
        if (this.keypadString.length === 10) return;

        this.keypadString = this.keypadString + char;
        if (this.keypadString.length === 2 || this.keypadString.length === 5) {
          this.keypadString = this.keypadString + '/';
        }
        this.updateInputBullets();
    },
    clearKeypadChar: function () {
        if (this.keypadString.length === 1) {
            this.keypadString = '';
            this.updateInputBullets();
        }
        if (this.keypadString.length !== 0) {
          if (this.keypadString[this.keypadString.length - 1] === '/') {
            this.keypadString = this.keypadString.substr(0, this.keypadString.length - 1);
          }
            this.keypadString = this.keypadString.substr(0, this.keypadString.length - 1);
            this.updateInputBullets();
        }
    },
  validateDOB:function()
  {
      var DOB = this.keypadString;
      applicationManager.getPresentationUtility().showLoadingScreen();
      var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
      authModule.presentationController.validateDOB(DOB);  
  },
   bindViewError : function(msg)
  {
    applicationManager.getPresentationUtility().dismissLoadingScreen();
//     applicationManager.getDataProcessorUtility().showToastMessageError(this,msg);
    this.showToastMessageError(msg);
  },
  showToastMessageError : function(msg){
      this.showToastMessage("sknFlxf54b5e", "errormessage.png",msg);
  },
  showToastMessage : function(skin,img,msg){
    var scope = this;
    this.view.flxMainContainer.top = "60dp";
    this.view.flxPopup.isVisible = true;
    if (this.timerCounter === undefined || this.timerCounter === null)
      this.timerCounter = 0;
    this.timerCounter=parseInt(this.timerCounter)+1;
    var timerId="timerPopupSuccess"+this.timerCounter;
    this.view.flxPopup.skin = "" + skin;
    this.view.customPopup.imgPopup.src = "" + img;
    this.view.customPopup.lblPopup.text = msg;
    try{
      kony.print(timerId);
      kony.timer.schedule(timerId, function() {
        scope.view.flxPopup.setVisibility(false);
        scope.view.flxPopup.isVisible = false;
        scope.view.flxMainContainer.top = "56dp";
      }, 3, false);
    }
    catch(e)
    {
      kony.print(timerId);
      kony.print(e);
    }
  },
  goBack:function()
  {
    var navManager = applicationManager.getNavigationManager();
    navManager.setCustomInfo("dateOfBirth", "");
    navManager.goBack();
  },
  onCancel : function()
  {
    var navManager = applicationManager.getNavigationManager();
    navManager.clearStack();
   var i18n = applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.enroll.cancelPopup");
    kony.ui.Alert(i18n, this.alertCB, constants.ALERT_TYPE_CONFIRMATION, "Yes", "No", "");
  },
  alertCB : function(response){
    if(response){
      var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
      authModule.presentationController.navigateToLogin(); 
    }
  }
 
});