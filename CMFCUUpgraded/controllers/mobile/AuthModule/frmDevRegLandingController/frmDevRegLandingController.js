define({ 

  init : function(){
    var navManager = applicationManager.getNavigationManager();
    var currentForm=navManager.getCurrentForm();
    applicationManager.getPresentationFormUtility().initCommonActions(this,"YES",currentForm);
  },
  preShow : function(){
    if(kony.os.deviceInfo().name !== "iPhone"){
      this.view.flxHeader.isVisible = true;
    }
    else
      this.view.flxHeader.isVisible = false;
    var navManager = applicationManager.getNavigationManager();
    var currentForm = navManager.getCurrentForm();
    applicationManager.getPresentationFormUtility().logFormName(currentForm);
    applicationManager.getPresentationUtility().dismissLoadingScreen(); 
    this.view.btnNoThanks.onClick = this.onNoThanksClick.bind(this);
    this.view.btnRegNow.onClick= this.regNow.bind(this);
    this.setDetails();
  },
  onNoThanksClick : function(){
    applicationManager.getPresentationUtility().showLoadingScreen();
    var authMode = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
    authMode.presentationController.defaultLoginToAccounts();
    authMode.presentationController.setDeviceRegisterflag(false);
  },
  setDetails : function(){
    var authMode = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
    var Phoneno = authMode.presentationController.getMobileNo();
    this.view.lblMobNoValue.text = applicationManager.getDataProcessorUtility().maskAccountNumber(Phoneno);
    var userEmail = authMode.presentationController.getEmail();  
    this.view.lblEmailIdValue.text = "ravijarwal10@gmail.com";//applicationManager.getDataProcessorUtility().maskAccountEmail(userEmail);
  },
  regNow: function(){
    
    var authMode = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
    applicationManager.getPresentationUtility().showLoadingScreen();
    var destination;
    var type;
	var mailText = this.view.lblEmailIdValue.text;
    var phoneText = this.view.lblMobNoValue.text;
    var navManager = applicationManager.getNavigationManager();
    
    if (mailText!== undefined && mailText!== ' '){
      destination = mailText;
      type = "m";
    } else if (phoneText!== undefined && phoneText!== ' '){
      destination = phoneText;
      type = "t";
    }
    
    var otpInfo = {"dest":destination, "type":type};
    navManager.setCustomInfo("frmDevRegLanding",otpInfo);
    authMode.presentationController.invokeIdentity();
	
  }
});