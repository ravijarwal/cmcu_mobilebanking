define({
  popupMsg: '',
  timerCounter: 0,
  onNavigate: function(obj) {
    var navMan = applicationManager.getNavigationManager();
    var dataTrnfr= navMan.getCustomInfo("frmTransfersSuccess");

    if (obj === undefined || dataTrnfr===undefined) {
      var newObj = {
        "popup": "none"
      };
      obj = newObj;
      data=newObj;
    }
    if (obj.popup === "successAddRecipient") {
      this.popupMsg = kony.i18n.getLocalizedString("kony.mb.p2p.successAddRecipient");
    }
    if (obj.popup === "none") {
      this.popupMsg = '';
    }
    if(dataTrnfr !== undefined){
      if(dataTrnfr.popup === "InternalTransfer")
      {
                var refId=dataTrnfr.referenceId;
        if(refId!==null && refId!=="")
          {
             this.popupMsg="Transaction was done successfully with reference ID:"+refId;
          }
       
        dataTrnfr.referenceId="";   
      }
    }
  },
  init : function(){
    var navManager = applicationManager.getNavigationManager();
    var currentForm = navManager.getCurrentForm();
    applicationManager.getPresentationFormUtility().initCommonActions(this,"YES",currentForm);
  },

  // modified by Mahalakshmi 
  preShow: function() {
    //     if (this.view.flxHeaderSearchbox.isVisible === true) {
    //       this.view.flxHeaderSearchbox.isVisible = false;
    //       this.view.flxSearch.isVisible = true;
    //       this.view.flxHeader.isVisible = true;
    //       this.view.flxMainContainer.top = "56dp";
    //       this.view.flxTransferOptions.skin = "sknCommonHeader";
    // 	  this.view.flxSearch.skin = "sknCommonHeader"; 
    //       this.view.flxHeader.skin = "sknCommonHeader";
    //     } 
    // binding events for the widgets
    this.view.customFooter.flxAccounts.onClick = this.onClickFlxAccounts;
    this.view.customFooter.flxMore.onClick = function(){
      this.view.flxHamburger.isVisible = true;
    };  
    this.view.customFooter.flxMoreSelect.isVisible = false;
    // formatting UI
    this.view.segTransactions.sectionHeaderSkin = "f9f9";
    //var flxTransHdr = {skin : "f9f9"};

    this.view.customHeader.flxHeader.skin = "sknCommonHeaderPrimaryGrey";
    this.view.flxTransferOptions.skin = "sknCommonHeaderPrimaryGrey";
    this.view.flxSearch.skin = "sknCommonHeaderPrimaryGrey";
    this.view.lblMakeTransfers.skin = "sknLblGrey98";
    this.view.lblMakeTransfers.text = kony.i18n.getLocalizedString("kony.mb.common.transfer");
    this.view.customSearchbox.btnCancel.skin = "sknLblGrey98";
    this.view.lblPayAPerson.skin = "sknLblGrey98";
    this.view.customSearchbox.btnCancel.skin = "sknLblGrey98";
    this.view.lblManage.skin = "sknLblGrey98";
    this.view.tbxSearch.placeholder = kony.i18n.getLocalizedString("kony.mb.accdetails.searchTransactions");
    this.view.imgMakeTransfer.src = "transfer.png";
    this.view.customFooter.imgTransfer.src = "transferactive.png";
    this.view.imgPayAPerson.src = "pay.png";
    this.view.imgManage.src = "manage.png";
    this.view.customFooter.flxAccSelect.setVisibility(false);
    this.view.customFooter.flxTransferSel.setVisibility(true); 
    this.view.flxHeader.isVisible = true;
    this.view.flxFooter.isVisible = true;
    this.view.flxHeaderSearchbox.isVisible = false;
    this.view.flxHamburger.isVisible = false;
    this.view.flxSearch.isVisible = true;

    this.view.customHeader.flxBack.isVisible = false;
    this.view.customHeader.lblLocateUs.text = kony.i18n.getLocalizedString("kony.mb.transfers.headerTransfers");
    this.view.customSearchbox.tbxSearch.placeholder = kony.i18n.getLocalizedString("kony.mb.accdetails.searchTransactions");
    this.view.customHeader.lblLocateUs.centerX = "50%";
    this.view.flxMainContainer.top = "8%";
    this.view.flxMainContainer.bottom = "60dp";
    this.view.segTransactions.top = "25%";
    this.view.flxTransferOptions.isVisible = true;
    //     this.view.flxMainContainer.top = "56dp";
    //     this.view.flxMainContainer.bottom = "60dp";



    //this.setSegmentData();
    this.setSegTransactionsData();
    this.initActions();
    //this.P2PEntitlement();
    if ((this.popupMsg !== null) && (this.popupMsg !== '')) {
      this.showPopupSuccess();
    }
    applicationManager.getPresentationUtility().dismissLoadingScreen();
    var navManager = applicationManager.getNavigationManager();
    var currentForm=navManager.getCurrentForm();
    applicationManager.getPresentationFormUtility().logFormName(currentForm);  
  },

  onClickFlxAccounts : function(){
    var navManager = applicationManager.getNavigationManager();
    var accountObj = applicationManager.getAccountManager();
    var configManager = applicationManager.getConfigurationManager(); 
    var accountData = "";
    accountData = accountObj.getInternalAccounts();
    var custominfo = navManager.getCustomInfo("frmDashboard");
    if(!custominfo){
      custominfo = {};
    }
    custominfo.accountData = accountData;
    navManager.setCustomInfo("frmDashboard",custominfo);
    navManager.navigateTo("frmDashboardAggregated");
  },

  initActions: function() {
    var scope = this;
    var configManager = applicationManager.getConfigurationManager();
    var MenuHandler =  applicationManager.getMenuHandler();
    MenuHandler.setUpHamburgerForForm(scope,configManager.constants.MENUTRANSFERS);
    this.view.flxPayAPerson.setVisibility(false);
    this.view.tbxSearch.onTouchEnd = this.showSearch;
    this.view.customSearchbox.btnCancel.onClick = this.cancelSearch;
    this.view.flxPayAPerson.onClick = this.flxPayAPersonOnClick;
    this.view.flxMakeTransfer.onClick= this.maketransferOnclick;
    this.view.segTransactions.onRowClick=this.segTransactionsOnRowClick;
    this.view.flxManage.onClick = function() {
      var transferMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
      transferMod.presentationController.commonFunctionForNavigation("frmManageRecipientType");
    };
    this.view.flxMakeTransfer.onTouchStart = function(){
      scope.view.imgMakeTransfer.src = "transfer.png";
    };
    this.view.flxPayAPerson.onTouchStart = function(){
      scope.view.imgPayAPerson.src = "payapersonwhitetap.png";
    };
    this.view.flxManage.onTouchStart = function(){
      scope.view.imgManage.src = "manage.png";
    };
    this.view.flxMakeTransfer.onTouchEnd = function(){
      scope.view.imgMakeTransfer.src = "transfer.png";
    };
    this.view.flxPayAPerson.onTouchEnd = function(){
      scope.view.imgPayAPerson.src = "payapersonwhite.png";
    };
    this.view.flxManage.onTouchEnd = function(){
      scope.view.imgManage.src = "manage.png";
    };
    this.view.segTransactions.onScrolling = function () {
      scope.transactionsSegmentOnScrolling();
    };
  },

  P2PEntitlement : function(){
    var configManager = applicationManager.getConfigurationManager();
    if (configManager.getConfigurationValue("ispayAPersonEnabled") !== "true") {
      this.view.flxPayAPerson.setVisibility(false);
      this.view.flxMakeTransfer.left = "50dp";
      this.view.flxManage.right = "50dp";
      return;
    }
  },

  // modified by Mahalakshmi
  segTransactionsOnRowClick:function(){
    var navMan = applicationManager.getNavigationManager();
    /* this is causing an error */
    //     var selectedSectionIndex = sec;
    //     var selectedRowIndex = row;
    //     var selectedSectionIndex=Math.floor(this.view.segTransactions.selectedRowIndex[0]);
    //     var selectedRowIndex=Math.floor(this.view.segTransactions.selectedRowIndex[1]);
    // var transactionData=this.view.segTransactions.data[selectedSectionIndex][1][selectedRowIndex];
    var transactionData = this.view.segTransactions.selectedRowItems;
    //alert("segment onClick---- "+JSON.stringify(transactionData));
    var transMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransactionModule");
    //transMod.presentationController.setEntryPoints(transactionData.transactionType);
    navMan.setCustomInfo("frmTransactionScheduledDetails",transactionData);
    navMan.setEntryPoint("frmTransactionDetails","Transfers");
    transMod.presentationController.commonFunctionForNavigation("frmTransactionDetails");
  },
  setSegmentData: function() {
    var pendingaccounts=[];

    var navMan=applicationManager.getNavigationManager();
    // var configManager = applicationManager.getConfigurationManager();
    var forUtility=applicationManager.getFormatUtilManager();
    var transactions=navMan.getCustomInfo("frmTransfers");
    if (transactions){

      if(transactions.res!==undefined&&transactions.res!==null)
      {
        if(transactions.type=="error")
          this.showErrorPopup(transactions.res);
        else
          this.showSuccessPopup(transactions.res,transactions.typeOfTransaction);
      }
      transactions.res=null;
      navMan.setCustomInfo("frmTransfers",transactions);
      var postedTransaction=transactions.postedTransaction;
      var scheduledTransactions=transactions.scheduledTransactions;
      this.view.segTransactions.widgetDataMap={
        lblTransaction:"description",
        lblDate:"scheduledDate",              
        lblAmount:"amount",
        transactionId:"transactionId",
        lblAccount:"fromAccountName",
        lblHeader:"lblHeader",
        imgAccount:"image"
      };
      if(postedTransaction.length>0&&scheduledTransactions.length>0)
      {
        var data=  [[{"lblHeader": "Scheduled Transactions"},scheduledTransactions],
                    [{"lblHeader": "Posted Transactions" },postedTransaction]];
        this.segmentData=data;
        this.view.segTransactions.setData(data);
        this.pendingaccounts=this.view.segTransactions.data[0][1];
        this.postedaccounts=this.view.segTransactions.data[1][1];
        this.view.segTransactions.isVisible = true;
        this.view.flxNoTransactions.isVisible = false;
      }
      else if(scheduledTransactions.length>0)
      {
        var data=  [[{"lblHeader": "Scheduled Transactions"},scheduledTransactions]];

        this.segmentData=data;
        this.view.segTransactions.setData(data);
        this.pendingaccounts=this.view.segTransactions.data[0][1];
        this.postedaccounts=[];
        this.view.segTransactions.isVisible = true;
        this.view.flxNoTransactions.isVisible = false;
      }
      else if(postedTransaction.length>0)
      {
        var data=  [[{ "lblHeader": "Posted Transactions"},postedTransaction]];
        this.segmentData=data;
        this.view.segTransactions.setData(data);
        this.postedaccounts=this.view.segTransactions.data[0][1];
        this.pendingaccounts=[];
        this.view.segTransactions.isVisible = true;
        this.view.flxNoTransactions.isVisible = false;
      }
      else
      {
        this.segmentData=[];
        this.pendingaccounts=[];
        this.postedaccounts=[];
        this.view.segTransactions.isVisible=false;
        this.view.flxNoTransactions.isVisible=true;
      }
    }
  },

  // defined by Mahalakshmi 
  getPrimaryName : function(accntData){
    var primaryName = "";
    for(i=0 ; i<accntData.length ; i++){
      if(accntData[i]["nameType"] !== undefined && accntData[i]["LastName"] !== undefined && accntData[i]["FirstName"] !== undefined){
        if(accntData[i]["nameType"] == "2" && accntData[i]["LastName"] !== null && accntData[i]["FirstName"] !== null){
          primaryName = accntData[i]["FirstName"] + " " + accntData[i]["LastName"];
          break;
        }
      }
    }
    return primaryName;
  },

  // defined by Mahalakshmi 
  getNickNameById : function(accntData,Id,IdType){
    var recLocation = "";
    var reqData = {};
    for(var i=0 ; i<accntData.length ; i++){
      if(accntData[i]["accountID"] !== undefined && accntData[i]["accountID"] !== null && accntData[i]["accountID"] === Id){
        if(IdType === "0"){
          if(accntData[i]["nickName"] !== null)
            recLocation = accntData[i]["nickName"];
          else{
            if(accntData[i]["STDesc"] !== null)
              recLocation = accntData[i]["STDesc"];
          }
        }
        else{
          if(accntData[i]["nickName"] !== null)
            recLocation = accntData[i]["nickName"];
          else{
            if(accntData[i]["LTDesc"] !== null)
              recLocation = accntData[i]["LTDesc"];
          }
        }
      }
    }
    reqData = {"recLocation" : recLocation, "id" : Id};
    return reqData;
  },

  // defined by Mahalakshmi 
  getRecipientLocation : function(accntData){
    var prefList = [];
    var recLocation = {};
    //alert("accntData: "+JSON.stringify(accntData));
    for(var i=0 ; i<accntData.length ; i++){
      if(accntData[i]["PreferenceAccessList"] != undefined && accntData[i]["PreferenceAccessList"] != null){
        prefList = accntData[i]["PreferenceAccessList"];
        //alert("prefList: "+JSON.stringify(accntData[i].PreferenceAccessList));
        for(var j=0 ; j<prefList.length ; j++){
          if(prefList[j]["IdType"] !== undefined && prefList[j]["Id"] !== undefined && prefList[j]["IdType"] !== null && prefList[j]["Id"] !== null){
            recLocation = this.getNickNameById(accntData,prefList[j]["Id"],prefList[j]["IdType"]);
          } 
        }
      }
    }
    return recLocation;
  },

  // defined by Mahalakshmi
  setSegTransactionsData : function(){
    var navMan=applicationManager.getNavigationManager();
    var transactions=navMan.getCustomInfo("frmTransfers");
    var accntData = navMan.getCustomInfo("frmAccountsForTransfers");
    //alert("transactions: "+JSON.stringify(transactions));
    var lblDateModified,lblAccountModified,lblDateModified,lblAmountModified,lblTransactionModified,freqModified,accNumModified,id,recLocation;
    var data = [];
    var temp = {},reqData = {};
    var imgAccountModified = {"isVisible" : false};
    var flxSeperatorTransModified = {skin : "sknFlxSeparatorSegTrnsfrs"};
    if(transactions !== undefined || transactions !== null){
      if(transactions.length > 0){
        for(var i=0 ; i<transactions.length ; i++){
          if(transactions[i].listType === "eftList"){
            lblDateModified = {text : transactions[i]["tranDate"],height : "15dp"};
            lblAccountModified = {text : transactions[i]['amtData'],left : "0dp"};
            temp = {
              "lblTransaction":transactions[i]["reference2"],
              "lblDate":lblDateModified,              
              "lblAmount":transactions[i]["tranAmt"],
              "lblAccount":lblAccountModified,
              "imgAccount" : imgAccountModified,
              "acctType":transactions[i]["acctType"],
              "acctText":transactions[i]["acctText"],
              "amountCode":transactions[i]["amtCode"],
              "listType":transactions[i].listType,
              "frequency":transactions[i]["freq"],
              "id":transactions[i]["id"],
              "locator":transactions[i]["eftLocator"],
              "flxSeparator":flxSeperatorTransModified
            }
            data.push(temp);
          }
          else if(transactions[i].listType === "shareList"){
            lblDateModified = {text : transactions[i]["sTEfftDate"],height : "15dp"};
            lblAmountModified = transactions[i]["sTAmount"];
            lblTransactionModified = this.getPrimaryName(accntData);
            reqData = this.getRecipientLocation(accntData);
            recLocation = reqData["recLocation"];
            lblAccountModified = {text : recLocation,left : "0dp"};
            if(reqData !== "" && lblTransactionModified !== ""){
              temp = {
                "lblTransaction":lblTransactionModified,
                "lblDate":lblDateModified,              
                "lblAmount":lblAmountModified,
                "lblAccount":lblAccountModified,
                "imgAccount" : imgAccountModified,
                "listType":transactions[i].listType,         
                "acctNum":transactions[i].sTAccountNumber,
                "frequency":transactions[i]["sTFrequency"],
                "id":transactions[i]["sTId"],
                "sessionId":reqData["id"],
                "locator":transactions[i].sTLocator,
                "idType":transactions[i]["sTIdType"],
                "shareId":transactions[i]["shareId"],
                "flxSeparator":flxSeperatorTransModified
              }
              data.push(temp);
            }

          }
          else{
            lblDateModified = {text : transactions[i]["efftDate"],height : "15dp"};
            lblAmountModified = transactions[i]["amount"];
            lblTransactionModified = this.getPrimaryName(accntData);
            reqData = this.getRecipientLocation(accntData);
            recLocation = reqData["recLocation"];  
            lblAccountModified = {text : recLocation,left : "0dp"};
            if(reqData !== "" && lblTransactionModified !== ""){
              var temp = {
                "lblTransaction":lblTransactionModified,
                "lblDate":lblDateModified,              
                "lblAmount":lblAmountModified,
                "lblAccount":lblAccountModified,
                "imgAccount" : imgAccountModified,
                "listType":transactions[i].listType,
                "acctNum":transactions[i].acctNum,
                "frequency":transactions[i]["lnFrequency"],
                "id":transactions[i].lnId,
                "sessionId":reqData["id"],
                "locator":transactions[i].locator,
                "idType":transactions[i].lnIdType,
                "loanId":transactions[i].loanId,
                "flxSeparator":flxSeperatorTransModified
              }
              data.push(temp);
            }

          }
          //alert("temp: "+JSON.stringify(temp));
        }
        if(data.length > 0){
          var flxTransHdrMdfd = {skin : "sknScrFlxffffff"};
          var finalData = [[{"flxTransHeader":flxTransHdrMdfd,"lblHeader": "Scheduled Transactions"},data]];  
          this.segmentData=data;
          this.view.segTransactions.setData(finalData);
          //this.view.segTransactions.setData(data);
          this.view.flxNoTransactions.isVisible=false;
          this.view.segTransactions.isVisible = true;
        }
        else{
          this.segmentData=[];
          this.view.segTransactions.isVisible=false;
          this.view.flxNoTransactions.isVisible=true;
        }
      }
      else{
        this.segmentData=[];
        this.view.segTransactions.isVisible=false;
        this.view.flxNoTransactions.isVisible=true;
      }
    }
    else{
      this.segmentData=[];
      this.view.segTransactions.isVisible=false;
      this.view.flxNoTransactions.isVisible=true;
    }
  }, 
  showSearch: function() {
    //     if (kony.os.deviceInfo().name === "iPhone") {
    //       if (this.view.flxHeaderSearchbox.isVisible == true) {
    //         this.view.flxHeaderSearchbox.isVisible = false;
    //         this.view.flxSearch.isVisible = true;
    //         this.view.flxMainContainer.top = "0dp";
    //       } else {
    //         this.view.flxHeaderSearchbox.isVisible = true;
    //         this.view.flxSearch.isVisible = false;
    //         this.view.flxMainContainer.top = "40dp";
    //         this.view.customSearchbox.tbxSearch.text="";
    //         this.view.customSearchbox.tbxSearch.setFocus(true);
    //         this.view.customSearchbox.tbxSearch.onTextChange = this.tbxSearchOnTextChange;
    //       }
    //     } else {
    //       if (this.view.flxHeaderSearchbox.isVisible == true) {
    //         this.view.flxHeaderSearchbox.isVisible = false;
    //         this.view.flxSearch.isVisible = true;
    //         this.view.flxHeader.isVisible = true;
    //         this.view.flxMainContainer.top = "56dp";
    //         this.view.flxGradient.top = "56dp";
    //       } else {
    //         this.view.flxSearch.isVisible = false;
    //         this.view.flxHeader.isVisible = false;
    //         this.view.flxMainContainer.top = "40dp";
    //         this.view.flxGradient.top = "40dp";
    //         this.view.flxHeaderSearchbox.isVisible = true;
    //         this.view.customSearchbox.tbxSearch.text="";
    //         this.view.customSearchbox.tbxSearch.setFocus(true);
    //         this.view.customSearchbox.tbxSearch.onTextChange = this.tbxSearchOnTextChange;
    //       }
    //     }
    this.view.flxHeader.isVisible = false;
    this.view.flxSearch.isVisible = false;
    this.view.flxTransferOptions.isVisible = false;
    this.view.flxHeaderSearchbox.isVisible = true;
    this.view.flxHeaderSearchbox.top = "0%";
    this.view.flxHeaderSearchbox.height = "6%";
    this.view.customSearchbox.height = "100%";
    this.view.customSearchbox.flxSearchMain.height = "100%";
    this.view.flxMainContainer.top = "6%";
    this.view.segTransactions.top = "0%";
    this.view.flxMainContainer.bottom = "60dp";
    this.view.flxNoTransactions.top = "6%";
    this.view.flxNoTransactions.height = "86%";
    this.view.customSearchbox.tbxSearch.text="";
    this.view.customSearchbox.tbxSearch.setFocus(true);
    this.view.customSearchbox.tbxSearch.onTextChange = this.tbxSearchOnTextChange;
  },
  cancelSearch:function(){
    this.view.flxHeaderSearchbox.isVisible = false;
    this.view.customHeader.flxBack.isVisible = false;
    this.view.customHeader.lblLocateUs.centerX = "50%";
    this.view.flxNoTransactions.top = "29%";
    this.view.flxNoTransactions.height = "63%";
    this.view.flxMainContainer.top = "8%";
    this.view.segTransactions.top = "25%";
    this.view.flxHeader.isVisible = true;
    this.view.flxTransferOptions.isVisible = true;
    this.view.flxSearch.isVisible = true;
    if (this.segmentData.length > 0) {
      this.view.segTransactions.setData(this.segmentData);
    	//this.addDummyRows();
      this.view.segTransactions.isVisible = true;
      this.view.flxNoTransactions.isVisible = false;
    } else {
      this.view.segTransactions.isVisible = false;
      this.view.flxNoTransactions.isVisible = true;
      // this.view.flxHeaderNT.isVisible = false;
    }
  },
  flxPayAPersonOnClick: function() {
    var navMan=applicationManager.getNavigationManager();
    navMan.setEntryPoint("payaperson","frmTransfers");
    var payeeMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule");
    payeeMod.presentationController.getAllPayees();

    var P2P = applicationManager.getLoggerManager();          
    P2P.setCustomMetrics(this, false, "P2P");

  },
  showPopupSuccess: function() {
    // alert("popup called");
    var scopeObj = this;
    this.timerCounter = parseInt(this.timerCounter) + 1;
    var timerId = "timerPopupSuccess" + this.timerCounter;
    this.view.flxPopup.skin = "sknFlx43ce6e";
    this.view.customPopup.imgPopup.src = "confirmation.png";
    this.view.customPopup.lblPopup.text = this.popupMsg;
    this.view.flxPopup.setVisibility(true);
    kony.timer.schedule(timerId, function() {
      scopeObj.view.flxPopup.setVisibility(false);
    }, 8, false);

  },
  maketransferOnclick:function()
  {
    var transMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
    //transMod.presentationController.transfersModule();
    var Transfers = applicationManager.getLoggerManager();                          
    Transfers.setCustomMetrics(this, false, "Transfers");
    var navManager = applicationManager.getNavigationManager(); 
    navManager.navigateTo("frmTransactionMode");

  },
  showSuccessPopup : function(refID,type){
    // TO DO i18n's
    var msg;
    if(type==="delete")
    {
      msg = "Transaction was cancelled successfully with reference ID : " + (refID.transactionId||refID.refernceId);
    }
    else{
      if(refID.referenceId)
        msg = "Transfer completed successfully. Transaction ID: "+ refID.referenceId;
      else
        msg = "Transaction was edited successfully with reference ID : " + refID.transactionId;
    }
    applicationManager.getDataProcessorUtility().showToastMessageSuccess(this,msg);

  },
  showErrorPopup: function(err){

    applicationManager.getDataProcessorUtility().showToastMessageError(this,JSON.stringify(err));
  },
  tbxSearchOnTextChange: function() {
    var navObj = applicationManager.getNavigationManager();
    var searchtext = this.view.customSearchbox.tbxSearch.text.toLowerCase();
    if (searchtext) {
      var data=[],headers=[],finalData=[];
      //       var lblPendingTranHeader = applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.accdetails.pendingTransactions");
      //       var lblPostedTranHeader = applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.accdetails.postedTransactions");
      //       headers.push(lblPendingTranHeader);
      //       headers.push(lblPostedTranHeader);
      //       data.push(this.pendingaccounts);
      //       data.push(this.postedaccounts);
      data.push(this.segmentData);
      finalData = data[0];
      this.view.segTransactions.isVisible = true;
      this.view.flxNoTransactions.isVisible = false;
      this.view.segTransactions.removeAll();
      var searchobj = applicationManager.getDataProcessorUtility().commonSegmentSearch("lblTransaction",searchtext,finalData);
      if (searchobj.length > 0) {
        this.view.segTransactions.setData(searchobj);
      } else {
        this.view.segTransactions.isVisible = false;
        this.view.flxNoTransactions.isVisible = true;
      }
    } else {
      if (this.segmentData.length > 0) {
        this.view.segTransactions.setData(this.segmentData);
        this.view.segTransactions.isVisible = true;
        this.view.flxNoTransactions.isVisible = false;
      } else {
        this.view.segTransactions.isVisible = false;
        this.view.flxNoTransactions.isVisible = true;
        // this.view.flxHeaderNT.isVisible = false;
      }
    }
  },

  addDummyRows: function () {
    var segWidgetDataMap = this.view.segTransactions.widgetDataMap;
    segWidgetDataMap["flxEmptyHeader"] = "flxEmptyHeader";
    segWidgetDataMap["flxEmptyRow"] = "flxEmptyRow";
    this.view.segTransactions.widgetDataMap = segWidgetDataMap;
    var segData = this.view.segTransactions.data;
            if(segData == null || segData == undefined){
      segData = [];
    }
    var segLength = 0;
    for (let i = 0; i < segData.length; i++) {
        segLength = segLength + (segData[i][1].length * 70) + 49; //66 is the row height and 49 is the header height
    }
    segData.unshift([{
            "template": "flxEmptyHeader",
            "flxEmptyHeader": {
                "height": "0dp"
            }
        },
        [{
            "template": "flxEmptyRow",
            "flxEmptyRow": {
                "height": "145dp"
            }
        }]
    ]);
    segLength = segLength + 145;
    this.view.segTransactions.setData(segData);
    this.segLength = segLength;
},
    transactionsSegmentOnScrolling: function () {
        var parallaxSpeed = 1;
      var yOffset = this.view.segTransactions.contentOffsetMeasured.y;
        if(this.view.flxHeaderSearchbox.isVisible)
          this.view.flxTransferOptions.top = 40 - (yOffset * parallaxSpeed) + "dp";
        else
          this.view.flxTransferOptions.top = 55 - (yOffset * parallaxSpeed) + "dp";
        this.view.flxSearch.top = 0 - (yOffset * parallaxSpeed) + "dp";
        this.view.flxGradient.top = 0 - (yOffset * parallaxSpeed) + "dp";   
    },
});