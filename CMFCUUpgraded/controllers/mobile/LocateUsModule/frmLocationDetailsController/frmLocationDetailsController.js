define({
  timerCounter : 0,
  init : function(){
    var navManager = applicationManager.getNavigationManager();
    var currentForm=navManager.getCurrentForm();
    applicationManager.getPresentationFormUtility().initCommonActions(this,"YES",currentForm);
  },
  frmLocationPreshow: function () {
    if(kony.os.deviceInfo().name !== "iPhone"){
      this.view.flxHeader.setVisibility(true);
    }
    else{
      this.view.flxHeader.setVisibility(true);
    }
    this.setFlowActions();
    this.setDetailsData();
    applicationManager.getPresentationUtility().dismissLoadingScreen();
  },
  frmPostShow : function(){
    var Detailsheight = this.view.flxDetailsMain.frame.height;
    this.view.flxDetailsDirections.centerY = (Detailsheight/2) + "dp";
    this.view.flxSeparator.height = (Detailsheight-40) + "dp";
    this.view.forceLayout();
  },
  setFlowActions : function(){
    var scope = this;
    this.view.flxImgNavigation.onClick = function () {
        // navigate to directions form
      scope.getDirections();
    };
    
    this.view.flxCmcuOrg.onClick = function(){
      var navUrl="https://www.cmcu.org";
        kony.application.openURL(navUrl);

    };
    this.view.customHeader.flxBack.onClick = function(){
      scope.navigateBack();
    };
  },
  navigateBack : function(){
    var navManager = applicationManager.getNavigationManager();
    navManager.goBack();
  },
  setDetailsData : function(){
    var navigationManager = applicationManager.getNavigationManager();
    var data = navigationManager.getCustomInfo("frmLocationDetails");
    var selectedLocation = data.selectedLocation;    
    var details = data.locationDetails;
    var status;
    this.selectedLocation = selectedLocation;
    this.view.lblBranchName.text = selectedLocation.name;
    if(selectedLocation.calloutStatus.text == "OPEN")
      status = "Open";
    else
      status = "Closed";
    this.view.lblStatus.text  = status;
    this.view.lblStatus.skin = selectedLocation.calloutStatus.skin;
    this.view.lblAddress1.text = selectedLocation.desc;
    this.view.lblAddress2.text = "";
    this.view.lblDistance.text = " - " + selectedLocation.distanceLabel;
    
    if(details && details.services){
      selectedLocation.services = details.services;
    }
    else{
      selectedLocation.services = "No Data Available";
    }
    if(selectedLocation.services === "No Data Available"){
      this.view.flxServices.setVisibility(false);
      this.view.segServices.setVisibility(false);
    }
    else{
      this.view.flxServices.setVisibility(true);
      this.view.segServices.setVisibility(true);
      var serviceListValue=selectedLocation.services.split("||");
      var segListServiceData = [];
      for(var i=0; i<serviceListValue.length;i++){
        if(serviceListValue[i] !== 'point_of_interest'){
          segListServiceData.push({
            "lblBullet": ".",
            "lblService":serviceListValue[i]  
          });
        }
      }
      this.view.segServices.setData(segListServiceData);
    }
    
    if(details && details.workingHours){
      selectedLocation.workingHours = details.workingHours;
    }
    else{
      selectedLocation.workingHours = "No Data Available";
    }
    if(selectedLocation.workingHours === "No Data Available"){
      this.view.flxOperationHours.setVisibility(false);
      this.view.segOperationalHours.setVisibility(false);
    }
    else{
      this.view.flxOperationHours.setVisibility(true);
      this.view.segOperationalHours.setVisibility(true);
      var workingHoursValue=selectedLocation.workingHours.split("||");
      var segListOperationData = [];
      for(i=0; i<workingHoursValue.length;i++){
        var splitRes = workingHoursValue[i].split(":");
        var resultHrs = splitRes[0];
        resultHrs = resultHrs.substring(0, 3) + ":";
        var resultsHrsValue = "";
        for(index = 1;index<splitRes.length;index++){
          if(index == 1){
            resultsHrsValue = resultsHrsValue+splitRes[index];
          }
          else{
            resultsHrsValue = resultsHrsValue+":"+splitRes[index];
          }
        }
        if(resultsHrsValue.includes("AM"))
          resultsHrsValue = resultsHrsValue.replace("AM","am");
        if(resultsHrsValue.includes("PM"))
          resultsHrsValue = resultsHrsValue.replace("PM","pm");
        if(resultsHrsValue === " Closed"){	
            resultHrs = {skin : "sknLblLightGrey112",text : resultHrs};
        	resultsHrsValue = {skin : "sknLblLightGrey112",text : resultsHrsValue};
        }
        segListOperationData.push({
          "lblDay":resultHrs,
          "lblTimings":resultsHrsValue
        });
      }
      this.view.segOperationalHours.setData(segListOperationData);
    } 
    this.setDataToCallBranch(details);
  },
  
  /**
  * it enable or disable the CALL Branch button based phone value
  */
  setDataToCallBranch : function(data){
    var scopeObject = this;
    if(data && data.phone){
      this.view.btnCallBranch.isVisible = true;
      this.view.btnCallBranch.onClick = scopeObject.onBtnCallBranchClick.bind(scopeObject,data);
    }
    else{
      this.view.btnCallBranch.isVisible = false;
    }
  },

  /**
  *on CALL BRANCH click it make call to the Branch number
  */
  onBtnCallBranchClick : function(data){
    if(data.phone){
      kony.phone.dial(data.phone);
    }
  },
  
  getDirections : function(){
    var scopeObj = this;
    applicationManager.getPresentationUtility().showLoadingScreen();
    var selectedLocationData = this.selectedLocation;
    if(selectedLocationData !== undefined){
      var source = {};
      var destination = {};
      destination.latitude = selectedLocationData.lat;
      destination.longitude = selectedLocationData.lon;

      var positionoptions = {timeout:64000,fastestInterval:0,minimumTime : 0};
      kony.location.getCurrentPosition(success,failure,positionoptions);
      function success(response){
        if(response && response.coords && response.coords.latitude && response.coords.longitude){
          source.latitude = response.coords.latitude;
          source.longitude = response.coords.longitude;
          var navManager = applicationManager.getNavigationManager();
          navManager.setCustomInfo('LocationsCurrentForm','frmLocationDetails');
          var locateUsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("LocateUsModule");
          locateUsModule.presentationController.getDirections(source,destination);
        }
      }
      function failure(error){
        scopeObj.geoLocationErrorCallBack(error);
        applicationManager.getPresentationUtility().dismissLoadingScreen();
      }
    }
  },
  geoLocationErrorCallBack: function(err) {
    var scopeObj = this;
    var deviceUtilManager = applicationManager.getDeviceUtilManager();
    var isIphone = deviceUtilManager.isIPhone();
    applicationManager.getPresentationUtility().dismissLoadingScreen();
    if (err.code == 1) {
      var i18nKey = applicationManager.getPresentationUtility().getStringFromi18n("i18n.maps.locationPermissionDenied");
      scopeObj.bindGenericError(i18nKey);
    }
    if (err.code == 3 && !isIphone) {
      var i18n_timeOut = applicationManager.getPresentationUtility().getStringFromi18n("i18n.maps.locationTimeOut");
      scopeObj.bindGenericError(i18n_timeOut);
    }
    if (err.code == 2 && !isIphone) {
      var i18n_turnOnLocationAlert = applicationManager.getPresentationUtility().getStringFromi18n("i18n.maps.turnOnLocationAlert");
      kony.ui.Alert(i18n_turnOnLocationAlert, scopeObj.onClickSettingsOrCancelHandler.bind(scopeObj), constants.ALERT_TYPE_CONFIRMATION, "Cancel", "Settings", "");
    }
  },
  bindError : function(msg){
    //applicationManager.getDataProcessorUtility().showToastMessageError(this, msg);
    this.showToastMessageError(msg);
  },
  bindGenericError : function(msg){
    //applicationManager.getDataProcessorUtility().showToastMessageError(this,msg);
    this.showToastMessageError(msg);
  },
  showToastMessageError : function(msg){
    this.showToastMessage("sknFlxf54b5e", "errormessage.png",msg);
  },
  showToastMessage : function(skin,img,msg){
    var scope = this;
    this.view.flxMainContainer.top = "60dp";
    this.view.flxPopup.isVisible = true;
    if (this.timerCounter === undefined || this.timerCounter === null)
      this.timerCounter = 0;
    this.timerCounter=parseInt(this.timerCounter)+1;
    var timerId="timerPopupSuccess"+this.timerCounter;
    this.view.flxPopup.skin = "" + skin;
    this.view.customPopup.imgPopup.src = "" + img;
    this.view.customPopup.lblPopup.text = msg;
    try{
      kony.print(timerId);
      kony.timer.schedule(timerId, function() {
        scope.view.flxPopup.setVisibility(false);
        scope.view.flxPopup.isVisible = false;
        scope.view.flxMainContainer.top = "56dp";
      }, 3, false);
    }
    catch(e)
    {
      kony.print(timerId);
      kony.print(e);
    }
  },
});