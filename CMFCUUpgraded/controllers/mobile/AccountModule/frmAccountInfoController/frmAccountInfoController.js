define({
  objRec: '',
  init : function(){
    var navManager = applicationManager.getNavigationManager();
    var currentForm=navManager.getCurrentForm();
    applicationManager.getPresentationFormUtility().initCommonActions(this,"YES",currentForm);
  },
  onNavigate: function(obj) {
    if (obj === undefined) {
      var newObj = {
        "view": "familyCheckingAcc"
      };
      obj = newObj;
    }
    this.objRec = obj;
  },
  accDetails:'',
  preshowAccInfo: function() {
    //     if(kony.os.deviceInfo().name !== "iPhone"){
    //       this.view.flxHeader.isVisible = true;
    //     }
    //     else{
    //       this.view.flxHeader.isVisible = false;
    //     }
    this.view.flxHeader.setVisibility(true);
    var navigationManager =applicationManager.getNavigationManager();
    this.accDetails =navigationManager.getCustomInfo("frmAccountDetails"); 
    this.btnCancelOnClick();
    //this.accDetails=this.accDetails.selectedAccountData;
    this.view.flxAccNoToggleHL.onClick = this.flxAccNoToggleHLToggleOnClick;
    this.view.flxAccNoToggleCC.onClick = this.flxAccNoToggleCCOnClick;
    this.view.flxAccNoToggle.onClick = this.flxAccNoToggleOnClick;
    this.view.flxAccNoToggleIRA.onClick = this.flxAccNoToggleIRAOnClick;
    //this.view.flxRouteNoToggle.onClick = this.flxRouteNoToggleOnClick;
    this.view.customHeader.btnRight.onClick=this.customHeaderBtnRightOnClick;
    this.view.btnEditNickName.onClick=this.btnEditNickNameOnClick;
    this.view.btnCancel.onClick=this.btnCancelOnClick;
    this.view.flxDismiss.onClick = this.CancelOptions;
    this.view.customHeader.flxBack.onClick=this.flxBackOnClick;
    this.view.flxAccNoToggleDA.onClick=this.flxAccNoToggleDAOnClick;
    this.view.flxPopupNickName.setVisibility(false); 
    //this.view.customHeader.lblLocateUs.text = this.accDetails.nickName;
    //this.view.title=this.accDetails.nickName; 

    this.view.customHeader.flxHeader.skin ="sknCommonHeaderPrimaryGrey";
    this.view.customHeader.lblLocateUs.centerX = "50%";

    this.view.customHeader.flxHeader.btnRight.text=kony.i18n.getLocalizedString("kony.mb.accdetails.edit");
    this.view.customHeader.flxHeader.btnRight.skin="sknLblb8dcffSSP32px";

    if(this.accDetails.Transactions.Nickname!==""&& this.accDetails.Transactions.Nickname!==null&&typeof this.accDetails.Transactions.Nickname!=="undefined"){
      this.view.customHeader.lblLocateUs.text = this.accDetails.Transactions.Nickname;
      this.view.title= this.accDetails.Transactions.Nickname;
    } 
    else if(this.accDetails.Transactions.description!==""&& this.accDetails.Transactions.description!==null&&typeof this.accDetails.Transactions.description!=="undefined"){
      this.view.customHeader.lblLocateUs.text = this.accDetails.Transactions.description;
      this.view.title= this.accDetails.Transactions.description;
    }   

    this.view.imgAccNoToggle.src = "view.png";
    //this.view.imgRouteNoToggle.src = "view.png";
    this.view.imgAccNoToggleIRA.src = "view.png";
    this.view.imgAccNoToggleHL.src = "view.png"; 	
    this.view.imgAccNoToggleCC.src = "view.png"; 

    var configManager = applicationManager.getConfigurationManager();
    if(String(this.accDetails.type).trim().toLowerCase() === "external") {
      this.createViewForExternalAccountDetails();
    } else {
      //Added By Sirisha(KHC15066)
      if (this.accDetails.selectedAccountData.accountType === configManager.constants.CHECKING || 
          this.accDetails.selectedAccountData.accountType === configManager.constants.SAVINGS) {
        this.createViewForFamilyCheckingAcc();
      }
      else if (this.accDetails.selectedAccountData.accountType === "CD" ||
               this.accDetails.selectedAccountData.accountType === "IRA") {
        this.createViewForCDandIRAAccount();
      }
      else if (this.accDetails.selectedAccountData.accountType === configManager.constants.CREDITCARD ||
               this.accDetails.selectedAccountData.accountType === "LOC") {
        this.createViewForCreditCard();
      }
      else if (this.accDetails.selectedAccountData.accountType === configManager.constants.LOAN ||
               this.accDetails.selectedAccountData.accountType === configManager.constants.MORTGAGE) {
        this.createViewForHomeLoanAcc();
      }
      else if (this.accDetails.selectedAccountData.accountType === configManager.constants.DEPOSIT) {
        this.createViewForDepositAccount();
      }
      //Ends
    }
    var currentForm=navigationManager.getCurrentForm();
    applicationManager.getPresentationFormUtility().logFormName(currentForm);
    applicationManager.getPresentationUtility().dismissLoadingScreen();
  },
  CancelOptions : function(){
    this.view.flxPopupNickName.setVisibility(false);
    this.view.flxHeader.setEnabled(true);
    this.view.flxContainerCheckingAcc.setEnabled(true);
    this.view.flxContainerCreditCard.setEnabled(true);
    this.view.flxContainerHomeLoan.setEnabled(true);
    this.view.flxContainerDepositAccount.setEnabled(true);
  },

  //Starts Added By Sirisha (KHC15066)
  createViewForFamilyCheckingAcc: function() {
    this.view.flxContainerCheckingAcc.setVisibility(true);
    this.view.flxExternalAccountContainer.setVisibility(false);
    this.view.flxContainerCreditCard.setVisibility(false);
    this.view.flxContainerHomeLoan.setVisibility(false);
    this.view.flxContainerDepositAccount.setVisibility(false);
    this.view.flxContainerIraAccount.setVisibility(false);
    this.view.btnCallBank.onClick=this.callBank;
    this.view.btnMsgBank.onClick=this.messageBank;
    this.PopulateCheckingAccount();
    this.view.forceLayout();
  },
  //For Savings and Checkings 
  PopulateCheckingAccount:function() {
    var configManager = applicationManager.getConfigurationManager();
    var forUtility=applicationManager.getFormatUtilManager();

    this.view.flxAccType.setVisibility(false);
    this.view.flxAccHolder.setVisibility(false);
    this.view.flxJointAccHolder.setVisibility(false);

    this.view.flxSeperator.setVisibility(false);
    this.view.flxAccDetails.setVisibility(false);
    this.view.flxSeperator2.skin="sknFlxSeparatorSegments";
    this.view.flxSeperator3.setVisibility(false);
    this.view.btnCallBank.setVisibility(true);
    this.view.btnMsgBank.setVisibility(true);

    this.view.flxBalDetails.skin ="sknFlxDetailsGrey";
    this.view.lblBalanceDetails.text = kony.i18n.getLocalizedString("kony.mb.externalBank.AccountDetials"); 
    this.view.lblBalanceDetails.skin = "sknLbl424242SSP26px";

    if(typeof this.accDetails.selectedAccountData.availableBalance !== "undefined" && this.accDetails.selectedAccountData.availableBalance !== null && this.accDetails.selectedAccountData.availableBalance !== "")
    {
      this.accDetails.selectedAccountData.availableBalance = this.formatValue(this.accDetails.selectedAccountData.availableBalance);
      this.view.lblAvailBalValue.text="$"+(this.accDetails.selectedAccountData.availableBalance);
    }
    else
      this.view.flxAvailBalance.setVisibility(false);

    if(typeof this.accDetails.selectedAccountData.currentBalance !== "undefined" && this.accDetails.selectedAccountData.currentBalance !== null&& this.accDetails.selectedAccountData.currentBalance !=="")
    {
      this.accDetails.selectedAccountData.currentBalance = this.formatValue(this.accDetails.selectedAccountData.currentBalance);
      this.view.lblCurrBalValue.text="$"+(this.accDetails.selectedAccountData.currentBalance);
    }
    else
      this.view.flxCurrentBalance.setVisibility(false);

    if(typeof this.accDetails.Transactions.DivRate !== "undefined" && this.accDetails.Transactions.DivRate !== null && this.accDetails.Transactions.DivRate !=="")
    {
      this.accDetails.Transactions.DivRate = this.formatValue(this.accDetails.Transactions.DivRate);
      this.view.lblRateValue.text = "$"+(this.accDetails.Transactions.DivRate);}
    else
      this.view.flxRate.setVisibility(false);

    if(typeof this.accDetails.Transactions.DivYtd !== "undefined" && this.accDetails.Transactions.DivYtd !== null && this.accDetails.Transactions.DivYtd !=="")
    {
      this.accDetails.Transactions.DivYtd = this.formatValue(this.accDetails.Transactions.DivYtd);
      this.view.lblIntrsdErndYtdValue.text = "$"+(this.accDetails.Transactions.DivYtd);}
    else
      this.view.flxIntrsdErndYtd.setVisibility(false);

    if(typeof this.accDetails.Transactions.DivLastYear !== "undefined" && this.accDetails.Transactions.DivLastYear !== null && this.accDetails.Transactions.DivLastYear !=="")
      {
        this.accDetails.Transactions.DivLastYear = this.formatValue(this.accDetails.Transactions.DivLastYear);
        this.view.lblIntrsdErndLstYrValue.text = "$"+(this.accDetails.Transactions.DivLastYear);}
    else
      this.view.flxIntrsdErndLstYr.setVisibility(false); 

    if(this.accDetails.selectedAccountData.accountType === configManager.constants.CHECKING ){
      this.view.flxPendingDeposit.setVisibility(true);
      this.view.flxPendingWithDraw.setVisibility(true);

      this.view.lblPendingDeposit.text = kony.i18n.getLocalizedString("kony.mb.accdetails.courtesyPayAvailable"); 
      if((typeof this.accDetails.Transactions.OverdraftTolerance !=="undefined" && this.accDetails.Transactions.OverdraftTolerance !== null && this.accDetails.Transactions.OverdraftTolerance !=="") ||
         (typeof this.accDetails.selectedAccountData.currentBalance !== "undefined" && this.accDetails.selectedAccountData.currentBalance !== null && this.accDetails.selectedAccountData.currentBalance !==""))
      {
        if(this.accDetails.selectedAccountData.currentBalance > 0)
        {
          this.accDetails.Transactions.OverdraftTolerance = this.formatValue(this.accDetails.Transactions.OverdraftTolerance);
          this.view.lblPendingDepValue.text="$"+(this.accDetails.Transactions.OverdraftTolerance);}
        else if(this.accDetails.selectedAccountData.currentBalance < 0){
          var absBal = Math.abs(this.accDetails.selectedAccountData.currentBalance);
          var penDepValue = this.accDetails.Transactions.OverdraftTolerance - absBal;
          penDepValue = this.formatValue(penDepValue);
          this.view.lblPendingDepValue.text="$"+(penDepValue);
        }
      } else
        this.view.flxPendingDeposit.setVisibility(false);

      this.view.lblPendingWithdraw.text = kony.i18n.getLocalizedString("kony.mb.accdetails.courtesyPayLimit"); 
      if(typeof this.accDetails.Transactions.OverdraftTolerance !== "undefined" && this.accDetails.Transactions.OverdraftTolerance !== null && this.accDetails.Transactions.OverdraftTolerance !=="")
      {
        this.accDetails.Transactions.OverdraftTolerance = this.formatValue(this.accDetails.Transactions.OverdraftTolerance);
        this.view.lblWithdrawValue.text="$"+(this.accDetails.Transactions.OverdraftTolerance);}
      else
        this.view.flxPendingWithDraw.setVisibility(false);
    }
    if(this.accDetails.selectedAccountData.accountType === configManager.constants.SAVINGS){
      this.view.flxPendingDeposit.setVisibility(false);
      this.view.flxPendingWithDraw.setVisibility(false);
    }

    if(typeof this.accDetails.Transactions.MicrAcctNumber !== "undefined" && this.accDetails.Transactions.MicrAcctNumber !== null && this.accDetails.Transactions.MicrAcctNumber !==""){
      this.view.lblAccNoValue.bottom = "0dp";
      //this.maskAccountNumber(this.accDetails.Transactions.MicrAcctNumber);
      this.view.lblAccNoValue.text=applicationManager.getDataProcessorUtility().maskAccountNumber(this.accDetails.Transactions.MicrAcctNumber);
    } else
      this.view.flxAccountNumber.setVisibility(false);

    this.view.lblRoutingNoValue.bottom = "0dp";
    this.view.lblRoutingNoValue.text="253075028";

    this.view.lblAccNickNameVal.bottom = "0dp";
    if(typeof this.accDetails.Transactions.Nickname !== "undefined" && this.accDetails.Transactions.Nickname !== null && this.accDetails.Transactions.Nickname!==""){
      this.view.lblAccNickNameVal.text=this.accDetails.Transactions.Nickname;
    } else if(typeof this.accDetails.Transactions.description !== "undefined" && this.accDetails.Transactions.description !== null && this.accDetails.Transactions.description !=="")
      this.view.lblAccNickNameVal.text=this.accDetails.Transactions.description;
    else
      this.view.flxAccNickName.setVisibility(false);

    this.view.lblSwiftCode.text = kony.i18n.getLocalizedString("kony.mb.accdetails.openDate");  
    if(typeof this.accDetails.Transactions.OpenDate !== "undefined" && this.accDetails.Transactions.OpenDate !== null && this.accDetails.Transactions.OpenDate !=="") {
      this.view.lblSwiftCodeValue.bottom = "0dp";
      this.view.lblSwiftCodeValue.text = this.accDetails.Transactions.OpenDate;
    }else
      this.view.flxSwiftCode.setVisibility(false);
  },

  createViewForCDandIRAAccount: function() {
    this.view.flxContainerCheckingAcc.setVisibility(false);
    this.view.flxExternalAccountContainer.setVisibility(false);
    this.view.flxContainerCreditCard.setVisibility(false);
    this.view.flxContainerHomeLoan.setVisibility(false);
    this.view.flxContainerDepositAccount.setVisibility(false);
    this.view.flxContainerIraAccount.setVisibility(true);
    this.PopulateCDandIRAAccount();
    this.view.btnCallBankIRA.onClick=this.callBank;
    this.view.btnMsgBankIRA.onClick=this.messageBank;
    this.view.forceLayout();
  },
  //For CD and IRA
  PopulateCDandIRAAccount:function() {
    var configManager = applicationManager.getConfigurationManager();
    var forUtility=applicationManager.getFormatUtilManager();

    this.view.flxBankDetailsIRA.setVisibility(false);
    this.view.flxSeperatorIRA.setVisibility(false);
    this.view.flxDividerIRA.setVisibility(false);
    this.view.flxDivider2IRA.setVisibility(false);
    this.view.btnCallBankIRA.setVisibility(true);
    this.view.btnMsgBankIRA.setVisibility(true);
    this.view.flxSeperator2IRA.skin="sknFlxSeparatorSegments";

    this.view.flxAccountDetailsIRA.skin ="sknFlxDetailsGrey";
    this.view.lblAccountDetailsIRA.skin = "sknLbl424242SSP26px";

    if(typeof this.accDetails.selectedAccountData.currentBalance !== "undefined" && this.accDetails.selectedAccountData.currentBalance !== null && this.accDetails.selectedAccountData.currentBalance !=="")
      {
        this.accDetails.selectedAccountData.currentBalance = this.formatValue(this.accDetails.selectedAccountData.currentBalance);
        this.view.lblCurrBalValueIRA.text="$"+(this.accDetails.selectedAccountData.currentBalance);}
    else
      this.view.flxCurrentBalanceIRA.setVisibility(false); 

    if(typeof this.accDetails.Transactions.DivRate !== "undefined" && this.accDetails.Transactions.DivRate !== null && this.accDetails.Transactions.DivRate !=="")
      {
        this.accDetails.Transactions.DivRate = this.formatValue(this.accDetails.Transactions.DivRate);
        this.view.lblRateValueIRA.text = "$"+(this.accDetails.Transactions.DivRate);}
    else
      this.view.flxRateIRA.setVisibility(false);

    if(typeof this.accDetails.Transactions.DivYtd !== "undefined" && this.accDetails.Transactions.DivYtd !== null && this.accDetails.Transactions.DivYtd !=="")
      {
        this.accDetails.Transactions.DivYtd = this.formatValue(this.accDetails.Transactions.DivYtd);
        this.view.lblIntrsdErndYtdValueIRA.text = "$"+(this.accDetails.Transactions.DivYtd);}
    else
      this.view.flxIntrsdErndYtdIRA.setVisibility(false);

    if(typeof this.accDetails.Transactions.DivLastYear !== "undefined" && this.accDetails.Transactions.DivLastYear !== null && this.accDetails.Transactions.DivLastYear !== "")
      {
        this.accDetails.Transactions.DivLastYear = this.formatValue(this.accDetails.Transactions.DivLastYear);
        this.view.lblIntrsdErndLstYrValueIRA.text = "$"+(this.accDetails.Transactions.DivLastYear);}
    else
      this.view.flxIntrsdErndLstYrIRA.setVisibility(false);

    if(typeof this.accDetails.Transactions.DivType !=="undefined" && this.accDetails.Transactions.DivType !== null && this.accDetails.Transactions.DivType !==""){
      if(this.accDetails.Transactions.DivType === 50)
        this.view.lblIntrstFreqValueIRA.text=kony.i18n.getLocalizedString("kony.mb.frequency.Quaterly");
      else  if(this.accDetails.Transactions.DivType === 51)
        this.view.lblIntrstFreqValueIRA.text=kony.i18n.getLocalizedString("kony.mb.frequency.Monthly");
      else
        this.view.lblIntrstFreqValueIRA.text="Daily";
    }else
      this.view.flxIntrstFreqIRA.setVisibility(false);

    if(typeof this.accDetails.Transactions.DivPostCode !== "undefined" && this.accDetails.Transactions.DivPostCode !== null && this.accDetails.Transactions.DivPostCode !=="")
      if(this.accDetails.Transactions.DivPostCode === "0")
        this.view.lblIntrstOptnValueIRA.text=kony.i18n.getLocalizedString("kony.mb.accdetails.compoudIt");
      else if(this.accDetails.Transactions.DivPostCode === "1")
        this.view.lblIntrstOptnValueIRA.text=kony.i18n.getLocalizedString("kony.mb.accdetails.mailToU");
      else if(this.accDetails.Transactions.DivPostCode === "2")
        this.view.lblIntrstOptnValueIRA.text=kony.i18n.getLocalizedString("kony.mb.accdetails.trnsfrToAcc");
      else if(this.accDetails.Transactions.DivPostCode === "3")
        this.view.lblIntrstOptnValueIRA.text=kony.i18n.getLocalizedString("kony.mb.accdetails.givenUp");  
      else
        this.view.flxIntrstOptnIRA.setVisibility(false);

    if(this.accDetails.selectedAccountData.accountType === "CD"){
      this.view.flxContributionIRA.setVisibility(false);
      this.view.flxContributionContainerIRA.setVisibility(false);
      this.view.flxDistributionIRA.setVisibility(false);
      this.view.flxDistributionContainerIRA.setVisibility(false);
      this.view.flxOriginalDepIRA.setVisibility(true);
      if(typeof this.accDetails.Transactions.OriginalDeposit !== "undefined" && this.accDetails.Transactions.OriginalDeposit !== null && this.accDetails.Transactions.OriginalDeposit !== "")
        {
          this.accDetails.Transactions.OriginalDeposit = this.formatValue(this.accDetails.Transactions.OriginalDeposit);
          this.view.lblOriginalDepValueIRA.text = "$"+(this.accDetails.Transactions.OriginalDeposit);}
      else
        this.view.flxOriginalDepIRA.setVisibility(false);
    }
    if(this.accDetails.selectedAccountData.accountType === "IRA"){
      this.view.flxOriginalDepIRA.setVisibility(false);
      this.view.flxContributionIRA.setVisibility(false);
      this.view.flxContributionContainerIRA.setVisibility(false);
      this.view.flxDistributionIRA.setVisibility(false);
      this.view.flxDistributionContainerIRA.setVisibility(false);
    }

    /*if(this.accDetails.selectedAccountData.accountType === "IRA"){
      this.view.flxContributionIRA.setVisibility(true);
      this.view.flxContributionContainerIRA.setVisibility(true);
      this.view.flxDistributionIRA.setVisibility(true);
      this.view.flxDistributionContainerIRA.setVisibility(true);

    if(typeof this.accDetails.Transactions. !=="undefined" && this.accDetails.Transactions. !== null && this.accDetails.Transactions. !=="")
      this.view.lblCurrCalYrValueIRA.text="$"+(this.accDetails.Transactions.DivType);
    else
      this.view.flxCurrCalYrIRA.setVisibility(false);

     if(typeof this.accDetails.Transactions. !=="undefined" && this.accDetails.Transactions. !== null && this.accDetails.Transactions. !=="")
      this.view.lblPrevCalYrValueIRA.text="$"+(this.accDetails.Transactions.DivType);
    else
      this.view.flxPrevCalYrIRA.setVisibility(false);

     if(typeof this.accDetails.Transactions. !=="undefined" && this.accDetails.Transactions. !== null && this.accDetails.Transactions. !=="")
      this.view.lblCurrCalYrValue2IRA.text="$"+(this.accDetails.Transactions.DivType);
    else
      this.view.flxCurrCalYr2IRA.setVisibility(false);

     if(typeof this.accDetails.Transactions. !=="undefined" && this.accDetails.Transactions. !== null && this.accDetails.Transactions. !=="")
      this.view.lblPrevCalYrValue2IRA.text="$"+(this.accDetails.Transactions.DivType);
    else
      this.view.flxPrevCalYr2IRA.setVisibility(false);
    }*/

    if ((typeof this.accDetails.selectedAccountData.primaryAccntNumber !== "undefined" && this.accDetails.selectedAccountData.primaryAccntNumber !== null && this.accDetails.selectedAccountData.primaryAccntNumber !== "") &&
        (typeof this.accDetails.selectedAccountData.shareID !== "undefined" && this.accDetails.selectedAccountData.shareID !== null && this.accDetails.selectedAccountData.shareID !== "")){
      var accountNumber = applicationManager.getDataProcessorUtility().maskAccountNumber(this.accDetails.selectedAccountData.primaryAccntNumber);
      var shareId = this.accDetails.selectedAccountData.shareID;
      this.view.lblAccNoValueIRA.text = accountNumber + "-S"+shareId;
    } else
      this.view.flxAccountNumberIRA.setVisibility(false);

    if(typeof this.accDetails.Transactions.Nickname !== "undefined" && this.accDetails.Transactions.Nickname !== null && this.accDetails.Transactions.Nickname !=="")
      this.view.lblAccNickNameValIRA.text=this.accDetails.Transactions.Nickname;
    else if(typeof this.accDetails.Transactions.description !== "undefined" && this.accDetails.Transactions.description !== null && this.accDetails.Transactions.description !=="")
      this.view.lblAccNickNameValIRA.text=this.accDetails.Transactions.description;
    else
      this.view.flxAccNickNameIRA.setVisibility(false);

    if(typeof this.accDetails.Transactions.OpenDate !== "undefined" && this.accDetails.Transactions.OpenDate !== null&& this.accDetails.Transactions.OpenDate !== "") {
      this.view.lblOpenDateValueIRA.text = this.accDetails.Transactions.OpenDate;
    }else
      this.view.flxOpenDateIRA.setVisibility(false);

    if(typeof this.accDetails.Transactions.MaturityDate !== "undefined" && this.accDetails.Transactions.MaturityDate !== null&& this.accDetails.Transactions.MaturityDate !== "") {
      this.view.lblMatDateValueIRA.text = this.accDetails.Transactions.MaturityDate;
    }else
      this.view.flxMaturityDateIRA.setVisibility(false);

    if(typeof this.accDetails.Transactions.MaturityPostCode !== "undefined" && this.accDetails.Transactions.MaturityPostCode !== null&& this.accDetails.Transactions.MaturityPostCode !=="")
      if(this.accDetails.Transactions.MaturityPostCode === "0")
        this.view.lblMatOptValueIRA.text=kony.i18n.getLocalizedString("kony.mb.accdetails.autoRenew");
      else if(this.accDetails.Transactions.MaturityPostCode === "1")
        this.view.lblMatOptValueIRA.text=kony.i18n.getLocalizedString("kony.mb.accdetails.sendByCheck");
      else if(this.accDetails.Transactions.MaturityPostCode === "2")
        this.view.lblMatOptValueIRA.text=kony.i18n.getLocalizedString("kony.mb.transaction.transfer");
      else if(this.accDetails.Transactions.MaturityPostCode === "3")
        this.view.lblMatOptValueIRA.text=kony.i18n.getLocalizedString("kony.mb.accdetails.suspend");  
      else if(this.accDetails.Transactions.MaturityPostCode === "4")
        this.view.lblMatOptValueIRA.text=kony.i18n.getLocalizedString("kony.mb.accdetails.trnsfrNRenew");
      else
        this.view.flxMaturityOptionIRA.setVisibility(false);
  },

  createViewForHomeLoanAcc: function() {
    this.view.flxContainerCheckingAcc.setVisibility(false);
    this.view.flxContainerCreditCard.setVisibility(false);
    this.view.flxContainerHomeLoan.setVisibility(true);
    this.view.flxExternalAccountContainer.setVisibility(false);
    this.view.flxContainerDepositAccount.setVisibility(false);
    this.view.flxContainerIraAccount.setVisibility(false);
    this.populateLoanAccount();
    this.view.btnCallBankHL.onClick=this.callBank;
    this.view.btnMsgBankHL.onClick=this.messageBank;
    this.view.forceLayout();
  },
  //For loans 
  populateLoanAccount : function()
  {
    var configManager = applicationManager.getConfigurationManager();
    var forUtility=applicationManager.getFormatUtilManager();

    this.view.flxSeperatorHL.setVisibility(false);
    this.view.flxInterestDetails.setVisibility(false);
    this.view.flxDivider.setVisibility(false);
    this.view.lblLastPmtAmt.setVisibility(false);
    this.view.lblLastPmtAmtValue.setVisibility(false);
    this.view.lblPayOffAmt.setVisibility(false);
    this.view.lblPayOffAmtValue.setVisibility(false);
    this.view.flxAccDetailsHL.setVisibility(false);
    this.view.flxSeperatorHL1.skin="sknFlxSeparatorSegments";
    this.view.flxPropertyAddress.setVisibility(false);
    this.view.flxSeperatorHL3.setVisibility(false);
    this.view.btnCallBank.setVisibility(true);
    this.view.btnMsgBank.setVisibility(true);

    this.view.flxBalDetailsHL.skin ="sknFlxDetailsGrey";
    this.view.lblBalanceDetailsHL.text = kony.i18n.getLocalizedString("kony.mb.externalBank.AccountDetials"); 
    this.view.lblBalanceDetailsHL.skin = "sknLbl424242SSP26px";

    this.view.lblOrgnlLoanAmt.text = kony.i18n.getLocalizedString("kony.mb.accounts.CurrentBalance"); 
    if(typeof this.accDetails.selectedAccountData.currentBalance !== "undefined" && this.accDetails.selectedAccountData.currentBalance !== null && this.accDetails.selectedAccountData.currentBalance !=="")
     { 
       this.accDetails.selectedAccountData.currentBalance = this.formatValue(this.accDetails.selectedAccountData.currentBalance);
       this.view.lblOutstandingBalVal.text="$"+(this.accDetails.selectedAccountData.currentBalance);}
    else
      this.view.flxOrgnlLoanAmt.setVisibility(false);

    this.view.lblPrincipalBal.text = kony.i18n.getLocalizedString("kony.mb.BillPay.PaymentAmount");
    if(typeof this.accDetails.Transactions.PaymentDue !== "undefined" && this.accDetails.Transactions.PaymentDue !== null && this.accDetails.Transactions.PaymentDue !=="")
     { 
        this.accDetails.Transactions.PaymentDue = this.formatValue(this.accDetails.Transactions.PaymentDue);
       	this.view.lblPrincipalBalVal.text="$"+(this.accDetails.Transactions.PaymentDue);}
    else
      this.view.flxPrincipalBal.setVisibility(fasle);

    this.view.lblLastPmtDate.text = kony.i18n.getLocalizedString("kony.mb.accdetails.dueDate");
    if(typeof this.accDetails.Transactions.DueDate !== "undefined" && this.accDetails.Transactions.DueDate !== null && this.accDetails.Transactions.DueDate !==""){
      this.view.lblLastPmtDateValue.text=  this.accDetails.Transactions.DueDate;
    }
    else
      this.view.flxLastPmtDate.setVisibility(false);

    this.view.lblPrincipalAmt.text = kony.i18n.getLocalizedString("kony.mb.accdetails.term");
    this.view.lblPrincipalAmtValue.text="$00.00";
    /*if(typeof this.accDetails.term !== "undefined" && this.accDetails.term !== null && this.accDetails.term !== "")
     this.view.lblPrincipalAmtValue.text="$"+(this.accDetails.term);
    else
    this.view.flxPrincipalAmt.setVisibility(false); */

    this.view.lblInterestRate.text = kony.i18n.getLocalizedString("kony.mb.accdetails.rate");
    if(typeof this.accDetails.Transactions.InterestRate !== "undefined" && this.accDetails.Transactions.InterestRate !== null && this.accDetails.Transactions.InterestRate !=="")
     { 
       this.accDetails.Transactions.InterestRate = this.formatValue(this.accDetails.Transactions.InterestRate);
       this.view.lblInterestRateValue.text="$"+(this.accDetails.Transactions.InterestRate);}
    else
      this.view.flxInterestRate.setVisibility(false);

    this.view.lblInterestPaid.text = kony.i18n.getLocalizedString("kony.mb.accdetails.interestPaidYTD");
    if(typeof this.accDetails.Transactions.InterestYtd !== "undefined" && this.accDetails.Transactions.InterestYtd !== null && this.accDetails.Transactions.InterestYtd !=="")
      {
        this.accDetails.Transactions.InterestYtd = this.formatValue(this.accDetails.Transactions.InterestYtd);
        this.view.lblInterestPaidValue.text="$"+(this.accDetails.Transactions.InterestYtd);}
    else
      this.view.flxInterestPaid.setVisibility(false);

    this.view.lblInterestPaid.text = kony.i18n.getLocalizedString("kony.mb.accdetails.interestPaidLY");
    if(typeof this.accDetails.Transactions.InterestLastYear !== "undefined" && this.accDetails.Transactions.InterestLastYear !== null && this.accDetails.Transactions.InterestLastYear !=="")   
      this.view.lblIntPaidLastYearValue.text="$"+(this.accDetails.Transactions.InterestLastYear);
    else
      this.view.flxInterestPaidLastYear.setVisibility(false);

    if(typeof this.accDetails.Transactions.MicrAcctNumber !== "undefined" && this.accDetails.Transactions.MicrAcctNumber !== null && this.accDetails.Transactions.MicrAcctNumber !=="") {  
      this.view.lblAccNoValueHL.bottom="0dp"; 
      this.view.lblAccNoValueHL.text=applicationManager.getDataProcessorUtility().maskAccountNumber(this.accDetails.Transactions.MicrAcctNumber);
    }else
      this.view.flxAccountNumberHL.setVisibility(false);

    this.view.lblAccTypeValHL.bottom="0dp";
    if(typeof this.accDetails.Transactions.Nickname !== "undefined" && this.accDetails.Transactions.Nickname !== null && this.accDetails.Transactions.Nickname !=="")
      this.view.lblAccTypeValHL.text=this.accDetails.Transactions.Nickname;
    else if(typeof this.accDetails.Transactions.description !== "undefined" && this.accDetails.Transactions.description !== null&& this.accDetails.Transactions.description !=="")
      this.view.lblAccTypeValHL.text=this.accDetails.Transactions.description;
    else
      this.view.flxAccTypeHL.setVisibility(false);

    this.view.lblLoanType.text = kony.i18n.getLocalizedString("kony.mb.accdetails.paymentFrequency");
    if(typeof this.accDetails.Transactions.PaymentFrequency !== "undefined" && this.accDetails.Transactions.PaymentFrequency !== null && this.accDetails.Transactions.PaymentFrequency !=="") {
      this.view.lblLoanTypeValue.bottom="0dp";  
      if(this.accDetails.Transactions.PaymentFrequency === "0")
        this.view.lblLoanTypeValue.text=kony.i18n.getLocalizedString("kony.mb.frequency.singlePay");
      else if(this.accDetails.Transactions.PaymentFrequency === "1")
        this.view.lblLoanTypeValue.text=kony.i18n.getLocalizedString("kony.mb.frequency.annual");
      else if(this.accDetails.Transactions.PaymentFrequency === "2")
        this.view.lblLoanTypeValue.text=kony.i18n.getLocalizedString("kony.mb.frequency.HalfYearly");
      else if(this.accDetails.Transactions.PaymentFrequency === "3")
        this.view.lblLoanTypeValue.text=kony.i18n.getLocalizedString("kony.mb.frequency.Quaterly"); 
      else if(this.accDetails.Transactions.PaymentFrequency === "4" || this.accDetails.Transactions.PaymentFrequency === "14")
        this.view.lblLoanTypeValue.text=kony.i18n.getLocalizedString("kony.mb.frequency.Monthly");
      else if(this.accDetails.Transactions.PaymentFrequency === "5")
        this.view.lblLoanTypeValue.text=kony.i18n.getLocalizedString("kony.mb.frequency.twicePerMonth");
      else if(this.accDetails.Transactions.PaymentFrequency === "6"|| this.accDetails.Transactions.PaymentFrequency === "7" || this.accDetails.Transactions.PaymentFrequency === "8")
        this.view.lblLoanTypeValue.text=kony.i18n.getLocalizedString("kony.mb.frequency.BiWeekly");
      else if(this.accDetails.Transactions.PaymentFrequency === "9"|| this.accDetails.Transactions.PaymentFrequency === "11" || this.accDetails.Transactions.PaymentFrequency === "12")
        this.view.lblLoanTypeValue.text=kony.i18n.getLocalizedString("kony.mb.frequency.Weekly");
      else if(this.accDetails.Transactions.PaymentFrequency === "10")
        this.view.lblLoanTypeValue.text=kony.i18n.getLocalizedString("kony.mb.frequency.immediate");
      else if(this.accDetails.Transactions.PaymentFrequency === "13")
        this.view.lblLoanTypeValue.text=kony.i18n.getLocalizedString("kony.mb.frequency.everyOtherMonth");
    }else
      this.view.flxLoanTypeHL.setVisibility(false);

    this.view.lblLoanOriginationDate.text=kony.i18n.getLocalizedString("kony.mb.accdetails.openDate"); 
    if(typeof this.accDetails.Transactions.OpenDate !== "undefined" && this.accDetails.Transactions.OpenDate !== null && this.accDetails.Transactions.OpenDate !=="") {
      this.view.lblLoanOriginationDateVal.bottom="0dp";
      this.view.lblLoanOriginationDateVal.text=  this.accDetails.Transactions.OpenDate;
    }else
      this.view.flxOriginationDate.setVisibility(false); 
  },

  createViewForCreditCard: function() {
    this.view.flxContainerCheckingAcc.setVisibility(false);
    this.view.flxContainerCreditCard.setVisibility(true);
    this.view.flxExternalAccountContainer.setVisibility(false);
    this.view.flxContainerHomeLoan.setVisibility(false);
    this.view.flxContainerDepositAccount.setVisibility(false);
    this.view.flxContainerIraAccount.setVisibility(false);
    this.populateCreditCardAccount();
    this.view.btnCallBankCC.onClick=this.callBank;
    this.view.btnMsgBankCC.onClick=this.messageBank;
    this.view.forceLayout();
  },

  //Credit Card and LOC
  populateCreditCardAccount:function() {
    var configManager = applicationManager.getConfigurationManager();
    var forUtility=applicationManager.getFormatUtilManager();

    this.view.flxSeperatorCC.setVisibility(false);
    this.view.flxBalSummary.setVisibility(false);
    this.view.flxDividerCC.setVisibility(false);
    this.view.flxSeperatorCC1.skin="sknFlxSeparatorSegments";
    this.view.flxAccDetailsCC.setVisibility(false);
    this.view.flxCardHolderName.setVisibility(false);
    this.view.flxSeperatorCC3.setVisibility(false);
    this.view.btnCallBankCC.setVisibility(true);
    this.view.btnMsgBankCC.setVisibility(true);

    this.view.flxBalDetailsCC.skin ="sknFlxDetailsGrey";
    this.view.lblBalanceDetailsCC.text = kony.i18n.getLocalizedString("kony.mb.externalBank.AccountDetials");
    this.view.lblBalanceDetailsCC.skin = "sknLbl424242SSP26px";

    if(typeof this.accDetails.selectedAccountData.currentBalance !== "undefined" && this.accDetails.selectedAccountData.currentBalance !== null && this.accDetails.selectedAccountData.currentBalance !=="")
    {
      this.accDetails.selectedAccountData.currentBalance = this.formatValue(this.accDetails.selectedAccountData.currentBalance);
      this.view.lblCurrentBalanceValue.text = "$"+this.accDetails.selectedAccountData.currentBalance;
    }
    else
      this.view.flxCurrentBalanceCC.setVisibility(false);

    if(typeof this.accDetails.Transactions.PaymentDue !== "undefined" && this.accDetails.Transactions.PaymentDue !== null && this.accDetails.Transactions.PaymentDue !=="")
      {
        this.accDetails.Transactions.PaymentDue = this.formatValue(this.accDetails.Transactions.PaymentDue);
        this.view.lblPaymentAmountCCValue.text="$"+(this.accDetails.Transactions.PaymentDue);}
    else
      this.view.flxPaymentAmountCC.setVisibility(false);

    if(typeof this.accDetails.Transactions.DueDate !== "undefined" && this.accDetails.Transactions.DueDate !== null && this.accDetails.Transactions.DueDate !==""){
      this.view.lblDueDateCCValue.text=  this.accDetails.Transactions.DueDate;
    }
    else
      this.view.flxDueDateCC.setVisibility(false);

    if(typeof this.accDetails.Transactions.AvailableCredit !== "undefined" && this.accDetails.Transactions.AvailableCredit !== null && this.accDetails.Transactions.AvailableCredit !=="")
      {
        this.accDetails.Transactions.AvailableCredit = this.formatValue(this.accDetails.Transactions.AvailableCredit);
        this.view.lblAvailCreditCCValue.text="$"+(this.accDetails.Transactions.AvailableCredit);}
    else
      this.view.flxAvailCreditCC.setVisibility(false);

    if((typeof this.accDetails.Transactions.CreditLimit !== "undefined" && this.accDetails.Transactions.CreditLimit !== null && this.accDetails.Transactions.CreditLimit !=="") ||
       (this.accDetails.Transactions.CredLimitExpiration!=="undefined" && this.accDetails.Transactions.CredLimitExpiration!==null))
      if(this.accDetails.Transactions.CredLimitExpiration !== "")
        {
          this.accDetails.Transactions.CreditLimit = this.formatValue(this.accDetails.Transactions.CreditLimit);
          this.view.lblCreditLmtValue.text="$"+(this.accDetails.Transactions.CreditLimit);
        }
      else
        this.view.lblCreditLmtValue.text="$"+("00.00");
    else
      this.view.flxCreditLimitCC.setVisibility(false);    

    /*if(typeof this.accDetails.Transactions.RewardBalance !== "undefined" && this.accDetails.Transactions.RewardBalance !== null && this.accDetails.Transactions.RewardBalance !=="")
      this.view.lblRwdBalValue.text="$"+(this.accDetails.Transactions.RewardBalance);
    else
      this.view.flxRwdBalCC.setVisibility(false);*/
    this.view.lblRwdBalValue.text="$"+("00.00");

    if(this.accDetails.selectedAccountData.accountType === configManager.constants.CREDITCARD){
      this.view.flxCashAdvRateCC.setVisibility(true);
      this.view.flxBalTransferRateCC.setVisibility(true);  

      if(typeof this.accDetails.loanData.BalanceXfrInterestRate !== "undefined" && this.accDetails.loanData.BalanceXfrInterestRate !== null && this.accDetails.loanData.BalanceXfrInterestRate !=="") {
        this.accDetails.loanData.BalanceXfrInterestRate = this.formatValue(this.accDetails.loanData.BalanceXfrInterestRate);
        this.view.lblCashAdvRateCCValue.text="$"+this.accDetails.loanData.BalanceXfrInterestRate;
      }
      else
        this.view.flxCashAdvRateCC.setVisibility(false);

      if(typeof this.accDetails.loanData.CashAdvanceInterestRate !== "undefined" && this.accDetails.loanData.CashAdvanceInterestRate !== null && this.accDetails.loanData.CashAdvanceInterestRate !=="")
        {
          this.accDetails.loanData.CashAdvanceInterestRate = this.formatValue(this.accDetails.loanData.CashAdvanceInterestRate);
          this.view.lblBalTransRateCCValue.text="$"+(this.accDetails.loanData.CashAdvanceInterestRate);}
      else
        this.view.flxBalTransferRateCC.setVisibility(false);  
    }

    if(this.accDetails.selectedAccountData.accountType === "LOC"){
      this.view.flxCashAdvRateCC.setVisibility(false);
      this.view.flxBalTransferRateCC.setVisibility(false);
    }

    this.view.lblntRate.text = kony.i18n.getLocalizedString("kony.mb.accdetails.rate");
    if(typeof this.accDetails.Transactions.InterestRate !== "undefined" && this.accDetails.Transactions.InterestRate !== null && this.accDetails.Transactions.InterestRate !=="")
      {
        this.accDetails.Transactions.InterestRate = this.formatValue(this.accDetails.Transactions.InterestRate);
        this.view.lblntRateValue.text="$"+(this.accDetails.Transactions.InterestRate);}
    else
      this.view.flxIntRateCC.setVisibility(false);

    if(typeof this.accDetails.Transactions.InterestYtd !== "undefined" && this.accDetails.Transactions.InterestYtd !== null && this.accDetails.Transactions.InterestYtd !=="")
      {
        this.accDetails.Transactions.InterestYtd = this.formatValue(this.accDetails.Transactions.InterestYtd);
        this.view.lblIntrsdPaidYtdCCValue.text="$"+(this.accDetails.Transactions.InterestYtd);}
    else
      this.view.flxIntrsdPaidYtdCC.setVisibility(false);

    if(typeof this.accDetails.Transactions.InterestLastYear !== "undefined" && this.accDetails.Transactions.InterestLastYear !== null && this.accDetails.Transactions.InterestLastYear !=="")   
      {
        this.accDetails.Transactions.InterestLastYear = this.formatValue(this.accDetails.Transactions.InterestLastYear);
        this.view.lblIntrsdPaidLstYrCCValue.text="$"+(this.accDetails.Transactions.InterestLastYear);}
    else
      this.view.flxIntrsdPaidLstYrCC.setVisibility(false);

    if(typeof this.accDetails.Transactions.MicrAcctNumber !== "undefined" && this.accDetails.Transactions.MicrAcctNumber !== null && this.accDetails.Transactions.MicrAcctNumber !=="") {  
      this.view.lblAccNoValueCC.bottom="0dp"; 
      this.view.lblAccNoValueCC.text=applicationManager.getDataProcessorUtility().maskAccountNumber(this.accDetails.Transactions.MicrAcctNumber);
    }else
      this.view.flxAccountNumberCC.setVisibility(false);

    this.view.lblNickNameValue.bottom="0dp";
    if(typeof this.accDetails.Transactions.Nickname !== "undefined" && this.accDetails.Transactions.Nickname !== null && this.accDetails.Transactions.Nickname !== "")
      this.view.lblNickNameValue.text=this.accDetails.Transactions.Nickname;
    else if(typeof this.accDetails.Transactions.description !== "undefined" && this.accDetails.Transactions.description !== null && this.accDetails.Transactions.description !== "")
      this.view.lblNickNameValue.text=this.accDetails.Transactions.description;
    else
      this.view.flxNickName.setVisibility(false);

    if(this.accDetails.selectedAccountData.accountType === configManager.constants.CREDITCARD)
      this.view.flxCardType.setVisibility(false);

    if(this.accDetails.selectedAccountData.accountType === "LOC"){
      this.view.flxCardType.setVisibility(true);
      this.view.lblCardType.text = kony.i18n.getLocalizedString("kony.mb.accdetails.paymentFrequency");
      if(typeof this.accDetails.Transactions.PaymentFrequency !== "undefined" && this.accDetails.Transactions.PaymentFrequency !== null && this.accDetails.Transactions.PaymentFrequency !=="") {
        this.view.lblCardTypeValue.bottom="0dp";  
        if(this.accDetails.Transactions.PaymentFrequency === "0")
          this.view.lblCardTypeValue.text=kony.i18n.getLocalizedString("kony.mb.frequency.singlePay");
        else if(this.accDetails.Transactions.PaymentFrequency === "1")
          this.view.lblCardTypeValue.text=kony.i18n.getLocalizedString("kony.mb.frequency.annual");
        else if(this.accDetails.Transactions.PaymentFrequency === "2")
          this.view.lblCardTypeValue.text=kony.i18n.getLocalizedString("kony.mb.frequency.HalfYearly");
        else if(this.accDetails.Transactions.PaymentFrequency === "3")
          this.view.lblCardTypeValue.text=kony.i18n.getLocalizedString("kony.mb.frequency.Quaterly"); 
        else if(this.accDetails.Transactions.PaymentFrequency === "4" || this.accDetails.Transactions.PaymentFrequency === "14")
          this.view.lblCardTypeValue.text=kony.i18n.getLocalizedString("kony.mb.frequency.Monthly");
        else if(this.accDetails.Transactions.PaymentFrequency === "5")
          this.view.lblCardTypeValue.text=kony.i18n.getLocalizedString("kony.mb.frequency.twicePerMonth");
        else if(this.accDetails.Transactions.PaymentFrequency === "6"|| this.accDetails.Transactions.PaymentFrequency === "7" || this.accDetails.Transactions.PaymentFrequency === "8")
          this.view.lblCardTypeValue.text=kony.i18n.getLocalizedString("kony.mb.frequency.BiWeekly");
        else if(this.accDetails.Transactions.PaymentFrequency === "9"|| this.accDetails.Transactions.PaymentFrequency === "11" || this.accDetails.Transactions.PaymentFrequency === "12")
          this.view.lblCardTypeValue.text=kony.i18n.getLocalizedString("kony.mb.frequency.Weekly");
        else if(this.accDetails.Transactions.PaymentFrequency === "10")
          this.view.lblCardTypeValue.text=kony.i18n.getLocalizedString("kony.mb.frequency.immediate");
        else if(this.accDetails.Transactions.PaymentFrequency === "13")
          this.view.lblCardTypeValue.text=kony.i18n.getLocalizedString("kony.mb.frequency.everyOtherMonth");
      }else
        this.view.flxCardType.setVisibility(false);
    }
    this.view.lblCardIssueDate.text=kony.i18n.getLocalizedString("kony.mb.accdetails.openDate"); 
    this.view.lblCardIssueDateVal.bottom="0dp";
    if(typeof this.accDetails.Transactions.OpenDate !== "undefined" && this.accDetails.Transactions.OpenDate !== null && this.accDetails.Transactions.OpenDate !=="") {
      this.view.lblCardIssueDateVal.text=  this.accDetails.Transactions.OpenDate;
    }else
      this.view.flxCardIssueDate.setVisibility(false); 
  },

  callBank:function() {     
    kony.phone.dial("7043750183");  
  },

  messageBank:function() { 
    kony.phone.openEmail(["info@cmcu.org"], [],  [],  " ", " ",false, {},0,true);
  },
  flxBackOnClick:function(){
    var navMan=applicationManager.getNavigationManager();
    navMan.goBack();
  },
  //To masking the accountNumber
  maskAccountNumber:function(accNumber){
    var str1 = accNumber.slice(0, 5);
    var maskedValue = accNumber.slice(5,9);
    var str2 = accNumber.slice(9,13);
    maskedValue = maskedValue.replace(maskedValue, 'XXXX');
    return str1+ maskedValue + str2;
  },
  //Formatting value ex: 0.0 to 00.0
  formatValue : function(value){
    var parts = value.split(".");
    if(parts[0].length === 1){
      parts[0] ="0"+parts[0];
    }
    value = parts[0] + "."+ parts[1];
    return value;
  },
  flxAccNoToggleCCOnClick: function() {
    if (this.view.imgAccNoToggleCC.src === "view.png") {
      this.view.imgAccNoToggleCC.src = "viewactive.png";
      this.view.lblAccNoValueCC.text = this.accDetails.Transactions.MicrAcctNumber;
      this.view.flxAccountNumberCC.forceLayout();
    } else {
      this.view.imgAccNoToggleCC.src = "view.png";
      this.view.lblAccNoValueCC.text = applicationManager.getDataProcessorUtility().maskAccountNumber(this.accDetails.Transactions.MicrAcctNumber);
      this.view.flxAccountNumberCC.forceLayout();
    }
  },
  flxRouteNoToggleOnClick: function() {
    if (this.view.imgRouteNoToggle.src === "view.png") {
      this.view.imgRouteNoToggle.src = "viewactive.png";
      this.view.lblRoutingNoValue.text = "253075028";
      this.view.flxRouteNoToggle.forceLayout();
    } else {
      this.view.imgRouteNoToggle.src = "view.png";
      this.view.lblRoutingNoValue.text = "253075028";
      this.view.flxRouteNoToggle.forceLayout();
    }
  },
  flxAccNoToggleHLToggleOnClick: function() {
    if (this.view.imgAccNoToggleHL.src === "view.png") {
      this.view.imgAccNoToggleHL.src = "viewactive.png";
      this.view.lblAccNoValueHL.text = this.accDetails.Transactions.MicrAcctNumber;
      this.view.flxAccountNumberHL.forceLayout();
    } else {
      this.view.imgAccNoToggleHL.src = "view.png";
      this.view.lblAccNoValueHL.text = applicationManager.getDataProcessorUtility().maskAccountNumber(this.accDetails.Transactions.MicrAcctNumber);
      this.view.flxAccountNumberHL.forceLayout();
    }
  },
  flxAccNoToggleOnClick: function() {
    if (this.view.imgAccNoToggle.src === "view.png") {
      this.view.imgAccNoToggle.src = "viewactive.png";
      this.view.lblAccNoValue.text = this.accDetails.Transactions.MicrAcctNumber;
      this.view.flxAccountNumber.forceLayout();
    } else {
      this.view.imgAccNoToggle.src = "view.png";
      this.view.lblAccNoValue.text = applicationManager.getDataProcessorUtility().maskAccountNumber(this.accDetails.Transactions.MicrAcctNumber);
      this.view.flxAccountNumber.forceLayout();
    }
  },

  flxAccNoToggleIRAOnClick:function(){
    var accountNumber = this.accDetails.selectedAccountData.primaryAccntNumber;
    var shareId = this.accDetails.selectedAccountData.shareID;
    if (this.view.imgAccNoToggleIRA.src === "view.png") {
      this.view.imgAccNoToggleIRA.src = "viewactive.png";
      this.view.lblAccNoValueIRA.text = accountNumber + "-S"+shareId;
      this.view.flxAccountNumberIRA.forceLayout();
    } else {
      this.view.imgAccNoToggleIRA.src = "view.png";
      accountNumber = applicationManager.getDataProcessorUtility().maskAccountNumber(accountNumber);
      this.view.lblAccNoValueIRA.text = accountNumber + "-S"+shareId;
      this.view.flxAccountNumberIRA.forceLayout();
    }
  },

  btnEditNickNameOnClick:function(){
    applicationManager.getPresentationUtility().showLoadingScreen();
    var navMan=applicationManager.getNavigationManager();
    var isExternal = this.accDetails.type === "external" ? true : false ;
    if(isExternal) {
      navMan.setCustomInfo("frmAccInfoEdit", this.accDetails.externalAccountDetails.NickName);
    }
    else {
      if(typeof this.accDetails.Transactions.Nickname !== "undefined" && this.accDetails.Transactions.Nickname !== null && this.accDetails.Transactions.Nickname !== "")
        navMan.setCustomInfo("frmAccInfoEdit",this.accDetails.Transactions.Nickname);
      else if(typeof this.accDetails.Transactions.description !== "undefined" && this.accDetails.Transactions.description !== null && this.accDetails.Transactions.description !== "")
        navMan.setCustomInfo("frmAccInfoEdit",this.accDetails.Transactions.description);
    }
    var accountMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountModule");
    accountMod.presentationController.commonFunctionForNavigation("frmAccInfoEdit");
  },

  //Ends Added By Sirisha (KHC15066)

  getJointHolderNames: function(jointHoldersList){
    var jntHldrList="";
    for(var jntHldrNum=0;jntHldrNum<jointHoldersList.length;jntHldrNum++){
      if(jntHldrList!="")
        jntHldrList = jntHldrList+",";
      jntHldrList = jntHldrList+jointHoldersList[jntHldrNum]["fullname"];
    }
    return jntHldrList;
  },

  createViewForExternalAccountDetails: function() {
    this.view.flxExternalAccountContainer.setVisibility(true);
    this.view.flxContainerCheckingAcc.setVisibility(false);
    this.view.flxContainerCreditCard.setVisibility(false);
    this.view.flxContainerHomeLoan.setVisibility(false);
    this.view.flxContainerDepositAccount.setVisibility(false);
    this.view.btnCallBank.onClick = this.callBank;
    this.view.btnMsgBank.onClick = this.messageBank;
    this.populateExternalAccountDetail();
    this.view.forceLayout();
  },

  populateExternalAccountDetail: function() {
    var configManager = applicationManager.getConfigurationManager();
    var forUtility=applicationManager.getFormatUtilManager();
    var externalAccountDetails = this.accDetails.externalAccountDetails;
    var navMan = applicationManager.getNavigationManager();
    if(!kony.sdk.isNullOrUndefined(externalAccountDetails.NickName))
    {
      var accountsDetails = navMan.getCustomInfo("frmAccountDetails");
      accountsDetails.selectedAccountData.nickName = externalAccountDetails.NickName;
      navMan.setCustomInfo("frmAccountDetails",accountsDetails);
      this.view.customHeader.lblLocateUs.text = externalAccountDetails.NickName;
      this.view.title=externalAccountDetails.NickName;
    }
    else
    {
      this.view.customHeader.lblLocateUs.text = externalAccountDetails.AccountName;
      this.view.title=externalAccountDetails.AccountName;
    }
    this.view.lblExternalAccountAvailBalValue.text = forUtility.formatAmountandAppendCurrencySymbol(externalAccountDetails.AvailableBalance);
    this.view.lblExternalAccountNoValue.text = applicationManager.getDataProcessorUtility().maskAccountNumber(externalAccountDetails.Number);
    this.view.lblExternalAccountTypeValue.text = externalAccountDetails.TypeDescription;
    this.view.lblExternalAccountHoldrValue.text = externalAccountDetails.AccountHolder;
    this.view.lblExternalAccountNickNameVal.text = externalAccountDetails.NickName;
    this.view.lblExternalAccountBankNameValue.text = externalAccountDetails.BankName;
  },

  createViewForDepositAccount:function(){
    this.view.flxContainerCheckingAcc.setVisibility(false);
    this.view.flxContainerCreditCard.setVisibility(false);
    this.view.flxContainerHomeLoan.setVisibility(false);
    this.view.flxContainerDepositAccount.setVisibility(true);
    this.view.flxExternalAccountContainer.setVisibility(false);
    this.populateDepositAccount();
    this.view.btnCallBankDA.onClick=this.callBank;
    this.view.btnMsgBankDA.onClick=this.messageBank;
    this.view.forceLayout();
  },
  populateDepositAccount:function()
  {
    var configManager = applicationManager.getConfigurationManager();
    var forUtility=applicationManager.getFormatUtilManager();
    this.view.lblAvailBalValueDA.text=forUtility.formatAmountandAppendCurrencySymbol(this.accDetails.availableBalance);
    this.view.lblCurrBalValueDA.text=forUtility.formatAmountandAppendCurrencySymbol(this.accDetails.currentBalance);
    this.view.lblInterestEarnedValue.text=forUtility.formatAmountandAppendCurrencySymbol(this.accDetails.interestEarned);
    this.view.lblMaturityAmtValue.text=forUtility.formatAmountandAppendCurrencySymbol(this.accDetails.maturityAmount);
    var dateobj=forUtility.getDateObjectfromString(this.accDetails.maturityDate,"YYYY-MM-DD");    
    this.view.lblMaturityDateValue.text =  forUtility.getFormatedDateString(dateobj,forUtility.APPLICATION_DATE_FORMAT);
    this.view.lblMaturityOptionValue.text=this.accDetails.maturityOption;
    this.view.lblDividentRateValue.text=this.accDetails.dividendRate + "%";
    this.view.lblDividentPaidValue.text=forUtility.formatAmountandAppendCurrencySymbol(this.accDetails.dividendPaidYTD);
    this.view.lblDividentPaidAmtValue.text=forUtility.formatAmountandAppendCurrencySymbol(this.accDetails.dividendLastPaidAmount);
    this.view.lblAccNoValueDA.text=applicationManager.getDataProcessorUtility().maskAccountNumber(this.accDetails.accountID);
    // this.view.lbllAccHolderValueDA.text=this.accDetails.accountHolder;
    var accJson=JSON.parse(this.accDetails.accountHolder);
    this.view.lbllAccHolderValueDA.text=accJson.fullname;
    this.view.lblNickNameDAValue.text=this.accDetails.nickName;

  },
  customHeaderBtnRightOnClick:function(){
    this.view.flxPopupNickName.setVisibility(true);
    this.view.flxHeader.setEnabled(false);
    this.view.flxContainerCheckingAcc.setEnabled(false);
    this.view.flxContainerCreditCard.setEnabled(false);
    this.view.flxContainerHomeLoan.setEnabled(false);
    this.view.flxContainerDepositAccount.setEnabled(false);
    //this.view.flxFooter.setEnabled(false); 
  },
  btnCancelOnClick:function(){
    this.view.flxPopupNickName.setVisibility(false);
    this.view.flxHeader.setEnabled(true);
    this.view.flxContainerCheckingAcc.setEnabled(true);
    this.view.flxContainerCreditCard.setEnabled(true);
    this.view.flxContainerHomeLoan.setEnabled(true);
    this.view.flxContainerDepositAccount.setEnabled(true);
    // this.view.flxFooter.setEnabled(true);   
  },
  flxAccNoToggleDAOnClick:function(){
    if (this.view.imgAccNoToggleDA.src === "view.png") {
      this.view.imgAccNoToggleDA.src = "viewactive.png";
      this.view.lblAccNoValueDA.text = this.accDetails.accountID;
      this.view.flxAccNoToggleDA.forceLayout();
    } else {
      this.view.imgAccNoToggleDA.src = "view.png";
      this.view.lblAccNoValueDA.text = applicationManager.getDataProcessorUtility().maskAccountNumber(this.accDetails.accountID);
      this.view.flxAccNoToggleDA.forceLayout();
    }
  },

  /*   callBank:function()
  {    
    applicationManager.getPresentationUtility().showLoadingScreen(); 
    var infoCall = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("InformationModule");
    infoCall.presentationController.onClickCallUs();  

  },
  showDial: function (phoneNumber) {     
    applicationManager.getPresentationUtility().dismissLoadingScreen();
    kony.phone.dial(phoneNumber);         
  },
  messageBank:function()
  { 
    applicationManager.getPresentationUtility().showLoadingScreen();
    var messagesMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("MessagesModule");
    messagesMod.presentationController.getCategories();
  }, 

   PopulateCheckingAccount:function()
  {
    var configManager = applicationManager.getConfigurationManager();
    var forUtility=applicationManager.getFormatUtilManager();
    this.view.lblAvailBalValue.text=forUtility.formatAmountandAppendCurrencySymbol(this.accDetails.availableBalance);
    this.view.lblCurrBalValue.text=forUtility.formatAmountandAppendCurrencySymbol(this.accDetails.currentBalance);
    this.view.lblPendingDepValue.text=forUtility.formatAmountandAppendCurrencySymbol(this.accDetails.pendingDeposit);
    this.view.lblWithdrawValue.text=forUtility.formatAmountandAppendCurrencySymbol(this.accDetails.pendingWithdrawal);
    this.view.lblAccNoValue.text=applicationManager.getDataProcessorUtility().maskAccountNumber(this.accDetails.accountID);
    this.view.lblRoutingNoValue.text=this.accDetails.routingNumber;
    this.view.lblSwiftCodeValue.text=this.accDetails.swiftCode;
    this.view.lblAccTypeValue.text=this.accDetails.accountType;
    //this.view.lblAccHoldrValue.text=this.accDetails.accountHolder;
    var accJson=JSON.parse(this.accDetails.accountHolder);
    this.view.lblAccHoldrValue.text=accJson.fullname;
    if(this.accDetails.jointHolders)
       this.view.lblJointAccHoldrValue.text=this.getJointHolderNames(JSON.parse(this.accDetails.jointHolders));
    else
      this.view.flxJointAccHolder.setVisibility(false);
    this.view.lblAccNickNameVal.text=this.accDetails.nickName;
  },*/

  /* populateLoanAccount:function()
  {
    var configManager = applicationManager.getConfigurationManager();
    var forUtility=applicationManager.getFormatUtilManager();
    this.view.lblOutstandingBalVal.text=forUtility.formatAmountandAppendCurrencySymbol(this.accDetails.outstandingBalance);
    this.view.lblPrincipalBalVal.text=forUtility.formatAmountandAppendCurrencySymbol(this.accDetails.principalBalance);
    this.view.lblPrincipalAmtValue.text=forUtility.formatAmountandAppendCurrencySymbol(this.accDetails.principalValue);
    this.view.lblInterestRateValue.text=this.accDetails.interestRate + "%";
    this.view.lblInterestPaidValue.text=this.accDetails.interestPaidYTD;
    this.view.lblIntPaidLastYearValue.text=forUtility.formatAmountandAppendCurrencySymbol(this.accDetails.interestPaidLastYear);
    this.view.lblLastPmtAmtValue.text=forUtility.formatAmountandAppendCurrencySymbol(this.accDetails.lastPaymentAmount);
    var dateobj=forUtility.getDateObjectfromString(this.accDetails.lastPaymentDate,"YYYY-MM-DD");    
    this.view.lblLastPmtDateValue.text=  forUtility.getFormatedDateString(dateobj,forUtility.APPLICATION_DATE_FORMAT);
    this.view.lblPayOffAmtValue.text=forUtility.formatAmountandAppendCurrencySymbol(this.accDetails.payoffAmount);
    this.view.lblAccNoValueHL.text=applicationManager.getDataProcessorUtility().maskAccountNumber(this.accDetails.accountID);
    this.view.lblAccTypeValHL.text=this.accDetails.accountType;
    this.view.lblLoanTypeValue.text=this.accDetails.accountType;
    this.view.lblPropertyAddressValue.text=""; 
    var dateobj1=forUtility.getDateObjectfromString(this.accDetails.openingDate,"YYYY-MM-DD");    
    this.view.lblLoanOriginationDateVal.text=  forUtility.getFormatedDateString(dateobj1,forUtility.APPLICATION_DATE_FORMAT);

  },*/ 


  /*  populateCreditCardAccount:function()
  {
    var configManager = applicationManager.getConfigurationManager();
    var forUtility=applicationManager.getFormatUtilManager();
    this.view.lblOutstandingBalValue.text=forUtility.formatAmountandAppendCurrencySymbol(this.accDetails.outstandingBalance);
    this.view.lblUpcomingBalvalue.text=forUtility.formatAmountandAppendCurrencySymbol(this.accDetails.currentAmountDue);
    var dateobj=forUtility.getDateObjectfromString(this.accDetails.dueDate,"YYYY-MM-DD");    
    this.view.lblDueDateValue.text=  forUtility.getFormatedDateString(dateobj,forUtility.APPLICATION_DATE_FORMAT);
    this.view.lblCurrentBalanceValue.text=forUtility.formatAmountandAppendCurrencySymbol(this.accDetails.availableCredit);
    this.view.lblCreditLmtValue.text=forUtility.formatAmountandAppendCurrencySymbol(this.accDetails.creditLimit);
    this.view.lblRwdBalValue.text=forUtility.formatAmountandAppendCurrencySymbol(this.accDetails.availablePoints);
    this.view.lblntRateValue.text=this.accDetails.interestRate + "%";
    this.view.lblAccNoValueCC.text=applicationManager.getDataProcessorUtility().maskAccountNumber(this.accDetails.accountID);
    this.view.lblCardTypeValue.text=this.accDetails.accountType;
    var dateobj1=forUtility.getDateObjectfromString(this.accDetails.openingDate,"YYYY-MM-DD");    
    this.view.lblCardIssueDateVal.text=  forUtility.getFormatedDateString(dateobj1,forUtility.APPLICATION_DATE_FORMAT);
   // this.view.lblCardHolderName.text=this.accDetails.accountHolder;
    var accJson=JSON.parse(this.accDetails.accountHolder);
    this.view.lblCardHolderNameVal.text=accJson.fullname;
    this.view.lblNickNameValue.text=this.accDetails.nickName;
  },*/

});