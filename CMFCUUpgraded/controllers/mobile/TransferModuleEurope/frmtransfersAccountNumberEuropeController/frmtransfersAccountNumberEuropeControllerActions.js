define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for flxBack **/
    AS_FlexContainer_jcaea33a14c94a6eb312c6d699d6e5df: function AS_FlexContainer_jcaea33a14c94a6eb312c6d699d6e5df(eventobject) {
        var self = this;
        var transMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
        transMod.presentationController.commonFunctionForNavigation("frmTransactionMode");
    },
    /** onClick defined for flxClose **/
    AS_FlexContainer_j8341a26743f43ff8009d5b9d183e64e: function AS_FlexContainer_j8341a26743f43ff8009d5b9d183e64e(eventobject) {
        var self = this;
        self.view.tbxTo.text = "";
    },
    /** onRowClick defined for segToAccount **/
    AS_Segment_e65331251a9e486285ed2d30c5890000: function AS_Segment_e65331251a9e486285ed2d30c5890000(eventobject, sectionNumber, rowNumber) {
        var self = this;
        self.view.segToAccount.isVisible = false;
        self.view.flxNewRecipient.isVisible = true;
        self.view.tbxTo.text = "FR 4567 7456 3425 4532 1423 4356 664 ";
    },
    /** onClick defined for undefined **/
    AS_BarButtonItem_hfe677b98fd346b6adafad1533951b4b: function AS_BarButtonItem_hfe677b98fd346b6adafad1533951b4b(eventobject) {
        var self = this;
        this.cancelOnClick();
    },
    /** init defined for frmtransfersAccountNumberEurope **/
    AS_Form_f63fec6580274ad1a9635e6768d5809a: function AS_Form_f63fec6580274ad1a9635e6768d5809a(eventobject) {
        var self = this;
        this.init();
    },
    /** preShow defined for frmtransfersAccountNumberEurope **/
    AS_Form_c8f3e95e441c4eca829ccf415be09355: function AS_Form_c8f3e95e441c4eca829ccf415be09355(eventobject) {
        var self = this;
        this.preShow();
    }
});