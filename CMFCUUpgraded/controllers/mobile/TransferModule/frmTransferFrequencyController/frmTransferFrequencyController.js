define({
   init : function(){
    var navManager = applicationManager.getNavigationManager();
    var currentForm = navManager.getCurrentForm();
    applicationManager.getPresentationFormUtility().initCommonActions(this,"YES",currentForm);
  },
    preShow: function () {
    if(kony.os.deviceInfo().name==="iPhone"){
      // this.view.flxHeader.isVisible = false;
      this.view.flxHeader.setVisibility(false);
    }
        this.initActions();
        applicationManager.getPresentationUtility().dismissLoadingScreen();
        var navManager = applicationManager.getNavigationManager();
        var currentForm=navManager.getCurrentForm();
        applicationManager.getPresentationFormUtility().logFormName(currentForm);  
    },
    initActions: function () {
      var scope = this;
      var transMod = applicationManager.getModulesPresentationController("TransferModule");
    var index= transMod.presentationController.getSelectedFrequencyIndex();
//       var navMan=applicationManager.getNavigationManager();
//       var freqDet= navMan.getCustomInfo("frmTransferFrequency");
//       var index=freqDet.index;
   //   this.view.segFrequency.rowFocusSkin = "sknFlxf9f9f9";
     // this.view.segFrequency.retainSelection = true;
      this.view.segFrequency.rowFocusSkin ="";
      this.view.segFrequency.retainSelection = false;
      if(index!==null&&index!==undefined&&index!=="")
      {
         this.view.segFrequency.rowFocusSkin = "sknFlxf9f9f9";
      this.view.segFrequency.retainSelection = true;
       // this.view.segFrequency.selectedRowIndices = [[0,[index]]];
       this.view.segFrequency.selectedRowIndex = [0,index];
      }
      
      this.view.segFrequency.onRowClick = function () {
        scope.segmentRowClick();
      }
       this.view.customHeader.imgBack.src="back_icon.png";
     this.view.customHeader.flxBack.onClick = function(){
           var navMan=applicationManager.getNavigationManager();    
           navMan.goBack();   
        }
     this.view.customHeader.btnRight.skin="sknFlxBorderPrimaryGrey";
        this.view.customHeader.btnRight.onClick = function(){
        scope.cancelOnClick(); 
        }
    },
  cancelOnClick:function()
  {
    var navMan=applicationManager.getNavigationManager(); 
    navMan.navigateTo("frmTransfers");
  },
    segmentRowClick: function () {
        var index = this.view.segFrequency.data[this.view.segFrequency.selectedIndex[1]].lblFrequency;
        
      var transferModule = applicationManager.getModulesPresentationController("TransferModule");
        transferModule.presentationController.switchFrequencyType(index); 
     
    }
 
  
  
});