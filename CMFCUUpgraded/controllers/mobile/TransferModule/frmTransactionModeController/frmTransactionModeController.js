define({ 

  preShow: function(){
    if(kony.os.deviceInfo().name==="iPhone"){
      this.view.flxHeader.isVisible = false;
    }
    this.setSegmentData();
    this.initActions();
    applicationManager.getPresentationUtility().dismissLoadingScreen();
    var navManager = applicationManager.getNavigationManager();
    var currentForm=navManager.getCurrentForm();
    applicationManager.getPresentationFormUtility().logFormName(currentForm);  
  },
  init : function(){
    var navManager = applicationManager.getNavigationManager();
    var currentForm = navManager.getCurrentForm();
    applicationManager.getPresentationFormUtility().initCommonActions(this,"YES",currentForm);
  },
  initActions: function(){
    var scope = this;
   this.view.customHeader.imgBack.src="back_icon.png";
    this.view.segTransactionMode.onRowClick = function(){
      scope.segmentRowClick();
    }
    // this.view.btnTemp.onClick = function(){

    // }
  },
  segmentRowClick: function(){
    var type = this.view.segTransactionMode.data[this.view.segTransactionMode.selectedIndex[1]].lblTransactionMode;
    applicationManager.getPresentationUtility().showLoadingScreen();
    var transMod =  applicationManager.getModulesPresentationController("TransactionModule");
    transMod.presentationController.transactionMode = type;
    transMod.presentationController.showAccounts("InternalTransfer");
  },
  backNavigation:function()
  {
    var navMan=applicationManager.getNavigationManager();
    navMan.goBack();

  },

  setSegmentData: function(){

    var data = [];
    var transferMod =  applicationManager.getModulesPresentationController("TransferModule");
       
    data = [{
      lblTMode:"My Own CMCU Accounts",
        lblTModeDesc:"Transfer Money to your CMCU Accounts",
        imgArrow:"chevronright.png",
      template:"flxTransactionMode"
    },
           {
      lblTMode:"Another CMCU Customer",
        lblTModeDesc:"Transfer money to another CMCU Customer",
        imgArrow:"chevronright.png",
      template:"flxTransactionMode"
    },
           {
      lblTMode:"Other Bank Accounts",
        lblTModeDesc:"Transfer money to accounts at other banks",
        imgArrow:"chevronright.png",
      template:"flxTransactionMode"
    }];

   this.view.segTransactionMode.widgetDataMap = {lblTransactionMode: "lblTMode", lblTransactionModeDescription:"lblTModeDesc",imgArrow:"imgArrow","flxTransactionMode":"flxTransactionMode"};

     this.view.segTransactionMode.setData(data);
  }
});