define({
  init : function(){
     var navManager = applicationManager.getNavigationManager();
    var currentForm=navManager.getCurrentForm();
    applicationManager.getPresentationFormUtility().initCommonActions(this,"YES",currentForm);
   },
  preShow : function () {
    this.view.flxSearch.setVisibility(false);
    if(kony.os.deviceInfo().name !== "iPhone"){
      this.view.flxHeader.isVisible = true;
    }
    else{
      this.view.flxHeader.isVisible = false;
    }
    this.initActions();
  },
  initActions: function () {
    var scope = this;
    this.view.segCountry.onRowClick = function () {
      scope.segmentRowClick();
    };
    this.view.customHeader.flxBack.onClick = function () {
      var navManager = applicationManager.getNavigationManager();
      navManager.goBack();
    };
    this.view.customHeader.btnRight.onClick = function(){
      var settingsMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("SettingsModule");
      settingsMod.presentationController.commonFunctionForNavigation("frmProfilePersonalDetails");
    };
  },
  setFormUI: function(data) {
    var widgetDataMap = {
      lblFrequency : "Name"
    };
    this.view.segCountry.widgetDataMap = widgetDataMap;
    this.view.segCountry.setData(data);
    applicationManager.getPresentationUtility().dismissLoadingScreen();
  },
  segmentRowClick : function () {
    var settingsMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("SettingsModule");
    var phoneData = {};
    var navManager = applicationManager.getNavigationManager();
    phoneData = navManager.getCustomInfo("frmProfileEditPhoneNumberMain");
    if(phoneData &&  phoneData.data){
      phoneData.data.phoneCountryCode = this.view.segCountry.selectedRowItems[0].ISDCode;
    }
    else{
      phoneData.phoneCountryCode = this.view.segCountry.selectedRowItems[0].ISDCode;
    }
    navManager.setCustomInfo("frmProfileEditPhoneNumberMain",phoneData);
    settingsMod.presentationController.commonFunctionForNavigation("frmProfileEditPhoneNumberMain");
  },
});