define({
    keypadString: '0.00',
    isPeriodUsed: false,
  
  	init : function() {
        try {
          var navManager = applicationManager.getNavigationManager();
          var currentForm=navManager.getCurrentForm();
          applicationManager.getPresentationFormUtility().initCommonActions(this,"YES",currentForm);
        }
        catch(err) {
          throw GlobalExceptionHandler.addMessageAndActionForException(err, "kony.error.LoadingFormFailed", GlobalExceptionHandler.ActionConstants.ALERT, arguments.callee.name);
        }     
    },
  
    /**
     * Description
     * @method preShow
     * @return 
     */
    preShow: function() {
      try {
        if (kony.os.deviceInfo().name === "iPhone") {
            this.view.flxHeader.isVisible = false;
        }
        this.initActions();
        this.setKeyPadActions();
        this.keypadString = '0.00';
      }
      catch(err) {
        throw GlobalExceptionHandler.addMessageAndActionForException(err, "kony.error.LoadingFormFailed", GlobalExceptionHandler.ActionConstants.ALERT, arguments.callee.name);
      } 
     
    },
  
    initActions: function() {
      this.view.customHeader.flxBack.onClick = function() {
        applicationManager.getPresentationUtility().showLoadingScreen();
       	 var navMan=applicationManager.getNavigationManager();
        navMan.goBack(); 
      };
    },
  
  	setHeaderTitle: function(headerName){
      this.view.title = headerName;
      this.view.customHeader.lblLocateUs.text = headerName;
    },
  	
    /**
     * Description
     * @method setKeyPadActions
     * @return 
     */
    setKeyPadActions: function() {
      try {
        var scopeObj = this;
        /**
         * Description
         * @method onClick
         * @return 
         */
        this.view.keypad.btnOne.onClick = function() {
            scopeObj.setKeypadChar(1);
        };
        /**
         * Description
         * @method onClick
         * @return 
         */
        this.view.keypad.btnTwo.onClick = function() {
            scopeObj.setKeypadChar(2);
        };
        /**
         * Description
         * @method onClick
         * @return 
         */
        this.view.keypad.btnThree.onClick = function() {
            scopeObj.setKeypadChar(3);
        };
        /**
         * Description
         * @method onClick
         * @return 
         */
        this.view.keypad.btnFour.onClick = function() {
            scopeObj.setKeypadChar(4);
        };
        /**
         * Description
         * @method onClick
         * @return 
         */
        this.view.keypad.btnFive.onClick = function() {
            scopeObj.setKeypadChar(5);
        };
        /**
         * Description
         * @method onClick
         * @return 
         */
        this.view.keypad.btnSix.onClick = function() {
            scopeObj.setKeypadChar(6);
        };
        /**
         * Description
         * @method onClick
         * @return 
         */
        this.view.keypad.btnSeven.onClick = function() {
            scopeObj.setKeypadChar(7);
        };
        /**
         * Description
         * @method onClick
         * @return 
         */
        this.view.keypad.btnEight.onClick = function() {
            scopeObj.setKeypadChar(8);
        };
        /**
         * Description
         * @method onClick
         * @return 
         */
        this.view.keypad.btnNine.onClick = function() {
            scopeObj.setKeypadChar(9);
        };
        /**
         * Description
         * @method onClick
         * @return 
         */
        this.view.keypad.btnZero.onClick = function() {
            scopeObj.setKeypadChar(0);
        };
        /**
         * Description
         * @method onTouchEnd
         * @return 
         */
        this.view.keypad.imgClearKeypad.onTouchEnd = function() {
            scopeObj.clearKeypadChar();
        };
        /**
         * Description
         * @method onClick
         * @return 
         */
        this.view.btnDot.onClick = function() {
            scopeObj.setKeypadChar('.');
        };
      }
      catch(err) {
        throw GlobalExceptionHandler.addMessageAndActionForException(err, "kony.error.LoadingFormFailed", GlobalExceptionHandler.ActionConstants.ALERT, arguments.callee.name);
      }         
    },
	
	/**
     * Description - used to return the subscription value based on the switch index value.
     * @method switchSubscriptionValue
     * @return {String} subscription value for setting alert preferences.
     */
  
    switchSubscriptionValue: function(switchIndex) {
      if(switchIndex) {
        return "false";
      }
      else {
        return "true";
      }  
    },
  
    /**
     * Description - to set the alert preferences
     * @method updateMinBalanceValue
     * @param {object} typeData - contains the alert type preferences of selected alert.
     * @param {Integer} masterSwitchStatus - master switch toggle value for setting alert preferences.	 
     */
    updateMinBalanceValue: function(typeData, masterSwitchStatus) {
      try{
        applicationManager.getPresentationUtility().showLoadingScreen();
        var amount = this.view.lblAmount.text;
      	var amountWithoutCommas=amount.replace(/,/g , '');
      	var validAmountString=""+amountWithoutCommas;
        var  settingsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("SettingsModule");
        var categoryID = settingsModule.presentationController.getAlertsCategoryID();
        var switchIndex = this.view.switchReceiveAlerts.selectedIndex;
        var subscriptionValue = this.switchSubscriptionValue(switchIndex);
        var masterSwitchSubscription = this.switchSubscriptionValue(masterSwitchStatus);
        if (validAmountString.length<=16) {
            var params = {
                "AlertCategoryId": categoryID,
                "isSubscribed": masterSwitchSubscription,
                "typePreference": [{
                  "typeId": typeData.alerttype_id,
                  "isSubscribed": subscriptionValue,
                  "value1": validAmountString
                }]
            };
           
           settingsModule.presentationController.setAlertPreferences(params, false); 
        } else {
            alert("Please enter the valid amount");
			applicationManager.getPresentationUtility().dismissLoadingScreen();
        }
      }
      catch(err) {
        applicationManager.getPresentationUtility().dismissLoadingScreen();
        throw GlobalExceptionHandler.addMessageAndActionForException(err, "kony.error.FailedToUpdateAlertSettings", GlobalExceptionHandler.ActionConstants.ALERT, arguments.callee.name);
      } 
    },

    decimalPlaces: function(num) {
      try {
        var match = ('' + num).match(/(?:\.(\d+))?(?:[eE]([+-]?\d+))?$/);
        if (!match) {
            return 0;
        }
        return Math.max(
            0, (match[1] ? match[1].length : 0) - (match[2] ? +match[2] : 0));
      }
      catch(err) {
        throw GlobalExceptionHandler.addMessageAndActionForException(err, "kony.error.processingError", GlobalExceptionHandler.ActionConstants.LOG, arguments.callee.name);
      }     
    },

    /**
     * Description
     * @method setKeypadChar
     * @param {char} char - each character from the amount entered.
     * @return 
     */
    setKeypadChar: function(char) {
      try{
        if (char == '.') {
            if (this.isPeriodUsed === false) {
                this.isPeriodUsed = true;
            } else {
                return;
            }
        }
      	
        this.keypadString = this.keypadString + char;
        var firstChar = this.keypadString[0];
        this.keypadString = this.keypadString.split("");
        for(var i=1; i<this.keypadString.length; i++){
          if(this.keypadString[i]=='.'){
            this.keypadString[i-1] = this.keypadString[i+1];
            i++;
          }else{
            this.keypadString[i-1]=this.keypadString[i];
          }
        }
        this.keypadString = this.keypadString.join("");
        this.keypadString = this.keypadString.substr(0, this.keypadString.length-1);
        if(firstChar!=='0'){
          this.keypadString = firstChar + this.keypadString;
        }
        this.updateAmountValue();
      }
      catch(err) {
        throw GlobalExceptionHandler.addMessageAndActionForException(err, "kony.error.processingError", GlobalExceptionHandler.ActionConstants.LOG, arguments.callee.name);
      } 
    },

      /**
* used to clear the amount value from the amount label.
* @param {} 
*/
  
  clearKeypadChar: function () {
      try{
        if(this.keypadString ==='0.00') return;

        this.keypadString = this.keypadString.split("");
        for(var i=this.keypadString.length-2; i>=0; i--){
          if(this.keypadString[i]=='.'){
            this.keypadString[i+1] = this.keypadString[i-1];
            i--;
          }else{
            this.keypadString[i+1] = this.keypadString[i];
          }
        } 
        this.keypadString = this.keypadString.join("");
        this.keypadString = this.keypadString.substr(1);
        if(this.keypadString[0]=='.'){
          this.keypadString = '0'+ this.keypadString;
        }
        this.updateAmountValue();
      }
      catch(err) {
        throw GlobalExceptionHandler.addMessageAndActionForException(err, "kony.error.processingError", GlobalExceptionHandler.ActionConstants.LOG, arguments.callee.name);
      } 
    },
  
      /**
* used to update the amount value to the amount label.
* @param {}
*/

    updateAmountValue: function(){
      try {
        if(this.keypadString==='0.00'){
          this.view.btnContinue.skin = "sknBtnOnBoardingInactive";
          this.view.btnContinue.setEnabled(false);
          this.view.lblAmount.text = '0.00';
        }
        else{
          var keypadStringCommas = '0.00';
          var beforeDecimal = this.keypadString.split('.')[0];
          var afterDecimal = this.keypadString.split('.')[1];
          if(afterDecimal === undefined) {
            afterDecimal = "00";
          }
          if(beforeDecimal.length>3){
            var withCommas = (beforeDecimal.length)/3;
            var withoutCommas = (beforeDecimal.length)%3;
            var temp = '';
            if(withoutCommas!=0){
              temp = beforeDecimal.substr(0, withoutCommas)+',';
            }
            for(var i = withoutCommas; i<beforeDecimal.length; i+=3){
              temp+=beforeDecimal.substr(i, 3)+',';
            }
            beforeDecimal = temp.substr(0, temp.length-1);
          }
          keypadStringCommas = beforeDecimal + '.'+afterDecimal;
          this.view.btnContinue.skin = "sknBtn0095e4RoundedffffffSSP26px";
          this.view.btnContinue.setEnabled(true);
          this.view.lblAmount.text = keypadStringCommas;
        }
      }
      catch(err) {
        throw GlobalExceptionHandler.addMessageAndActionForException(err, "kony.error.FailedToUpdateAlertSettings", GlobalExceptionHandler.ActionConstants.ALERT, arguments.callee.name);
      }         
    },
  
    /**
* used to set the toggle UI.
* @param {String} state - state of switch of the selected alert type preference.
*/
  
  setToggleUI: function(state) {
    if(state === "true") {
      this.view.switchReceiveAlerts.selectedIndex = 0;
      this.view.lblStatus.text = kony.i18n.getLocalizedString("kony.mb.common.enabled");
      this.view.lblDollar.skin="sknLbl4a4a4aSSP42px";
      this.view.lblAmount.skin="sknLbl4a4a4aSSP42px";
      this.view.flxKeypad.setEnabled(true);
    }
    else {
      this.view.switchReceiveAlerts.selectedIndex = 1;
      this.view.lblStatus.text = kony.i18n.getLocalizedString("kony.mb.common.disabled");
      this.view.lblDollar.skin="sknLblA0A0A0SSP42px";
      this.view.lblAmount.skin="sknLblA0A0A0SSP42px";
      this.view.flxKeypad.setEnabled(false);
    }
  },

      /**
* used to initiate toggle action.
* @param {object} typeData - typeData to be set.
*/
  
  switchToggle:function(typeData){
   if(this.view.switchReceiveAlerts.selectedIndex === 0){
     this.view.lblStatus.text = kony.i18n.getLocalizedString("kony.mb.common.enabled");
     this.view.flxKeypad.setEnabled(true);
     this.setToggleUI("true");
    }
    else{
      this.view.lblStatus.text = kony.i18n.getLocalizedString("kony.mb.common.disabled");
      this.view.flxKeypad.setEnabled(false);
      this.setToggleUI("false");
    }
  },
  
    /**
* used to set the alert data to the form.
* @param {object} typeData - typeData to be set.
* @param {Integer} masterSwitchIndex - master switch index of alert category.
*/
  
  bindData: function(typeData, masterSwitchIndex) {
    this.setHeaderTitle(typeData.alerttypetext_DisplayName);    
    if(typeData.isSubscribed == "true") {
      if(typeData.preference.hasOwnProperty("Value1") && typeData.preference.Value1 !== ""){
        this.keypadString = typeData.preference.Value1;
      }
      else{
        this.keypadString = "0.00";
      }   
    }
    else {
      this.keypadString = "0.00";
    }
    this.setToggleUI(typeData.isSubscribed);
	this.view.flxAmount.setEnabled(true);
    this.updateAmountValue();
    this.view.switchReceiveAlerts.onSlide = this.switchToggle.bind(this, typeData);
    this.view.btnContinue.onClick = this.updateMinBalanceValue.bind(this, typeData, masterSwitchIndex);
  }
});