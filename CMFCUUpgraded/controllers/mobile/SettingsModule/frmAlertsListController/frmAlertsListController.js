define({
  
  bodyTxtAlerts: "",
  
//   onNavigate:function(obj) {
// 	  this.navigateToSelectedAlerts();
//   },
  
  init : function(){
  },
  
  preShow: function() {
    this.view.customHeader.lblLocateUs.text = "";
    this.view.title = "";
    this.view.lblBodyTxt.text = "";
    if(kony.os.deviceInfo().name !== "iPhone"){
      this.view.flxHeader.isVisible = true;
    }
    else{
      this.view.flxHeader.isVisible = false;
    }
    this.setFlowActions();
    this.view.switchDisabled.setEnabled(false);
    this.setAlertFlexesVisibility(false);
    this.navigateToSelectedAlerts();
  },
  
   /**
* used to get alert preferences based on categoryID.
*/ 
 
  navigateToSelectedAlerts: function() {
      var  settingsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("SettingsModule");
      var categoryID = settingsModule.presentationController.getAlertsCategoryID();
      settingsModule.presentationController.getAlertsBasedOnCategoryID(categoryID);
    },
  
  setFlowActions : function(){
    var scope = this;
    this.view.customHeader.flxBack.onClick = function() {
      applicationManager.getPresentationUtility().showLoadingScreen();
      var navMan=applicationManager.getNavigationManager();
      navMan.goBack();
    };
  },
  
   /**
* used to set the alert switch visibility.
* @param {boolean} visible - boolean value of visibility.
*/
  
  setAlertSwitchesVisibility: function(visible) {
    for (var i = 0; i < this.view.flxAlertSwitches.widgets().length; i++) {
            this.view[i+"switchRow"].setVisibility(visible);
      		this.view[i+"switchDisabled"].setEnabled(false);
       	    this.view[i+"switchDisabled"].setVisibility(!visible);
          }
    for (var j = 0; j < this.view.flxAlertList.widgets().length; j++) {
            this.view[j+"switchRowType"].setVisibility(visible);
      		this.view[j+"switchTypeDisabled"].setEnabled(false);
      		this.view[j+"switchTypeDisabled"].setVisibility(!visible);
          }
  },
  
   /**
* used to set the UI on toggle of Master Switch.
* @param {Integer} isDisable - switch index value on toggle of master switch
*/
  
  setMasterSwitchToggleUI: function(isDisable) {
    if(isDisable) {
      this.view.lblStatus.text = kony.i18n.getLocalizedString("kony.mb.common.disabled");
      this.view.lblBodyTxt.text = kony.i18n.getLocalizedString("kony.mb.Alerts.DisableReceiveAlerts");
    }
    else {
      this.view.lblStatus.text = kony.i18n.getLocalizedString("kony.mb.common.enabled");
      this.view.lblBodyTxt.text = this.bodyTxtAlerts;
    }
    var visibleStatus;
    if(isDisable) {
      visibleStatus = false;
    }
    else {
      visibleStatus = true;
    }
    this.setAlertSwitchesVisibility(visibleStatus);
  },
  
   /**
* used to disable alerts.
* @param {object} alertData - used to set the UI of corresponding switches.
*/

  disableAlerts: function(alertData) {
     var scope = this;
     var basicConfig = {
        message: kony.i18n.getLocalizedString("kony.mb.Alerts.DisableAlert"),
        alertType: constants.ALERT_TYPE_CONFIRMATION,
        yesLabel: "Yes",
        noLabel: "No",
        alertHandler: handle
      };
      kony.ui.Alert(basicConfig, {});
    function handle(response) {
        if(response) {
          scope.setMasterSwitchToggleUI(1);
          scope.buildParamsforMasterSwitchToggle(alertData, false);
        }
        else {
          scope.view.switchReceiveAlerts.selectedIndex = 0;
        }  
    }
  },
  
   /**
* used to build params for setting the alert preferences of particular alert.
* @param {Integer} switchIndexValue - Toggle status of switch
* @param {object} channelPref - JSON Array of statuses channel preferences.
* @param {object} typePref - JSON Array of statuses type preferences.
* @param {Boolean} isMasterSwitchEnable - To check if the toggle is a master switch Enable
*/
  
  buildParamsAndSetAlertPreferences: function(switchIndexValue, channelPref, typePref, isMasterSwitchEnable) {
    var categorySubscriptionValue = "";
    if(switchIndexValue) {
      categorySubscriptionValue = "false";
    }
    else {
      categorySubscriptionValue = "true";
    }
    var settingsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("SettingsModule");  
    var categoryId = settingsModule.presentationController.getAlertsCategoryID();
    var params = {
      "AlertCategoryId": categoryId,
      "isSubscribed": categorySubscriptionValue,
      "channelPreference": [],
      "typePreference": []
    };
    params.channelPreference = channelPref;
    params.typePreference = typePref;
    applicationManager.getPresentationUtility().showLoadingScreen();
    settingsModule.presentationController.setAlertPreferences(params, isMasterSwitchEnable);
  },
  
    /**
* used to build params for setting the alert preferences for master switch toggle.
* @param {object} alertData - statuses of alert switches to be picked up to set preferences.
* @param {Boolean} isMasterSwitchEnable - To check if the toggle is a master switch enable
*/
  
  buildParamsforMasterSwitchToggle: function(alertData, isMasterSwitchEnable) {
    var channelPref = [];
    var typePref = [];
    if(isMasterSwitchEnable) {
      var isInitialLoad = alertData.categorySubscription.isInitialLoad;
      this.setMasterSwitchToggleUI(0);
      if(isInitialLoad == "true") {
        for(var i = 0; i < alertData.channelPreferences.length; i++) {
          channelPref.push({"channelId": alertData.channelPreferences[i].channel_id, "isSubscribed": "true"});
        }
        for(var j = 0; j < alertData.typePreferences.length; j++) {
          typePref.push({"typeId": alertData.typePreferences[j].alerttype_id, "isSubscribed": "true"});
        }  
      }
      this.buildParamsAndSetAlertPreferences(0, channelPref, typePref, isMasterSwitchEnable);
    }
    else {
      this.buildParamsAndSetAlertPreferences(1, channelPref, typePref, isMasterSwitchEnable);
    }
  },
  
    /**
* used to set the behavior on master switch toggle.
* @param {object} alertData - to set the statuses using alertData.
*/
  
  onMasterSwitchToggle: function(alertData) {
    var switchIndex = this.view.switchReceiveAlerts.selectedIndex;
    if(switchIndex) {
      this.disableAlerts(alertData);
    }
    else {
      this.setMasterSwitchToggleUI(switchIndex);
      this.buildParamsforMasterSwitchToggle(alertData, true);  
    }
    this.view.flxRowDummy.forceLayout();
    this.view.flxAlertList.forceLayout();
  },
  
    /**
* used to set the behavior on channel switches toggle.
* @param {Integer} index - Toggle status of switch
* @param {object} channelData - JSON Object consisting of channel data.
*/
  
  onChannelSwitchToggle: function(index, channelData) {
    var channelPref = [];
    var switchStatus = "";
    if(this.view[index + "switchRow"].selectedIndex) {
      switchStatus = "false";
    }
    else {
      switchStatus = "true";
    }
    channelPref.push({"channelId": channelData.channel_id, "isSubscribed": switchStatus});
    this.buildParamsAndSetAlertPreferences(0, channelPref, [], false);
  },

      /**
* used to set the behavior on type switches toggle.
* @param {Integer} index - Toggle status of switch
* @param {object} typeData - JSON Object consisting of alert type preferences.
*/
  
  onTypeSwitchToggle: function(index, typeData) {
    var typePref = [];
    var switchStatus = "";
    if(this.view[index + "switchRowType"].selectedIndex) {
      switchStatus = "false";
    }
    else {
      switchStatus = "true";
    }
    typePref.push({"typeId": typeData.alerttype_id, "isSubscribed": switchStatus});
    this.buildParamsAndSetAlertPreferences(0, [], typePref, false);
  },
  
      /**
* used to set the Alert switches Status based on subscription value.
* @param {String} subscriptionValue - subscription status of alert switch based on backend response.
* @returns {Integer} Index value.
*/
  
  setAlertSwitchStatus: function(subscriptionValue) {
    if(subscriptionValue == "true") {
      return 0;
    }
    else {
      return 1;
    }
  },
  
        /**
* used to set the channel switch status based on channel support value.
* @param {object} channelData - Backend channel response.
*/
  setUIforChannelSupport: function(channelData) {
	for(var i = 0; i < channelData.length; i++) {
		if(channelData[i].isChannelSupported == "false") {
			this.view[i+"switchRow"].setVisibility(false);
      		this.view[i+"switchDisabled"].setEnabled(false);
       	    this.view[i+"switchDisabled"].setVisibility(true);
		}
	}  
  },
  
   /**
* used to set the alert minimum balance value for the applicables.
* @param {object} typeData - contains the typeData value selected.
*/
  
  onNavToAlertMinBalPref: function(typeData) {
    applicationManager.getPresentationUtility().showLoadingScreen();
  	var settingsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("SettingsModule");  
    settingsModule.presentationController.commonFunctionForNavigation("frmAlertsMinimumBalance");
    settingsModule.presentationController.setDataForMinimumBalance(typeData, this.view.switchReceiveAlerts.selectedIndex);
  },
  
   /**
* used to set the navigation for type preferences with values.
* @param {object} typeData - contains the typeData value selected.
*/
  
  setNavForPreferences: function(typeData){
    for(var i = 0; i < typeData.length; i++) {
      if(typeData[i].hasOwnProperty("alertCondition") && typeData[i].alertCondition.NoOfFields==="1"){   
        this.view[i+"flxImage"].setVisibility(true);
        this.view[i+"imgNav"].setVisibility(true);
        this.view[i+"switchRowType"].setVisibility(false);
        this.view[i+"switchTypeDisabled"].setVisibility(false);
        this.view[i+"flxImage"].onClick = this.onNavToAlertMinBalPref.bind(this, typeData[i]);
        this.view.flxAlertList.forceLayout();
      }
    }
  }, 
  
    /**
* used to set the channel data based on backend response.
* @param {object} channelData - Backend channel response to be set.
*/
  
  setChannelData: function(channelData) {
    this.view.flxAlertSwitches.removeAll();
    for(var i = 0; i < channelData.length; i++) {
       var channel = this.view.flxRowDummy.clone("" + i);
       this.view.flxAlertSwitches.add(channel);
       this.view[i+"lblData"].text = channelData[i].channeltext_Description;
       this.view[i+"switchRow"].selectedIndex = this.setAlertSwitchStatus(channelData[i].isSubscribed);
       this.view[i+"flxRowDummy"].isVisible=true;
       this.view[i+"switchRow"].onSlide = this.onChannelSwitchToggle.bind(this, i, channelData[i]);
    }
  },
  
    /**
* used to set the type data based on backend response.
* @param {object} typeData - Backend type response to be set.
*/
  
  setTypeData: function(typeData) {
    this.view.flxAlertList.removeAll();
    for(var i = 0; i < typeData.length; i++) {
      var type = this.view.flxTypeDummy.clone("" + i);
      this.view.flxAlertList.add(type);
      this.view[i+"lblDataType"].text = typeData[i].alerttypetext_DisplayName;
  	  this.view[i+"switchRowType"].selectedIndex = this.setAlertSwitchStatus(typeData[i].isSubscribed);
      this.view[i+"flxTypeDummy"].isVisible=true;
      this.view[i+"switchRowType"].onSlide = this.onTypeSwitchToggle.bind(this, i, typeData[i]);
    }
  },
  
   /**
* used to set the alert flexes visibility.
* @param {boolean} visible - boolean value of visibility.
*/
  
  setAlertFlexesVisibility: function(visible) {
    this.view.flxEnable.setVisibility(visible);
    this.view.flxSelectAccount.setVisibility(visible);
    this.view.flxAlertSwitches.setVisibility(visible);
    this.view.flxAlertList.setVisibility(visible);
  },
  
   /**
* used to set the alert data to the form.
* @param {object} alertData - alertData from backend service response.
*/
  
  bindData: function(alertData) {
    if(alertData.categoryId === "ALERT_CAT_TRANSACTIONAL") {
      this.view.customHeader.lblLocateUs.text = kony.i18n.getLocalizedString("kony.mb.Alerts.TransactionAndPaymentAlerts");
      this.view.title = kony.i18n.getLocalizedString("kony.mb.Alerts.TransactionAndPaymentAlerts");
      this.bodyTxtAlerts = kony.i18n.getLocalizedString("kony.mb.Alerts.ReceiveTransactional");
    }
    else {
      this.view.customHeader.lblLocateUs.text = kony.i18n.getLocalizedString("kony.mb.Alerts.SecurityAlerts");
      this.view.title = kony.i18n.getLocalizedString("kony.mb.Alerts.SecurityAlerts");
      this.bodyTxtAlerts = kony.i18n.getLocalizedString("kony.mb.Alerts.EnableReceiveAlerts");
    }
    var categoryData = alertData.categorySubscription;
    var switchValue = this.setAlertSwitchStatus(categoryData.isSubscribed);
    this.view.switchReceiveAlerts.selectedIndex = switchValue;
    this.setChannelData(alertData.channelPreferences);
    this.setTypeData(alertData.typePreferences);
    this.setMasterSwitchToggleUI(switchValue);
	this.setUIforChannelSupport(alertData.channelPreferences);
    this.setNavForPreferences(alertData.typePreferences);
	this.setAlertFlexesVisibility(true);
    this.view.switchReceiveAlerts.onSlide = this.onMasterSwitchToggle.bind(this, alertData);
    this.view.flxRowDummy.forceLayout();
    this.view.flxAlertList.forceLayout();
    applicationManager.getPresentationUtility().dismissLoadingScreen();
  }
});