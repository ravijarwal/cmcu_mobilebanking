define({
  init : function(){
    var navManager = applicationManager.getNavigationManager();
    var currentForm=navManager.getCurrentForm();
    counterPosted=0;
    applicationManager.getPresentationFormUtility().initCommonActions(this,"YES",currentForm);
  },
  preshow: function () {
    this.initActions();
    var navManager = applicationManager.getNavigationManager();
    var searchParams = navManager.getCustomInfo("frmAdvanceSearch");
    this.view.lblSearchTransactions.text = searchParams.searchDescription ;
    applicationManager.getPresentationUtility().dismissLoadingScreen();
    var navManager = applicationManager.getNavigationManager();
    var currentForm=navManager.getCurrentForm();
    applicationManager.getPresentationFormUtility().logFormName(currentForm);
  },
  initActions: function () {
    this.view.segTransactions.onRowClick = this.segTransactionsOnRowClick;
    this.view.flxHeaderSearchbox.customSearchbox.flxSearchMain.btnCancel.onClick= this.btnCancelClick ;
    this.view.flxDummySearch.onTouchEnd = this.textEdit1 ;

  },
  setTransactionData : function() {
    var scope = this;
    var navManager = applicationManager.getNavigationManager();
    var searchParams = navManager.getCustomInfo("frmAdvanceSearch");
    var TransModPresentationController = applicationManager.getModulesPresentationController("TransactionModule");
    var transactionData =  TransModPresentationController.getPendPostTransactions();
    var configManager = applicationManager.getConfigurationManager();
   // var pendingTransactions = transactionData.pendingTransactions;
    var postedTransactiondata = transactionData.postedTransactions.accountTypeList;
    if(postedTransactiondata.length===0){
      this.view.flxSearchResults.flxNoTransactions.setVisibility(true);
      this.view.segTransactions.setVisibility(false);
    }
    else{
      this.view.flxSearchResults.flxNoTransactions.setVisibility(false);
      this.view.segTransactions.setVisibility(true);
     var newPostedList = postedTransactiondata.slice(0,counterPosted +21);
     
    if(counterPosted<=postedTransactiondata.length){
        counterPosted=counterPosted+20;
        this.showPostedList(newPostedList);}
      else{
        //adding remaining records
        newPostedList=newPostedList.slice(0,postedTransactiondata.length+1);
        this.showPostedList(newPostedList);
      }
      
//       var data = [];   
//       var data2=[];
//       var max_size;
//       this.view.segTransactions.widgetDataMap = this.getDataMap();
//       max_size = Math.abs(pendingTransactions.length-25);
//       if(max_size>postedTransactions.length){
//         max_size = postedTransactions.length;
//       }

//       //pending transaction data
//       for(var i=0;i<pendingTransactions.length;i++){
//         var forUtility=applicationManager.getFormatUtilManager();
//         var trandateobj=forUtility.getDateObjectfromString(pendingTransactions[i]["transactionDate"],"YYYY-MM-DD");
//         pendingTransactions[i].transactionDate =  forUtility.getFormatedDateString(trandateobj,forUtility.APPLICATION_DATE_FORMAT);
//         pendingTransactions[i].amount = configManager.getCurrencyCode() + pendingTransactions[i].amount;
//       }

//       for(var i=0;i<max_size;i++){
//         var forUtility = applicationManager.getFormatUtilManager();
//         var trandateobj=forUtility.getDateObjectfromString(postedTransactions[i]["transactionDate"],"YYYY-MM-DD");
//         postedTransactions[i].transactionDate =  forUtility.getFormatedDateString(trandateobj,forUtility.APPLICATION_DATE_FORMAT);
//         postedTransactions[i].amount = configManager.getCurrencyCode() + postedTransactions[i].amount;
//       }
//       var dataSeg=  [
//         [
//           {
//             "lblHeader": "PendingTransactions"
//           },
//           pendingTransactions
//         ],[{"lblHeader": "PostedTransactions"},postedTransactions]];
//       this.view.segTransactions.setData(dataSeg);
//     }
  }},
  // Setting the datamap for the segment 
  getDataMap: function(){
    var dataMap = {"lblTransactionAmount": "amount",
                   "lblDate":"transactionDate",
                   "lblTransaction":"description",
                   "transactionId": "transactionId",
                   "lblHeader":"lblHeader"
                  }
    return dataMap;

  },
  segPrevTransactionsOnRowClick:function(){
    var navMan = applicationManager.getNavigationManager();
    var selectedSectionIndex=Math.floor(this.view.segTransactions.selectedRowIndex[0]);
    var selectedRowIndex=Math.floor(this.view.segTransactions.selectedRowIndex[1]);
    var data = navMan.getCustomInfo("frmAdvanceSearch");

    var transactionData=this.view.segTransactions.data[selectedSectionIndex][1][selectedRowIndex];
    navMan.setCustomInfo("frmTransactionDetails",transactionData);
    navMan.setEntryPoint("frmTransactionDetails","AdvanceSearch"); 
    var transModPresentationController = applicationManager.getModulesPresentationController("TransactionModule");
    transModPresentationController.commonFunctionForNavigation("frmTransactionDetails");
  },
  navigateBack: function(){      
    var navMan=applicationManager.getNavigationManager();
    navMan.goBack(); 
  },
  onScrollEndTransactions : function(){
    
    applicationManager.getPresentationUtility().showLoadingScreen();
    var navManager = applicationManager.getNavigationManager();
    var searchParams = navManager.getCustomInfo("frmAdvanceSearch");
     applicationManager.getPresentationUtility().showLoadingScreen();
     kony.timer.schedule("mytimer12",this.paginationPosted, 3, false);
//     var segData = this.view.segTransactions.data ;
//     var TransMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransactionModule");
//     TransMod.presentationController.getNextPostedTransactions(searchParams,segData);


  },
  assignScrollEndData : function(response){
    if(response){
      var data2=[] ;
      var configManager = applicationManager.getConfigurationManager();
      // looping the data
      var len =  this.view.segTransactions.data[1][1].length ;

      for(var i=0;i<response.length;i++){

        var forUtility=applicationManager.getFormatUtilManager();
        var trandateobj=forUtility.getDateObjectfromString(response[i]["transactionDate"],"YYYY-MM-DD");
        response[i].transactionDate =  forUtility.getFormatedDateString(trandateobj,forUtility.getApplicationDateFormat());
        response[i].amount = configManager.getCurrencyCode() + response[i].amount;

        this.view.segTransactions.addDataAt(response[i],i+len,1);

      } 

    }

  },
  textEdit1 : function(){
    this.view.flxHeaderSearchbox.customSearchbox.tbxSearch.setFocus = false;
    var navMan = applicationManager.getNavigationManager();
    navMan.goBack();

  },
  btnCancelClick : function()
  {
    var transModPresentationController = applicationManager.getModulesPresentationController("TransactionModule");
    transModPresentationController.commonFunctionForNavigation("frmAccountDetails");

  },
   showPostedList :function(newPostedList){ 
     var formatUtil = applicationManager.getFormatUtilManager();
      var lblDateModified;
      var lblrunningBalanceModified;
      var temp = {};
      var data = [];
      var lblTransactionAmountMod;
      for(var l=0;l<newPostedList.length;l++){
        var amount=newPostedList[l]["TranAmount"];
        var runningBalance=newPostedList[l]["newBalance"];
        if(runningBalance===null || runningBalance===""|| runningBalance===undefined){
          newPostedList[l]["newBalance"]="";
        }
        var  lblPrevAvailBalanceModified=newPostedList[l]["PrevAvailBalance"];
        if(amount>0){
          lblDateModified={text : newPostedList[l]["effectiveDate"],isVisible :true}
          lblrunningBalanceModified={text: newPostedList[l]["newBalance"],isVisible :false}
          lblTransactionAmountMod = {text:newPostedList[l]["TranAmount"]};
          
        }
        else if(amount<0){
          lblDateModified={text : newPostedList[l]["effectiveDate"],isVisible :true}
          lblrunningBalanceModified={text: newPostedList[l]["newBalance"],isVisible :false}
          lblTransactionAmountMod = {text:newPostedList[l]["TranAmount"]};
        }
        else{
          lblDateModified={text : newPostedList[l]["effectiveDate"],isVisible :true}
          lblTransactionAmountMod = {text:newPostedList[l]["TranAmount"]}
          lblrunningBalanceModified={text:newPostedList[l]["newBalance"],isVisible :false}
        }
        temp = {

          "lblTransactions":newPostedList[l]["StmtDesc"],
          "lblDate": lblDateModified,
          "lblRunningBalance":lblrunningBalanceModified,
          "lblTransactionsAmount": lblTransactionAmountMod,
          "lblPostedDate":newPostedList[l]["PostDate"],
          "lblSourceCode":newPostedList[l]["SourceCode"],
          "lblPrevAvailBalance":lblPrevAvailBalanceModified,
          "lblActionCode":newPostedList[l]["ActionCode"],
          "lblPostTime":newPostedList[l]["PostTime"],    
          "lblCommentCode":newPostedList[l]["CommentCode"],
          "lblRegECode":newPostedList[l]["RegECode"]
        };

        if(amount.indexOf('$')==-1){
          temp.lblTransactionsAmount.text = formatUtil.formatAmountandAppendCurrencySymbol(newPostedList[l]["TranAmount"]); } 
        if(temp.lblRunningBalance.text!==undefined && temp.lblRunningBalance.text!=="" &&temp.lblRunningBalance.text!==null ){
          temp.lblRunningBalance.text=formatUtil.formatAmountandAppendCurrencySymbol(temp.lblRunningBalance.text);}
        if(temp.lblPrevAvailBalance!==undefined && temp.lblPrevAvailBalance!=="" && temp.lblPrevAvailBalance!==null){
          temp.lblPrevAvailBalance = formatUtil.formatAmountandAppendCurrencySymbol(newPostedList[l]["PrevAvailBalance"]); }
 
          data.push(temp);   
    }
     var dataSeg=  [
        [{"lblHeader": "PostedTransactions"},data]];
      this.view.segTransactions.setData(dataSeg);    
      this.view.segTransactions.isVisible=true;
   },
  
  segTransactionsOnRowClick: function() {
    try{       	
      var navMan = applicationManager.getNavigationManager();
      var accountsDetails = navMan.getCustomInfo("frmAccountDetails");
      var type = accountsDetails.selectedAccountData["type"];
      if (type.toLowerCase().trim() === "external") {
        return;
      }

      var selectedSectionItems = this.view.segTransactions.selectedRowItems[0];
      var trnsmodule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransactionModule");
      navMan.setCustomInfo("frmTransactions", selectedSectionItems);
      navMan.setEntryPoint("Transactions","frmTransactions");
      trnsmodule.presentationController.commonFunctionForNavigation("frmTransactions");
    }catch(e){
      kony.print("Exception oc  curred at segTransactionsOnRowClick : "+e);
    }
  },
  
  paginationPosted:function(){
     applicationManager.getPresentationUtility().dismissLoadingScreen();
      this.setTransactionData();
    
  }
  
  
});