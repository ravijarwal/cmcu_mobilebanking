define(function() {

	return {
        _customCallback : null,
        _customNavigateTo : null,
        strDOB:"",
        strSSN:"",
        acceptTerms:false,
		constructor: function(baseConfig, layoutConfig, pspConfig) {
			this.setFlowAction();
		},
		//Logic for getters/setters of custom properties
		initGettersSetters: function() {

		},
        
        setCustomCallback : function(callback){
          this._customCallback = callback;
        },
      	
      	setCustomNavigateTo : function(navigate){
          this._customNavigateTo = navigate;
        },
      
        setFlowAction  : function(){
           this.view.txtEnterAccountNumber.onTextChange = this.onChangeAccountNumber;
           this.view.flxInputSSN.onClick = this.onClickInputSSN;
           this.view.txtSSN.onTextChange = this.onChangeSSN;
           this.view.flxInputDOB.onClick = this.onClickInputDOB;
           this.view.txtDOB.onTextChange = this.onChangeDOB;
           this.view.flxAcceptTerms.onClick = this.onClickAcceptTerms;
           this.view.btnContinue.onClick = this.onClickContinue;
        },
        navToSecurityCheck : function(){
          var enrollMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("EnrollModule");
          enrollMod.presentationController.commonFunctionForNavigation("frmEnrollSecurityCheck");
        },
        
        changeSkinBtnContinue : function(){
           if(this.isValidInputs()){
            // this.view.btnContinue.skin = "skinButtonEnabled";
               // this.view.btnContinue.focusSkin = "skinButtonFocus";
             	this.view.btnContinue.skin = "sknBtn0095e4RoundedffffffSSP26px";
      	        this.view.btnContinue.setEnabled(true);
           }else{
             this.view.btnContinue.skin = "skinButtonDisabled";
               this.view.btnContinue.focusSkin = "skinButtonDisabled";
              this.view.btnContinue.setEnabled(false);
           }
        },
      
        isValidDate : function(){
           if(this.strDOB.length == 10){
             let date = new Date(this.strDOB);
             var res=date instanceof Date && !isNaN(date);
             var today=new Date();
             if(res===true && date<today)
             {
               return true;
             }
             else 
              { 
                var controller = applicationManager.getPresentationUtility().getController('frmEnrollVerifyIdentity', true);
               var errorMsg = applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.common.validDOB");
               controller.bindViewError(errorMsg);
               
              }
             
           }
           return false;
        },
      
        isValidInputs : function(){
          return this.strSSN.length == 11 && this.isValidDate() && this.view.txtEnterAccountNumber.text.length>0 && this.acceptTerms;
        },

        onClickInputSSN : function(){
          this.view.txtSSN.setFocus(true);
        },

        onClickInputDOB : function(){
          this.view.txtDOB.setFocus(true);
        },

        onClickContinue : function(){
           this.validateAccountNum(this.view.txtEnterAccountNumber.text);
         // this._customNavigateTo();
        },
        onChangeAccountNumber : function(){
          this.changeSkinBtnContinue();
        },
      
        onChangeSSN : function(){
          this.updateInputBulletsSSN(this.view.txtSSN.text);
          this.changeSkinBtnContinue();
        },

        onChangeDOB : function(){
          this.updateInputBulletsDOB(this.view.txtDOB.text);
          this.changeSkinBtnContinue();
        },

        onClickAcceptTerms : function(){
          this.acceptTerms = !this.acceptTerms;
          this.view.imgCheck.src = this.acceptTerms ? "checkbox_active.png" : "checkbox_empty.png";
          this.changeSkinBtnContinue();
        },
       validateAccountNum: function(accNum) {
            accNum = this.view.txtEnterAccountNumber.text;
            ssn = this.view.txtSSN.text;
            dob=this.strDOB;
            
            var enrollModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("EnrollModule");
         enrollModule.presentationController.validateAccountNum(accNum, dob, ssn);
            
        },
        
        updateInputBulletsSSN: function(text) {
          var dummyString = '___-__-____';
          let strSSN = "";

          for(let i = 0; i < text.length; i++){
            let char = text.charAt(i);
            strSSN = strSSN + char;
            if (strSSN.length === 3 || strSSN.length === 6) {
              strSSN = strSSN + '-';
            }
          }

          this.strSSN = strSSN;
          var widgets = this.view.flxInputSSN.widgets();
          for (let i = 0; i < strSSN.length; i++) {
            // widgets[i].skin = "sknLbl979797SSP60px";
            if(strSSN[i] === '-'){
              widgets[i].text = strSSN[i];
            }else{
              widgets[i].text = "•";
            }
          }
          for (let i = strSSN.length; i < widgets.length; i++) {
            widgets[i].text = dummyString[i];
          }
          this.view.forceLayout();
        },

        updateInputBulletsDOB: function (text) {
          var scope = this;
          var dummyString = 'MM/DD/YYYY';
          let strDOB = "";

          for(let i = 0; i < text.length; i++){
            let char = text.charAt(i);
            strDOB = strDOB + char;
            if (strDOB.length === 2 || strDOB.length === 5) {
              strDOB = strDOB + '/';
            }
          }
          this.strDOB = strDOB;
          var widgets = this.view.flxInputDOB.widgets();
          for (let i = 0; i < strDOB.length; i++) {
            widgets[i].skin = "sknLbl979797SSP60px";
            widgets[i].text = strDOB[i];
          }

          for (let i = strDOB.length; i < widgets.length; i++) {
            widgets[i].skin = "sknLble3e3e3SSP60px";
            widgets[i].text = dummyString[i];
          }

          this.view.forceLayout();
        },
clearSSN : function(){
     var widgets = this.view.flxInputSSN.widgets();
    for (var i = 0; i < 11; i++) {
      if(i==3 || i==6){
        widgets[i].text = '-';
      }
      else{
        widgets[i].text = '_';
      }
      
    }
    this.view.forceLayout();
  }

	};
});