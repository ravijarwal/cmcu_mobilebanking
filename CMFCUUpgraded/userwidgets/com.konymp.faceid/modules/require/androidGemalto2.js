define(function () {
  var konymp = konymp || {};
  var konyLoggerModule = require("com/konymp/faceid/konyLogger");
  konymp.logger = (new konyLoggerModule("FaceId Component")) || function() {};
  konymp.logger.setLogLevel("DEBUG");
  constants.TOKEN_CONFIG_TYPE = {
    EPS: 0,
    CLEAR_TEXT: 1,
    OFFLINE: 2
  };
  constants.PROTOCOLS_VERSION = {
    V1: 1,
    V2: 2,
    V3: 3
  };
  constants.TOKEN_TYPE = {
    OATH: 0,
    CAP: 1
  };
  constants.LIVENESS_MODE = {
    IMAGE: 0,
    LIVENESS_PASSIVE: 1,
  };
  var initGemaltoSDK;
  return {
    /**
         * @function
         *
         * @param serviceConfig 
         * @param context 
         */
    FaceIdService:function(serviceConfig,context){
      this.context = context;
      this.config = null;
      this.serviceConfig = serviceConfig;
      this.androidBindings = require("com/konymp/faceid/androidBindings2");
      this.context.nativeClasses = this.androidBindings.import();
      this.config = JSON.parse(JSON.stringify(serviceConfig));
      this.initializeGemaltoSDK(this.config);
    },

    /**
      	 * @function
      	 *
      	 */
    initializeGemaltoSDK:function(serviceConfig){
      this.gemaltoCallback = java.newClass("gemaltoCallback","java.lang.Object",
                                           ["com.kony.konygemaltosdk.configuration.GemaltoCallback"],
                                           {
        /**
     											 * @function
     											 *
     											 */
        onVerificationSuccess:function(){
          kony.application.dismissLoadingScreen();
          this.context.onVerifySuccess();}.bind(this),
        /**
           										 * @function
           										 *
           										 * @param e 
           										 */
        onVerificationFailed:function(e){
          kony.application.dismissLoadingScreen();
          this.context.onVerifyFailure(e);}.bind(this),
        /**
            									 * @function
            									 *
            									 */
        onEnrollmentSuccess:function(){
          kony.application.dismissLoadingScreen();
          this.context.onEnrollSuccess();}.bind(this),
        /**
            									 * @function
            									 *
            									 * @param e 
            									 */
        onEnrollmentFailed:function(e){
          kony.application.dismissLoadingScreen();
          this.context.onEnrollFailure(e);}.bind(this),
        /**
            									 * @function
            									 *
            									 */
        onLicenseConfigurationSuccess:function(){
          kony.application.dismissLoadingScreen();
          this.context.onInitSuccess();}.bind(this),
        /**
            									 * @function
            									 *
            									 * @param e 
            									 */
        onLicenseConfigurationFailure:function(e){
          kony.application.dismissLoadingScreen();
          this.context.onInitFailure(e);}.bind(this),
        /**
            									 * @function
            									 *
            									 */
        onUnenrollSuccess:function(){
          kony.application.dismissLoadingScreen();
          this.context.onUnenrollSuccess();}.bind(this),
        /**
            									 * @function
            									 *
            									 * @param e 
            									 */
        onUnenrollFailure:function(e){
          kony.application.dismissLoadingScreen();
          this.context.onUnenrollFailure(e);}.bind(this)
      }
                                          ); 
      var KonyMain = this.context.nativeClasses.KonyMain;
      this.AppConfigManager = this.context.nativeClasses.AppConfigManager;
      this.AppConfigManager.initInstance(KonyMain.getAppContext());
      this.AppConfigManager.setPin("1234");
      this.AppConfigManager.setSecret("secret12345678901234");
      this.AppConfigManager.setServiceKey("ea278064-db29-4b57-abae-2f89c1a2616b");
      this.AppConfigManager.setServiceURL("https://ezio-uat.gemalto.sentinelcloud.com/ems");
      this.AppConfigManager.setGemaltoCallback(new this.gemaltoCallback());
      this.AppConfigManager.getInstance().setBlinkTimeout(2000);
      this.AppConfigManager.getInstance().setEnrollLivenessMode(true);
      this.AppConfigManager.getInstance().setMatchingThreshold(48);
      this.AppConfigManager.getInstance().setQualityThreshold(50);
      this.AppConfigManager.getInstance().setVerifyLivenessMode(true);
      this.AppConfigManager.getInstance().setLivenessThreshold(0);
      this.AppConfigManager.setTokenName("login");

    },
    permissionCallback: function(initCallbacks, permissionStatus) {
      konymp.logger.debug("", konymp.logger.FUNCTION_ENTRY);
      try {
        if (permissionStatus.status == kony.application.PERMISSION_GRANTED) {
          try {
              initGemaltoSDK = new (java.import("com.kony.konygemaltosdk.configuration.InitGemaltoSDK"))();
          initGemaltoSDK.initiateGemalto(this.context.nativeClasses.KonyMain.getAppContext());
          } catch (exception) {
            konymp.logger.error(JSON.stringify(exception), konymp.logger.EXCEPTION);
          }
                } else {
          initCallbacks.onFailed("Initialization failed. Permission denied to access camera");
        }
      } catch (exception) {
        konymp.logger.error(JSON.stringify(exception), konymp.logger.EXCEPTION);
      }
      konymp.logger.debug("", konymp.logger.FUNCTION_EXIT);
    },
    requestPhoneStatePermission: function() {
      konymp.logger.debug("", konymp.logger.FUNCTION_ENTRY);
         var uid = kony.os.deviceInfo().uid; //This is a hack to get phone state permission
      konymp.logger.debug("", konymp.logger.FUNCTION_EXIT);
    },
    /**
       * @function
       *
       * @param self 
       */
    initialize:function(self){
      try {
               
        	    this.requestPhoneStatePermission();
                var cameraPermission = kony.application.checkPermission(kony.os.RESOURCE_CAMERA);
                if (cameraPermission.status == kony.application.PERMISSION_DENIED) {
                    kony.application.requestPermission(kony.os.RESOURCE_CAMERA, this.permissionCallback.bind(this,self));
                } else {
                  initGemaltoSDK = new (java.import("com.kony.konygemaltosdk.configuration.InitGemaltoSDK"))();
     			  initGemaltoSDK.initiateGemalto(this.context.nativeClasses.KonyMain.getActContext());   
                }
            } catch (exception) {
                konymp.logger.error(JSON.stringify(exception), konymp.logger.EXCEPTION);
            }
   
  //    initGemaltoSDK = new (java.import("com.kony.konygemaltosdk.configuration.InitGemaltoSDK"))();
  //    initGemaltoSDK.initiateGemalto(this.context.nativeClasses.KonyMain.getActContext());
    },
    /**
       * @function
       *
       * @param self 
       */
    enroll:function(self){
      initGemaltoSDK.enroll();
//       var Intent = this.context.nativeClasses.Intent;
//       var FaceEnrollAvtivity = this.context.nativeClasses.GemaltoKony;
//       var intent = new Intent(this.context.nativeClasses.KonyMain.getActContext(), FaceEnrollAvtivity.class);
//       intent.putExtra("stepName","enroll");
//       this.context.nativeClasses.KonyMain.getActContext().startActivity(intent);
    },
    /**
       * @function
       *
       * @param self 
       */
    verify:function(self){
      initGemaltoSDK.verify();
    },
    /**
       * @function
       *
       * @param self 
       */
    unenroll:function(self){
      initGemaltoSDK.unenroll();
    },
    /**
       * @function
       *
       * @param self 
       */
    uninitialize:function(self){
      initGemaltoSDK.unitializeGemalto();
    }

  };
});