/**
#
#  Created by Team Kony.
#  Copyright (c) 2017 Kony Inc. All rights reserved.
#
**/
define(function () {
  	
  	var konymp = konymp || {};
	var konyLoggerModule = require("com/konymp/faceid/konyLogger");
	konymp.logger = (new konyLoggerModule("FaceId Component")) || function () {};
	konymp.logger.setLogLevel("DEBUG");
  
   this.instance=null;
    return {
     importClasses:   function (){
       	try{
		var nativeClasses = {};
		nativeClasses.KonyMain = java.import("com.konylabs.android.KonyMain");
		nativeClasses.AppConfigManager = java.import("com.kony.konygemaltosdk.configuration.AppConfigManager");
        nativeClasses.Intent = java.import("android.content.Intent");
        nativeClasses.GemaltoKony = java.import("com.kony.konygemaltosdk.GemaltoKony"); 
          
		return nativeClasses;
        }catch(exception){
          konymp.logger.error(JSON.stringify(exception), konymp.logger.EXCEPTION);
        }
	},
      import : function(){
     try{ 
			if(this.instance === undefined || this.instance === null){
              
				this.instance = this.importClasses();
			}
			return this.instance;
     		}catch(exception){
                  konymp.logger.error(JSON.stringify(exception), konymp.logger.EXCEPTION);

    		}
		}
    };
});