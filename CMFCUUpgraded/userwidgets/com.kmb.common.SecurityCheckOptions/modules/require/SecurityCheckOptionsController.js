define(function() {
  return {
    constructor: function(baseConfig, layoutConfig, pspConfig) {},
    //Logic for getters/setters of custom properties
    initGettersSetters: function() {},
    onRowClickEmailCode: function() {},
    onRowClickPhoneCall: function() {},
    onRowClicktextMessage: function() {},
    onClickCVV: function() {},
    selectedEmailRowIndex: function() {
      var selectedEmail = this.view.emailCode.selectedRowIndex();
      return selectedEmail;
    },
    selectedMobileNumRowIndex: function() {
      var selectedMobile = this.view.phoneCallCode.selectedMobileRowIndex();
      return selectedMobile;
    },
    selectedMobileForTextIndex: function() {
      var selectedNumber = this.view.textMessageCode.selectedMobileRowIndex();
      return selectedNumber;
    },
    onClickEmailSetData: function() {
      var navManager = applicationManager.getNavigationManager();
      var userDetails = navManager.getCustomInfo("frmEnrollSecurityCheck");
      var home, mobile,maskPhnNum;
      lblWorkEmailVal ="";
      lblPersonalEmailVal="";
      lblHomePhoneVal="";
      lblWorkPhoneVal="";
      lblMobilePhoneVal="";
      lblHomeEmailVal="";
      emails = [];
      phones = [];
      emailFlag = true;
      if (userDetails && userDetails.nameEmail === null && userDetails.nameEmail === undefined) {
        emailFlag = false;
      }
      navManager.setCustomInfo("frmEnrollSecurity", {
        "isEmailPresent": emailFlag
      });
      
      if (userDetails && userDetails.nameEmail !== null && userDetails.nameEmail !== undefined) {
        lblPersonalEmailVal = "Personal: " + userDetails.nameEmail;
        userDetails.nameEmail = applicationManager.getDataProcessorUtility().maskAccountEmail(userDetails.nameEmail);
        emails.push("Personal: " + userDetails.nameEmail);
      }
      
      if (userDetails && userDetails.nameAltEmail !== null && userDetails.nameAltEmail !== undefined) {
        lblWorkEmailVal = "Work: " + userDetails.nameAltEmail;
        userDetails.nameAltEmail = applicationManager.getDataProcessorUtility().maskAccountEmail(userDetails.nameAltEmail);
        emails.push("Work: " + userDetails.nameAltEmail);
      }
      
      if (userDetails && userDetails.nameMobilePhone !== null && userDetails.nameMobilePhone !== undefined) {
        lblMobilePhoneVal = "Mobile: " + userDetails.nameMobilePhone.replace(/-/g,"");
        maskPhnNum = this.maskedValue(userDetails.nameMobilePhone);
        mobile = "Mobile: " + maskPhnNum;
        phones.push(mobile);
      }
      
      if (userDetails && userDetails.nameHomePhone !== null && userDetails.nameHomePhone !== undefined) {
        lblHomePhoneVal = "Home: " + userDetails.nameHomePhone.replace(/-/g,"");
        maskPhnNum = this.maskedValue(userDetails.nameHomePhone);
        home = "Home: " + maskPhnNum;
        phones.push(home);
      }

      /*lblHomeEmailVal = "Home:" + "sirishayendamuri@gmail.com";
      var email = applicationManager.getDataProcessorUtility().maskAccountEmail("sirishayendamuri@gmail.com");
      emails.push("Home:" + email);*/
      
      lblHomeEmailVal = "Home: " + "anil85.p@gmail.com";
      var email = applicationManager.getDataProcessorUtility().maskAccountEmail("anil85.p@gmail.com");
      emails.push("Home: " + email);

      var phone = "248-839-1507";
      lblWorkPhoneVal = "Work: " + phone.replace(/-/g,"");
      maskPhnNum = this.maskedValue(phone);
      phones.push("Work: " + maskPhnNum);

      this.view.phoneCallCode.setSegmentData(phones);
      this.view.textMessageCode.setSegmentData(phones);
      this.view.emailCode.setSegmentData(emails);
    },

    //defined for masking the phone numbers
    maskedValue : function(data){
      var str1 = data.slice(0, 4);
      var maskedValue = data.slice(4,7);
      var str2 = data.slice(7,12);
      maskedValue = maskedValue.replace(maskedValue, '***');
      return str1+ maskedValue + str2;
    }
  };
});