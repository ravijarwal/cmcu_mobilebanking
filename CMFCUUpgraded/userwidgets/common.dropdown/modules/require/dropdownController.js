define(function() {
  return {
    _opened: false,
    flag: 0,
    prevSelItems: [],
    prevSelIndex: [],
    lblHiddenValue: "",
    row : "",
    row2:"",
    constructor: function(baseConfig, layoutConfig, pspConfig) {
      this.setBindings();
    },
    setBindings: function() {
      this.view.flxDropdownHeader.onClick = this.toggleDropdown.bind(this);
    },
    toggleDropdown: function() {
      if (this._opened) {
        this.closeDropdown();
      } else {
        this.openDropdown();
      }
    },
    closeDropdown: function() {
      this.view.sgmOptions.setVisibility(false);
      this.view.imgArrow.src = "calenderarrowright.png";
      this._opened = false;
    },
    openDropdown: function() {
      this.view.sgmOptions.setVisibility(true);
      this.view.imgArrow.src = "arrowdownblue.png";
      this._opened = true;
    },
    setSegmentData: function(array) {
      var data = [];
      var imgActive = {
        src: "radio_inactive.png"
      };
      for (var i = 0; i < array.length; i++) {
        var temp = {
          "lblOption": array[i],
          "imgRadio": imgActive,
          "lblWrkEmailHiddenVal": lblWorkEmailVal,
          "lblPrsnlEmailHiddenVal": lblPersonalEmailVal,
          "lblHomePhnHiddenVal": lblHomePhoneVal,
          "lblWrkPhnHiddenVal": lblWorkPhoneVal,
          "lblMobilePhnHiddenVal":lblMobilePhoneVal,
          "lblHomeEmailHiddenVal" :lblHomeEmailVal
        };
        data.push(temp);
      }
      this.view.sgmOptions.setData(data);
    },

    selectedRowIndex: function() {
      var selItems, selIndex;
      selItems = this.view.sgmOptions.selectedRowItems[0];
      selIndex = this.view.sgmOptions.selectedRowIndex[1];
      var imgActive = {
        src: "radio_active.png"
      };
      selItems.imgRadio = imgActive;
      if (selItems.lblOption.includes("Personal") && selItems.lblPrsnlEmailHiddenVal!=="") {
        row = selItems.lblPrsnlEmailHiddenVal;
        row2 = selItems.lblOption;
      } else if (selItems.lblOption.includes("Work") && selItems.lblWrkEmailHiddenVal!=="") {
        row = selItems.lblWrkEmailHiddenVal;
        row2 = selItems.lblOption;
      } else if (selItems.lblOption.includes("Home")&& selItems.lblHomeEmailHiddenVal!=="") {
        row = selItems.lblHomeEmailHiddenVal;
        row2 = selItems.lblOption;
      }
      lblHiddenValue = this.rowData(row);
      var lblOption =this.rowData(row2);
      var navManager = applicationManager.getNavigationManager();
      navManager.setCustomInfo("dropDownList",lblOption);
      this.view.sgmOptions.setDataAt(selItems, selIndex);
      return lblHiddenValue;
    },

    selectedMobileRowIndex: function() {
      var selItems, selIndex;
      selItems = this.view.sgmOptions.selectedRowItems[0];
      selIndex = this.view.sgmOptions.selectedRowIndex[1];
      var imgActive = {
        src: "radio_active.png"
      };
      selItems.imgRadio = imgActive;
      if (selItems.lblOption.includes("Mobile") && selItems.lblMobilePhnHiddenVal!=="") {
        row = selItems.lblMobilePhnHiddenVal;
        row2 = selItems.lblOption;
      } else if (selItems.lblOption.includes("Home")&&selItems.lblHomePhnHiddenVal!=="") {
        row = selItems.lblHomePhnHiddenVal;
        row2 = selItems.lblOption;
      } else if (selItems.lblOption.includes("Work")&&selItems.lblWrkPhnHiddenVal!=="") {
        row = selItems.lblWrkPhnHiddenVal;
        row2 = selItems.lblOption;
      }
      lblHiddenValue = this.rowData(row);
      var lblOption=this.rowData(row2);
      var navManager = applicationManager.getNavigationManager();
      navManager.setCustomInfo("dropDownList",lblOption);
      this.view.sgmOptions.setDataAt(selItems, selIndex);
      return lblHiddenValue;
    },

    rowData: function(row) {
      selectedVal = "";
      if (row !== null && row !== undefined && row !== "") {
        if (row.includes("Mobile")) {
          selectedVal = row.replace("Mobile: ", "");
        } else if (row.includes("Home")) {
          selectedVal = row.replace("Home: ", "");
        } else if (row.includes("Personal")) {
          selectedVal = row.replace("Personal: ", "");
        } else if (row.includes("Work")) {
          selectedVal = row.replace("Work: ", "");
        }
        return selectedVal;
      }
    },
    initGettersSetters: function() {}
  };
});