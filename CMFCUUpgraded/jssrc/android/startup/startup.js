//startup.js
var globalhttpheaders = {};
var appConfig = {
    appId: "KonyMobileBankin",
    appName: "CMCU Mobile Banking",
    appVersion: "4.1.1",
    isturlbase: "https://cmcu-dev.konycloud.com/services",
    isDebug: true,
    isMFApp: true,
    appKey: "82c3f73903f6704c38857c2898e0af64",
    appSecret: "5d94bc70154936eeb3c16a00e6b67e19",
    serviceUrl: "https://100025005.auth.konycloud.com/appconfig",
    svcDoc: {
        "identity_meta": {
            "LinkedIn": {
                "success_url": "allow_any"
            },
            "CMCUIdentityService": {
                "success_url": "allow_any"
            }
        },
        "app_version": "1.0",
        "messagingsvc": {
            "appId": "8adc4a13-c054-4339-a675-36976c97ec0a",
            "url": "https://cmcu-dev.messaging.konycloud.com/api/v1"
        },
        "baseId": "da0ed4af-f2bc-455c-94db-a221ea5e7760",
        "app_default_version": "1.0",
        "login": [{
            "mandatory_fields": [],
            "provider_type": "oauth2",
            "alias": "CMCUIdentityService",
            "type": "basic",
            "prov": "CMCUIdentityService",
            "url": "https://100025005.auth.konycloud.com"
        }, {
            "mandatory_fields": [],
            "provider_type": "custom",
            "alias": "DbxUserLogin",
            "type": "basic",
            "prov": "DbxUserLogin",
            "url": "https://100025005.auth.konycloud.com"
        }, {
            "mandatory_fields": [],
            "provider_type": "custom",
            "alias": "NUOApplicantLogin",
            "type": "basic",
            "prov": "NUOApplicantLogin",
            "url": "https://100025005.auth.konycloud.com"
        }, {
            "provider_type": "oauth2",
            "alias": "LinkedIn",
            "type": "oauth2",
            "prov": "LinkedIn",
            "url": "https://100025005.auth.konycloud.com"
        }, {
            "mandatory_fields": [],
            "provider_type": "custom",
            "alias": "CustomLogin",
            "type": "basic",
            "prov": "CustomLogin",
            "url": "https://100025005.auth.konycloud.com"
        }],
        "services_meta": {
            "dbpLocalServiceslogin": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://cmcu-dev.konycloud.com/services/dbpLocalServiceslogin"
            },
            "RBObjects": {
                "offline": false,
                "metadata_url": "https://cmcu-dev.konycloud.com/services/metadata/v1/RBObjects",
                "type": "objectsvc",
                "version": "1.0",
                "url": "https://cmcu-dev.konycloud.com/services/data/v1/RBObjects"
            },
            "dbpLibraryUtilitiesorch": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://cmcu-dev.konycloud.com/services/dbpLibraryUtilitiesorch"
            },
            "CUSOFinancial": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://cmcu-dev.konycloud.com/services/CUSOFinancial"
            },
            "ATMLocator": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://cmcu-dev.konycloud.com/services/ATMLocator"
            },
            "dbpRbLocalServicesdb": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://cmcu-dev.konycloud.com/services/dbpRbLocalServicesdb"
            },
            "IdologyIntServices": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://cmcu-dev.konycloud.com/services/IdologyIntServices"
            },
            "dbpDBX": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://cmcu-dev.konycloud.com/services/dbpDBX"
            },
            "EnrollmentLooping": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://cmcu-dev.konycloud.com/services/EnrollmentLooping"
            },
            "dbpAllLocations": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://cmcu-dev.konycloud.com/services/dbpAllLocations"
            },
            "CMCUDowntimeMessagingService": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://cmcu-dev.konycloud.com/services/CMCUDowntimeMessagingService"
            },
            "CMCUMessagingServices": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://cmcu-dev.konycloud.com/services/CMCUMessagingServices"
            },
            "dbpLibraryUtilitiesdb": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://cmcu-dev.konycloud.com/services/dbpLibraryUtilitiesdb"
            },
            "CommonOrchestrationService": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://cmcu-dev.konycloud.com/services/CommonOrchestrationService"
            },
            "IntegrationRegulator": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://cmcu-dev.konycloud.com/services/IntegrationRegulator"
            },
            "dbpAdminConsoleServices": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://cmcu-dev.konycloud.com/services/dbpAdminConsoleServices"
            },
            "KmsEmailOrch": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://cmcu-dev.konycloud.com/services/KmsEmailOrch"
            },
            "SymXchangeAccountServices": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://cmcu-dev.konycloud.com/services/SymXchangeAccountServices"
            },
            "SymXTransfersService": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://cmcu-dev.konycloud.com/services/SymXTransfersService"
            },
            "SymXchangeJavaService": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://cmcu-dev.konycloud.com/services/SymXchangeJavaService"
            },
            "CMCUUserServices": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://cmcu-dev.konycloud.com/services/CMCUUserServices"
            },
            "SymXchangeTransactionServices": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://cmcu-dev.konycloud.com/services/SymXchangeTransactionServices"
            },
            "PushNotification": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://cmcu-dev.konycloud.com/services/PushNotification"
            },
            "pushNotifAuthService": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://cmcu-dev.konycloud.com/services/pushNotifAuthService"
            },
            "LogoutService": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://cmcu-dev.konycloud.com/services/LogoutService"
            },
            "LinkedInService": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://cmcu-dev.konycloud.com/services/LinkedInService"
            },
            "dbpUserAttributes": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://cmcu-dev.konycloud.com/services/dbpUserAttributes"
            },
            "dbpBBJavaServices": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://cmcu-dev.konycloud.com/services/dbpBBJavaServices"
            },
            "LibraryUtilities": {
                "offline": false,
                "metadata_url": "https://cmcu-dev.konycloud.com/services/metadata/v1/LibraryUtilities",
                "type": "objectsvc",
                "version": "1.0",
                "url": "https://cmcu-dev.konycloud.com/services/data/v1/LibraryUtilities"
            },
            "UserAttributes": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://cmcu-dev.konycloud.com/services/UserAttributes"
            },
            "dbpDbxCustomerLogin": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://cmcu-dev.konycloud.com/services/dbpDbxCustomerLogin"
            },
            "dbpDbxExternalLogin": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://cmcu-dev.konycloud.com/services/dbpDbxExternalLogin"
            },
            "SymXchangeOrchestrationService": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://cmcu-dev.konycloud.com/services/SymXchangeOrchestrationService"
            },
            "GoogleAPIs": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://cmcu-dev.konycloud.com/services/GoogleAPIs"
            },
            "ApprovalRequestObjects": {
                "offline": false,
                "metadata_url": "https://cmcu-dev.konycloud.com/services/metadata/v1/ApprovalRequestObjects",
                "type": "objectsvc",
                "version": "1.0",
                "url": "https://cmcu-dev.konycloud.com/services/data/v1/ApprovalRequestObjects"
            },
            "ACHObjects": {
                "offline": false,
                "metadata_url": "https://cmcu-dev.konycloud.com/services/metadata/v1/ACHObjects",
                "type": "objectsvc",
                "version": "1.0",
                "url": "https://cmcu-dev.konycloud.com/services/data/v1/ACHObjects"
            },
            "dbpLibraryUtilitiesjava": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://cmcu-dev.konycloud.com/services/dbpLibraryUtilitiesjava"
            },
            "TestService": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://cmcu-dev.konycloud.com/services/TestService"
            },
            "DashboardLooping": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://cmcu-dev.konycloud.com/services/DashboardLooping"
            },
            "BackgroundVerification": {
                "offline": false,
                "metadata_url": "https://cmcu-dev.konycloud.com/services/metadata/v1/BackgroundVerification",
                "type": "objectsvc",
                "version": "1.0",
                "url": "https://cmcu-dev.konycloud.com/services/data/v1/BackgroundVerification"
            },
            "dbpRbLocalServicesJava": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://cmcu-dev.konycloud.com/services/dbpRbLocalServicesJava"
            },
            "DashBoard": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://cmcu-dev.konycloud.com/services/DashBoard"
            },
            "dbpLocalServiceNUOLogin": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://cmcu-dev.konycloud.com/services/dbpLocalServiceNUOLogin"
            },
            "backgroundVerificationOrch": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://cmcu-dev.konycloud.com/services/backgroundVerificationOrch"
            },
            "TransactionObjects": {
                "offline": false,
                "metadata_url": "https://cmcu-dev.konycloud.com/services/metadata/v1/TransactionObjects",
                "type": "objectsvc",
                "version": "1.0",
                "url": "https://cmcu-dev.konycloud.com/services/data/v1/TransactionObjects"
            },
            "BBMockServices": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://cmcu-dev.konycloud.com/services/BBMockServices"
            },
            "CMCUUserService": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://cmcu-dev.konycloud.com/services/CMCUUserService"
            },
            "SymXParameterService": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://cmcu-dev.konycloud.com/services/SymXParameterService"
            },
            "SymXchangePreLogin": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://cmcu-dev.konycloud.com/services/SymXchangePreLogin"
            },
            "VerifyBackground": {
                "offline": false,
                "metadata_url": "https://cmcu-dev.konycloud.com/services/metadata/v1/VerifyBackground",
                "type": "objectsvc",
                "version": "1.0",
                "url": "https://cmcu-dev.konycloud.com/services/data/v1/VerifyBackground"
            },
            "SymXFindBy": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://cmcu-dev.konycloud.com/services/SymXFindBy"
            },
            "DbxKMSLogin": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://cmcu-dev.konycloud.com/services/DbxKMSLogin"
            },
            "SendPushMessage": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://cmcu-dev.konycloud.com/services/SendPushMessage"
            }
        },
        "selflink": "https://100025005.auth.konycloud.com/appconfig",
        "integsvc": {
            "dbpLocalServiceslogin": "https://cmcu-dev.konycloud.com/services/dbpLocalServiceslogin",
            "dbpLibraryUtilitiesorch": "https://cmcu-dev.konycloud.com/services/dbpLibraryUtilitiesorch",
            "CUSOFinancial": "https://cmcu-dev.konycloud.com/services/CUSOFinancial",
            "LinkedInService": "https://cmcu-dev.konycloud.com/services/LinkedInService",
            "ATMLocator": "https://cmcu-dev.konycloud.com/services/ATMLocator",
            "dbpRbLocalServicesdb": "https://cmcu-dev.konycloud.com/services/dbpRbLocalServicesdb",
            "dbpUserAttributes": "https://cmcu-dev.konycloud.com/services/dbpUserAttributes",
            "dbpBBJavaServices": "https://cmcu-dev.konycloud.com/services/dbpBBJavaServices",
            "UserAttributes": "https://cmcu-dev.konycloud.com/services/UserAttributes",
            "IdologyIntServices": "https://cmcu-dev.konycloud.com/services/IdologyIntServices",
            "dbpDbxCustomerLogin": "https://cmcu-dev.konycloud.com/services/dbpDbxCustomerLogin",
            "dbpDbxExternalLogin": "https://cmcu-dev.konycloud.com/services/dbpDbxExternalLogin",
            "SymXchangeOrchestrationService": "https://cmcu-dev.konycloud.com/services/SymXchangeOrchestrationService",
            "dbpDBX": "https://cmcu-dev.konycloud.com/services/dbpDBX",
            "EnrollmentLooping": "https://cmcu-dev.konycloud.com/services/EnrollmentLooping",
            "dbpAllLocations": "https://cmcu-dev.konycloud.com/services/dbpAllLocations",
            "GoogleAPIs": "https://cmcu-dev.konycloud.com/services/GoogleAPIs",
            "CMCUDowntimeMessagingService": "https://cmcu-dev.konycloud.com/services/CMCUDowntimeMessagingService",
            "CMCUMessagingServices": "https://cmcu-dev.konycloud.com/services/CMCUMessagingServices",
            "dbpLibraryUtilitiesdb": "https://cmcu-dev.konycloud.com/services/dbpLibraryUtilitiesdb",
            "CommonOrchestrationService": "https://cmcu-dev.konycloud.com/services/CommonOrchestrationService",
            "dbpLibraryUtilitiesjava": "https://cmcu-dev.konycloud.com/services/dbpLibraryUtilitiesjava",
            "TestService": "https://cmcu-dev.konycloud.com/services/TestService",
            "IntegrationRegulator": "https://cmcu-dev.konycloud.com/services/IntegrationRegulator",
            "dbpAdminConsoleServices": "https://cmcu-dev.konycloud.com/services/dbpAdminConsoleServices",
            "DashboardLooping": "https://cmcu-dev.konycloud.com/services/DashboardLooping",
            "dbpRbLocalServicesJava": "https://cmcu-dev.konycloud.com/services/dbpRbLocalServicesJava",
            "DashBoard": "https://cmcu-dev.konycloud.com/services/DashBoard",
            "KmsEmailOrch": "https://cmcu-dev.konycloud.com/services/KmsEmailOrch",
            "_internal_logout": "https://cmcu-dev.konycloud.com/services/IST",
            "dbpLocalServiceNUOLogin": "https://cmcu-dev.konycloud.com/services/dbpLocalServiceNUOLogin",
            "SymXchangeAccountServices": "https://cmcu-dev.konycloud.com/services/SymXchangeAccountServices",
            "SymXTransfersService": "https://cmcu-dev.konycloud.com/services/SymXTransfersService",
            "SymXchangeJavaService": "https://cmcu-dev.konycloud.com/services/SymXchangeJavaService",
            "backgroundVerificationOrch": "https://cmcu-dev.konycloud.com/services/backgroundVerificationOrch",
            "BBMockServices": "https://cmcu-dev.konycloud.com/services/BBMockServices",
            "CMCUUserService": "https://cmcu-dev.konycloud.com/services/CMCUUserService",
            "CMCUUserServices": "https://cmcu-dev.konycloud.com/services/CMCUUserServices",
            "SymXParameterService": "https://cmcu-dev.konycloud.com/services/SymXParameterService",
            "SymXchangeTransactionServices": "https://cmcu-dev.konycloud.com/services/SymXchangeTransactionServices",
            "SymXchangePreLogin": "https://cmcu-dev.konycloud.com/services/SymXchangePreLogin",
            "PushNotification": "https://cmcu-dev.konycloud.com/services/PushNotification",
            "SymXFindBy": "https://cmcu-dev.konycloud.com/services/SymXFindBy",
            "DbxKMSLogin": "https://cmcu-dev.konycloud.com/services/DbxKMSLogin",
            "SendPushMessage": "https://cmcu-dev.konycloud.com/services/SendPushMessage",
            "pushNotifAuthService": "https://cmcu-dev.konycloud.com/services/pushNotifAuthService",
            "LogoutService": "https://cmcu-dev.konycloud.com/services/LogoutService"
        },
        "service_doc_etag": "0000016BACA388D8",
        "appId": "8adc4a13-c054-4339-a675-36976c97ec0a",
        "identity_features": {
            "reporting_params_header_allowed": true
        },
        "name": "DbpLocalServices",
        "reportingsvc": {
            "session": "https://cmcu-dev.konycloud.com/services/IST",
            "custom": "https://cmcu-dev.konycloud.com/services/CMS"
        },
        "Webapp": {
            "url": "https://cmcu-dev.konycloud.com/apps/KonyMobileBank"
        }
    },
    runtimeAppVersion: "1.0",
    eventTypes: ["FormEntry", "Error", "Crash"],
};
sessionID = "";

function setAppBehaviors() {
    kony.application.setApplicationBehaviors({
        applyMarginPaddingInBCGMode: false,
        adherePercentageStrictly: true,
        retainSpaceOnHide: true,
        marginsIncludedInWidgetContainerWeight: true,
        isMVC: true,
        APILevel: 8000
    })
};

function themeCallBack() {
    initializeGlobalVariables();
    applicationController = require("applicationController");
    callAppMenu();
    kony.application.setApplicationInitializationEvents({
        init: applicationController.appInit,
        preappinit: applicationController.AS_AppEvents_h4ded27d9dd845f885ad538266976f45,
        appservice: function(eventObject) {
            var value = applicationController.AS_AppEvents_hd4fc1bedd904d29862f6382068c9886(eventObject);
            return value;
        },
        postappinit: applicationController.postAppInitCallBack,
        showstartupform: function() {
            new kony.mvc.Navigation("frmLogin").navigate();
        }
    });
};

function onSuccess(oldlocalname, newlocalename, info) {
    loadResources();
};

function onFailure(errorcode, errormsg, info) {
    loadResources();
};

function loadResources() {
    globalhttpheaders = {};
    kony.os.loadLibrary({
        "javaclassname": "com.konylabs.ffi.N_LocationSettings"
    });
    kony.os.loadLibrary({
        "javaclassname": "com.konylabs.ffi.N_SignatureFFI"
    });
    kony.os.loadLibrary({
        "javaclassname": "com.konylabs.ffi.N_Barcode"
    });
    kony.os.loadLibrary({
        "javaclassname": "com.konylabs.ffi.N_ExceptionLogger"
    });
    kony.os.loadLibrary({
        "javaclassname": "com.konylabs.ffi.N_contactsAPI"
    });
    kony.os.loadLibrary({
        "javaclassname": "com.konylabs.ffi.N_KonyLogger"
    });
    kony.os.loadLibrary({
        "javaclassname": "com.konylabs.ffi.N_binarydata"
    });
    kony.os.loadLibrary({
        "javaclassname": "com.konylabs.ffi.ND_binary_util"
    });
    kony.os.loadLibrary({
        "javaclassname": "com.konylabs.ffi.N_WindowsOfflineObjects"
    });
    sdkInitConfig = {
        "appConfig": appConfig,
        "isMFApp": appConfig.isMFApp,
        "appKey": appConfig.appKey,
        "appSecret": appConfig.appSecret,
        "eventTypes": appConfig.eventTypes,
        "serviceUrl": appConfig.serviceUrl
    }
    kony.setupsdks(sdkInitConfig, onSuccessSDKCallBack, onSuccessSDKCallBack);
};

function onSuccessSDKCallBack() {
    kony.theme.setCurrentTheme("BlueTheme", themeCallBack, themeCallBack);
}
kony.application.setApplicationMode(constants.APPLICATION_MODE_NATIVE);
//This is the entry point for the application.When Locale comes,Local API call will be the entry point.
kony.i18n.setDefaultLocaleAsync("en", onSuccess, onFailure, null);
debugger;