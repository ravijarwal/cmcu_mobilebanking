define("EnrollModule/userfrmEnrollSecurityCheckController", {
    init: function() {
        var navManager = applicationManager.getNavigationManager();
        var currentForm = navManager.getCurrentForm();
        applicationManager.getPresentationFormUtility().initCommonActions(this, "YES", currentForm);
        // this.view.SecurityCheckOptions.setTextMessageOptions(["Mobile: 123-***-7890","Home: 987-***-4321"]);
        // this.view.SecurityCheckOptions.setPhoneCallOptions(["Mobile: 123-***-7890","Home: 987-***-4321"]);
        //  this.view.SecurityCheckOptions.setEmailCodeOptions(["si***n1@gmail.com","si***an@cmcu.com"]);
    },
    frmSecurityCheckPreShow: function() {
        this.setFlowActions();
        this.setPreshowData();
        type = "";
        destination = "";
        applicationManager.getPresentationUtility().dismissLoadingScreen();
        var navManager = applicationManager.getNavigationManager();
        var currentForm = navManager.getCurrentForm();
        applicationManager.getPresentationFormUtility().logFormName(currentForm);
    },
    setFlowActions: function() {
        var scopeObj = this;
        this.view.flxSeparator.setVisibility(false);
        this.view.customHeader.flxBack.onClick = function() {
            scopeObj.navBack();
        };
        this.view.customHeader.btnRight.onClick = function() {
            scopeObj.onClickCancel();
        };
        //     this.view.flxCVV.onClick = function(){
        //       scopeObj.getAllCards();
        //       //       scopeObj.navToCVV();
        //     };
        //     this.view.flxSecurityCode.onClick = function(){
        //       scopeObj.triggerOTP();
        //     };
        // this.view.SecurityCheckOptions.onClickCVV=function(){
        //     scopeObj.getAllCards();
        //    scopeObj.navToCVV();
        // };
        this.view.SecurityCheckOptions.onClickEmailSetData();
        this.view.SecurityCheckOptions.onRowClicktextMessage = function() {
            scopeObj.SelectedMobileText();
            destination = textNumber;
            type = "t";
            scopeObj.sendOTP(destination, type);
        };
        this.view.SecurityCheckOptions.onRowClickPhoneCall = function() {
            scopeObj.selectedMobileNum();
            destination = mobNumber;
            type = "v";
            scopeObj.sendOTP(destination, type);
        };
        this.view.SecurityCheckOptions.onRowClickEmailCode = function() {
            scopeObj.selectedEmail();
            destination = email;
            type = "m";
            scopeObj.sendOTP(destination, type);
        };
    },
    setPreshowData: function() {
        this.view.customHeader.lblLocateUs.text = kony.i18n.getLocalizedString("kony.mb.enroll.securityMethod");
        this.view.customHeader.btnRight.isVisible = true;
        this.view.customHeader.flxHeader.btnRight.text = kony.i18n.getLocalizedString("kony.mb.common.CancelSmall");
        this.view.customHeader.flxHeader.btnRight.skin = "sknLblb8dcffSSP32px";
        this.view.customHeader.flxHeader.btnRight.focusSkin = "sknLblb8dcffSSP32px";
        this.view.customHeader.flxBack.isVisible = false;
        this.view.lblSecurityCheck.text = kony.i18n.getLocalizedString("kony.mb.EnrollSecurityCheck.SecurityCheck");
        this.view.lblDescription.text = kony.i18n.getLocalizedString("kony.mb.enroll.verification");
        /*if(kony.os.deviceInfo().name !== "iPhone"){
          this.view.flxHeader.isVisible = true;
        }
        else{
          this.view.flxHeader.isVisible = false;
        }*/
        this.view.flxHeader.isVisible = true;
    },
    navBack: function() {
        var navManager = applicationManager.getNavigationManager();
        navManager.setCustomInfo("isValidUser", true);
        navManager.goBack();
    },
    onClickCancel: function() {
        /* kony.ui.Alert({
              "alertType": constants.ALERT_TYPE_CONFIRMATION,
              "alertTitle": null,
              "yesLabel": kony.i18n.getLocalizedString("kony.mb.enroll.dontCancel"),
              "noLabel": kony.i18n.getLocalizedString("kony.mb.common.AlertYes"),
              "message": kony.i18n.getLocalizedString("kony.mb.enroll.cancelPopup"),
              "alertHandler": this.alertCallback
            }, {
              "iconPosition": constants.ALERT_CONTENT_ALIGN_CENTER,
              "contentAlignment": constants.ALERT_ICON_POSITION_LEFT
            });*/
        var i18n = applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.enroll.cancelPopup");
        kony.ui.Alert(i18n, this.alertCallback, constants.ALERT_TYPE_CONFIRMATION, "Yes", "No", "");
    },
    alertCallback: function(response) {
        if (response === true) {
            var enrollMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("EnrollModule");
            enrollMod.presentationController.resetEnrollObj();
        }
    },
    navToCVV: function() {
        var enrollMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("EnrollModule");
        enrollMod.presentationController.commonFunctionForNavigation("frmEnrollCVV");
    },
    navToSecurityCode: function() {
        var enrollMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("EnrollModule");
        enrollMod.presentationController.commonFunctionForNavigation("frmEnrollSecurity");
    },
    /**
     *Code to fetch all cards for the entered SSN
     */
    getAllCards: function() {
        applicationManager.getPresentationUtility().showLoadingScreen();
        var enrollModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("EnrollModule");
        enrollModule.presentationController.getCardsForEnroll();
    },
    /**
     * Code to trigger OTP for the Mobile Number
     */
    //code for requesting otp 
    sendOTP: function(destination, type) {
        // type="m";
        var newUserManager = applicationManager.getNewUserBusinessManager();
        newUserManager.setEnrollAttribute("destination", destination);
        newUserManager.setEnrollAttribute("type", type);
        applicationManager.getPresentationUtility().showLoadingScreen();
        var enrollModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("EnrollModule");
        enrollModule.presentationController.invokeIdentity();
    },
    selectedEmail: function() {
        email = this.view.SecurityCheckOptions.selectedEmailRowIndex();
    },
    selectedMobileNum: function() {
        mobNumber = this.view.SecurityCheckOptions.selectedMobileNumRowIndex();
    },
    SelectedMobileText: function() {
        textNumber = this.view.SecurityCheckOptions.selectedMobileForTextIndex();
    },
    triggerOTP: function() {
        applicationManager.getPresentationUtility().showLoadingScreen();
        var enrollModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("EnrollModule");
        enrollModule.presentationController.requestOTP();
    },
    /*
     *Code to show error message
     */
    bindViewError: function(msg) {
        var scope = this;
        applicationManager.getDataProcessorUtility().showToastMessageError(scope, msg);
    }
});
define("EnrollModule/frmEnrollSecurityCheckControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for btnRight **/
    AS_Button_gf3ed60b012241e891cfa9369d7cc22c: function AS_Button_gf3ed60b012241e891cfa9369d7cc22c(eventobject) {
        var self = this;
        var  enrollMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("EnrollModule");
        enrollMod.presentationController.commonFunctionForNavigation("frmLogin");
    },
    /** onClick defined for undefined **/
    AS_BarButtonItem_f71234a8c5f74627a08fdec29b396260: function AS_BarButtonItem_f71234a8c5f74627a08fdec29b396260(eventobject) {
        var self = this;
        this.onClickCancel();
    },
    /** init defined for frmEnrollSecurityCheck **/
    AS_Form_a6c2f11db0e24c63930bec97a7a957fe: function AS_Form_a6c2f11db0e24c63930bec97a7a957fe(eventobject) {
        var self = this;
        this.init();
    },
    /** preShow defined for frmEnrollSecurityCheck **/
    AS_Form_d3e22f232a4447c8aad8b7ddd9de3b15: function AS_Form_d3e22f232a4447c8aad8b7ddd9de3b15(eventobject) {
        var self = this;
        this.frmSecurityCheckPreShow();
    }
});
define("EnrollModule/frmEnrollSecurityCheckController", ["EnrollModule/userfrmEnrollSecurityCheckController", "EnrollModule/frmEnrollSecurityCheckControllerActions"], function() {
    var controller = require("EnrollModule/userfrmEnrollSecurityCheckController");
    var controllerActions = ["EnrollModule/frmEnrollSecurityCheckControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
