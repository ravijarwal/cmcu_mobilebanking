define("EnrollModule/userfrmEnrollDOBController", {
    timerCounter: 0,
    keypadString: '',
    locale: kony.i18n.getCurrentLocale(),
    // locale : "sv",
    init: function() {
        var FormValidator = require("FormValidatorManager")
        this.fv = new FormValidator(1);
        var navManager = applicationManager.getNavigationManager();
        var currentForm = navManager.getCurrentForm();
        applicationManager.getPresentationFormUtility().initCommonActions(this, "YES", currentForm);
    },
    preShow: function() {
        this.view.flxHeaderPersonalInfo.customHeaderPersonalInfo.lblLocateUs.text = kony.i18n.getLocalizedString("kony.mb.enroll.enroll");
        this.locale = kony.i18n.getCurrentLocale();
        this.setDummyText();
        var dateOfBirthInLocaleFormat = "";
        this.view.flxHeaderPersonalInfo.customHeaderPersonalInfo.flxHeader.btnRight.text = kony.i18n.getLocalizedString("kony.mb.common.CancelSmall");
        var  enrollMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("EnrollModule");
        var dateOfBirth = enrollMod.presentationController.getEnrollDOB();
        if (dateOfBirth !== null && dateOfBirth !== "" && dateOfBirth !== undefined) {
            this.view.btnVerifyDOB.skin = "sknBtn0095e426pxEnabled";
            this.view.btnVerifyDOB.focusSkin = "sknBtn0095e426pxEnabled";
            this.view.btnVerifyDOB.setEnabled(true);
            this.keypadString = '';
            this.updateInputBullets();
        } else {
            this.view.btnVerifyDOB.skin = "sknBtnOnBoardingInactive";
            this.view.btnVerifyDOB.setEnabled(false);
            this.keypadString = '';
            this.updateInputBullets();
        }
        this.view.btnVerifyDOB.skin = "sknBtnOnBoardingInactive";
        this.view.btnVerifyDOB.setEnabled(false);
        this.setFlowActions();
        /*if(kony.os.deviceInfo().name !== "iPhone"){
              this.view.flxHeaderPersonalInfo.isVisible = true;
            }
            else{
              this.view.flxHeaderPersonalInfo.isVisible = false;
            }*/
        this.view.flxHeaderPersonalInfo.isVisible = true;
        this.fv.submissionView(this.view.btnVerifyDOB);
        this.fv.checkDOBLength(this.keypadString);
        applicationManager.getPresentationUtility().dismissLoadingScreen();
        var navManager = applicationManager.getNavigationManager();
        var currentForm = navManager.getCurrentForm();
        applicationManager.getPresentationFormUtility().logFormName(currentForm);
    },
    setDummyText: function() {
        var configManager = applicationManager.getConfigurationManager();
        var dummy = configManager.getCalendarDateFormat();
        var widgets = this.view["flxDOB"].widgets();
        for (var i = 0; i < this.keypadString.length; i++) {
            widgets[i].skin = "sknLbl979797SSP60px";
            widgets[i].text = dummy[i];
        }
    },
    setFlowActions: function() {
        var scope = this;
        this.view.imgDOBView.src = "viewicon.png";
        this.view.lblEnterPhoneNumberHeader.skin = "sknLbl424242SSPLight36px";
        this.view.customHeaderPersonalInfo.btnRight.skin = "sknLblb8dcffSSP32px";
        this.view.customHeaderPersonalInfo.btnRight.focusSkin = "sknLblb8dcffSSP32px";
        this.view.btnVerifyDOB.onClick = function() {
            scope.validateDOB();
        };
        this.view.customHeaderPersonalInfo.flxBack.onClick = function() {
            scope.navToLastName();
        };
        this.view.customHeaderPersonalInfo.btnRight.onClick = function() {
            scope.onClickCancel();
        };
        this.view.flxView.onClick = function() {
            scope.imgDOBViewOnClick();
        };
    },
    //to view the masked DOB 
    imgDOBViewOnClick: function() {
        var navManager = applicationManager.getNavigationManager();
        var dateOfBirth = navManager.getCustomInfo("dateOfBirth");
        if (dateOfBirth !== "" && dateOfBirth !== null && typeof dateOfBirth !== "undefined" && dateOfBirth.length === 10) {
            if (this.view.imgDOBView.src === "viewicon.png") {
                this.view.imgDOBView.src = "viewactive.png";
                this.view.lblMonthOne.text = dateOfBirth.charAt(0);
                this.view.lblMonthTwo.text = dateOfBirth.charAt(1);
                this.view.lblDayOne.text = dateOfBirth.charAt(3);
                this.view.lblDayTwo.text = dateOfBirth.charAt(4);
                this.view.lblYearOne.text = dateOfBirth.charAt(6);
                this.view.lblYearTwo.text = dateOfBirth.charAt(7);
                this.view.lblYearThree.text = dateOfBirth.charAt(8);
                this.view.lblYearFour.text = dateOfBirth.charAt(9);
                this.view.flxDOB.forceLayout();
            } else {
                this.view.imgDOBView.src = "viewicon.png";
                this.view.lblMonthOne.text = "•";
                this.view.lblMonthTwo.text = "•";
                this.view.lblDayOne.text = "•";
                this.view.lblDayTwo.text = "•";
                this.view.lblYearOne.text = "•";
                this.view.lblYearTwo.text = "•";
                this.view.lblYearThree.text = "•";
                this.view.lblYearFour.text = "•";
                this.view.flxDOB.forceLayout();
            }
        } else {
            this.view.imgDOBView.src = "viewicon.png";
            this.view.flxDOB.forceLayout();
        }
    },
    navToSSN: function() {
        var  enrollMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("EnrollModule");
        enrollMod.presentationController.commonFunctionForNavigation("frmEnrollSSn");
    },
    navToLastName: function() {
        var navManager = applicationManager.getNavigationManager();
        navManager.setCustomInfo("dateOfBirth", "");
        navManager.setCustomInfo("SSNtext", "");
        navManager.goBack();
    },
    onClickCancel: function() {
        /* kony.ui.Alert({
          "alertType": constants.ALERT_TYPE_CONFIRMATION,
          "alertTitle": null,
          "yesLabel": kony.i18n.getLocalizedString("kony.mb.enroll.dontCancel"),
          "noLabel": kony.i18n.getLocalizedString("kony.mb.common.AlertYes"),
          "message": kony.i18n.getLocalizedString("kony.mb.enroll.cancelPopup"),
          "alertHandler": this.alertCallback
        }, {
          "iconPosition": constants.ALERT_CONTENT_ALIGN_CENTER,
          "contentAlignment": constants.ALERT_ICON_POSITION_LEFT
        });*/
        var i18n = applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.enroll.cancelPopup");
        kony.ui.Alert(i18n, this.alertCallback, constants.ALERT_TYPE_CONFIRMATION, "Yes", "No", "");
    },
    alertCallback: function(response) {
        if (response === true) {
            var  enrollMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("EnrollModule");
            enrollMod.presentationController.resetEnrollObj();
        }
    },
    setKeypadChar: function(char) {
        if (this.keypadString.length === 10) return;
        this.keypadString = this.keypadString + char;
        if (this.keypadString.length === 2 || this.keypadString.length === 5) {
            this.keypadString = this.keypadString + '/';
        }
        this.updateInputBullets();
        this.fv.checkDOBLength(this.keypadString);
    },
    clearKeypadChar: function() {
        var navManager = applicationManager.getNavigationManager();
        navManager.setCustomInfo("dateOfBirth", "");
        if (this.keypadString.length === 1) {
            this.keypadString = '';
            this.updateInputBullets();
        }
        if (this.keypadString.length !== 0) {
            if (this.keypadString[this.keypadString.length - 1] === '/') {
                this.keypadString = this.keypadString.substr(0, this.keypadString.length - 1);
            }
            this.keypadString = this.keypadString.substr(0, this.keypadString.length - 1);
            this.updateInputBullets();
        }
        this.fv.checkDOBLength(this.keypadString);
    },
    updateInputBullets: function() {
        var scope = this;
        var dummyString = 'MM/DD/YYYY';
        var widgets = this.view["flxDOB"].widgets();
        for (var i = 0; i < this.keypadString.length; i++) {
            var navManager = applicationManager.getNavigationManager();
            navManager.setCustomInfo("dateOfBirth", this.keypadString);
            widgets[i].skin = "sknLbl979797SSP60px";
            if (i === 2 || i === 5) {
                widgets[i].text = "/";
            } else {
                widgets[i].text = "•";
            }
        }
        for (var j = this.keypadString.length; j < widgets.length; j++) {
            widgets[j].skin = "sknLble3e3e3SSP60px";
            widgets[j].text = dummyString[j];
            this.view.imgDOBView.src = "viewicon.png";
        }
        this.view.forceLayout();
    },
    validateAndNavigate: function() {
        var date = this.keypadString;
        if (date.length === 10) {
            var  NUOMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("NewUserModule");
            NUOMod.presentationController.validateDOBAndNavigate(date);
        } else {
            this.bindViewError(applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.common.validDOB"));
        }
    },
    assignDataToForm: function(newUserJSON) {
        var scope = this;
        var dob = (newUserJSON.dateOfBirth && newUserJSON.dateOfBirth !== "" && newUserJSON.dateOfBirth !== null) ? newUserJSON.dateOfBirth : "";
        if (dob !== "") {
            dob = dob.substr(0, 10);
            dob = dob.split("-");
            var dobText = dob[1] + dob[2] + dob[0];
            for (var i = 0; i < dobText.length; i++) {
                this.setKeypadChar(dobText.charAt(i));
            }
        } else {
            this.keypadString = "";
            this.updateInputBullets();
        }
        this.view.forceLayout();
    },
    //Development
    /**
     * validates Date of Birth
     */
    validateDOB: function() {
        var dob = this.keypadString;
        if (dob.length < 10) {
            this.bindViewError(applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.common.validDOB"));
        } else {
            var  enrollMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("EnrollModule");
            var navManager = applicationManager.getNavigationManager();
            navManager.setCustomInfo("isValidUser", true);
            navManager.setCustomInfo("dateOfBirth", "");
            navManager.setCustomInfo("SSNtext", "");
            enrollMod.presentationController.validateDOB(dob);
        }
    },
    /**
     * Shows Toast Message with red skin
     */
    bindViewError: function(msg) {
        applicationManager.getPresentationUtility().dismissLoadingScreen();
        //applicationManager.getDataProcessorUtility().showToastMessageError(this, msg);
        this.showToastMessageError(msg);
    },
    showToastMessageError: function(msg) {
        this.showToastMessage("sknFlxf54b5e", "errormessage.png", msg);
    },
    showToastMessage: function(skin, img, msg) {
        var scope = this;
        this.view.flxMainContainer.top = "60dp";
        this.view.flxPopup.isVisible = true;
        if (this.timerCounter === undefined || this.timerCounter === null) this.timerCounter = 0;
        this.timerCounter = parseInt(this.timerCounter) + 1;
        var timerId = "timerPopupSuccess" + this.timerCounter;
        this.view.flxPopup.skin = "" + skin;
        this.view.customPopup.imgPopup.src = "" + img;
        this.view.customPopup.lblPopup.text = msg;
        try {
            kony.print(timerId);
            kony.timer.schedule(timerId, function() {
                scope.view.flxPopup.setVisibility(false);
                scope.view.flxPopup.isVisible = false;
                scope.view.flxMainContainer.top = "56dp";
            }, 3, false);
        } catch (e) {
            kony.print(timerId);
            kony.print(e);
        }
    },
});
define("EnrollModule/frmEnrollDOBControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for btnOne **/
    AS_Button_a598bca576d8404181e298595aaa5962: function AS_Button_a598bca576d8404181e298595aaa5962(eventobject) {
        var self = this;
        this.setKeypadChar(1);
    },
    /** onClick defined for btnTwo **/
    AS_Button_i04695748cf145aba609814cd5a93125: function AS_Button_i04695748cf145aba609814cd5a93125(eventobject) {
        var self = this;
        this.setKeypadChar(2);
    },
    /** onClick defined for btnThree **/
    AS_Button_a60eb12fca4b411a9a9f15c5af27a50f: function AS_Button_a60eb12fca4b411a9a9f15c5af27a50f(eventobject) {
        var self = this;
        this.setKeypadChar(3);
    },
    /** onClick defined for btnFour **/
    AS_Button_f9e4ff49d89742c8a50dbdc49b155d2c: function AS_Button_f9e4ff49d89742c8a50dbdc49b155d2c(eventobject) {
        var self = this;
        this.setKeypadChar(4);
    },
    /** onClick defined for btnFive **/
    AS_Button_d31f089fae084c0ab1e71cad21071364: function AS_Button_d31f089fae084c0ab1e71cad21071364(eventobject) {
        var self = this;
        this.setKeypadChar(5);
    },
    /** onClick defined for btnSix **/
    AS_Button_je114c4cb2644f038e6ea33062d8f85f: function AS_Button_je114c4cb2644f038e6ea33062d8f85f(eventobject) {
        var self = this;
        this.setKeypadChar(6);
    },
    /** onClick defined for btnSeven **/
    AS_Button_ie26890de1dd4dd1ab1d12a62f9020f9: function AS_Button_ie26890de1dd4dd1ab1d12a62f9020f9(eventobject) {
        var self = this;
        this.setKeypadChar(7);
    },
    /** onClick defined for btnEight **/
    AS_Button_cbcd8cb5f6094b5dbecb259edf4c3cf3: function AS_Button_cbcd8cb5f6094b5dbecb259edf4c3cf3(eventobject) {
        var self = this;
        this.setKeypadChar(8);
    },
    /** onClick defined for btnNine **/
    AS_Button_d563b058b81c4f52915bbbaf049a9cdb: function AS_Button_d563b058b81c4f52915bbbaf049a9cdb(eventobject) {
        var self = this;
        this.setKeypadChar(9);
    },
    /** onClick defined for btnZero **/
    AS_Button_e65c9e264a2842e2b6478900e2a48489: function AS_Button_e65c9e264a2842e2b6478900e2a48489(eventobject) {
        var self = this;
        this.setKeypadChar(0);
    },
    /** onTouchEnd defined for imgClearKeypad **/
    AS_Image_e2c230c720e141f9a6e067f769344cfb: function AS_Image_e2c230c720e141f9a6e067f769344cfb(eventobject, x, y) {
        var self = this;
        this.clearKeypadChar();
    },
    /** onClick defined for undefined **/
    AS_BarButtonItem_ad085a900163495d9f76bb3437322381: function AS_BarButtonItem_ad085a900163495d9f76bb3437322381(eventobject) {
        var self = this;
        this.onClickCancel();
    },
    /** init defined for frmEnrollDOB **/
    AS_Form_d189778c876d4a0b981fad57744a5d97: function AS_Form_d189778c876d4a0b981fad57744a5d97(eventobject) {
        var self = this;
        this.init();
    },
    /** preShow defined for frmEnrollDOB **/
    AS_Form_ec9d31e623d948ab966e3e0305b92f01: function AS_Form_ec9d31e623d948ab966e3e0305b92f01(eventobject) {
        var self = this;
        this.preShow();
    }
});
define("EnrollModule/frmEnrollDOBController", ["EnrollModule/userfrmEnrollDOBController", "EnrollModule/frmEnrollDOBControllerActions"], function() {
    var controller = require("EnrollModule/userfrmEnrollDOBController");
    var controllerActions = ["EnrollModule/frmEnrollDOBControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
