define("EnrollModule/userfrmEnrollSecurityController", {
    keypadString: '',
    timerCounter: 0,
    init: function() {
        var navManager = applicationManager.getNavigationManager();
        var currentForm = navManager.getCurrentForm();
        navManager.setCustomInfo("isValidUser", true);
        applicationManager.getPresentationFormUtility().initCommonActions(this, "YES", currentForm);
    },
    //PreShow
    showSecurityCode: function() {
        var scope = this;
        this.view.btnVerifySecCode.onClick = function() {
            scope.submitOTP();
        };
        this.view.btnResend.onClick = function() {
            scope.requestResendOTP();
        };
        this.view.customHeader.btnRight.isVisible = false;
        //this.view.customHeader.flxHeader.btnRight.skin="sknLblb8dcffSSP32px";
        this.view.flxHeader.customHeader.lblLocateUs.text = kony.i18n.getLocalizedString("kony.mb.enroll.securityMethod");
        this.view.lblSecurityCode.skin = "sknLbl424242SSPLight36px";
        this.view.lblSecurityCode.text = kony.i18n.getLocalizedString("kony.mb.enroll.enterSecCode");
        this.view.flxHeader.customHeader.btnRight.text = "";
        this.keypadString = '';
        this.incompleteSecurityCodeView();
        this.updateInputBullets("flxInputSecurityCode");
        var navManager = applicationManager.getNavigationManager();
        var slctdOption = navManager.getCustomInfo("dropDownList");
        this.view.lblSelectedEmail.text = slctdOption;
        this.view.customHeader.flxBack.onClick = function() {
            var navManager = applicationManager.getNavigationManager();
            navManager.goBack();
        };
        //     if(kony.os.deviceInfo().name !== "iPhone"){
        //       this.view.flxHeader.isVisible = true;
        //     }
        //     else{
        //       this.view.flxHeader.isVisible = false;
        //     }
        this.view.flxHeader.setVisibility(true);
        scope.clearSecurityCode();
        applicationManager.getPresentationUtility().dismissLoadingScreen();
        var currentForm = navManager.getCurrentForm();
        applicationManager.getPresentationFormUtility().logFormName(currentForm);
        //CustomPopUp
        this.view.flxCustomPopup.setVisibility(false);
        var isValidUser = navManager.getCustomInfo("isValidUser");
        if (!isValidUser) {
            navManager.setCustomInfo("isValidUser", true);
            this.showErrorPopup();
        }
        this.view.flxCallUs.onClick = function() {
            kony.phone.dial("7043750183");
        };
        this.view.flxEmailUs.onClick = function() {
            var cc = [""];
            var bcc = [""];
            var sub = " ";
            var msgbody = "";
            var to = ["info@cmcu.org"];
            kony.phone.openEmail(to, cc, bcc, sub, msgbody);
        };
        this.view.flxVisitBranch.onClick = function() {
            var loginCntrlr = applicationManager.getPresentationUtility().getController('frmLogin', true);
            loginCntrlr.onLocateUSClick();
        };
        this.view.flxOk.onClick = function() {
            scope.onClickClosePopup();
        };
        this.view.flxCancel.onClick = function() {
            scope.onClickClosePopup();
        };
    },
    onClickClosePopup: function() {
        var enrollMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("EnrollModule");
        enrollMod.presentationController.resetEnrollObj();
    },
    showErrorPopup: function() {
        this.view.flxCustomPopup.setVisibility(true);
    },
    onClickCancel: function() {
        var  enrollMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("EnrollModule");
        enrollMod.presentationController.resetEnrollObj();
    },
    setKeypadChar: function(char) {
        this.keypadString = this.keypadString + char;
        if (this.keypadString.length === 6) {
            this.enterSecurityCodePostAction();
        } else if (this.keypadString.length < 6) {
            this.incompleteSecurityCodeView();
        } else if (this.keypadString.length > 6) {
            this.keypadString = this.keypadString.slice(0, 6);
            return;
        }
        this.updateInputBullets("flxInputSecurityCode");
    },
    clearKeypadChar: function() {
        if (this.keypadString.length === 1) {
            this.keypadString = '';
            this.updateInputBullets("flxInputSecurityCode");
        }
        if (this.keypadString.length !== 0) {
            this.keypadString = this.keypadString.substr(0, this.keypadString.length - 1);
            if (this.keypadString.length < 6) {
                this.incompleteSecurityCodeView();
            }
            this.updateInputBullets("flxInputSecurityCode");
        }
    },
    updateInputBullets: function(inputFlx) {
        var widgets = this.view[inputFlx].widgets();
        for (var i = 0; i < this.keypadString.length; i++) {
            widgets[i].skin = "sknLbl979797SSP60px";
            // widgets[i].text = this.keypadString[i];
            //widgets[i].text = "•";
        }
        for (var j = this.keypadString.length; j < widgets.length; j++) {
            widgets[i].skin = "sknLble3e3e3SSP60px";
            //widgets[j].text = '_';
        }
        this.view.forceLayout();
    },
    enterSecurityCodePostAction: function() {
        this.view.btnVerifySecCode.setEnabled(true);
        this.view.btnVerifySecCode.skin = "sknBtn0095e4RoundedffffffSSP26px";
        this.view.flxMainContainer.forceLayout();
    },
    incompleteSecurityCodeView: function() {
        this.view.btnVerifySecCode.skin = "sknBtna0a0a0SSPReg26px";
        this.view.btnVerifySecCode.setEnabled(false);
        this.view.flxMainContainer.forceLayout();
    },
    /*
     * Code to resend OTP
     */
    requestResendOTP: function() {
        applicationManager.getPresentationUtility().showLoadingScreen();
        var enrollModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("EnrollModule");
        enrollModule.presentationController.resendOTP();
    },
    /*
     * code to submit OTP
     */
    submitOTP: function() {
        applicationManager.getPresentationUtility().showLoadingScreen();
        var otp = this.keypadString;
        if (applicationManager.getPresentationValidationUtility().isValidTextBox(otp)) {
            var enrollModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("EnrollModule");
            enrollModule.presentationController.validateOTP(otp);
        } else {
            applicationManager.getPresentationUtility().dismissLoadingScreen();
            var errorMsg = applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.common.enterSecurityCode");
            this.bindGenericError(errorMsg);
        }
    },
    /*
     * Code to show error
     */
    bindGenericError: function(errorMsg) {
        var scopeObj = this;
        //applicationManager.getDataProcessorUtility().showToastMessageError(scopeObj,errorMsg);
        this.showToastMessageError(errorMsg);
        this.onResendOTP();
    },
    showToast: function() {
        //applicationManager.getDataProcessorUtility().showToastMessageSuccess(this,applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.enroll.codeResentToastMsg","Your security code has been re-sent successfully"));
        this.showToastMessageSuccess(applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.enroll.codeResentToastMsg"));
        this.onResendOTP();
    },
    onResendOTP: function() {
        applicationManager.getPresentationUtility().dismissLoadingScreen();
        this.keypadString = "";
        this.updateInputBullets("flxInputSecurityCode");
    },
    showToastMessageSuccess: function(msg) {
        this.showToastMessage("sknFlx43ce6e", "confirmation.png", msg);
    },
    showToastMessageError: function(msg) {
        this.showToastMessage("sknFlxf54b5e", "errormessage.png", msg);
    },
    showToastMessage: function(skin, img, msg) {
        var scope = this;
        this.view.flxMainContainer.top = "60dp";
        this.view.flxPopup.isVisible = true;
        if (this.timerCounter === undefined || this.timerCounter === null) this.timerCounter = 0;
        this.timerCounter = parseInt(this.timerCounter) + 1;
        var timerId = "timerPopupSuccess" + this.timerCounter;
        this.view.flxPopup.skin = "" + skin;
        this.view.customPopup.imgPopup.src = "" + img;
        this.view.customPopup.lblPopup.text = msg;
        try {
            kony.print(timerId);
            kony.timer.schedule(timerId, function() {
                scope.view.flxPopup.setVisibility(false);
                scope.view.flxPopup.isVisible = false;
                scope.view.flxMainContainer.top = "60dp";
            }, 3, false);
        } catch (e) {
            kony.print(timerId);
            kony.print(e);
        }
    },
    clearSecurityCode: function() {
        var widgets = this.view["flxInputSecurityCode"].widgets();
        for (var i = 0; i < 6; i++) {
            this.keypadString = '';
            // widgets[i].text = '_';
            //widgets[i].text = "•";
        }
        this.view.forceLayout();
    }
});
define("EnrollModule/frmEnrollSecurityControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for btnOne **/
    AS_Button_i7f9f5abb3664e5fb694af8feb4212a6: function AS_Button_i7f9f5abb3664e5fb694af8feb4212a6(eventobject) {
        var self = this;
        this.setKeypadChar(1);
    },
    /** onClick defined for btnTwo **/
    AS_Button_h3ea77cf91fc4f4d9ff45deb3685f828: function AS_Button_h3ea77cf91fc4f4d9ff45deb3685f828(eventobject) {
        var self = this;
        this.setKeypadChar(2);
    },
    /** onClick defined for btnThree **/
    AS_Button_i93a76d23b8647f791038f00a879241a: function AS_Button_i93a76d23b8647f791038f00a879241a(eventobject) {
        var self = this;
        this.setKeypadChar(3);
    },
    /** onClick defined for btnFour **/
    AS_Button_f1671d383324483c86104e017fa68a98: function AS_Button_f1671d383324483c86104e017fa68a98(eventobject) {
        var self = this;
        this.setKeypadChar(4);
    },
    /** onClick defined for btnSix **/
    AS_Button_a0158c26af96430caa4ed3bfd9e600dd: function AS_Button_a0158c26af96430caa4ed3bfd9e600dd(eventobject) {
        var self = this;
        this.setKeypadChar(6);
    },
    /** onClick defined for btnSeven **/
    AS_Button_ie3bcc135a944dd8baa8de8a0961937b: function AS_Button_ie3bcc135a944dd8baa8de8a0961937b(eventobject) {
        var self = this;
        this.setKeypadChar(7);
    },
    /** onClick defined for btnEight **/
    AS_Button_d995428196c94d1992d34e7680ead2d7: function AS_Button_d995428196c94d1992d34e7680ead2d7(eventobject) {
        var self = this;
        this.setKeypadChar(8);
    },
    /** onClick defined for btnNine **/
    AS_Button_ee1f4c4192904e4f934349eb25ff3430: function AS_Button_ee1f4c4192904e4f934349eb25ff3430(eventobject) {
        var self = this;
        this.setKeypadChar(9);
    },
    /** onClick defined for btnZero **/
    AS_Button_jd2f9ffdd6924b03bb0175998407e8d3: function AS_Button_jd2f9ffdd6924b03bb0175998407e8d3(eventobject) {
        var self = this;
        this.setKeypadChar(0);
    },
    /** onTouchEnd defined for imgClearKeypad **/
    AS_Image_b8275a188bb543bda67ceee35f4a8969: function AS_Image_b8275a188bb543bda67ceee35f4a8969(eventobject, x, y) {
        var self = this;
        this.clearKeypadChar();
    },
    /** onClick defined for btnRight **/
    AS_Button_f87c319a457c429fb7cf6529647c0995: function AS_Button_f87c319a457c429fb7cf6529647c0995(eventobject) {
        var self = this;
        this.onClickCancel();
    },
    /** onClick defined for btnFive **/
    AS_Button_fc8a1bcdd56b48b0a3ebb51798cd2c44: function AS_Button_fc8a1bcdd56b48b0a3ebb51798cd2c44(eventobject) {
        var self = this;
        this.setKeypadChar(5);
    },
    /** onClick defined for undefined **/
    AS_BarButtonItem_jddae95be6c74a6b8dfa063a85062889: function AS_BarButtonItem_jddae95be6c74a6b8dfa063a85062889(eventobject) {
        var self = this;
        this.onClickCancel();
    },
    /** init defined for frmEnrollSecurity **/
    AS_Form_fc588503a1774a3f901c11c058345abb: function AS_Form_fc588503a1774a3f901c11c058345abb(eventobject) {
        var self = this;
        this.init();
    },
    /** preShow defined for frmEnrollSecurity **/
    AS_Form_ffd70944f55440b7a9ad4faf55360679: function AS_Form_ffd70944f55440b7a9ad4faf55360679(eventobject) {
        var self = this;
        this.showSecurityCode();
    }
});
define("EnrollModule/frmEnrollSecurityController", ["EnrollModule/userfrmEnrollSecurityController", "EnrollModule/frmEnrollSecurityControllerActions"], function() {
    var controller = require("EnrollModule/userfrmEnrollSecurityController");
    var controllerActions = ["EnrollModule/frmEnrollSecurityControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
