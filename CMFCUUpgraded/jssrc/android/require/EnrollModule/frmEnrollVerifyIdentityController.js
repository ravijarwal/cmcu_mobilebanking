define("EnrollModule/userfrmEnrollVerifyIdentityController", {
    timerCounter: 0,
    init: function() {
        var navManager = applicationManager.getNavigationManager();
        var currentForm = navManager.getCurrentForm();
        applicationManager.getPresentationFormUtility().initCommonActions(this, "YES", currentForm);
    },
    frmEnrollVerifyIdentityPreShow: function() {
        this.setFlowAction();
        this.setPreShowData();
        this.view.verifyIdentity.customAccountNumtext = "";
        this.view.verifyIdentity.ssntext = "";
        this.view.verifyIdentity.updateInputBulletsSSN(this.view.verifyIdentity.ssntext);
        this.view.verifyIdentity.dobtext = "";
        this.view.verifyIdentity.updateInputBulletsDOB(this.view.verifyIdentity.dobtext);
        this.view.verifyIdentity.imgsrc = "checkbox_empty.png";
        this.view.verifyIdentity.continueskin = "skinButtonDisabled";
        this.view.verifyIdentity.acceptTerms = false;
        this.view.verifyIdentity.btnsetEnabled(false);
        applicationManager.getPresentationUtility().dismissLoadingScreen();
        var navManager = applicationManager.getNavigationManager();
        var currentForm = navManager.getCurrentForm();
        applicationManager.getPresentationFormUtility().logFormName(currentForm);
    },
    setFlowAction: function() {
        var scopeObj = this;
        this.view.customHeader.flxBack.onClick = function() {
            scopeObj.navBack();
        };
        this.view.customHeader.btnRight.onClick = function() {
            scopeObj.onClickCancel();
        };
        this.view.verifyIdentity.setCustomNavigateTo(this.navToSecurityCheck);
    },
    setPreShowData: function() {
        var scope = this;
        this.view.customHeader.lblLocateUs.text = "Verify Your Identity";
        if (kony.os.deviceInfo().name !== "iPhone") {
            this.view.flxHeader.isVisible = true;
        } else {
            this.view.flxHeader.isVisible = false;
        }
        // var enrollMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("EnrollModule");
        // var userlastname = enrollMod.presentationController.getEnrollLastName();
    },
    navToSecurityCheck: function() {
        var  enrollMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("EnrollModule");
        enrollMod.presentationController.commonFunctionForNavigation("frmEnrollSecurityCheck");
    },
    onClickCancel: function() {
        var  enrollMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("EnrollModule");
        enrollMod.presentationController.resetEnrollObj();
    },
    navToDOB: function() {
        var  enrollMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("EnrollModule");
        enrollMod.presentationController.commonFunctionForNavigation("frmEnrollDOB");
    },
    navBack: function() {
        var enrollPC = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("EnrollModule");
        enrollPC.presentationController.commonFunctionForNavigation("frmLogin");
    },
    /**
     *Shows Toast Message with red skin
     */
    bindViewError: function(msg) {
        applicationManager.getPresentationUtility().dismissLoadingScreen();
        applicationManager.getDataProcessorUtility().showToastMessageError(this, msg);
    },
});
define("EnrollModule/frmEnrollVerifyIdentityControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for btnRight **/
    AS_Button_f545f7cf18cb40bcb0acffcde049c152: function AS_Button_f545f7cf18cb40bcb0acffcde049c152(eventobject) {
        var self = this;
        var  enrollMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("EnrollModule");
        enrollMod.presentationController.resetEnrollObj();
        enrollMod.presentationController.commonFunctionForNavigation("frmLogin");
    },
    /** onClick defined for undefined **/
    AS_BarButtonItem_i27894f237264816b2432fb472285ed3: function AS_BarButtonItem_i27894f237264816b2432fb472285ed3(eventobject) {
        var self = this;
        this.navBack();
    },
    /** onClick defined for undefined **/
    AS_BarButtonItem_jba3a45783df456ca115dab716d128a6: function AS_BarButtonItem_jba3a45783df456ca115dab716d128a6(eventobject) {
        var self = this;
        this.onClickCancel();
    },
    /** init defined for frmEnrollVerifyIdentity **/
    AS_Form_ff8658d39750423baf897406102a3082: function AS_Form_ff8658d39750423baf897406102a3082(eventobject) {
        var self = this;
        this.init();
    },
    /** preShow defined for frmEnrollVerifyIdentity **/
    AS_Form_e031d3ff65ed4894b7953acf4fb53361: function AS_Form_e031d3ff65ed4894b7953acf4fb53361(eventobject) {
        var self = this;
        this.frmEnrollVerifyIdentityPreShow();
    }
});
define("EnrollModule/frmEnrollVerifyIdentityController", ["EnrollModule/userfrmEnrollVerifyIdentityController", "EnrollModule/frmEnrollVerifyIdentityControllerActions"], function() {
    var controller = require("EnrollModule/userfrmEnrollVerifyIdentityController");
    var controllerActions = ["EnrollModule/frmEnrollVerifyIdentityControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
