define("EnrollModule/frmEnrollSSn", function() {
    return function(controller) {
        function addWidgetsfrmEnrollSSn() {
            this.setDefaultUnit(kony.flex.DP);
            var flxHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "56dp",
                "id": "flxHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxffffffShadow",
                "top": "0%",
                "width": "100%",
                "zIndex": 30
            }, {}, {});
            flxHeader.setDefaultUnit(kony.flex.DP);
            var customHeader = new com.kmb.common.customHeader({
                "clipBounds": false,
                "height": "100%",
                "id": "customHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "btnRight": {
                        "isVisible": true,
                        "text": kony.i18n.getLocalizedString("kony.mb.common.Cancel")
                    },
                    "flxSearch": {
                        "isVisible": false
                    },
                    "lblLocateUs": {
                        "text": kony.i18n.getLocalizedString("kony.mb.enroll.SSN")
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            customHeader.flxBack.onClick = controller.AS_FlexContainer_fa79b10bf92440b1a8dc735f05d1d874;
            flxHeader.add(customHeader);
            var flxMainContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "65%",
                "id": "flxMainContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxWhite",
                "top": "56dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMainContainer.setDefaultUnit(kony.flex.DP);
            var flxEnterSSNo = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "60dp",
                "id": "flxEnterSSNo",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox",
                "top": "20dp",
                "width": "80%",
                "zIndex": 1
            }, {}, {});
            flxEnterSSNo.setDefaultUnit(kony.flex.DP);
            var lblEnterSSN = new kony.ui.Label({
                "centerX": "50.35%",
                "id": "lblEnterSSN",
                "isVisible": true,
                "text": kony.i18n.getLocalizedString("kony.mb.enroll.enterSSN"),
                "textStyle": {
                    "letterSpacing": 0,
                    "strikeThrough": false
                },
                "top": 0,
                "width": "90%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false
            });
            flxEnterSSNo.add(lblEnterSSN);
            var btnVerifySSN = new kony.ui.Button({
                "bottom": "60dp",
                "focusSkin": "sknBtn055BAF26px",
                "height": "45dp",
                "id": "btnVerifySSN",
                "isVisible": true,
                "left": "20dp",
                "right": "20dp",
                "skin": "sknBtna0a0a0SSPReg26px",
                "text": kony.i18n.getLocalizedString("kony.mb.common.continue"),
                "width": "90%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxInputSSN = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "10%",
                "id": "flxInputSSN",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20%",
                "width": "80%",
                "zIndex": 1
            }, {}, {});
            flxInputSSN.setDefaultUnit(kony.flex.DP);
            var lblSSNoOpenBrace = new kony.ui.Label({
                "height": "100%",
                "id": "lblSSNoOpenBrace",
                "isVisible": true,
                "left": "2%",
                "skin": "sknLbl484848SSPReg40px",
                "text": "(",
                "textStyle": {
                    "letterSpacing": 0,
                    "strikeThrough": false
                },
                "top": "0dp",
                "width": "20dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false
            });
            var lblSSNoDigit1 = new kony.ui.Label({
                "height": "100%",
                "id": "lblSSNoDigit1",
                "isVisible": true,
                "left": "2dp",
                "skin": "sknLbl484848SSPReg40px",
                "text": "_",
                "textStyle": {
                    "letterSpacing": 0,
                    "strikeThrough": false
                },
                "top": "0dp",
                "width": "20dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false
            });
            var lblSSNoDigit2 = new kony.ui.Label({
                "height": "100%",
                "id": "lblSSNoDigit2",
                "isVisible": true,
                "left": "2dp",
                "skin": "sknLbl484848SSPReg40px",
                "text": "_",
                "textStyle": {
                    "letterSpacing": 0,
                    "strikeThrough": false
                },
                "top": "0dp",
                "width": "20dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false
            });
            var lblSSNoDigit3 = new kony.ui.Label({
                "height": "100%",
                "id": "lblSSNoDigit3",
                "isVisible": true,
                "left": "2dp",
                "skin": "sknLbl484848SSPReg40px",
                "text": "_",
                "textStyle": {
                    "letterSpacing": 0,
                    "strikeThrough": false
                },
                "top": "0dp",
                "width": "20dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false
            });
            var lblSSNoHyphen1 = new kony.ui.Label({
                "height": "100%",
                "id": "lblSSNoHyphen1",
                "isVisible": true,
                "left": "2dp",
                "skin": "sknLbl484848SSPReg40px",
                "text": "-",
                "textStyle": {
                    "letterSpacing": 0,
                    "strikeThrough": false
                },
                "top": "0dp",
                "width": "20dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false
            });
            var lblSSNoDigit4 = new kony.ui.Label({
                "height": "100%",
                "id": "lblSSNoDigit4",
                "isVisible": true,
                "left": "2dp",
                "skin": "sknLbl484848SSPReg40px",
                "text": "_",
                "textStyle": {
                    "letterSpacing": 0,
                    "strikeThrough": false
                },
                "top": "0dp",
                "width": "20dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false
            });
            var lblSSNoDigit5 = new kony.ui.Label({
                "height": "100%",
                "id": "lblSSNoDigit5",
                "isVisible": true,
                "left": "2dp",
                "skin": "sknLbl484848SSPReg40px",
                "text": "_",
                "textStyle": {
                    "letterSpacing": 0,
                    "strikeThrough": false
                },
                "top": "0dp",
                "width": "20dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false
            });
            var lblSSNoHyphen2 = new kony.ui.Label({
                "height": "100%",
                "id": "lblSSNoHyphen2",
                "isVisible": true,
                "left": "2dp",
                "skin": "sknLbl484848SSPReg40px",
                "text": "-",
                "textStyle": {
                    "letterSpacing": 0,
                    "strikeThrough": false
                },
                "top": "0dp",
                "width": "20dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false
            });
            var lblSSNoDigit6 = new kony.ui.Label({
                "height": "100%",
                "id": "lblSSNoDigit6",
                "isVisible": true,
                "left": "2dp",
                "skin": "sknLbl484848SSPReg40px",
                "text": "_",
                "textStyle": {
                    "letterSpacing": 0,
                    "strikeThrough": false
                },
                "top": "0dp",
                "width": "20dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false
            });
            var lblSSNoDigit7 = new kony.ui.Label({
                "height": "100%",
                "id": "lblSSNoDigit7",
                "isVisible": true,
                "left": "2dp",
                "skin": "sknLbl484848SSPReg40px",
                "text": "_",
                "textStyle": {
                    "letterSpacing": 0,
                    "strikeThrough": false
                },
                "top": "0dp",
                "width": "20dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false
            });
            var lblSSNoDigit8 = new kony.ui.Label({
                "height": "100%",
                "id": "lblSSNoDigit8",
                "isVisible": true,
                "left": "2dp",
                "skin": "sknLbl484848SSPReg40px",
                "text": "_",
                "textStyle": {
                    "letterSpacing": 0,
                    "strikeThrough": false
                },
                "top": "0dp",
                "width": "20dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false
            });
            var lblSSNoDigit9 = new kony.ui.Label({
                "height": "100%",
                "id": "lblSSNoDigit9",
                "isVisible": true,
                "left": "4dp",
                "right": "0dp",
                "skin": "sknLbl484848SSPReg40px",
                "text": "_",
                "textStyle": {
                    "letterSpacing": 0,
                    "strikeThrough": false
                },
                "top": "0dp",
                "width": "20dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false
            });
            var lblSSNoCloseBrace = new kony.ui.Label({
                "height": "100%",
                "id": "lblSSNoCloseBrace",
                "isVisible": true,
                "left": "0%",
                "right": 0,
                "skin": "sknLbl484848SSPReg40px",
                "text": ")",
                "textStyle": {
                    "letterSpacing": 0,
                    "strikeThrough": false
                },
                "top": "0dp",
                "width": "5%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false
            });
            flxInputSSN.add(lblSSNoOpenBrace, lblSSNoDigit1, lblSSNoDigit2, lblSSNoDigit3, lblSSNoHyphen1, lblSSNoDigit4, lblSSNoDigit5, lblSSNoHyphen2, lblSSNoDigit6, lblSSNoDigit7, lblSSNoDigit8, lblSSNoDigit9, lblSSNoCloseBrace);
            var flxInputSSNBox = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50.05%",
                "clipBounds": true,
                "height": "50dp",
                "id": "flxInputSSNBox",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknflxffffffRadiusf6f6f64px",
                "top": "75dp",
                "zIndex": 1
            }, {}, {});
            flxInputSSNBox.setDefaultUnit(kony.flex.DP);
            var lblSSN = new kony.ui.Label({
                "centerX": "50%",
                "height": "100%",
                "id": "lblSSN",
                "isVisible": true,
                "skin": "sknLbl424242SSP160pr",
                "textStyle": {
                    "letterSpacing": 0,
                    "strikeThrough": false
                },
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false
            });
            flxInputSSNBox.add(lblSSN);
            var flxView = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "8.25%",
                "id": "flxView",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "85%",
                "isModalContainer": false,
                "right": "10%",
                "skin": "slFbox",
                "top": "21%",
                "width": "12%",
                "zIndex": 4
            }, {}, {});
            flxView.setDefaultUnit(kony.flex.DP);
            var imgSSNView = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "50%",
                "id": "imgSSNView",
                "isVisible": true,
                "left": "0dp",
                "skin": "slImage",
                "src": "viewicon.png",
                "top": "2%",
                "width": "100%",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxView.add(imgSSNView);
            flxMainContainer.add(flxEnterSSNo, btnVerifySSN, flxInputSSN, flxInputSSNBox, flxView);
            var flxKeypad = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0%",
                "clipBounds": true,
                "height": "230dp",
                "id": "flxKeypad",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxF5F6FB",
                "width": "100%",
                "zIndex": 2
            }, {}, {});
            flxKeypad.setDefaultUnit(kony.flex.DP);
            var keypad = new com.kmb.common.keypad({
                "clipBounds": true,
                "height": "100%",
                "id": "keypad",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {}
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            keypad.btnEight.onClick = controller.AS_Button_d995428196c94d1992d34e7680ead2d7;
            keypad.btnFive.onClick = controller.AS_Button_c80317ce605047b082c5464d12f5ff20;
            keypad.btnFour.onClick = controller.AS_Button_f1671d383324483c86104e017fa68a98;
            keypad.btnNine.onClick = controller.AS_Button_ee1f4c4192904e4f934349eb25ff3430;
            keypad.btnOne.onClick = controller.AS_Button_i7f9f5abb3664e5fb694af8feb4212a6;
            keypad.btnSeven.onClick = controller.AS_Button_ie3bcc135a944dd8baa8de8a0961937b;
            keypad.btnSix.onClick = controller.AS_Button_a0158c26af96430caa4ed3bfd9e600dd;
            keypad.btnThree.onClick = controller.AS_Button_i93a76d23b8647f791038f00a879241a;
            keypad.btnTwo.onClick = controller.AS_Button_h3ea77cf91fc4f4d9ff45deb3685f828;
            keypad.btnZero.onClick = controller.AS_Button_jd2f9ffdd6924b03bb0175998407e8d3;
            keypad.imgClearKeypad.onTouchEnd = controller.AS_Image_b8275a188bb543bda67ceee35f4a8969;
            flxKeypad.add(keypad);
            var flxPopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "10.50%",
                "id": "flxPopup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "sknFlxf54b5e",
                "top": "0dp",
                "width": "100%",
                "zIndex": 200
            }, {}, {});
            flxPopup.setDefaultUnit(kony.flex.DP);
            var customPopup = new com.kmb.common.customPopup({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "customPopup",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {}
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxPopup.add(customPopup);
            var flxCustomPopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxCustomPopup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxBorderDarkGrey",
                "top": "0dp",
                "width": "100%",
                "zIndex": 100
            }, {}, {});
            flxCustomPopup.setDefaultUnit(kony.flex.DP);
            var flxInnerPopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "35%",
                "id": "flxInnerPopup",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "50%",
                "isModalContainer": false,
                "skin": "CopysknScrFlxffffff0d1e21161723c43",
                "top": "50%",
                "width": "80%",
                "zIndex": 200
            }, {}, {});
            flxInnerPopup.setDefaultUnit(kony.flex.DP);
            var flxCancel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25dp",
                "id": "flxCancel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 200
            }, {}, {});
            flxCancel.setDefaultUnit(kony.flex.DP);
            var imgCancel = new kony.ui.Image2({
                "height": "25dp",
                "id": "imgCancel",
                "isVisible": true,
                "left": "90%",
                "skin": "slImage",
                "src": "cancelicon.png",
                "top": "0dp",
                "width": "5%",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCancel.add(imgCancel);
            var flxmessage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20%",
                "id": "flxmessage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0%",
                "width": "100%",
                "zIndex": 200
            }, {}, {});
            flxmessage.setDefaultUnit(kony.flex.DP);
            var lblMessageText = new kony.ui.Label({
                "centerX": "50%",
                "height": "90%",
                "id": "lblMessageText",
                "isVisible": true,
                "left": "0%",
                "skin": "sknBlackLbl",
                "text": kony.i18n.getLocalizedString("kony.mb.enroll.popUpMsgText"),
                "textStyle": {
                    "letterSpacing": 0,
                    "strikeThrough": false
                },
                "top": "0%",
                "width": "95%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false
            });
            flxmessage.add(lblMessageText);
            var flxSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxSeparatorSegments",
                "top": "0%",
                "width": "100%",
                "zIndex": 110
            }, {}, {});
            flxSeperator.setDefaultUnit(kony.flex.DP);
            flxSeperator.add();
            var flxOptions = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0%",
                "clipBounds": true,
                "height": "46.05%",
                "id": "flxOptions",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "1%",
                "width": "100%",
                "zIndex": 300
            }, {}, {});
            flxOptions.setDefaultUnit(kony.flex.DP);
            var flxCallUs = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "30%",
                "id": "flxCallUs",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "3%",
                "width": "100%",
                "zIndex": 300
            }, {}, {});
            flxCallUs.setDefaultUnit(kony.flex.DP);
            var lblCallUs = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "lblCallUs",
                "isVisible": true,
                "left": "40%",
                "skin": "sknLblBlue22px",
                "text": "Call Us",
                "textStyle": {
                    "letterSpacing": 0,
                    "strikeThrough": false
                },
                "top": "10%",
                "width": "50%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false
            });
            flxCallUs.add(lblCallUs);
            var flxEmailUs = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "30%",
                "id": "flxEmailUs",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "3%",
                "width": "100%",
                "zIndex": 300
            }, {}, {});
            flxEmailUs.setDefaultUnit(kony.flex.DP);
            var lblEmailUs = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "lblEmailUs",
                "isVisible": true,
                "left": "40%",
                "skin": "sknLblBlue22px",
                "text": "Email Us",
                "textStyle": {
                    "letterSpacing": 0,
                    "strikeThrough": false
                },
                "top": "10%",
                "width": "50%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false
            });
            flxEmailUs.add(lblEmailUs);
            var flxVisitBranch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0%",
                "clipBounds": true,
                "height": "30%",
                "id": "flxVisitBranch",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "3%",
                "width": "100%",
                "zIndex": 300
            }, {}, {});
            flxVisitBranch.setDefaultUnit(kony.flex.DP);
            var lblVisitBranch = new kony.ui.Label({
                "bottom": 0,
                "centerX": "50%",
                "centerY": "50%",
                "id": "lblVisitBranch",
                "isVisible": true,
                "left": "40%",
                "skin": "sknLblBlue22px",
                "text": "Visit Branch",
                "textStyle": {
                    "letterSpacing": 0,
                    "strikeThrough": false
                },
                "top": "10%",
                "width": "50%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false
            });
            flxVisitBranch.add(lblVisitBranch);
            flxOptions.add(flxCallUs, flxEmailUs, flxVisitBranch);
            var flxSeperator2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxSeperator2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxSeparatorSegments",
                "top": "0%",
                "width": "100%",
                "zIndex": 110
            }, {}, {});
            flxSeperator2.setDefaultUnit(kony.flex.DP);
            flxSeperator2.add();
            var flxOk = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "10%",
                "clipBounds": true,
                "height": "18%",
                "id": "flxOk",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0.05%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "2.00%",
                "width": "100%",
                "zIndex": 300
            }, {}, {});
            flxOk.setDefaultUnit(kony.flex.DP);
            var lblOk = new kony.ui.Label({
                "bottom": 0,
                "centerX": "50%",
                "centerY": "50%",
                "id": "lblOk",
                "isVisible": true,
                "left": "40%",
                "skin": "sknLblBlue22px",
                "text": "OK",
                "textStyle": {
                    "letterSpacing": 0,
                    "strikeThrough": false
                },
                "top": "10%",
                "width": "50%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false
            });
            flxOk.add(lblOk);
            flxInnerPopup.add(flxCancel, flxmessage, flxSeperator, flxOptions, flxSeperator2, flxOk);
            flxCustomPopup.add(flxInnerPopup);
            this.add(flxHeader, flxMainContainer, flxKeypad, flxPopup, flxCustomPopup);
        };
        return [{
            "addWidgets": addWidgetsfrmEnrollSSn,
            "enabledForIdleTimeout": true,
            "id": "frmEnrollSSn",
            "init": controller.AS_Form_f53b79ecc611413bafb4cee89c5a7044,
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "preShow": function(eventobject) {
                controller.AS_Form_d2247d4d100f476395bc7618bb12671d(eventobject);
            },
            "skin": "sknFormBckgrndGrey",
            "title": kony.i18n.getLocalizedString("kony.mb.enroll.SSN")
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "footerOverlap": false,
            "headerOverlap": false,
            "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
            "retainScrollPosition": false,
            "titleBar": false,
            "titleBarSkin": "sknTitle1a98ffffffff30px",
            "windowSoftInputMode": constants.FORM_ADJUST_PAN
        }]
    }
});