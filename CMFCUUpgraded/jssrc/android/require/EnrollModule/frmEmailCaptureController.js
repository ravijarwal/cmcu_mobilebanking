define("EnrollModule/userfrmEmailCaptureController", {
    //Type your controller code here 
    timerCounter: 0,
    init: function() {
        var navManager = applicationManager.getNavigationManager();
        var currentForm = navManager.getCurrentForm();
        applicationManager.getPresentationFormUtility().initCommonActions(this, "YES", currentForm);
    },
    frmEmailCapturePreShow: function() {
        this.setFlowAction();
        this.setPreShowData();
        this.view.txtboxEmail.text = "";
        this.view.btnContinue.skin = "skinButtonDisabled";
        this.view.btnContinue.focusSkin = "skinButtonDisabled";
        this.view.btnContinue.setEnabled(false);
        this.view.customHeader.btnRight.skin = "sknLblffffffSSP30px";
        applicationManager.getPresentationUtility().dismissLoadingScreen();
        var navManager = applicationManager.getNavigationManager();
        var currentForm = navManager.getCurrentForm();
        applicationManager.getPresentationFormUtility().logFormName(currentForm);
    },
    setFlowAction: function() {
        var scopeObj = this;
        this.view.customHeader.flxHeader.btnRight.skin = "sknLblb8dcffSSP32px";
        this.view.customHeader.flxHeader.btnRight.focusSkin = "sknLblb8dcffSSP32px";
        this.view.customHeader.flxBack.isVisible = false;
        this.view.customHeader.imgBack.isVisible = false;
        this.view.customHeader.btnRight.onClick = function() {
            scopeObj.onClickCancel();
        };
        this.view.txtboxEmail.onTextChange = this.onChangeEmail;
        this.view.btnContinue.onClick = this.onClickContinue;
    },
    onClickContinue: function() {
        this.saveEmail(this.view.txtboxEmail.text);
    },
    saveEmail: function(email) {
        var enrollModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("EnrollModule");
        enrollModule.presentationController.saveEmail(email);
    },
    onChangeEmail: function() {
        var email = this.view.txtboxEmail.text;
        if (this.isValidEmail(email)) {
            this.view.btnContinue.skin = "sknBtn0095e426pxEnabled";
            this.view.btnContinue.focusSkin = "sknBtn0095e426pxEnabled";
            this.view.btnContinue.setEnabled(true);
        } else {
            this.view.btnContinue.skin = "skinButtonDisabled";
            this.view.btnContinue.focusSkin = "skinButtonDisabled";
            this.view.btnContinue.setEnabled(false);
        }
    },
    verifyEmail: function() {
        if (!(this.isValidEmail(this.view.txtboxEmail.text))) {
            this.showError();
        }
    },
    showError: function() {
        var controller = applicationManager.getPresentationUtility().getController('frmEmailCapture', true);
        var errorMsg = applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.OnBoarding.InvalidEmail");
        controller.bindViewError(errorMsg);
    },
    isValidEmail: function(email) {
        return kony.string.isValidEmail(email);
    },
    navigatetoDevReg: function() {
        var navManager = applicationManager.getNavigationManager();
        var customInfo = navManager.getCustomInfo("frmEnrollSignUp");
        var enteredUserName = customInfo.userName;
        var enteredPassword = customInfo.password;
        var authMode = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
        authMode.presentationController.currentAuthMode = "password";
        authMode.presentationController.onLogin({
            "username": enteredUserName,
            "password": enteredPassword
        }, this);
    },
    onClickCancel: function() {
        var  enrollMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("EnrollModule");
        enrollMod.presentationController.resetEnrollObj();
    },
    setPreShowData: function() {
        var scope = this;
        this.view.flxHeader.customHeader.lblLocateUs.text = kony.i18n.getLocalizedString("kony.mb.enroll.enroll");
        this.view.flxHeader.customHeader.btnRight.text = "";
        this.view.flxHeader.setVisibility(true);
    },
    bindViewError: function(msg) {
        applicationManager.getPresentationUtility().dismissLoadingScreen();
        applicationManager.getDataProcessorUtility().showToastMessageError(this, msg);
    }
});
define("EnrollModule/frmEmailCaptureControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for btnRight **/
    AS_Button_f545f7cf18cb40bcb0acffcde049c152: function AS_Button_f545f7cf18cb40bcb0acffcde049c152(eventobject) {
        var self = this;
        var  enrollMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("EnrollModule");
        enrollMod.presentationController.resetEnrollObj();
        enrollMod.presentationController.commonFunctionForNavigation("frmLogin");
    },
    /** onEndEditing defined for txtboxEmail **/
    AS_TextField_e1ab6df807974dbf92077048536e42cd: function AS_TextField_e1ab6df807974dbf92077048536e42cd(eventobject, changedtext) {
        var self = this;
        this.verifyEmail();
    },
    /** init defined for frmEmailCapture **/
    AS_Form_e29a1039672d4473aa1428c94fd396e6: function AS_Form_e29a1039672d4473aa1428c94fd396e6(eventobject) {
        var self = this;
        this.init();
    },
    /** preShow defined for frmEmailCapture **/
    AS_Form_fee308713d344731a5bd1ed17bbd9b2c: function AS_Form_fee308713d344731a5bd1ed17bbd9b2c(eventobject) {
        var self = this;
        this.frmEmailCapturePreShow();
    },
    /** onDeviceBack defined for frmEmailCapture **/
    AS_Form_gd1019304e864142b2707c9dc7e61fba: function AS_Form_gd1019304e864142b2707c9dc7e61fba(eventobject) {
        var self = this;
        this.frmEmailCapturePreShow();
    }
});
define("EnrollModule/frmEmailCaptureController", ["EnrollModule/userfrmEmailCaptureController", "EnrollModule/frmEmailCaptureControllerActions"], function() {
    var controller = require("EnrollModule/userfrmEmailCaptureController");
    var controllerActions = ["EnrollModule/frmEmailCaptureControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
