define("EnrollModule/userfrmAccountPrimaryLinkingController", {
    flag: 0,
    prevSelItems: [],
    prevSelIndex: -1,
    init: function() {
        var navManager = applicationManager.getNavigationManager();
        var currentForm = navManager.getCurrentForm();
        applicationManager.getPresentationFormUtility().initCommonActions(this, "YES", currentForm);
    },
    frmAccPrimaryLinkPreshow: function() {
        this.setPreshowData();
        applicationManager.getPresentationUtility().dismissLoadingScreen();
        var navManager = applicationManager.getNavigationManager();
        var currentForm = navManager.getCurrentForm();
        applicationManager.getPresentationFormUtility().logFormName(currentForm);
        this.view.flxHeader.setVisibility(true);
    },
    setPreshowData: function() {
        var scopeObj = this;
        this.flag = 0;
        this.view.customHeader.flxBack.isVisible = false;
        this.view.customHeader.btnRight.text = kony.i18n.getLocalizedString("kony.mb.common.CancelSmall");
        this.view.flxHeader.customHeader.lblLocateUs.text = kony.i18n.getLocalizedString("kony.mb.enroll.enroll");
        this.view.lblText.text = kony.i18n.getLocalizedString("kony.mb.enroll.primAcc");
        this.view.lblTextdesc.text = kony.i18n.getLocalizedString("kony.mb.enroll.slctPrimAcc");
        this.view.lblAccTextDesc.text = kony.i18n.getLocalizedString("kony.mb.enroll.slctAnthrAccDesc");
        this.view.flxHeader.customHeader.btnRight.skin = "sknLblb8dcffSSP32px";
        this.view.customHeader.flxHeader.btnRight.focusskin = "sknLblb8dcffSSP32px";
        this.view.flxAccLink.skin = "sknFlxWhiteBorder1";
        this.view.btnContinue.skin = "skinButtonDisabled";
        this.view.btnContinue.setEnabled(false);
        this.setAccLinks();
        this.view.btnContinue.onClick = this.btnContinueOnClick;
        this.view.segAccLink.onRowClick = this.segAccLinkOnRowClick;
        this.view.customHeader.btnRight.onClick = function() {
            scopeObj.onClickCancel();
        };
    },
    btnContinueOnClick: function() {
        applicationManager.getPresentationUtility().showLoadingScreen();
        var navMan = applicationManager.getNavigationManager();
        var enrollMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("EnrollModule");
        var accountList = navMan.getCustomInfo("accountList");
        if (accountList.length !== 1) {
            enrollMod.presentationController.commonFunctionForNavigation("frmAccountLinking");
        } else navMan.navigateTo("frmEnrollSignUp");
    },
    setAccLinks: function() {
        var navMan = applicationManager.getNavigationManager();
        var accountList = navMan.getCustomInfo("accountList");
        var data = [];
        var temp = {};
        var lblAccNum, img, lblAccName, lblAccNameValue, lblAccType, lblAccTypeValue, accNumHidden, shareId, loanId;
        img = {
            src: "radio_inactive.png"
        };
        lblAccName = kony.i18n.getLocalizedString("kony.mb.enroll.primary");
        lblAccType = kony.i18n.getLocalizedString("kony.mb.enroll.accType");
        for (var i = 0; i < accountList.length; i++) {
            if (accountList[i].shareId !== null && accountList[i].shareId !== "" && typeof accountList[i].shareId !== "undefined") shareId = accountList[i].shareId;
            if (accountList[i].loanId !== null && accountList[i].loanId !== "" && typeof accountList[i].loanId !== "undefined") loanId = accountList[i].loanId;
            accNumHidden = accountList[i].accountNumber;
            lblAccNum = accountList[i].accountNumber.replace(/\d(?=\d{4})/g, "X");
            lblAccNameValue = accountList[i].name;
            lblAccTypeValue = this.accTypeValue(accountList[i].accountType);
            temp = {
                "lblAccNum": lblAccNum,
                "imgIndicator": img,
                "lblAccName": lblAccName,
                "lblAccNameValue": lblAccNameValue,
                "lblAccType": lblAccType,
                "lblAccTypeValue": lblAccTypeValue,
                "accNumHidden": accNumHidden,
                "shareId": shareId,
                "loanId": loanId
            };
            data.push(temp);
        }
        this.view.segAccLink.setData(data);
    },
    accTypeValue: function(response) {
        var typeValue;
        if (response !== null && response !== "" && typeof response !== "undefined") {
            if (response === "0" || response === "2" || response === "5" || response === "7" || response === "8") typeValue = "Personal";
            else if (response === "10") typeValue = "Business";
            else if (response === "11") typeValue = "Personal Agency";
            else if (response === "12") typeValue = "Irrevocable Trust";
            else if (response === "13") typeValue = "Revocable Trust";
            else if (response === "20") typeValue = "Minor";
            else if (response === "21") typeValue = "NC UTMA";
            else typeValue = "Other";
        }
        return typeValue;
    },
    segAccLinkOnRowClick: function() {
        var navManager = applicationManager.getNavigationManager();
        var selItems, selIndex, imgActive;
        var data = [];
        selIndex = this.view.segAccLink.selectedRowIndex[1];
        if (this.prevSelIndex >= 0 && selIndex === this.prevSelIndex) {
            return;
        }
        if (this.flag) {
            this.flag = 0;
            var imgInActive = {
                src: "radio_inactive.png"
            };
            this.prevSelItems.imgIndicator = imgInActive;
            this.view.segAccLink.setDataAt(this.prevSelItems, this.prevSelIndex);
        }
        if (!this.flag) {
            try {
                selItems = this.view.segAccLink.selectedRowItems[0];
                selIndex = this.view.segAccLink.selectedRowIndex[1];
                this.prevSelItems = selItems;
                this.prevSelIndex = selIndex;
                this.flag = 1;
                imgActive = {
                    src: "radio_active.png"
                };
                selItems.imgIndicator = imgActive;
                this.view.segAccLink.setDataAt(selItems, selIndex);
                data.push(selItems);
                navManager.setCustomInfo("frmAccountPrimaryLinking", data);
            } catch (e) {
                kony.print("Exception " + e);
            }
        }
        this.view.btnContinue.skin = "sknBtn0095e426pxEnabled";
        this.view.btnContinue.focusSkin = "sknBtn0095e426pxEnabled";
        this.view.btnContinue.setEnabled(true);
    },
    onClickCancel: function() {
        applicationManager.getPresentationUtility().dismissLoadingScreen();
        var enrollMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("EnrollModule");
        enrollMod.presentationController.resetEnrollObj();
    },
});
define("EnrollModule/frmAccountPrimaryLinkingControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** init defined for frmAccountPrimaryLinking **/
    AS_Form_h38f7a3499db4f9a9bb84d34f4c0a6f4: function AS_Form_h38f7a3499db4f9a9bb84d34f4c0a6f4(eventobject) {
        var self = this;
        return self.init.call(this);
    },
    /** preShow defined for frmAccountPrimaryLinking **/
    AS_Form_a8dc647dc0bb430ca00aeda24f539b4a: function AS_Form_a8dc647dc0bb430ca00aeda24f539b4a(eventobject) {
        var self = this;
        return self.frmAccPrimaryLinkPreshow.call(this);
    }
});
define("EnrollModule/frmAccountPrimaryLinkingController", ["EnrollModule/userfrmAccountPrimaryLinkingController", "EnrollModule/frmAccountPrimaryLinkingControllerActions"], function() {
    var controller = require("EnrollModule/userfrmAccountPrimaryLinkingController");
    var controllerActions = ["EnrollModule/frmAccountPrimaryLinkingControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
