define("EnrollModule/frmAccountLinking", function() {
    return function(controller) {
        function addWidgetsfrmAccountLinking() {
            this.setDefaultUnit(kony.flex.DP);
            var flxHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "56dp",
                "id": "flxHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxffffffShadow",
                "top": "0%",
                "width": "100%",
                "zIndex": 100
            }, {}, {});
            flxHeader.setDefaultUnit(kony.flex.DP);
            var customHeader = new com.kmb.common.customHeader({
                "clipBounds": false,
                "height": "100%",
                "id": "customHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "btnRight": {
                        "isVisible": true,
                        "text": "Cancel"
                    },
                    "flxBack": {
                        "isVisible": true
                    },
                    "flxSearch": {
                        "isVisible": false
                    },
                    "imgBack": {
                        "isVisible": true
                    },
                    "lblLocateUs": {
                        "text": kony.i18n.getLocalizedString("kony.mb.login.enroll")
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxHeader.add(customHeader);
            var flxPopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "10.50%",
                "id": "flxPopup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "sknFlxf54b5e",
                "top": "0dp",
                "width": "100%",
                "zIndex": 200
            }, {}, {});
            flxPopup.setDefaultUnit(kony.flex.DP);
            var customPopup = new com.kmb.common.customPopup({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "customPopup",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {}
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxPopup.add(customPopup);
            var flxMainContainer = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": "0%",
                "bounces": false,
                "clipBounds": false,
                "enableScrolling": true,
                "horizontalScrollIndicator": true,
                "id": "flxMainContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "sknScrFlxffffff",
                "top": "56dp",
                "verticalScrollIndicator": true,
                "width": "100%",
                "zIndex": 30
            }, {}, {});
            flxMainContainer.setDefaultUnit(kony.flex.DP);
            var lblText = new kony.ui.Label({
                "centerX": "50%",
                "id": "lblText",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl54565aSSP30",
                "text": kony.i18n.getLocalizedString("kony.mb.enroll.linkAcc"),
                "textStyle": {
                    "letterSpacing": 0,
                    "strikeThrough": false
                },
                "top": "3%",
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false
            });
            var lblTextdesc = new kony.ui.Label({
                "centerX": "50%",
                "id": "lblTextdesc",
                "isVisible": true,
                "left": "127dp",
                "skin": "sknLbl54565aSSPReg26",
                "text": kony.i18n.getLocalizedString("kony.mb.enroll.primAccDesc"),
                "textStyle": {
                    "letterSpacing": 0,
                    "strikeThrough": false
                },
                "top": "5%",
                "width": "75%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false
            });
            var flxAccLink = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAccLink",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10%",
                "isModalContainer": false,
                "top": "5%",
                "width": "80%",
                "zIndex": 1
            }, {}, {});
            flxAccLink.setDefaultUnit(kony.flex.DP);
            var segAccLink = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "data": [{
                    "imgIndicator": "checkbox_unactive_light.png",
                    "lblAccName": kony.i18n.getLocalizedString("kony.mb.enroll.primary"),
                    "lblAccNameValue": "David Cooper",
                    "lblAccNum": "1234567890",
                    "lblAccType": kony.i18n.getLocalizedString("kony.mb.enroll.accType"),
                    "lblAccTypeValue": "Business"
                }, {
                    "imgIndicator": "checkbox_unactive_light.png",
                    "lblAccName": kony.i18n.getLocalizedString("kony.mb.enroll.primary"),
                    "lblAccNameValue": "David Cooper",
                    "lblAccNum": "1234567890",
                    "lblAccType": kony.i18n.getLocalizedString("kony.mb.enroll.accType"),
                    "lblAccTypeValue": "Business"
                }, {
                    "imgIndicator": "checkbox_unactive_light.png",
                    "lblAccName": kony.i18n.getLocalizedString("kony.mb.enroll.primary"),
                    "lblAccNameValue": "David Cooper",
                    "lblAccNum": "1234567890",
                    "lblAccType": kony.i18n.getLocalizedString("kony.mb.enroll.accType"),
                    "lblAccTypeValue": "Business"
                }],
                "groupCells": false,
                "id": "segAccLink",
                "isVisible": true,
                "left": 0,
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowTemplate": "flxAccLinking",
                "scrollingEvents": {},
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorColor": "ddddde00",
                "separatorRequired": true,
                "separatorThickness": 1,
                "showScrollbars": false,
                "top": 0,
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxAccDetails": "flxAccDetails",
                    "flxAccLinking": "flxAccLinking",
                    "flxAccTypeDetails": "flxAccTypeDetails",
                    "flxImgIndicator": "flxImgIndicator",
                    "flxSeparator": "flxSeparator",
                    "imgIndicator": "imgIndicator",
                    "lblAccName": "lblAccName",
                    "lblAccNameValue": "lblAccNameValue",
                    "lblAccNum": "lblAccNum",
                    "lblAccType": "lblAccType",
                    "lblAccTypeValue": "lblAccTypeValue"
                },
                "width": "100%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 1, 0],
                "paddingInPixel": false
            }, {});
            flxAccLink.add(segAccLink);
            var btnSkip = new kony.ui.Button({
                "centerX": "50%",
                "height": "50dp",
                "id": "btnSkip",
                "isVisible": true,
                "left": "10%",
                "skin": "skinButtonDisabled",
                "text": kony.i18n.getLocalizedString("kony.mb.common.skip"),
                "top": "20%",
                "width": "90%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnContinue = new kony.ui.Button({
                "bottom": "5%",
                "centerX": "50%",
                "height": "50dp",
                "id": "btnContinue",
                "isVisible": true,
                "left": "0%",
                "skin": "skinButtonDisabled",
                "text": kony.i18n.getLocalizedString("kony.mb.common.continue"),
                "top": "3%",
                "width": "90%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxMainContainer.add(lblText, lblTextdesc, flxAccLink, btnSkip, btnContinue);
            this.add(flxHeader, flxPopup, flxMainContainer);
        };
        return [{
            "addWidgets": addWidgetsfrmAccountLinking,
            "enableScrolling": false,
            "enabledForIdleTimeout": true,
            "id": "frmAccountLinking",
            "init": controller.AS_Form_a8647c9d49ea4d3481d32cfad2b0a894,
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "preShow": function(eventobject) {
                controller.AS_Form_b3984f45ac7d444a898b7264c7e3ce8b(eventobject);
            },
            "skin": "sknFormBckgrndGrey"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "footerOverlap": false,
            "headerOverlap": false,
            "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
            "retainScrollPosition": false,
            "titleBar": true,
            "titleBarSkin": "sknTitle1a98ffffffff30px",
            "windowSoftInputMode": constants.FORM_ADJUST_PAN
        }]
    }
});