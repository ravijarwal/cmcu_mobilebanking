define("flxDropdownOption", function() {
    return function(controller) {
        var flxDropdownOption = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "55dp",
            "id": "flxDropdownOption",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxWhite",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxDropdownOption.setDefaultUnit(kony.flex.DP);
        var lblOption = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblOption",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknLbl424242SSP26px",
            "text": "si***n1@gmail.com",
            "textStyle": {
                "letterSpacing": 0,
                "strikeThrough": false
            },
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "textCopyable": false
        });
        var flxArrow = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxArrow",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "20dp",
            "skin": "slFbox",
            "top": "0dp",
            "width": "20dp",
            "zIndex": 1
        }, {}, {});
        flxArrow.setDefaultUnit(kony.flex.DP);
        var imgArrow = new kony.ui.Image2({
            "centerY": "50%",
            "height": "14dp",
            "id": "imgArrow",
            "isVisible": true,
            "right": "0dp",
            "skin": "slImage",
            "src": "calenderarrowright.png",
            "width": "14dp",
            "zIndex": 1
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxArrow.add(imgArrow);
        var flxRadio = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxRadio",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "20dp",
            "skin": "slFbox",
            "top": "0dp",
            "width": "20dp",
            "zIndex": 1
        }, {}, {});
        flxRadio.setDefaultUnit(kony.flex.DP);
        var imgRadio = new kony.ui.Image2({
            "centerY": "50%",
            "height": "14dp",
            "id": "imgRadio",
            "isVisible": true,
            "right": "0dp",
            "skin": "slImage",
            "src": "radio_inactive.png",
            "width": "14dp",
            "zIndex": 1
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxRadio.add(imgRadio);
        var lblWrkEmailHiddenVal = new kony.ui.Label({
            "centerY": "78.79%",
            "id": "lblWrkEmailHiddenVal",
            "isVisible": false,
            "left": "10dp",
            "skin": "sknLbl424242SSP26px",
            "text": "si***n1@gmail.com",
            "textStyle": {
                "letterSpacing": 0,
                "strikeThrough": false
            },
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "textCopyable": false
        });
        var lblPrsnlEmailHiddenVal = new kony.ui.Label({
            "centerY": "78.79%",
            "id": "lblPrsnlEmailHiddenVal",
            "isVisible": false,
            "left": "10dp",
            "skin": "sknLbl424242SSP26px",
            "text": "si***n1@gmail.com",
            "textStyle": {
                "letterSpacing": 0,
                "strikeThrough": false
            },
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "textCopyable": false
        });
        var lblHomeEmailHiddenVal = new kony.ui.Label({
            "centerY": "78.79%",
            "id": "lblHomeEmailHiddenVal",
            "isVisible": false,
            "left": "10dp",
            "skin": "sknLbl424242SSP26px",
            "text": "si***n1@gmail.com",
            "textStyle": {
                "letterSpacing": 0,
                "strikeThrough": false
            },
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "textCopyable": false
        });
        var lblWrkPhnHiddenVal = new kony.ui.Label({
            "centerY": "78.79%",
            "id": "lblWrkPhnHiddenVal",
            "isVisible": false,
            "left": "10dp",
            "skin": "sknLbl424242SSP26px",
            "text": "si***n1@gmail.com",
            "textStyle": {
                "letterSpacing": 0,
                "strikeThrough": false
            },
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "textCopyable": false
        });
        var lblMobilePhnHiddenVal = new kony.ui.Label({
            "centerY": "78.79%",
            "id": "lblMobilePhnHiddenVal",
            "isVisible": false,
            "left": "10dp",
            "skin": "sknLbl424242SSP26px",
            "text": "si***n1@gmail.com",
            "textStyle": {
                "letterSpacing": 0,
                "strikeThrough": false
            },
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "textCopyable": false
        });
        var lblHomePhnHiddenVal = new kony.ui.Label({
            "centerY": "78.79%",
            "id": "lblHomePhnHiddenVal",
            "isVisible": false,
            "left": "10dp",
            "skin": "sknLbl424242SSP26px",
            "text": "si***n1@gmail.com",
            "textStyle": {
                "letterSpacing": 0,
                "strikeThrough": false
            },
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "textCopyable": false
        });
        flxDropdownOption.add(lblOption, flxArrow, flxRadio, lblWrkEmailHiddenVal, lblPrsnlEmailHiddenVal, lblHomeEmailHiddenVal, lblWrkPhnHiddenVal, lblMobilePhnHiddenVal, lblHomePhnHiddenVal);
        return flxDropdownOption;
    }
})