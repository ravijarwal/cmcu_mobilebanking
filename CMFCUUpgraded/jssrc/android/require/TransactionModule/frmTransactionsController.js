define("TransactionModule/userfrmTransactionsController", {
    init: function() {
        var navManager = applicationManager.getNavigationManager();
        var currentForm = navManager.getCurrentForm();
        applicationManager.getPresentationFormUtility().initCommonActions(this, "YES", currentForm);
        this.view.customHeader.flxBack.onClick = this.flxBackOnClick;
    },
    onNavigate: function(obj) {
        if (typeof description === 'undefined') {
            description = "";
        }
        var MenuHandler = applicationManager.getMenuHandler();
        var navMan = applicationManager.getNavigationManager();
        var segmentClickData = navMan.getCustomInfo("frmTransactions");
        this.bindDataForTrans(segmentClickData);
    },
    bindDataForTrans: function(segData) {
        var desc;
        var regECode = segData.lblRegECode;
        var commmentCode = segData.lblCommentCode;
        //only display a single Comment or Description. 
        if (commmentCode == 1 && regECode == 0) {
            desc = segData.lblTransactions;
        }
        //only display a single Comment or Description.
        if (commmentCode == 0 && regECode == 0) {
            desc = segData.lblTransactions;
        }
        //comment goes with the next transaction in the response,store desc   
        if (regECode == 1 && commmentCode == 1) {
            desc = segData.lblTransactions;
            description += segData.lblTransactions;
        }
        //previous stored Description will be displayed with Comment or Description for this transaction added to the end.
        if (regECode == 1 && commmentCode == 0) {
            desc = description + " " + segData.lblTransactions;
        }
        if (segData.lblTransactionsAmount.text !== undefined && segData.lblTransactionsAmount.text !== "") {
            this.view.flxmain.setVisibility(true);
            this.view.lblDoller.text = segData.lblTransactionsAmount.text;
        } else {
            this.view.flxmain.setVisibility(false);
        }
        if (segData.lblPrevAvailBalance !== undefined && segData.lblPrevAvailBalance !== "") {
            this.view.flxBalance.setVisibility(true);
            this.view.lblTransBalanceShow.text = segData.lblPrevAvailBalance;
        } else {
            this.view.flxBalance.setVisibility(false);
        }
        if (segData.lblDate.text !== undefined && segData.lblPostTime !== undefined) {
            this.view.flxDateTime.setVisibility(true);
            this.view.lblTransDateTimeShow.text = segData.lblDate.text + " " + segData.lblPostTime;
        } else {
            this.view.flxDateTime.setVisibility(false);
        }
        if (desc !== "" && desc !== undefined) {
            this.view.flxDateTime.setVisibility(true);
            this.view.lblTransDescriptionShow.text = desc;
        } else {
            this.view.flxDateTime.setVisibility(false);
        }
        if (segData.lblRunningBalance.text !== undefined && segData.lblRunningBalance.text !== "") {
            this.view.flxNewBalance.setVisibility(true);
            this.view.lblNewBalanceValue.text = segData.lblRunningBalance.text;
        } else {
            this.view.flxNewBalance.setVisibility(false);
        }
        this.view.flxCheckNum.isVisible = false; // needs to configure at MF then update here
        this.view.flxNots.isVisible = false; //update when PFM clears this
        this.view.flxCategory.isVisible = false; //update when service clears this
        //  this.view.TxtAreaCommentsShow.text = ""	;
        applicationManager.getPresentationUtility().dismissLoadingScreen();
    },
    flxBackOnClick: function() {
        var navMan = applicationManager.getNavigationManager();
        navMan.goBack();
    }
});
define("TransactionModule/frmTransactionsControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for flxBack **/
    AS_FlexContainer_d05c21da5019437a9492c7add115ff1b: function AS_FlexContainer_d05c21da5019437a9492c7add115ff1b(eventobject) {
        var self = this;
        //this.navigateToBack();
        var navMan = applicationManager.getNavigationManager();
        navMan.goBack();
    }
});
define("TransactionModule/frmTransactionsController", ["TransactionModule/userfrmTransactionsController", "TransactionModule/frmTransactionsControllerActions"], function() {
    var controller = require("TransactionModule/userfrmTransactionsController");
    var controllerActions = ["TransactionModule/frmTransactionsControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
