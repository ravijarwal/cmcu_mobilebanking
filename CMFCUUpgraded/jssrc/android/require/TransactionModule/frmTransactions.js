define("TransactionModule/frmTransactions", function() {
    return function(controller) {
        function addWidgetsfrmTransactions() {
            this.setDefaultUnit(kony.flex.DP);
            var flxParent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxParent",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxWhite",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxParent.setDefaultUnit(kony.flex.DP);
            var flxHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "10%",
                "id": "flxHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 4
            }, {}, {});
            flxHeader.setDefaultUnit(kony.flex.DP);
            var customHeader = new com.kmb.common.customHeader({
                "clipBounds": false,
                "height": "100%",
                "id": "customHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxSearch": {
                        "isVisible": false
                    },
                    "imgBack": {
                        "centerY": "40%",
                        "src": "backbutton.png"
                    },
                    "lblLocateUs": {
                        "centerX": "50%",
                        "text": "Transactions Details"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            customHeader.flxBack.onClick = controller.AS_FlexContainer_d05c21da5019437a9492c7add115ff1b;
            flxHeader.add(customHeader);
            var flxScrollMain = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "90%",
                "horizontalScrollIndicator": true,
                "id": "flxScrollMain",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "1dp",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "0dp",
                "verticalScrollIndicator": true,
                "width": "100%",
                "zIndex": 5
            }, {}, {});
            flxScrollMain.setDefaultUnit(kony.flex.DP);
            var flxmain = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "65dp",
                "id": "flxmain",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxWhite",
                "top": 0,
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxmain.setDefaultUnit(kony.flex.DP);
            var flxInner = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxInner",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "55%",
                "zIndex": 1
            }, {}, {});
            flxInner.setDefaultUnit(kony.flex.DP);
            var lblTransAmt = new kony.ui.Label({
                "id": "lblTransAmt",
                "isVisible": true,
                "left": "17dp",
                "skin": "sknLblLatofontMedium",
                "text": "Transaction Amount",
                "textStyle": {
                    "letterSpacing": 0,
                    "strikeThrough": false
                },
                "top": "14dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false
            });
            var lblDoller = new kony.ui.Label({
                "id": "lblDoller",
                "isVisible": true,
                "left": "17dp",
                "skin": "sknLblVoietFont",
                "text": "$200.00",
                "textStyle": {
                    "letterSpacing": 0,
                    "strikeThrough": false
                },
                "top": "35dp",
                "width": "95%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false
            });
            flxInner.add(lblTransAmt, lblDoller);
            var flxInnerOne = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxInnerOne",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "206dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "45%",
                "zIndex": 1
            }, {}, {});
            flxInnerOne.setDefaultUnit(kony.flex.DP);
            var lblSuccessful = new kony.ui.Label({
                "id": "lblSuccessful",
                "isVisible": true,
                "right": "18%",
                "skin": "sknLblGreenFont",
                "text": "Successful",
                "textStyle": {
                    "letterSpacing": 0,
                    "strikeThrough": false
                },
                "top": "37dp",
                "width": "60%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false
            });
            flxInnerOne.add(lblSuccessful);
            var flxSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "centerX": "50%",
                "clipBounds": true,
                "height": "1dp",
                "id": "flxSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0.00%",
                "isModalContainer": false,
                "skin": "sknFlxSeprtrGrey",
                "width": "90%",
                "zIndex": 1
            }, {}, {});
            flxSeperator.setDefaultUnit(kony.flex.DP);
            flxSeperator.add();
            flxmain.add(flxInner, flxInnerOne, flxSeperator);
            var flxDateTime = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "65dp",
                "id": "flxDateTime",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxWhite",
                "top": "0%",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxDateTime.setDefaultUnit(kony.flex.DP);
            var lblTransDateTime = new kony.ui.Label({
                "height": "22dp",
                "id": "lblTransDateTime",
                "isVisible": true,
                "left": "17dp",
                "skin": "sknLblLatofontMedium",
                "text": "Transaction Date & Time",
                "textStyle": {
                    "letterSpacing": 0,
                    "strikeThrough": false
                },
                "top": "14dp",
                "width": "90%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false
            });
            var lblTransDateTimeShow = new kony.ui.Label({
                "id": "lblTransDateTimeShow",
                "isVisible": true,
                "left": "17dp",
                "skin": "sknLbl424242LatoBlack",
                "text": "04/17/2018, 3:42 PM",
                "textStyle": {
                    "letterSpacing": 0,
                    "strikeThrough": false
                },
                "top": "37dp",
                "width": "90%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false
            });
            var flxSeperatorDT = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "centerX": "50%",
                "clipBounds": true,
                "height": "1dp",
                "id": "flxSeperatorDT",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0.00%",
                "isModalContainer": false,
                "skin": "sknFlxSeprtrGrey",
                "width": "90%",
                "zIndex": 1
            }, {}, {});
            flxSeperatorDT.setDefaultUnit(kony.flex.DP);
            flxSeperatorDT.add();
            flxDateTime.add(lblTransDateTime, lblTransDateTimeShow, flxSeperatorDT);
            var flxBalance = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "65dp",
                "id": "flxBalance",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxWhite",
                "top": "0%",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxBalance.setDefaultUnit(kony.flex.DP);
            var lblTransBalance = new kony.ui.Label({
                "height": "22dp",
                "id": "lblTransBalance",
                "isVisible": true,
                "left": "17dp",
                "skin": "sknLblLatofontMedium",
                "text": "Running Balance before transaction",
                "textStyle": {
                    "letterSpacing": 0,
                    "strikeThrough": false
                },
                "top": "14dp",
                "width": "90%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false
            });
            var lblTransBalanceShow = new kony.ui.Label({
                "id": "lblTransBalanceShow",
                "isVisible": true,
                "left": "17dp",
                "skin": "sknLbl424242LatoBlack",
                "text": "NA",
                "textStyle": {
                    "letterSpacing": 0,
                    "strikeThrough": false
                },
                "top": "37dp",
                "width": "90%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false
            });
            var flxSeperatorBal = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "centerX": "50%",
                "clipBounds": true,
                "height": "1dp",
                "id": "flxSeperatorBal",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "sknFlxSeprtrGrey",
                "width": "90%",
                "zIndex": 1
            }, {}, {});
            flxSeperatorBal.setDefaultUnit(kony.flex.DP);
            flxSeperatorBal.add();
            flxBalance.add(lblTransBalance, lblTransBalanceShow, flxSeperatorBal);
            var flxNewBalance = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "65dp",
                "id": "flxNewBalance",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxWhite",
                "top": "0%",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNewBalance.setDefaultUnit(kony.flex.DP);
            var lblNewBalance = new kony.ui.Label({
                "id": "lblNewBalance",
                "isVisible": true,
                "left": "17dp",
                "skin": "sknLblLatofontMedium",
                "text": "Running Balance after transaction",
                "textStyle": {
                    "letterSpacing": 0,
                    "strikeThrough": false
                },
                "top": "14dp",
                "width": "90%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false
            });
            var lblNewBalanceValue = new kony.ui.Label({
                "id": "lblNewBalanceValue",
                "isVisible": true,
                "left": "17dp",
                "skin": "sknLbl424242LatoBlack",
                "text": "#54444",
                "textStyle": {
                    "letterSpacing": 0,
                    "strikeThrough": false
                },
                "top": "37dp",
                "width": "90%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false
            });
            var CopyflxSeperatorCN0jb6cb298a31140 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "centerX": "50%",
                "clipBounds": true,
                "height": "1dp",
                "id": "CopyflxSeperatorCN0jb6cb298a31140",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "sknFlxSeprtrGrey",
                "width": "90%",
                "zIndex": 1
            }, {}, {});
            CopyflxSeperatorCN0jb6cb298a31140.setDefaultUnit(kony.flex.DP);
            CopyflxSeperatorCN0jb6cb298a31140.add();
            flxNewBalance.add(lblNewBalance, lblNewBalanceValue, CopyflxSeperatorCN0jb6cb298a31140);
            var flxCheckNum = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "65dp",
                "id": "flxCheckNum",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxWhite",
                "top": "0%",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxCheckNum.setDefaultUnit(kony.flex.DP);
            var lblTransCheckNum = new kony.ui.Label({
                "id": "lblTransCheckNum",
                "isVisible": true,
                "left": "17dp",
                "skin": "sknLblLatofontMedium",
                "text": "Check Number",
                "textStyle": {
                    "letterSpacing": 0,
                    "strikeThrough": false
                },
                "top": "14dp",
                "width": "90%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false
            });
            var lblTransCheckNumShow = new kony.ui.Label({
                "id": "lblTransCheckNumShow",
                "isVisible": true,
                "left": "17dp",
                "skin": "sknLbl424242LatoBlack",
                "text": "#54444",
                "textStyle": {
                    "letterSpacing": 0,
                    "strikeThrough": false
                },
                "top": "37dp",
                "width": "90%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false
            });
            var flxSeperatorCN = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "centerX": "50%",
                "clipBounds": true,
                "height": "1dp",
                "id": "flxSeperatorCN",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "sknFlxSeprtrGrey",
                "width": "90%",
                "zIndex": 1
            }, {}, {});
            flxSeperatorCN.setDefaultUnit(kony.flex.DP);
            flxSeperatorCN.add();
            flxCheckNum.add(lblTransCheckNum, lblTransCheckNumShow, flxSeperatorCN);
            var flxDescription = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxDescription",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxWhite",
                "top": "0%",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxDescription.setDefaultUnit(kony.flex.DP);
            var lblTransDescription = new kony.ui.Label({
                "height": "22dp",
                "id": "lblTransDescription",
                "isVisible": true,
                "left": "17dp",
                "skin": "sknLblLatofontMedium",
                "text": "Description",
                "textStyle": {
                    "letterSpacing": 0,
                    "strikeThrough": false
                },
                "top": "14dp",
                "width": "90%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false
            });
            var lblTransDescriptionShow = new kony.ui.Label({
                "id": "lblTransDescriptionShow",
                "isVisible": true,
                "left": "17dp",
                "skin": "sknLbl424242LatoBlack",
                "text": "Here goes the description  ",
                "textStyle": {
                    "letterSpacing": 0,
                    "strikeThrough": false
                },
                "top": "3dp",
                "width": "90%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false
            });
            var flxSeperatorDesc = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "1dp",
                "id": "flxSeperatorDesc",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "39dp",
                "isModalContainer": false,
                "skin": "sknFlxSeprtrGrey",
                "top": "8dp",
                "width": "90%",
                "zIndex": 1
            }, {}, {});
            flxSeperatorDesc.setDefaultUnit(kony.flex.DP);
            flxSeperatorDesc.add();
            flxDescription.add(lblTransDescription, lblTransDescriptionShow, flxSeperatorDesc);
            var flxCategory = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "65dp",
                "id": "flxCategory",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxWhite",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxCategory.setDefaultUnit(kony.flex.DP);
            var lblTransCategory = new kony.ui.Label({
                "id": "lblTransCategory",
                "isVisible": true,
                "left": "17dp",
                "skin": "sknLblLatofontMedium",
                "text": "Category",
                "textStyle": {
                    "letterSpacing": 0,
                    "strikeThrough": false
                },
                "top": "14dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false
            });
            var lblTransCategoryShow = new kony.ui.Label({
                "id": "lblTransCategoryShow",
                "isVisible": true,
                "left": "17dp",
                "skin": "sknLbl424242LatoBlack",
                "text": "Category",
                "textStyle": {
                    "letterSpacing": 0,
                    "strikeThrough": false
                },
                "top": "37dp",
                "width": "90%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false
            });
            var flxSeperatorCat = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "centerX": "50%",
                "clipBounds": true,
                "height": "1dp",
                "id": "flxSeperatorCat",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0.00%",
                "isModalContainer": false,
                "skin": "sknFlxSeprtrGrey",
                "width": "90%",
                "zIndex": 1
            }, {}, {});
            flxSeperatorCat.setDefaultUnit(kony.flex.DP);
            flxSeperatorCat.add();
            flxCategory.add(lblTransCategory, lblTransCategoryShow, flxSeperatorCat);
            var flxNots = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "65dp",
                "id": "flxNots",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxWhite",
                "top": "0.00%",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNots.setDefaultUnit(kony.flex.DP);
            var lblTransNotes = new kony.ui.Label({
                "id": "lblTransNotes",
                "isVisible": true,
                "left": "17dp",
                "skin": "sknLblLatofontMedium",
                "text": "Notes",
                "textStyle": {
                    "letterSpacing": 0,
                    "strikeThrough": false
                },
                "top": "14dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false
            });
            var lblTransNotesShow = new kony.ui.Label({
                "id": "lblTransNotesShow",
                "isVisible": true,
                "left": "17dp",
                "skin": "sknLbl424242LatoBlack",
                "text": "Here goes the Notes",
                "textStyle": {
                    "letterSpacing": 0,
                    "strikeThrough": false
                },
                "top": "37dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false
            });
            var flxSeperatorNotes = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "centerX": "50%",
                "clipBounds": true,
                "height": "1dp",
                "id": "flxSeperatorNotes",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0.00%",
                "isModalContainer": false,
                "skin": "sknFlxSeprtrGrey",
                "width": "90%",
                "zIndex": 1
            }, {}, {});
            flxSeperatorNotes.setDefaultUnit(kony.flex.DP);
            flxSeperatorNotes.add();
            flxNots.add(lblTransNotes, lblTransNotesShow, flxSeperatorNotes);
            var flxComments = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "120dp",
                "id": "flxComments",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxWhite",
                "top": "0%",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxComments.setDefaultUnit(kony.flex.DP);
            var lblComments = new kony.ui.Label({
                "id": "lblComments",
                "isVisible": true,
                "left": "17dp",
                "skin": "sknLblLatofontMedium",
                "text": "Comments",
                "textStyle": {
                    "letterSpacing": 0,
                    "strikeThrough": false
                },
                "top": "14dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false
            });
            var TxtAreaCommentsShow = new kony.ui.TextArea2({
                "autoCapitalize": constants.TEXTAREA_AUTO_CAPITALIZE_NONE,
                "focusSkin": "defTextAreaFocus",
                "height": "67dp",
                "id": "TxtAreaCommentsShow",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTAREA_KEY_BOARD_STYLE_DEFAULT,
                "left": "16dp",
                "numberOfVisibleLines": 3,
                "placeholder": "Add a new comment",
                "skin": "defTextAreaNormal",
                "textInputMode": constants.TEXTAREA_INPUT_MODE_ANY,
                "top": "38dp",
                "width": "90%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [2, 2, 2, 2],
                "paddingInPixel": false
            }, {
                "placeholderSkin": "defTextAreaPlaceholder"
            });
            flxComments.add(lblComments, TxtAreaCommentsShow);
            var flxSearchButton = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "18%",
                "id": "flxSearchButton",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0%",
                "width": "100%",
                "zIndex": 5
            }, {}, {});
            flxSearchButton.setDefaultUnit(kony.flex.DP);
            var btnSearch = new kony.ui.Button({
                "focusSkin": "sknBtn0095e4RoundedffffffSSP26px",
                "height": "50dp",
                "id": "btnSearch",
                "isVisible": false,
                "left": "20dp",
                "right": "20dp",
                "skin": "sknBtn0095e4RoundedffffffSSP26px",
                "text": "dispute",
                "top": "11dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSearchButton.add(btnSearch);
            flxScrollMain.add(flxmain, flxDateTime, flxBalance, flxNewBalance, flxCheckNum, flxDescription, flxCategory, flxNots, flxComments, flxSearchButton);
            flxParent.add(flxHeader, flxScrollMain);
            this.add(flxParent);
        };
        return [{
            "addWidgets": addWidgetsfrmTransactions,
            "enabledForIdleTimeout": false,
            "id": "frmTransactions",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "skin": "slForm"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "footerOverlap": false,
            "headerOverlap": false,
            "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
            "retainScrollPosition": false,
            "titleBar": true,
            "titleBarSkin": "slTitleBar",
            "windowSoftInputMode": constants.FORM_ADJUST_PAN
        }]
    }
});