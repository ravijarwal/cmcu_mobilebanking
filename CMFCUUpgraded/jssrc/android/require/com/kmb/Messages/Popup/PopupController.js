define("com/kmb/Messages/Popup/userPopupController", function() {
    return {};
});
define("com/kmb/Messages/Popup/PopupControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/kmb/Messages/Popup/PopupController", ["com/kmb/Messages/Popup/userPopupController", "com/kmb/Messages/Popup/PopupControllerActions"], function() {
    var controller = require("com/kmb/Messages/Popup/userPopupController");
    var actions = require("com/kmb/Messages/Popup/PopupControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
