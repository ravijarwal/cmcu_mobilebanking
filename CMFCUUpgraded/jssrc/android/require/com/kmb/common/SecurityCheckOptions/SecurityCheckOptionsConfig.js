define(function() {
    return {
        "properties": [],
        "apis": ["setTextMessageOptions", "setPhoneCallOptions", "setEmailCodeOptions", "onRowClickEmailCode", "onClickEmailSetData", "onRowClickPhoneCall", "selectedEmailRowIndex", "selectedMobileNumRowIndex", "onRowClicktextMessage", "selectedMobileForTextIndex", "onClickCVV"],
        "events": ["onClickCVV", "onRowClicktextMessage", "onRowClickPhoneCall", "onRowClickEmailCode"]
    }
});