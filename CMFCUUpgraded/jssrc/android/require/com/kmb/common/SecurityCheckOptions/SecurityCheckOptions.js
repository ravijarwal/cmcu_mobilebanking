define(function() {
    return function(controller) {
        var SecurityCheckOptions = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0dp",
            "clipBounds": true,
            "isMaster": true,
            "id": "SecurityCheckOptions",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "f9f9",
            "top": "175dp",
            "width": "100%",
            "zIndex": 10
        }, controller.args[0], "SecurityCheckOptions"), extendConfig({}, controller.args[1], "SecurityCheckOptions"), extendConfig({}, controller.args[2], "SecurityCheckOptions"));
        SecurityCheckOptions.setDefaultUnit(kony.flex.DP);
        var CVVCode = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "50%",
            "clipBounds": true,
            "height": "55dp",
            "id": "CVVCode",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "onClick": controller.AS_onClickCVV_be2d1001ab53454d9d5de3d2dd39d59d,
            "skin": "CopyslFbox0aee127faeffd4c",
            "top": "30dp",
            "width": "90%"
        }, controller.args[0], "CVVCode"), extendConfig({}, controller.args[1], "CVVCode"), extendConfig({}, controller.args[2], "CVVCode"));
        CVVCode.setDefaultUnit(kony.flex.DP);
        var richTxtOptionTitle = new kony.ui.RichText(extendConfig({
            "centerY": "50%",
            "height": "55dp",
            "id": "richTxtOptionTitle",
            "isVisible": true,
            "left": "53dp",
            "linkSkin": "defRichTextLink",
            "right": "50dp",
            "skin": "sknRtx424242SSP26px",
            "text": "Enter CVV Code\n\n",
            "zIndex": 1
        }, controller.args[0], "richTxtOptionTitle"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "richTxtOptionTitle"), extendConfig({}, controller.args[2], "richTxtOptionTitle"));
        var flxArrow = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxArrow",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "20dp",
            "skin": "slFbox",
            "top": "0dp",
            "width": "20dp",
            "zIndex": 1
        }, controller.args[0], "flxArrow"), extendConfig({}, controller.args[1], "flxArrow"), extendConfig({}, controller.args[2], "flxArrow"));
        flxArrow.setDefaultUnit(kony.flex.DP);
        var imgArrow = new kony.ui.Image2(extendConfig({
            "centerY": "50%",
            "height": "14dp",
            "id": "imgArrow",
            "isVisible": true,
            "right": "0dp",
            "skin": "slImage",
            "src": "calenderarrowright.png",
            "width": "14dp",
            "zIndex": 1
        }, controller.args[0], "imgArrow"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgArrow"), extendConfig({}, controller.args[2], "imgArrow"));
        flxArrow.add(imgArrow);
        var flxImg = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxImg",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "45dp",
            "zIndex": 1
        }, controller.args[0], "flxImg"), extendConfig({}, controller.args[1], "flxImg"), extendConfig({}, controller.args[2], "flxImg"));
        flxImg.setDefaultUnit(kony.flex.DP);
        var imgIcon = new kony.ui.Image2(extendConfig({
            "centerY": "50%",
            "height": "24dp",
            "id": "imgIcon",
            "isVisible": true,
            "left": "10dp",
            "skin": "slImage",
            "src": "entercvvicon.png",
            "width": "22dp",
            "zIndex": 1
        }, controller.args[0], "imgIcon"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgIcon"), extendConfig({}, controller.args[2], "imgIcon"));
        var CopyflxSeparator0a9c19d1e3d614b = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "CopyflxSeparator0a9c19d1e3d614b",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "0dp",
            "skin": "sknFlxSeprtrGrey",
            "top": "0dp",
            "width": "1dp",
            "zIndex": 1
        }, controller.args[0], "CopyflxSeparator0a9c19d1e3d614b"), extendConfig({}, controller.args[1], "CopyflxSeparator0a9c19d1e3d614b"), extendConfig({}, controller.args[2], "CopyflxSeparator0a9c19d1e3d614b"));
        CopyflxSeparator0a9c19d1e3d614b.setDefaultUnit(kony.flex.DP);
        CopyflxSeparator0a9c19d1e3d614b.add();
        flxImg.add(imgIcon, CopyflxSeparator0a9c19d1e3d614b);
        CVVCode.add(richTxtOptionTitle, flxArrow, flxImg);
        var textMessageCode = new common.dropdown(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "centerX": "50%",
            "clipBounds": true,
            "id": "textMessageCode",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "masterType": constants.MASTER_TYPE_USERWIDGET,
            "isModalContainer": false,
            "skin": "sknflxffffffBorderf1f1f13px",
            "top": "10dp",
            "width": "90%",
            "zIndex": 1,
            "overrides": {
                "sgmOptions": {
                    "data": [{
                        "imgArrow": "calenderarrowright.png",
                        "imgRadio": "radio_inactive.png",
                        "lblHomeEmailHiddenVal": "si***n1@gmail.com",
                        "lblHomePhnHiddenVal": "si***n1@gmail.com",
                        "lblMobilePhnHiddenVal": "si***n1@gmail.com",
                        "lblOption": "si***n1@gmail.com",
                        "lblPrsnlEmailHiddenVal": "si***n1@gmail.com",
                        "lblWrkEmailHiddenVal": "si***n1@gmail.com",
                        "lblWrkPhnHiddenVal": "si***n1@gmail.com"
                    }, {
                        "imgArrow": "calenderarrowright.png",
                        "imgRadio": "radio_inactive.png",
                        "lblHomeEmailHiddenVal": "si***n1@gmail.com",
                        "lblHomePhnHiddenVal": "si***n1@gmail.com",
                        "lblMobilePhnHiddenVal": "si***n1@gmail.com",
                        "lblOption": "si***n1@gmail.com",
                        "lblPrsnlEmailHiddenVal": "si***n1@gmail.com",
                        "lblWrkEmailHiddenVal": "si***n1@gmail.com",
                        "lblWrkPhnHiddenVal": "si***n1@gmail.com"
                    }],
                    "rowTemplate": "flxDropdownOption"
                },
                "imgIcon": {
                    "src": "securitycode.png"
                },
                "richTxtOptionTitle": {
                    "text": "Text me a Security Code"
                },
                "dropdown": {
                    "left": "viz.val_cleared",
                    "right": "viz.val_cleared",
                    "bottom": "viz.val_cleared",
                    "height": "viz.val_cleared",
                    "minWidth": "viz.val_cleared",
                    "minHeight": "viz.val_cleared",
                    "maxWidth": "viz.val_cleared",
                    "maxHeight": "viz.val_cleared",
                    "centerY": "viz.val_cleared"
                }
            }
        }, controller.args[0], "textMessageCode"), extendConfig({
            "overrides": {}
        }, controller.args[1], "textMessageCode"), extendConfig({
            "overrides": {}
        }, controller.args[2], "textMessageCode"));
        textMessageCode.onClick = controller.AS_onClickCVV_be2d1001ab53454d9d5de3d2dd39d59d;
        textMessageCode.onRowClick = controller.AS_onRowClicktextMessage_a64f92f7d26640dda17266a3cfcbc6fe;
        var phoneCallCode = new common.dropdown(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "centerX": "50%",
            "clipBounds": true,
            "id": "phoneCallCode",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "masterType": constants.MASTER_TYPE_USERWIDGET,
            "isModalContainer": false,
            "skin": "sknflxffffffBorderf1f1f13px",
            "top": "10dp",
            "width": "90%",
            "zIndex": 1,
            "overrides": {
                "sgmOptions": {
                    "data": [{
                        "imgArrow": "calenderarrowright.png",
                        "imgRadio": "radio_inactive.png",
                        "lblHomeEmailHiddenVal": "si***n1@gmail.com",
                        "lblHomePhnHiddenVal": "si***n1@gmail.com",
                        "lblMobilePhnHiddenVal": "si***n1@gmail.com",
                        "lblOption": "si***n1@gmail.com",
                        "lblPrsnlEmailHiddenVal": "si***n1@gmail.com",
                        "lblWrkEmailHiddenVal": "si***n1@gmail.com",
                        "lblWrkPhnHiddenVal": "si***n1@gmail.com"
                    }, {
                        "imgArrow": "calenderarrowright.png",
                        "imgRadio": "radio_inactive.png",
                        "lblHomeEmailHiddenVal": "si***n1@gmail.com",
                        "lblHomePhnHiddenVal": "si***n1@gmail.com",
                        "lblMobilePhnHiddenVal": "si***n1@gmail.com",
                        "lblOption": "si***n1@gmail.com",
                        "lblPrsnlEmailHiddenVal": "si***n1@gmail.com",
                        "lblWrkEmailHiddenVal": "si***n1@gmail.com",
                        "lblWrkPhnHiddenVal": "si***n1@gmail.com"
                    }],
                    "rowTemplate": "flxDropdownOption"
                },
                "imgIcon": {
                    "src": "call_icon.png"
                },
                "richTxtOptionTitle": {
                    "text": "Call me with a Security Code"
                },
                "dropdown": {
                    "left": "viz.val_cleared",
                    "right": "viz.val_cleared",
                    "bottom": "viz.val_cleared",
                    "height": "viz.val_cleared",
                    "minWidth": "viz.val_cleared",
                    "minHeight": "viz.val_cleared",
                    "maxWidth": "viz.val_cleared",
                    "maxHeight": "viz.val_cleared",
                    "centerY": "viz.val_cleared"
                }
            }
        }, controller.args[0], "phoneCallCode"), extendConfig({
            "overrides": {}
        }, controller.args[1], "phoneCallCode"), extendConfig({
            "overrides": {}
        }, controller.args[2], "phoneCallCode"));
        phoneCallCode.onClick = controller.AS_onClickCVV_be2d1001ab53454d9d5de3d2dd39d59d;
        phoneCallCode.onRowClick = controller.AS_onRowClicktextMessage_a64f92f7d26640dda17266a3cfcbc6fe;
        phoneCallCode.onRowClick = controller.AS_onRowClickPhoneCall_e96d253620ff40128ba60014eb0acbf7;
        var emailCode = new common.dropdown(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "centerX": "50%",
            "clipBounds": true,
            "id": "emailCode",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "masterType": constants.MASTER_TYPE_USERWIDGET,
            "isModalContainer": false,
            "skin": "sknflxffffffBorderf1f1f13px",
            "top": "10dp",
            "width": "90%",
            "zIndex": 1,
            "overrides": {
                "sgmOptions": {
                    "data": [{
                        "imgArrow": "calenderarrowright.png",
                        "imgRadio": "radio_inactive.png",
                        "lblHomeEmailHiddenVal": "si***n1@gmail.com",
                        "lblHomePhnHiddenVal": "si***n1@gmail.com",
                        "lblMobilePhnHiddenVal": "si***n1@gmail.com",
                        "lblOption": "si***n1@gmail.com",
                        "lblPrsnlEmailHiddenVal": "si***n1@gmail.com",
                        "lblWrkEmailHiddenVal": "si***n1@gmail.com",
                        "lblWrkPhnHiddenVal": "si***n1@gmail.com"
                    }, {
                        "imgArrow": "calenderarrowright.png",
                        "imgRadio": "radio_inactive.png",
                        "lblHomeEmailHiddenVal": "si***n1@gmail.com",
                        "lblHomePhnHiddenVal": "si***n1@gmail.com",
                        "lblMobilePhnHiddenVal": "si***n1@gmail.com",
                        "lblOption": "si***n1@gmail.com",
                        "lblPrsnlEmailHiddenVal": "si***n1@gmail.com",
                        "lblWrkEmailHiddenVal": "si***n1@gmail.com",
                        "lblWrkPhnHiddenVal": "si***n1@gmail.com"
                    }],
                    "rowTemplate": "flxDropdownOption"
                },
                "imgIcon": {
                    "src": "message_icon.png"
                },
                "richTxtOptionTitle": {
                    "text": "Email Security Code"
                },
                "dropdown": {
                    "left": "viz.val_cleared",
                    "right": "viz.val_cleared",
                    "bottom": "viz.val_cleared",
                    "height": "viz.val_cleared",
                    "minWidth": "viz.val_cleared",
                    "minHeight": "viz.val_cleared",
                    "maxWidth": "viz.val_cleared",
                    "maxHeight": "viz.val_cleared",
                    "centerY": "viz.val_cleared"
                }
            }
        }, controller.args[0], "emailCode"), extendConfig({
            "overrides": {}
        }, controller.args[1], "emailCode"), extendConfig({
            "overrides": {}
        }, controller.args[2], "emailCode"));
        emailCode.onClick = controller.AS_onClickCVV_be2d1001ab53454d9d5de3d2dd39d59d;
        emailCode.onRowClick = controller.AS_onRowClicktextMessage_a64f92f7d26640dda17266a3cfcbc6fe;
        emailCode.onRowClick = controller.AS_onRowClickPhoneCall_e96d253620ff40128ba60014eb0acbf7;
        emailCode.onRowClick = controller.AS_onRowClickEmailCode_dfaa8706d2054d4ba9bf57636eda874a;
        SecurityCheckOptions.add(CVVCode, textMessageCode, phoneCallCode, emailCode);
        return SecurityCheckOptions;
    }
})