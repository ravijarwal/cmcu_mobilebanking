define(function() {
    return {
        "properties": [{
            "name": "customAccountNumtext",
            "enumerable": true,
            "configurable": false,
            "writable": true
        }, {
            "name": "ssntext",
            "enumerable": true,
            "configurable": false,
            "writable": true
        }, {
            "name": "dobtext",
            "enumerable": true,
            "configurable": false,
            "writable": true
        }, {
            "name": "imgsrc",
            "enumerable": true,
            "configurable": false,
            "writable": true
        }, {
            "name": "continueskin",
            "enumerable": true,
            "configurable": false,
            "writable": true
        }, {
            "name": "blockedUISkin",
            "enumerable": true,
            "configurable": false,
            "writable": true
        }, {
            "name": "acceptTerms",
            "enumerable": true,
            "configurable": false,
            "writable": true
        }],
        "apis": ["btnsetEnabled", "setCustomCallback", "setCustomNavigateTo", "updateInputBulletsSSN", "updateInputBulletsDOB"],
        "events": []
    }
});