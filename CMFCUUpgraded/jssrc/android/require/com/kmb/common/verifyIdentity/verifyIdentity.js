define(function() {
    return function(controller) {
        var verifyIdentity = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "110%",
            "id": "verifyIdentity",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "CopyslFbox0ffe7217f495c42",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "verifyIdentity"), extendConfig({}, controller.args[1], "verifyIdentity"), extendConfig({}, controller.args[2], "verifyIdentity"));
        verifyIdentity.setDefaultUnit(kony.flex.DP);
        var flxMainContainer = new kony.ui.FlexScrollContainer(extendConfig({
            "allowHorizontalBounce": false,
            "allowVerticalBounce": true,
            "bounces": true,
            "clipBounds": false,
            "enableScrolling": true,
            "height": "100%",
            "horizontalScrollIndicator": true,
            "id": "flxMainContainer",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "pagingEnabled": false,
            "scrollDirection": kony.flex.SCROLL_VERTICAL,
            "skin": "slFSbox",
            "top": "0dp",
            "verticalScrollIndicator": true,
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxMainContainer"), extendConfig({}, controller.args[1], "flxMainContainer"), extendConfig({}, controller.args[2], "flxMainContainer"));
        flxMainContainer.setDefaultUnit(kony.flex.DP);
        var flxVerify = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxVerify",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "CopyskinLblTitle0d38af5e460c644",
            "top": "20dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxVerify"), extendConfig({}, controller.args[1], "flxVerify"), extendConfig({}, controller.args[2], "flxVerify"));
        flxVerify.setDefaultUnit(kony.flex.DP);
        var lblVerify = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "id": "lblVerify",
            "isVisible": true,
            "text": "Verify",
            "textStyle": {
                "letterSpacing": 0,
                "strikeThrough": false
            },
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblVerify"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblVerify"), extendConfig({
            "textCopyable": false
        }, controller.args[2], "lblVerify"));
        flxVerify.add(lblVerify);
        var flxEnterAccountNumber = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "200dp",
            "id": "flxEnterAccountNumber",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "CopyslFbox0j73093318a674a",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxEnterAccountNumber"), extendConfig({}, controller.args[1], "flxEnterAccountNumber"), extendConfig({}, controller.args[2], "flxEnterAccountNumber"));
        flxEnterAccountNumber.setDefaultUnit(kony.flex.DP);
        var lblEnter = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "id": "lblEnter",
            "isVisible": true,
            "left": "0dp",
            "text": "Enter",
            "textStyle": {
                "letterSpacing": 0,
                "strikeThrough": false
            },
            "top": "20dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblEnter"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblEnter"), extendConfig({
            "textCopyable": false
        }, controller.args[2], "lblEnter"));
        var lblEnterAccountNumber = new kony.ui.Label(extendConfig({
            "centerX": "50.00%",
            "id": "lblEnterAccountNumber",
            "isVisible": true,
            "left": "0dp",
            "skin": "CopysknLbl0e91ae48fdf2845",
            "text": "Account Number, or CardNumber, or Checking Account Number",
            "textStyle": {
                "letterSpacing": 0,
                "strikeThrough": false
            },
            "top": "0dp",
            "width": "200dp",
            "zIndex": 1
        }, controller.args[0], "lblEnterAccountNumber"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblEnterAccountNumber"), extendConfig({
            "textCopyable": false
        }, controller.args[2], "lblEnterAccountNumber"));
        var txtEnterAccountNumber = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "centerX": "50.05%",
            "focusSkin": "sknTbx424242SSPRegular28px",
            "height": "40dp",
            "id": "txtEnterAccountNumber",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_NUMBER_PAD,
            "left": "0dp",
            "maxTextLength": 16,
            "right": "20dp",
            "secureTextEntry": false,
            "skin": "sknTbx424242SSPRegular28px",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
            "top": "20dp",
            "width": "90%",
            "zIndex": 1
        }, controller.args[0], "txtEnterAccountNumber"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtEnterAccountNumber"), extendConfig({
            "autoFilter": false,
            "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
            "placeholderSkin": "sknTbxA0A0A0SSPRegular28px",
            "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
        }, controller.args[2], "txtEnterAccountNumber"));
        flxEnterAccountNumber.add(lblEnter, lblEnterAccountNumber, txtEnterAccountNumber);
        var flxEnterSSN = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "180dp",
            "id": "flxEnterSSN",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxEnterSSN"), extendConfig({}, controller.args[1], "flxEnterSSN"), extendConfig({}, controller.args[2], "flxEnterSSN"));
        flxEnterSSN.setDefaultUnit(kony.flex.DP);
        var lblEnterSSN = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "id": "lblEnterSSN",
            "isVisible": true,
            "left": "0dp",
            "text": "Enter your Social Security Number (SSN)",
            "textStyle": {
                "letterSpacing": 0,
                "strikeThrough": false
            },
            "top": "20dp",
            "width": "90%",
            "zIndex": 1
        }, controller.args[0], "lblEnterSSN"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblEnterSSN"), extendConfig({
            "textCopyable": false
        }, controller.args[2], "lblEnterSSN"));
        var flxInputSSNContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "50%",
            "clipBounds": true,
            "height": "50dp",
            "id": "flxInputSSNContainer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "20dp",
            "width": "250dp",
            "zIndex": 1
        }, controller.args[0], "flxInputSSNContainer"), extendConfig({}, controller.args[1], "flxInputSSNContainer"), extendConfig({}, controller.args[2], "flxInputSSNContainer"));
        flxInputSSNContainer.setDefaultUnit(kony.flex.DP);
        var txtSSN = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "centerX": "50%",
            "centerY": "50%",
            "focusSkin": "CopydefTextBoxFocus0dfcfc8de3bb14b",
            "height": "43dp",
            "id": "txtSSN",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_NUMBER_PAD,
            "maxTextLength": 9,
            "secureTextEntry": false,
            "skin": "CopyslTextBox0g330a7078b644f",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
            "width": "235dp",
            "zIndex": 1
        }, controller.args[0], "txtSSN"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtSSN"), extendConfig({
            "autoFilter": false,
            "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
            "placeholderSkin": "CopydefTextBoxPlaceholder0hf01f38acc584a",
            "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
        }, controller.args[2], "txtSSN"));
        var flxInputSSN = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "50%",
            "centerY": "50%",
            "clipBounds": true,
            "height": "43dp",
            "id": "flxInputSSN",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "isModalContainer": false,
            "skin": "CopyslFbox0dbb631e00c4f44",
            "width": "235dp",
            "zIndex": 1
        }, controller.args[0], "flxInputSSN"), extendConfig({}, controller.args[1], "flxInputSSN"), extendConfig({}, controller.args[2], "flxInputSSN"));
        flxInputSSN.setDefaultUnit(kony.flex.DP);
        var lblSSNoDigit1 = new kony.ui.Label(extendConfig({
            "height": "100%",
            "id": "lblSSNoDigit1",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl484848SSPReg40px",
            "text": "_",
            "textStyle": {
                "letterSpacing": 0,
                "strikeThrough": false
            },
            "top": "0dp",
            "width": "20dp",
            "zIndex": 1
        }, controller.args[0], "lblSSNoDigit1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSSNoDigit1"), extendConfig({
            "textCopyable": false
        }, controller.args[2], "lblSSNoDigit1"));
        var lblSSNoDigit2 = new kony.ui.Label(extendConfig({
            "height": "100%",
            "id": "lblSSNoDigit2",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl484848SSPReg40px",
            "text": "_",
            "textStyle": {
                "letterSpacing": 0,
                "strikeThrough": false
            },
            "top": "0dp",
            "width": "20dp",
            "zIndex": 1
        }, controller.args[0], "lblSSNoDigit2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSSNoDigit2"), extendConfig({
            "textCopyable": false
        }, controller.args[2], "lblSSNoDigit2"));
        var lblSSNoDigit3 = new kony.ui.Label(extendConfig({
            "height": "100%",
            "id": "lblSSNoDigit3",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl484848SSPReg40px",
            "text": "_",
            "textStyle": {
                "letterSpacing": 0,
                "strikeThrough": false
            },
            "top": "0dp",
            "width": "20dp",
            "zIndex": 1
        }, controller.args[0], "lblSSNoDigit3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSSNoDigit3"), extendConfig({
            "textCopyable": false
        }, controller.args[2], "lblSSNoDigit3"));
        var lblSSNoHyphen1 = new kony.ui.Label(extendConfig({
            "height": "100%",
            "id": "lblSSNoHyphen1",
            "isVisible": true,
            "left": "3dp",
            "skin": "sknLbl484848SSPReg40px",
            "text": "-",
            "textStyle": {
                "letterSpacing": 0,
                "strikeThrough": false
            },
            "top": "0dp",
            "width": "20dp",
            "zIndex": 1
        }, controller.args[0], "lblSSNoHyphen1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSSNoHyphen1"), extendConfig({
            "textCopyable": false
        }, controller.args[2], "lblSSNoHyphen1"));
        var lblSSNoDigit4 = new kony.ui.Label(extendConfig({
            "height": "100%",
            "id": "lblSSNoDigit4",
            "isVisible": true,
            "left": "3dp",
            "skin": "sknLbl484848SSPReg40px",
            "text": "_",
            "textStyle": {
                "letterSpacing": 0,
                "strikeThrough": false
            },
            "top": "0dp",
            "width": "20dp",
            "zIndex": 1
        }, controller.args[0], "lblSSNoDigit4"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSSNoDigit4"), extendConfig({
            "textCopyable": false
        }, controller.args[2], "lblSSNoDigit4"));
        var lblSSNoDigit5 = new kony.ui.Label(extendConfig({
            "height": "100%",
            "id": "lblSSNoDigit5",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl484848SSPReg40px",
            "text": "_",
            "textStyle": {
                "letterSpacing": 0,
                "strikeThrough": false
            },
            "top": "0dp",
            "width": "20dp",
            "zIndex": 1
        }, controller.args[0], "lblSSNoDigit5"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSSNoDigit5"), extendConfig({
            "textCopyable": false
        }, controller.args[2], "lblSSNoDigit5"));
        var lblSSNoHyphen2 = new kony.ui.Label(extendConfig({
            "height": "100%",
            "id": "lblSSNoHyphen2",
            "isVisible": true,
            "left": "3dp",
            "skin": "sknLbl484848SSPReg40px",
            "text": "-",
            "textStyle": {
                "letterSpacing": 0,
                "strikeThrough": false
            },
            "top": "0dp",
            "width": "20dp",
            "zIndex": 1
        }, controller.args[0], "lblSSNoHyphen2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSSNoHyphen2"), extendConfig({
            "textCopyable": false
        }, controller.args[2], "lblSSNoHyphen2"));
        var lblSSNoDigit6 = new kony.ui.Label(extendConfig({
            "height": "100%",
            "id": "lblSSNoDigit6",
            "isVisible": true,
            "left": "3dp",
            "skin": "sknLbl484848SSPReg40px",
            "text": "_",
            "textStyle": {
                "letterSpacing": 0,
                "strikeThrough": false
            },
            "top": "0dp",
            "width": "20dp",
            "zIndex": 1
        }, controller.args[0], "lblSSNoDigit6"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSSNoDigit6"), extendConfig({
            "textCopyable": false
        }, controller.args[2], "lblSSNoDigit6"));
        var lblSSNoDigit7 = new kony.ui.Label(extendConfig({
            "height": "100%",
            "id": "lblSSNoDigit7",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl484848SSPReg40px",
            "text": "_",
            "textStyle": {
                "letterSpacing": 0,
                "strikeThrough": false
            },
            "top": "0dp",
            "width": "20dp",
            "zIndex": 1
        }, controller.args[0], "lblSSNoDigit7"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSSNoDigit7"), extendConfig({
            "textCopyable": false
        }, controller.args[2], "lblSSNoDigit7"));
        var lblSSNoDigit8 = new kony.ui.Label(extendConfig({
            "height": "100%",
            "id": "lblSSNoDigit8",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl484848SSPReg40px",
            "text": "_",
            "textStyle": {
                "letterSpacing": 0,
                "strikeThrough": false
            },
            "top": "0dp",
            "width": "20dp",
            "zIndex": 1
        }, controller.args[0], "lblSSNoDigit8"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSSNoDigit8"), extendConfig({
            "textCopyable": false
        }, controller.args[2], "lblSSNoDigit8"));
        var lblSSNoDigit9 = new kony.ui.Label(extendConfig({
            "height": "100%",
            "id": "lblSSNoDigit9",
            "isVisible": true,
            "left": "3dp",
            "skin": "sknLbl484848SSPReg40px",
            "text": "_",
            "textStyle": {
                "letterSpacing": 0,
                "strikeThrough": false
            },
            "top": "0dp",
            "width": "6%",
            "zIndex": 1
        }, controller.args[0], "lblSSNoDigit9"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSSNoDigit9"), extendConfig({
            "textCopyable": false
        }, controller.args[2], "lblSSNoDigit9"));
        flxInputSSN.add(lblSSNoDigit1, lblSSNoDigit2, lblSSNoDigit3, lblSSNoHyphen1, lblSSNoDigit4, lblSSNoDigit5, lblSSNoHyphen2, lblSSNoDigit6, lblSSNoDigit7, lblSSNoDigit8, lblSSNoDigit9);
        flxInputSSNContainer.add(txtSSN, flxInputSSN);
        flxEnterSSN.add(lblEnterSSN, flxInputSSNContainer);
        var flxEnterDOB = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "150dp",
            "id": "flxEnterDOB",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxEnterDOB"), extendConfig({}, controller.args[1], "flxEnterDOB"), extendConfig({}, controller.args[2], "flxEnterDOB"));
        flxEnterDOB.setDefaultUnit(kony.flex.DP);
        var lblEnterDOB = new kony.ui.Label(extendConfig({
            "centerX": "50.00%",
            "id": "lblEnterDOB",
            "isVisible": true,
            "left": "0dp",
            "text": "Enter your Date of Birth",
            "textStyle": {
                "letterSpacing": 0,
                "strikeThrough": false
            },
            "top": "20dp",
            "width": "90%",
            "zIndex": 1
        }, controller.args[0], "lblEnterDOB"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblEnterDOB"), extendConfig({
            "textCopyable": false
        }, controller.args[2], "lblEnterDOB"));
        var flxInputDOBContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "50%",
            "clipBounds": true,
            "height": "40dp",
            "id": "flxInputDOBContainer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "60dp",
            "width": "260dp",
            "zIndex": 1
        }, controller.args[0], "flxInputDOBContainer"), extendConfig({}, controller.args[1], "flxInputDOBContainer"), extendConfig({}, controller.args[2], "flxInputDOBContainer"));
        flxInputDOBContainer.setDefaultUnit(kony.flex.DP);
        var txtDOB = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "centerX": "50%",
            "centerY": "50%",
            "focusSkin": "CopydefTextBoxFocus0dfcfc8de3bb14b",
            "height": "43dp",
            "id": "txtDOB",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_NUMBER_PAD,
            "maxTextLength": 8,
            "secureTextEntry": false,
            "skin": "CopyslTextBox0g330a7078b644f",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
            "width": "235dp",
            "zIndex": 1
        }, controller.args[0], "txtDOB"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtDOB"), extendConfig({
            "autoFilter": false,
            "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
            "placeholderSkin": "CopydefTextBoxPlaceholder0hf01f38acc584a",
            "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
        }, controller.args[2], "txtDOB"));
        var flxInputDOB = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "50.00%",
            "clipBounds": true,
            "height": "40dp",
            "id": "flxInputDOB",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "isModalContainer": false,
            "skin": "CopyslFbox0d4a39bd212cc45",
            "top": "0dp",
            "width": "260dp",
            "zIndex": 1
        }, controller.args[0], "flxInputDOB"), extendConfig({}, controller.args[1], "flxInputDOB"), extendConfig({}, controller.args[2], "flxInputDOB"));
        flxInputDOB.setDefaultUnit(kony.flex.DP);
        var lblMonthOne = new kony.ui.Label(extendConfig({
            "height": "100%",
            "id": "lblMonthOne",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknLble3e3e3SSP60px",
            "text": "M",
            "textStyle": {
                "letterSpacing": 0,
                "strikeThrough": false
            },
            "top": "0dp",
            "width": "30dp",
            "zIndex": 1
        }, controller.args[0], "lblMonthOne"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblMonthOne"), extendConfig({
            "textCopyable": false
        }, controller.args[2], "lblMonthOne"));
        var lblMonthTwo = new kony.ui.Label(extendConfig({
            "height": "100%",
            "id": "lblMonthTwo",
            "isVisible": true,
            "left": "2dp",
            "skin": "sknLble3e3e3SSP60px",
            "text": "M",
            "textStyle": {
                "letterSpacing": 0,
                "strikeThrough": false
            },
            "top": "0dp",
            "width": "30dp",
            "zIndex": 1
        }, controller.args[0], "lblMonthTwo"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblMonthTwo"), extendConfig({
            "textCopyable": false
        }, controller.args[2], "lblMonthTwo"));
        var lblSlashMonth = new kony.ui.Label(extendConfig({
            "height": "100%",
            "id": "lblSlashMonth",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknLble3e3e3SSP60px",
            "text": "/",
            "textStyle": {
                "letterSpacing": 0,
                "strikeThrough": false
            },
            "top": "0dp",
            "width": "13dp",
            "zIndex": 1
        }, controller.args[0], "lblSlashMonth"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSlashMonth"), extendConfig({
            "textCopyable": false
        }, controller.args[2], "lblSlashMonth"));
        var lblDayOne = new kony.ui.Label(extendConfig({
            "height": "100%",
            "id": "lblDayOne",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLble3e3e3SSP60px",
            "text": "D",
            "textStyle": {
                "letterSpacing": 0,
                "strikeThrough": false
            },
            "top": "0dp",
            "width": "25dp",
            "zIndex": 1
        }, controller.args[0], "lblDayOne"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDayOne"), extendConfig({
            "textCopyable": false
        }, controller.args[2], "lblDayOne"));
        var lblDayTwo = new kony.ui.Label(extendConfig({
            "height": "100%",
            "id": "lblDayTwo",
            "isVisible": true,
            "left": "2dp",
            "skin": "sknLble3e3e3SSP60px",
            "text": "D",
            "textStyle": {
                "letterSpacing": 0,
                "strikeThrough": false
            },
            "top": "0dp",
            "width": "25dp",
            "zIndex": 1
        }, controller.args[0], "lblDayTwo"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDayTwo"), extendConfig({
            "textCopyable": false
        }, controller.args[2], "lblDayTwo"));
        var lblSlashDay = new kony.ui.Label(extendConfig({
            "height": "100%",
            "id": "lblSlashDay",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknLble3e3e3SSP60px",
            "text": "/",
            "textStyle": {
                "letterSpacing": 0,
                "strikeThrough": false
            },
            "top": "0dp",
            "width": "13dp",
            "zIndex": 1
        }, controller.args[0], "lblSlashDay"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSlashDay"), extendConfig({
            "textCopyable": false
        }, controller.args[2], "lblSlashDay"));
        var lblYearOne = new kony.ui.Label(extendConfig({
            "height": "100%",
            "id": "lblYearOne",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknLble3e3e3SSP60px",
            "text": "Y",
            "textStyle": {
                "letterSpacing": 0,
                "strikeThrough": false
            },
            "top": "0dp",
            "width": "23dp",
            "zIndex": 1
        }, controller.args[0], "lblYearOne"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblYearOne"), extendConfig({
            "textCopyable": false
        }, controller.args[2], "lblYearOne"));
        var lblYearTwo = new kony.ui.Label(extendConfig({
            "height": "100%",
            "id": "lblYearTwo",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLble3e3e3SSP60px",
            "text": "Y",
            "textStyle": {
                "letterSpacing": 0,
                "strikeThrough": false
            },
            "top": "0dp",
            "width": "23dp",
            "zIndex": 1
        }, controller.args[0], "lblYearTwo"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblYearTwo"), extendConfig({
            "textCopyable": false
        }, controller.args[2], "lblYearTwo"));
        var lblYearThree = new kony.ui.Label(extendConfig({
            "height": "100%",
            "id": "lblYearThree",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLble3e3e3SSP60px",
            "text": "Y",
            "textStyle": {
                "letterSpacing": 0,
                "strikeThrough": false
            },
            "top": "0dp",
            "width": "23dp",
            "zIndex": 1
        }, controller.args[0], "lblYearThree"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblYearThree"), extendConfig({
            "textCopyable": false
        }, controller.args[2], "lblYearThree"));
        var lblYearFour = new kony.ui.Label(extendConfig({
            "height": "100%",
            "id": "lblYearFour",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLble3e3e3SSP60px",
            "text": "Y",
            "textStyle": {
                "letterSpacing": 0,
                "strikeThrough": false
            },
            "top": "0dp",
            "width": "23dp",
            "zIndex": 1
        }, controller.args[0], "lblYearFour"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblYearFour"), extendConfig({
            "textCopyable": false
        }, controller.args[2], "lblYearFour"));
        flxInputDOB.add(lblMonthOne, lblMonthTwo, lblSlashMonth, lblDayOne, lblDayTwo, lblSlashDay, lblYearOne, lblYearTwo, lblYearThree, lblYearFour);
        flxInputDOBContainer.add(txtDOB, flxInputDOB);
        flxEnterDOB.add(lblEnterDOB, flxInputDOBContainer);
        var flxContactUs = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "92dp",
            "id": "flxContactUs",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxContactUs"), extendConfig({}, controller.args[1], "flxContactUs"), extendConfig({}, controller.args[2], "flxContactUs"));
        flxContactUs.setDefaultUnit(kony.flex.DP);
        var lblNotHave = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "id": "lblNotHave",
            "isVisible": true,
            "left": "0dp",
            "skin": "CopyslLabel0a970c36f66e543",
            "text": "Don't have any of the above?",
            "textStyle": {
                "letterSpacing": 0,
                "strikeThrough": false
            },
            "top": "20dp",
            "width": "90%",
            "zIndex": 1
        }, controller.args[0], "lblNotHave"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblNotHave"), extendConfig({
            "textCopyable": false
        }, controller.args[2], "lblNotHave"));
        var flxContacts = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50dp",
            "id": "flxContacts",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxContacts"), extendConfig({}, controller.args[1], "flxContacts"), extendConfig({}, controller.args[2], "flxContacts"));
        flxContacts.setDefaultUnit(kony.flex.DP);
        var richtxtCall = new kony.ui.RichText(extendConfig({
            "height": "100%",
            "id": "richtxtCall",
            "isVisible": true,
            "left": "5%",
            "linkSkin": "skinRichTextLInk",
            "skin": "skinRichTextLInk",
            "text": "<a>Give us a call </a>",
            "top": "0dp",
            "width": "25%",
            "zIndex": 1
        }, controller.args[0], "richtxtCall"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "richtxtCall"), extendConfig({}, controller.args[2], "richtxtCall"));
        var flxDivide1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "60%",
            "id": "flxDivide1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "2%",
            "zIndex": 1
        }, controller.args[0], "flxDivide1"), extendConfig({}, controller.args[1], "flxDivide1"), extendConfig({}, controller.args[2], "flxDivide1"));
        flxDivide1.setDefaultUnit(kony.flex.DP);
        var flxDivideLine1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "50%",
            "clipBounds": true,
            "height": "100%",
            "id": "flxDivideLine1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "CopyslFbox0e3ddc5bea59244",
            "top": "0dp",
            "width": "1px",
            "zIndex": 1
        }, controller.args[0], "flxDivideLine1"), extendConfig({}, controller.args[1], "flxDivideLine1"), extendConfig({}, controller.args[2], "flxDivideLine1"));
        flxDivideLine1.setDefaultUnit(kony.flex.DP);
        flxDivideLine1.add();
        flxDivide1.add(flxDivideLine1);
        var richtxtSendEmail = new kony.ui.RichText(extendConfig({
            "height": "100%",
            "id": "richtxtSendEmail",
            "isVisible": true,
            "left": "0dp",
            "linkSkin": "skinRichTextLInk",
            "skin": "skinRichTextLInk",
            "text": "<a>Send us an email</a>",
            "top": "0dp",
            "width": "30%",
            "zIndex": 1
        }, controller.args[0], "richtxtSendEmail"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "richtxtSendEmail"), extendConfig({}, controller.args[2], "richtxtSendEmail"));
        var flxDivide2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "60%",
            "id": "flxDivide2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "2%",
            "zIndex": 1
        }, controller.args[0], "flxDivide2"), extendConfig({}, controller.args[1], "flxDivide2"), extendConfig({}, controller.args[2], "flxDivide2"));
        flxDivide2.setDefaultUnit(kony.flex.DP);
        var flxDivideLine2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "50%",
            "clipBounds": true,
            "height": "100%",
            "id": "flxDivideLine2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "CopyslFbox0e3ddc5bea59244",
            "top": "0dp",
            "width": "1px",
            "zIndex": 1
        }, controller.args[0], "flxDivideLine2"), extendConfig({}, controller.args[1], "flxDivideLine2"), extendConfig({}, controller.args[2], "flxDivideLine2"));
        flxDivideLine2.setDefaultUnit(kony.flex.DP);
        flxDivideLine2.add();
        flxDivide2.add(flxDivideLine2);
        var richtxtBranch = new kony.ui.RichText(extendConfig({
            "height": "100%",
            "id": "richtxtBranch",
            "isVisible": true,
            "left": "0dp",
            "linkSkin": "skinRichTextLInk",
            "right": "3%",
            "skin": "skinRichTextLInk",
            "text": "<a>Stop by a branch</a>",
            "top": "0dp",
            "width": "30%",
            "zIndex": 1
        }, controller.args[0], "richtxtBranch"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "richtxtBranch"), extendConfig({}, controller.args[2], "richtxtBranch"));
        flxContacts.add(richtxtCall, flxDivide1, richtxtSendEmail, flxDivide2, richtxtBranch);
        flxContactUs.add(lblNotHave, flxContacts);
        var flxTermsOfServices = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "95dp",
            "id": "flxTermsOfServices",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxTermsOfServices"), extendConfig({}, controller.args[1], "flxTermsOfServices"), extendConfig({}, controller.args[2], "flxTermsOfServices"));
        flxTermsOfServices.setDefaultUnit(kony.flex.DP);
        var flxAcceptTerms = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxAcceptTerms",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "25dp",
            "zIndex": 1
        }, controller.args[0], "flxAcceptTerms"), extendConfig({}, controller.args[1], "flxAcceptTerms"), extendConfig({}, controller.args[2], "flxAcceptTerms"));
        flxAcceptTerms.setDefaultUnit(kony.flex.DP);
        var flxCheck = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "25dp",
            "id": "flxCheck",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "right": 10,
            "skin": "slFbox",
            "top": "20dp",
            "width": "25dp",
            "zIndex": 1
        }, controller.args[0], "flxCheck"), extendConfig({}, controller.args[1], "flxCheck"), extendConfig({}, controller.args[2], "flxCheck"));
        flxCheck.setDefaultUnit(kony.flex.DP);
        var imgCheck = new kony.ui.Image2(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "20dp",
            "id": "imgCheck",
            "isVisible": true,
            "right": "20dp",
            "skin": "slImage",
            "src": "checkbox_empty.png",
            "top": 20,
            "width": "20dp",
            "zIndex": 1
        }, controller.args[0], "imgCheck"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgCheck"), extendConfig({}, controller.args[2], "imgCheck"));
        flxCheck.add(imgCheck);
        flxAcceptTerms.add(flxCheck);
        var flxTemsOfConditionText = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxTemsOfConditionText",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "300dp",
            "zIndex": 1
        }, controller.args[0], "flxTemsOfConditionText"), extendConfig({}, controller.args[1], "flxTemsOfConditionText"), extendConfig({}, controller.args[2], "flxTemsOfConditionText"));
        flxTemsOfConditionText.setDefaultUnit(kony.flex.DP);
        var richtxtTems = new kony.ui.RichText(extendConfig({
            "height": "100%",
            "id": "richtxtTems",
            "isVisible": true,
            "left": "0dp",
            "linkSkin": "CopyskinRichTextLInk0e56e22a2cc8f4e",
            "skin": "CopyslRichText0fa340886ba3441",
            "text": "By checking this box I agree to the following <br> terms of service <a>Electronic  Fund Transfers <br> Agreement and  Disclosure</a> <a>Consent for <br> Electronic Disclosure </a>",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "richtxtTems"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "richtxtTems"), extendConfig({}, controller.args[2], "richtxtTems"));
        flxTemsOfConditionText.add(richtxtTems);
        flxTermsOfServices.add(flxAcceptTerms, flxTemsOfConditionText);
        var flxBtnContinue = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxBtnContinue",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "2dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxBtnContinue"), extendConfig({}, controller.args[1], "flxBtnContinue"), extendConfig({}, controller.args[2], "flxBtnContinue"));
        flxBtnContinue.setDefaultUnit(kony.flex.DP);
        var btnContinue = new kony.ui.Button(extendConfig({
            "bottom": 0,
            "centerX": "50%",
            "focusSkin": "skinButtonDisabled",
            "height": "40dp",
            "id": "btnContinue",
            "isVisible": true,
            "left": "0dp",
            "skin": "skinButtonDisabled",
            "text": kony.i18n.getLocalizedString("kony.mb.common.continue"),
            "top": "0dp",
            "width": "80%",
            "zIndex": 1
        }, controller.args[0], "btnContinue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnContinue"), extendConfig({}, controller.args[2], "btnContinue"));
        flxBtnContinue.add(btnContinue);
        flxMainContainer.add(flxVerify, flxEnterAccountNumber, flxEnterSSN, flxEnterDOB, flxContactUs, flxTermsOfServices, flxBtnContinue);
        verifyIdentity.add(flxMainContainer);
        return verifyIdentity;
    }
})