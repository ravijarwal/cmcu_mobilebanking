define(function() {
    return function(controller) {
        var areachart = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "100%",
            "id": "areachart",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "CopyCopyCopyslFbox0f92765b801484b",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "areachart"), extendConfig({}, controller.args[1], "areachart"), extendConfig({}, controller.args[2], "areachart"));
        areachart.setDefaultUnit(kony.flex.DP);
        var areaChartBrowser = new kony.ui.Browser(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "detectTelNumber": true,
            "enableZoom": false,
            "height": "100%",
            "id": "areaChartBrowser",
            "isVisible": true,
            "requestURLConfig": {
                "URL": "",
                "requestMethod": constants.BROWSER_REQUEST_METHOD_GET
            },
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "areaChartBrowser"), extendConfig({}, controller.args[1], "areaChartBrowser"), extendConfig({}, controller.args[2], "areaChartBrowser"));
        areachart.add(areaChartBrowser);
        return areachart;
    }
})