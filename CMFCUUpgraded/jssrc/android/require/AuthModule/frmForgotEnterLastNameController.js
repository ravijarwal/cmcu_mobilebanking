define("AuthModule/userfrmForgotEnterLastNameController", {
    timerCounter: 0,
    init: function() {
        var navManager = applicationManager.getNavigationManager();
        var currentForm = navManager.getCurrentForm();
        applicationManager.getPresentationFormUtility().initCommonActions(this, "YES", currentForm);
    },
    preShow: function() {
        this.view.txtFirstName.setFocus(true);
        this.view.txtLastName.setFocus(false);
        this.view.flxPopup.setVisibility(false);
        this.initActions();
        this.renderTitleBar();
        this.handleData();
        applicationManager.getPresentationUtility().dismissLoadingScreen();
        var navManager = applicationManager.getNavigationManager();
        var currentForm = navManager.getCurrentForm();
        applicationManager.getPresentationFormUtility().logFormName(currentForm);
    },
    renderTitleBar: function() {
        var deviceUtilManager = applicationManager.getDeviceUtilManager();
        var isIphone = deviceUtilManager.isIPhone();
        /*if(!isIphone){
          this.view.flxHeader.isVisible = true;
        }
        else{
          this.view.flxHeader.isVisible = false;
        }*/
        this.view.flxHeader.isVisible = true;
    },
    handleData: function() {
        var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
        var forgotObj = authModule.presentationController.getForgotObjectForView();
        this.view.customHeader.btnRight.text = kony.i18n.getLocalizedString("kony.mb.common.CancelSmall");
        this.view.customHeader.btnRight.skin = "sknRtx424242SSP26px";
        this.view.customHeader.lblLocateUs.skin = "sknRtx424242SSP26px";
        this.view.customHeader.lblLocateUs.text = kony.i18n.getLocalizedString("kony.mb.cantLogin.cantLogin");
        this.view.txtLastName.text = "";
        this.view.txtFirstName.text = "";
        this.view.btnUpdatePassword.skin = "sknBtnOnBoardingInactive";
        this.view.btnUpdatePassword.setEnabled(false);
    },
    initActions: function() {
        this.view.btnUpdatePassword.onClick = this.validateUserName;
        this.view.customHeader.flxBack.onClick = this.goBack;
        this.view.customHeader.btnRight.onClick = this.onCancel;
        this.view.txtFirstName.onTextChange = this.onFirstNameChange;
        this.view.txtLastName.onTextChange = this.onLastNameChange;
    },
    onFirstNameChange: function() {
        if (this.view.txtLastName.text !== null || this.view.txtLastName.text !== "") {
            this.changeSkinbtnUpdatePassword();
        }
    },
    onLastNameChange: function() {
        if (this.view.txtFirstName.text !== null || this.view.txtFirstName.text !== "") {
            this.changeSkinbtnUpdatePassword();
        }
    },
    changeSkinbtnUpdatePassword: function() {
        if (this.isValidInputs()) {
            this.view.btnUpdatePassword.skin = "sknBtn0095e426pxEnabled";
            this.view.btnUpdatePassword.setEnabled(true);
        } else {
            this.view.btnUpdatePassword.skin = "skinButtonDisabled";
            this.view.btnUpdatePassword.focusSkin = "skinButtonDisabled";
            this.view.btnUpdatePassword.setEnabled(false);
        }
    },
    isValidInputs: function() {
        return this.view.txtFirstName.text.length > 0 && this.view.txtLastName.text.length > 0;
    },
    validateUserName: function() {
        var lastName = this.view.txtLastName.text;
        var firstName = this.view.txtFirstName.text;
        if (lastName === '' && firstName === '' || lastName === null && firstName === null || lastName === undefined && firstName === undefined) {
            this.bindViewError(applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.common.invalidLastName"));
        } else {
            var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
            authModule.presentationController.naviagteToDOB(firstName, lastName);
        }
    },
    bindViewError: function(msg) {
        applicationManager.getPresentationUtility().dismissLoadingScreen();
        applicationManager.getDataProcessorUtility().showToastMessageError(this, msg);
    },
    goBack: function() {
        var navManager = applicationManager.getNavigationManager();
        navManager.goBack();
    },
    onCancel: function() {
        var navManager = applicationManager.getNavigationManager();
        navManager.clearStack();
        var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
        authModule.presentationController.navigateToLogin();
    }
});
define("AuthModule/frmForgotEnterLastNameControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for flxBack **/
    AS_FlexContainer_b36153b67ba942a2aae35f1f9ef4d000: function AS_FlexContainer_b36153b67ba942a2aae35f1f9ef4d000(eventobject) {
        var self = this;
        var authMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
        authMod.presentationController.commonFunctionForNavigation("frmLogin");
    },
    /** onClick defined for btnRight **/
    AS_Button_bca535d933294115b6f52552a731b32a: function AS_Button_bca535d933294115b6f52552a731b32a(eventobject) {
        var self = this;
        var authMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
        authMod.presentationController.commonFunctionForNavigation("frmLogin");
    },
    /** onClick defined for undefined **/
    AS_BarButtonItem_b5bafb63d13a4543937016771bf5f0af: function AS_BarButtonItem_b5bafb63d13a4543937016771bf5f0af(eventobject) {
        var self = this;
        this.goBack();
    },
    /** onClick defined for undefined **/
    AS_BarButtonItem_bb01710ab2274df68dff399556ff4159: function AS_BarButtonItem_bb01710ab2274df68dff399556ff4159(eventobject) {
        var self = this;
        this.onCancel();
    },
    /** init defined for frmForgotEnterLastName **/
    AS_Form_da8197074f2e43f1a1d0e20dfde5dd69: function AS_Form_da8197074f2e43f1a1d0e20dfde5dd69(eventobject) {
        var self = this;
        this.init();
    },
    /** preShow defined for frmForgotEnterLastName **/
    AS_Form_ca1ba59b895d46689d370aeec4845dd6: function AS_Form_ca1ba59b895d46689d370aeec4845dd6(eventobject) {
        var self = this;
        this.preShow();
    }
});
define("AuthModule/frmForgotEnterLastNameController", ["AuthModule/userfrmForgotEnterLastNameController", "AuthModule/frmForgotEnterLastNameControllerActions"], function() {
    var controller = require("AuthModule/userfrmForgotEnterLastNameController");
    var controllerActions = ["AuthModule/frmForgotEnterLastNameControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
