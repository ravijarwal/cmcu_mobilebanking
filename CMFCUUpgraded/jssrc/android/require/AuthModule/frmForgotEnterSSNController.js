define("AuthModule/userfrmForgotEnterSSNController", {
    keypadString: '',
    timerCounter: 0,
    init: function() {
        var navManager = applicationManager.getNavigationManager();
        var currentForm = navManager.getCurrentForm();
        navManager.setCustomInfo("isValidUser", true);
        applicationManager.getPresentationFormUtility().initCommonActions(this, "YES", currentForm);
    },
    initActions: function() {
        this.view.btnVerify.onClick = this.validateSSNForgotPwd;
        this.view.customHeader.flxBack.onClick = this.goBack;
        this.view.customHeader.btnRight.onClick = this.onCancel;
        this.setKeyPadActions();
        this.view.flxView.onClick = this.imgSSNViewOnClick;
        this.view.flxCancel.onClick = this.onClickClosePopup;
        this.view.flxOk.onClick = this.onClickClosePopup;
        this.view.flxCallUs.onClick = function() {
            kony.phone.dial("7043750183");
        };
        this.view.flxEmailUs.onClick = function() {
            var cc = [""];
            var bcc = [""];
            var sub = " ";
            var msgbody = "";
            var to = ["info@cmcu.org"];
            kony.phone.openEmail(to, cc, bcc, sub, msgbody);
        };
        this.view.flxVisitBranch.onClick = function() {
            var loginCntrlr = applicationManager.getPresentationUtility().getController('frmLogin', true);
            loginCntrlr.onLocateUSClick();
        };
    },
    preShow: function() {
        this.keypadString = "";
        this.view.lblSSN.text = "";
        this.view.flxPopup.setVisibility(false);
        this.view.flxCustomPopup.setVisibility(false);
        var navManager = applicationManager.getNavigationManager();
        var isValidUser = navManager.getCustomInfo("isValidUser");
        if (!isValidUser) {
            navManager.setCustomInfo("isValidUser", true);
            this.showErrorPopup();
        }
        this.initActions();
        this.renderTitleBar();
        this.handleData();
        applicationManager.getPresentationUtility().dismissLoadingScreen();
        var currentForm = navManager.getCurrentForm();
        applicationManager.getPresentationFormUtility().logFormName(currentForm);
    },
    renderTitleBar: function() {
        var deviceUtilManager = applicationManager.getDeviceUtilManager();
        var isIphone = deviceUtilManager.isIPhone();
        /*if(!isIphone){
          this.view.flxHeader.isVisible = true;
        }
        else{
          this.view.flxHeader.isVisible = false;
        }*/
        this.view.flxHeader.isVisible = true;
    },
    handleData: function() {
        this.view.imgSSNView.src = "viewicon.png";
        this.view.customHeader.btnRight.text = kony.i18n.getLocalizedString("kony.mb.common.CancelSmall");
        this.view.customHeader.lblLocateUs.text = kony.i18n.getLocalizedString("kony.mb.cantLogin.cantLogin");
        this.view.customHeader.btnRight.skin = "sknRtx424242SSP26px";
        this.view.customHeader.btnRight.focusSkin = "sknRtx424242SSP26px";
        this.view.customHeader.lblLocateUs.skin = "sknRtx424242SSP26px";
        this.keypadString = "";
        this.updateInputBullets();
    },
    showErrorPopup: function() {
        this.view.flxCustomPopup.setVisibility(true);
    },
    onClickClosePopup: function() {
        var enrollMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("EnrollModule");
        enrollMod.presentationController.resetEnrollObj();
    },
    imgSSNViewOnClick: function() {
        var navManager = applicationManager.getNavigationManager();
        var sSNtext = navManager.getCustomInfo("SSNtext");
        if (sSNtext !== null && typeof sSNtext !== "undefined" && sSNtext.length === 9) {
            if (this.view.imgSSNView.src === "viewicon.png") {
                this.view.imgSSNView.src = "viewactive.png";
                this.view.lblSSNOne.text = sSNtext.charAt(0);
                this.view.lblSSNTwo.text = sSNtext.charAt(1);
                this.view.lblSSNThree.text = sSNtext.charAt(2);
                this.view.lblSSNFour.text = sSNtext.charAt(3);
                this.view.lblSSNFive.text = sSNtext.charAt(4);
                this.view.lblSSNSix.text = sSNtext.charAt(5);
                this.view.lblSSNSeven.text = sSNtext.charAt(6);
                this.view.lblSSNEight.text = sSNtext.charAt(7);
                this.view.lblSSNNine.text = sSNtext.charAt(8);
                this.view.flxInputSSNDummy.forceLayout();
            } else {
                this.view.imgSSNView.src = "viewicon.png";
                this.view.lblSSNOne.text = "•";
                this.view.lblSSNTwo.text = "•";
                this.view.lblSSNThree.text = "•";
                this.view.lblSSNFour.text = "•";
                this.view.lblSSNFive.text = "•";
                this.view.lblSSNSix.text = "•";
                this.view.lblSSNSeven.text = "•";
                this.view.lblSSNEight.text = "•";
                this.view.lblSSNNine.text = "•";
                this.view.flxInputSSNDummy.forceLayout();
            }
        } else {
            this.view.imgSSNView.src = "viewicon.png";
            this.view.flxInputSSNDummy.forceLayout();
        }
    },
    getKeypadString: function() {
        var ssn = this.keypadString;
        ssn = ssn.replace(/-/g, "");
        return ssn;
    },
    //KEYPAD OPS:
    updateInputBullets: function() {
        var dummyString = '(___-__-____)';
        if (this.keypadString.length === 0) {
            this.keypadString = this.keypadString + '(';
        } else if (this.keypadString.length === 12) {
            this.keypadString = this.keypadString + ')';
        } else if (this.keypadString.length === 4 || this.keypadString.length === 7) {
            this.keypadString = this.keypadString + '-';
        }
        var widgets = this.view["flxInputSSNDummy"].widgets();
        for (var i = 0; i < this.keypadString.length; i++) {
            // widgets[i].skin = "sknLbl979797SSP60px";
            if (this.keypadString[i] === '-') {
                widgets[i].text = this.keypadString[i];
            } else if (this.keypadString[i] === '(') {
                widgets[i].text = this.keypadString[i];
            } else if (this.keypadString[i] === ')') {
                widgets[i].text = this.keypadString[i];
            } else {
                widgets[i].text = "•";
            }
        }
        for (var i = this.keypadString.length; i < widgets.length; i++) {
            widgets[i].text = dummyString[i];
            this.view.imgSSNView.src = "viewicon.png";
        }
        if (this.keypadString.length === 13) {
            var navManager = applicationManager.getNavigationManager();
            var SSN = this.keypadString.replace(/[-() ]/g, "");
            navManager.setCustomInfo("SSNtext", SSN);
            this.view.btnVerify.skin = "sknBtn0095e426pxEnabled";
            this.view.btnVerify.setEnabled(true);
        } else {
            this.view.btnVerify.skin = "sknBtnOnBoardingInactive";
            this.view.btnVerify.setEnabled(false);
        }
        this.view.forceLayout();
    },
    setKeypadChar: function(char) {
        if (this.keypadString.length === 13) {
            return;
        }
        this.keypadString = this.keypadString + char;
        this.updateInputBullets();
    },
    clearKeypadChar: function() {
        var navManager = applicationManager.getNavigationManager();
        this.keypadString = this.keypadString.replace(/[)]/g, "");
        if (this.keypadString.length === 1) {
            this.keypadString = '';
        }
        if (this.keypadString.length !== 0) {
            if (this.keypadString[this.keypadString.length - 1] == '-') {
                this.keypadString = this.keypadString.substr(0, this.keypadString.length - 1);
            }
            this.keypadString = this.keypadString.substr(0, this.keypadString.length - 1);
        }
        navManager.setCustomInfo("SSNtext", "");
        this.updateInputBullets();
    },
    validateSSNForgotPwd: function() {
        var scope = this;
        var temp = scope.keypadString;
        SSN = temp.replace(/[-() ]/g, "");
        if (SSN === null || SSN.length === 0) {
            scope.bindViewError(applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.enroll.enterSSN"));
        } else {
            if (this.isValidSSN(SSN)) {
                var enrollModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("EnrollModule");
                //var params = {
                // "dateOfBirth":enrollModule.presentationController.getEnrollDOB(),
                // "ssn":SSN,
                // "userlastname":enrollModule.presentationController.getEnrollLastName()
                //  };
                // enrollModule.presentationController.checkUserEnrolled(params);
                enrollModule.presentationController.verifySsnEnrolled(SSN, false);
            } else {
                scope.bindViewError(applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.enroll.validSsn"));
            }
        }
    },
    //method to check whether SSN is valid or not 
    isValidSSN: function(SSN) {
        var validssn = true;
        var invalidSsn = ["000000000", "111111111", "222222222", "333333333", "444444444", "555555555", "666666666", "777777777", "888888888", "999999999", "123456789", "987654320", "987654321", "987654322", "987654323", "987654324", "987654325", "987654326", "987654327", "987654328", "987654329"];
        for (var i = 0; i < invalidSsn.length; i++) {
            if (SSN == invalidSsn[i]) {
                validssn = false;
                break;
            }
        }
        if (validssn) return true;
        else return false;
    },
    bindViewError: function(msg) {
        //applicationManager.getDataProcessorUtility().showToastMessageError(this, msg);
        this.showToastMessageError(msg);
    },
    showToastMessageError: function(msg) {
        this.showToastMessage("sknFlxf54b5e", "errormessage.png", msg);
    },
    showToastMessage: function(skin, img, msg) {
        var scope = this;
        this.view.flxMainContainer.top = "60dp";
        this.view.flxPopup.isVisible = true;
        if (this.timerCounter === undefined || this.timerCounter === null) this.timerCounter = 0;
        this.timerCounter = parseInt(this.timerCounter) + 1;
        var timerId = "timerPopupSuccess" + this.timerCounter;
        this.view.flxPopup.skin = "" + skin;
        this.view.customPopup.imgPopup.src = "" + img;
        this.view.customPopup.lblPopup.text = msg;
        try {
            kony.print(timerId);
            kony.timer.schedule(timerId, function() {
                scope.view.flxPopup.setVisibility(false);
                scope.view.flxPopup.isVisible = false;
                scope.view.flxMainContainer.top = "56dp";
            }, 3, false);
        } catch (e) {
            kony.print(timerId);
            kony.print(e);
        }
    },
    goBack: function() {
        var navManager = applicationManager.getNavigationManager();
        navManager.setCustomInfo("isValidUser", true);
        navManager.setCustomInfo("SSNtext", "");
        navManager.goBack();
    },
    onCancel: function() {
        var navManager = applicationManager.getNavigationManager();
        navManager.clearStack();
        var i18n = applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.enroll.cancelPopup");
        kony.ui.Alert(i18n, this.alertCB, constants.ALERT_TYPE_CONFIRMATION, "Yes", "No", "");
    },
    alertCB: function(response) {
        if (response) {
            var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
            authModule.presentationController.navigateToLogin();
        }
    },
    setKeyPadActions: function() {
        var scopeObj = this;
        this.view.keypad.btnOne.onClick = function() {
            scopeObj.setKeypadChar(1);
        };
        this.view.keypad.btnTwo.onClick = function() {
            scopeObj.setKeypadChar(2);
        };
        this.view.keypad.btnThree.onClick = function() {
            scopeObj.setKeypadChar(3);
        };
        this.view.keypad.btnFour.onClick = function() {
            scopeObj.setKeypadChar(4);
        };
        this.view.keypad.btnFive.onClick = function() {
            scopeObj.setKeypadChar(5);
        };
        this.view.keypad.btnSix.onClick = function() {
            scopeObj.setKeypadChar(6);
        };
        this.view.keypad.btnSeven.onClick = function() {
            scopeObj.setKeypadChar(7);
        };
        this.view.keypad.btnEight.onClick = function() {
            scopeObj.setKeypadChar(8);
        };
        this.view.keypad.btnNine.onClick = function() {
            scopeObj.setKeypadChar(9);
        };
        this.view.keypad.btnZero.onClick = function() {
            scopeObj.setKeypadChar(0);
        };
        this.view.keypad.imgClearKeypad.onTouchEnd = function() {
            scopeObj.clearKeypadChar();
        };
        // this.view.btnDot.onClick = function() {
        //    scopeObj.setKeypadChar('.');
        // };
    }
});
define("AuthModule/frmForgotEnterSSNControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for flxBack **/
    AS_FlexContainer_c8f9981c4572412fa03c30f152c7d63e: function AS_FlexContainer_c8f9981c4572412fa03c30f152c7d63e(eventobject) {
        var self = this;
        var authMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
        authMod.presentationController.commonFunctionForNavigation("frmLogin");
    },
    /** onClick defined for btnRight **/
    AS_Button_b1c723d7333d40f18c346b58d0066d7b: function AS_Button_b1c723d7333d40f18c346b58d0066d7b(eventobject) {
        var self = this;
        var authMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
        authMod.presentationController.commonFunctionForNavigation("frmLogin");
    },
    /** onClick defined for undefined **/
    AS_BarButtonItem_c322744e8bbb416f95f577fb44a3df56: function AS_BarButtonItem_c322744e8bbb416f95f577fb44a3df56(eventobject) {
        var self = this;
        this.onCancel();
    },
    /** init defined for frmForgotEnterSSN **/
    AS_Form_d44e0690a04a4e72b73051ac557674ef: function AS_Form_d44e0690a04a4e72b73051ac557674ef(eventobject) {
        var self = this;
        this.init();
    },
    /** preShow defined for frmForgotEnterSSN **/
    AS_Form_j08a833dfea14ad2a5c98aa429f0aa0f: function AS_Form_j08a833dfea14ad2a5c98aa429f0aa0f(eventobject) {
        var self = this;
        this.preShow();
    }
});
define("AuthModule/frmForgotEnterSSNController", ["AuthModule/userfrmForgotEnterSSNController", "AuthModule/frmForgotEnterSSNControllerActions"], function() {
    var controller = require("AuthModule/userfrmForgotEnterSSNController");
    var controllerActions = ["AuthModule/frmForgotEnterSSNControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
