define("AuthModule/userfrmForgotCreatePasswordController", {
    timerCounter: 0,
    isPasswordValid: false,
    passwordsMatch: false,
    init: function() {
        var navManager = applicationManager.getNavigationManager();
        var currentForm = navManager.getCurrentForm();
        applicationManager.getPresentationFormUtility().initCommonActions(this, "YES", currentForm);
    },
    preShow: function() {
        this.view.flxPopup.setVisibility(false);
        this.initActions();
        this.handleData();
        this.renderTitleBar();
        // 	var authMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
        //     var pwddata = authMod.presentationController.passwordRules;
        //     this.view.rtxRulesPwd.text = pwddata;
        applicationManager.getPresentationUtility().dismissLoadingScreen();
        var navManager = applicationManager.getNavigationManager();
        var currentForm = navManager.getCurrentForm();
        applicationManager.getPresentationFormUtility().logFormName(currentForm);
        //this.view.customHeader.flxBack.setVisibility(true);
        this.view.flxSecurityRequirements.setVisibility(false);
    },
    renderTitleBar: function() {
        var deviceUtilManager = applicationManager.getDeviceUtilManager();
        var isIphone = deviceUtilManager.isIPhone();
        //     if(!isIphone){
        //       this.view.flxHeader.isVisible = true;
        //     }
        //     else{
        //       this.view.flxHeader.isVisible = false;
        //     }
        this.view.flxHeader.isVisible = true;
    },
    handleData: function() {
        var scope = this;
        if (kony.os.deviceInfo().name === "iPhone") this.view.btnUpdatePassword.top = "55%";
        this.view.customHeader.lblLocateUs.text = kony.i18n.getLocalizedString("kony.mb.cantLogin.resetPassword");
        this.view.customHeader.btnRight.text = kony.i18n.getLocalizedString("kony.mb.common.CancelSmall");
        this.view.customHeader.flxHeader.btnRight.skin = "sknLblb8dcffSSP32px";
        this.view.customHeader.flxHeader.btnRight.focusSkin = "sknLblb8dcffSSP32px";
        this.view.customHeader.lblLocateUs.skin = "sknRtx424242SSP26px";
        this.view.lblCreateNewPassword.skin = "sknLblb8dcffSSP32px";
        this.view.btnUpdatePassword.text = kony.i18n.getLocalizedString("kony.mb.common.continue");
        this.view.customHeader.btnRight.skin = "sknRtx424242SSP26px";
        this.view.txtNewPassword.text = '';
        this.view.txtReEnterPassword.text = '';
        this.view.customHeader.flxBack.setVisibility(false);
        this.view.customHeader.flxBack.isVisible = false;
        this.view.flxSecurityRequirements.setVisibility(false);
        this.view.imgPasswordsMatch.src = "tickmark.png";
        this.view.imgMaskUnmask.src = "viewicon.png";
        this.view.btnUpdatePassword.skin = "sknBtnOnBoardingInactive";
        this.view.btnUpdatePassword.setEnabled(false);
    },
    initActions: function() {
        this.view.customHeader.flxBack.onClick = this.goBack;
        this.view.customHeader.btnRight.onClick = this.onCancel;
        this.view.txtNewPassword.onTextChange = this.matchPasswords;
        this.view.txtReEnterPassword.onTextChange = this.matchPasswords;
        this.view.txtNewPassword.onTouchStart = this.hideFlxRequirements;
        this.view.txtNewPassword.onEndEditing = this.validatePassword;
        this.view.imgMaskUnmask.onTouchEnd = this.maskUnmaskPassword;
        this.view.txtReEnterPassword.onTouchStart = this.maskNewPwd;
    },
    maskNewPwd: function() {
        this.view.txtNewPassword.secureTextEntry = true;
        this.view.imgMaskUnmask.src = "viewicon.png";
        this.view.txtNewPassword.skin = "sknTbx424242SSPRegular28px";
    },
    goBack: function() {
        var navManager = applicationManager.getNavigationManager();
        navManager.goBack();
    },
    onCancel: function() {
        var navManager = applicationManager.getNavigationManager();
        navManager.clearStack();
        var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
        authModule.presentationController.navigateToLogin();
    },
    maskUnmaskPassword: function() {
        if (this.view.txtNewPassword.text === "") {
            this.view.imgMaskUnmask.src = "viewicon.png";
            this.view.txtNewPassword.secureTextEntry = true;
            return;
        }
        if (this.view.txtNewPassword.secureTextEntry === true) {
            this.view.txtNewPassword.secureTextEntry = false;
            this.view.imgMaskUnmask.src = "viewactive.png";
        } else {
            this.view.txtNewPassword.secureTextEntry = true;
            this.view.imgMaskUnmask.src = "viewicon.png";
        }
    },
    matchPasswords: function() {
        if (this.view.txtNewPassword.text === "") {
            this.view.imgMaskUnmask.src = "viewicon.png";
            this.maskUnmaskPassword();
        }
        if (applicationManager.getPresentationValidationUtility().isValidTextBox(this.view.txtNewPassword.text)) {
            if (this.view.txtNewPassword.text === this.view.txtReEnterPassword.text) {
                this.view.imgPasswordsMatch.src = "greentick.png";
                this.view.lblCreateNewPassword.text = "Reset Password";
                this.passwordsMatch = true;
            } else {
                this.view.imgPasswordsMatch.src = "tickmark.png";
                this.passwordsMatch = false;
            }
        } else {
            this.view.imgPasswordsMatch.src = "tickmark.png";
            this.passwordsMatch = false;
        }
        if (this.passwordsMatch && this.isPasswordValid) {
            this.view.btnUpdatePassword.skin = "sknBtn0095e426pxEnabled";
            this.view.btnUpdatePassword.setEnabled(true);
        } else {
            this.view.btnUpdatePassword.skin = "sknBtnOnBoardingInactive";
            this.view.btnUpdatePassword.setEnabled(false);
        }
        this.view.forceLayout();
    },
    validatePassword: function() {
        var password = this.view.txtNewPassword.text;
        var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
        authModule.presentationController.validatePassword(password);
    },
    updatePasswordAction: function() {
        applicationManager.getPresentationUtility().showLoadingScreen();
        var newPassword = this.view.txtNewPassword.text;
        var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
        authModule.presentationController.updatePassword(newPassword);
    },
    //     showPopupUpdatePasswordSuccessful: function () {
    bindGenericError: function(errorMsg) {
        var scopeObj = this;
        applicationManager.getDataProcessorUtility().showToastMessageError(scopeObj, errorMsg);
    },
    passwordValid: function() {
        this.isPasswordValid = true;
        this.matchPasswords();
    },
    // utitlity function to show password rules
    showFlxSecurityRequirements: function() {
        this.isPasswordValid = false;
        this.view.txtNewPassword.secureTextEntry = true;
        this.view.imgMaskUnmask.src = "viewicon.png";
        this.view.flxSecurityRequirements.isVisible = true;
        /* if (this.view.flxSecurityRequirements.height !== "150dp") {
             this.view.flxSecurityRequirements.animate(
                 kony.ui.createAnimation({
                     "0": {
                         "anchorPoint": {
                             "x": 0.5,
                             "y": 0.5
                         },
                         "stepConfig": {
                             "timingFunction": kony.anim.EASE
                         },
                         "rectified": true,
                         "height": "0dp",
                     },
                     "100": {
                         "anchorPoint": {
                             "x": 0.5,
                             "y": 0.5
                         },
                         "stepConfig": {
                             "timingFunction": kony.anim.EASE
                         },
                         "rectified": true,
                         "height": "150dp",
                     }
                 }), {
                     "delay": 0,
                     "iterationCount": 1,
                     "fillMode": kony.anim.FILL_MODE_FORWARDS,
                     "duration": 0.5
                 }, {
                     "animationEnd": function() {}
                 });
         } */
    },
    // utitlity function to hide password rules
    hideFlxRequirements: function() {
        this.view.flxSecurityRequirements.setVisibility(false);
        /*  var scope = this;
          if (this.view.flxSecurityRequirements.height !== "0dp") {
              this.view.flxSecurityRequirements.animate(
                  kony.ui.createAnimation({
                      "0": {
                          "anchorPoint": {
                              "x": 0.5,
                              "y": 0.5
                          },
                          "stepConfig": {
                              "timingFunction": kony.anim.EASE
                          },
                          "rectified": true,
                          "height": "150dp",
                      },
                      "100": {
                          "anchorPoint": {
                              "x": 0.5,
                              "y": 0.5
                          },
                          "stepConfig": {
                              "timingFunction": kony.anim.EASE
                          },
                          "rectified": true,
                          "height": "0dp",
                      }
                  }), {
                      "delay": 0,
                      "iterationCount": 1,
                      "fillMode": kony.anim.FILL_MODE_FORWARDS,
                      "duration": 0.5
                  }, {
                      "animationEnd": function() {
                          scope.view.flxSecurityRequirements.isVisible = true;
                      }
                  });
          } */
    },
    bindGenericError: function(errorMsg) {
        applicationManager.getPresentationUtility().dismissLoadingScreen();
        var scopeObj = this;
        applicationManager.getDataProcessorUtility().showToastMessageError(scopeObj, errorMsg);
    }
});
define("AuthModule/frmForgotCreatePasswordControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for btnRight **/
    AS_Button_j4fd09b548f54d51bb0108e43eae39be: function AS_Button_j4fd09b548f54d51bb0108e43eae39be(eventobject) {
        var self = this;
        var authMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
        authMod.presentationController.commonFunctionForNavigation("frmLogin");
    },
    /** onClick defined for btnUpdatePassword **/
    AS_Button_ce03fcc04c4d4378a355dcbaa4497ee0: function AS_Button_ce03fcc04c4d4378a355dcbaa4497ee0(eventobject) {
        var self = this;
        this.updatePasswordAction();
    },
    /** onClick defined for undefined **/
    AS_BarButtonItem_if1523b5f2dd440e8d6c0d66124f79ef: function AS_BarButtonItem_if1523b5f2dd440e8d6c0d66124f79ef(eventobject) {
        var self = this;
        this.onCancel();
    },
    /** init defined for frmForgotCreatePassword **/
    AS_Form_c2cf6abb111947c08cea3c53589f48c4: function AS_Form_c2cf6abb111947c08cea3c53589f48c4(eventobject) {
        var self = this;
        this.init();
    },
    /** preShow defined for frmForgotCreatePassword **/
    AS_Form_bf368211a1ed42dda4007a81fa285b1f: function AS_Form_bf368211a1ed42dda4007a81fa285b1f(eventobject) {
        var self = this;
        this.preShow();
    }
});
define("AuthModule/frmForgotCreatePasswordController", ["AuthModule/userfrmForgotCreatePasswordController", "AuthModule/frmForgotCreatePasswordControllerActions"], function() {
    var controller = require("AuthModule/userfrmForgotCreatePasswordController");
    var controllerActions = ["AuthModule/frmForgotCreatePasswordControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
