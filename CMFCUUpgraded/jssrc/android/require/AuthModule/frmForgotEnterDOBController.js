define("AuthModule/userfrmForgotEnterDOBController", {
    keypadString: '',
    timerCounter: 0,
    init: function() {
        var navManager = applicationManager.getNavigationManager();
        var currentForm = navManager.getCurrentForm();
        applicationManager.getPresentationFormUtility().initCommonActions(this, "YES", currentForm);
    },
    preShow: function() {
        this.view.flxPopup.setVisibility(false);
        this.initActions();
        this.renderTitleBar();
        this.handleData();
        applicationManager.getPresentationUtility().dismissLoadingScreen();
        var navManager = applicationManager.getNavigationManager();
        var currentForm = navManager.getCurrentForm();
        applicationManager.getPresentationFormUtility().logFormName(currentForm);
    },
    renderTitleBar: function() {
        var deviceUtilManager = applicationManager.getDeviceUtilManager();
        var isIphone = deviceUtilManager.isIPhone();
        /*if(!isIphone){
          this.view.flxHeader.isVisible = true;
        }
        else{
          this.view.flxHeader.isVisible = false;
        }*/
        this.view.flxHeader.isVisible = true;
    },
    handleData: function() {
        var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
        var forgotObj = authModule.presentationController.getForgotObjectForView();
        this.view.customHeader.btnRight.text = kony.i18n.getLocalizedString("kony.mb.common.CancelSmall");
        this.view.customHeader.btnRight.skin = "sknRtx424242SSP26px";
        this.view.customHeader.btnRight.focusSkin = "sknRtx424242SSP26px";
        this.view.customHeader.lblLocateUs.skin = "sknRtx424242SSP26px";
        var navManager = applicationManager.getNavigationManager();
        navManager.setCustomInfo("dateOfBirth", "");
        this.keypadString = "";
        this.updateInputBullets();
    },
    initActions: function() {
        this.view.btnVerify.onClick = this.validateDOB;
        this.view.customHeader.flxBack.onClick = this.goBack;
        this.view.customHeader.btnRight.onClick = this.onCancel;
        this.view.flxView.onClick = this.imgDOBViewOnClick;
    },
    imgDOBViewOnClick: function() {
        var navManager = applicationManager.getNavigationManager();
        var dateOfBirth = navManager.getCustomInfo("dateOfBirth");
        if (dateOfBirth !== "" && dateOfBirth !== null && typeof dateOfBirth !== "undefined" && dateOfBirth.length === 10) {
            if (this.view.imgSSNView.src === "viewicon.png") {
                this.view.imgSSNView.src = "viewactive.png";
                this.view.lblMonthOne.text = dateOfBirth.charAt(0);
                this.view.lblMonthTwo.text = dateOfBirth.charAt(1);
                this.view.lblDayOne.text = dateOfBirth.charAt(3);
                this.view.lblDayTwo.text = dateOfBirth.charAt(4);
                this.view.lblYearOne.text = dateOfBirth.charAt(6);
                this.view.lblYearTwo.text = dateOfBirth.charAt(7);
                this.view.lblYearThree.text = dateOfBirth.charAt(8);
                this.view.lblYearFour.text = dateOfBirth.charAt(9);
                this.view.flxInputDOB.forceLayout();
            } else {
                this.view.imgSSNView.src = "viewicon.png";
                this.view.lblMonthOne.text = "•";
                this.view.lblMonthTwo.text = "•";
                this.view.lblDayOne.text = "•";
                this.view.lblDayTwo.text = "•";
                this.view.lblYearOne.text = "•";
                this.view.lblYearTwo.text = "•";
                this.view.lblYearThree.text = "•";
                this.view.lblYearFour.text = "•";
                this.view.flxInputDOB.forceLayout();
            }
        } else {
            this.view.imgSSNView.src = "viewicon.png";
            this.view.flxInputDOB.forceLayout();
        }
    },
    //KEYPAD OPS:
    updateInputBullets: function() {
        var scope = this;
        var dummyString = 'MM/DD/YYYY';
        var navManager = applicationManager.getNavigationManager();
        var widgets = this.view["flxInputDOB"].widgets();
        for (var i = 0; i < this.keypadString.length; i++) {
            navManager.setCustomInfo("dateOfBirth", this.keypadString);
            widgets[i].skin = "sknLbl979797SSP60px";
            //widgets[i].text = this.keypadString[i];
            if (i === 2 || i === 5) {
                widgets[i].text = "/";
            } else {
                widgets[i].text = "•";
            }
        }
        for (var i = this.keypadString.length; i < widgets.length; i++) {
            widgets[i].skin = "sknLble3e3e3SSP60px";
            widgets[i].text = dummyString[i];
            this.view.imgSSNView.src = "viewicon.png";
        }
        if (this.keypadString.length !== 10) {
            this.view.btnVerify.skin = "sknBtnOnBoardingInactive";
            this.view.btnVerify.setEnabled(false);
        } else {
            this.view.btnVerify.skin = "sknBtn0095e426pxEnabled";
            this.view.btnVerify.setEnabled(true);
        }
        this.view.forceLayout();
    },
    setKeypadChar: function(char) {
        if (this.keypadString.length === 10) return;
        this.keypadString = this.keypadString + char;
        if (this.keypadString.length === 2 || this.keypadString.length === 5) {
            this.keypadString = this.keypadString + '/';
        }
        this.updateInputBullets();
    },
    clearKeypadChar: function() {
        if (this.keypadString.length === 1) {
            this.keypadString = '';
            this.updateInputBullets();
        }
        if (this.keypadString.length !== 0) {
            if (this.keypadString[this.keypadString.length - 1] === '/') {
                this.keypadString = this.keypadString.substr(0, this.keypadString.length - 1);
            }
            this.keypadString = this.keypadString.substr(0, this.keypadString.length - 1);
            this.updateInputBullets();
        }
    },
    validateDOB: function() {
        var DOB = this.keypadString;
        applicationManager.getPresentationUtility().showLoadingScreen();
        var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
        authModule.presentationController.validateDOB(DOB);
    },
    bindViewError: function(msg) {
        applicationManager.getPresentationUtility().dismissLoadingScreen();
        //     applicationManager.getDataProcessorUtility().showToastMessageError(this,msg);
        this.showToastMessageError(msg);
    },
    showToastMessageError: function(msg) {
        this.showToastMessage("sknFlxf54b5e", "errormessage.png", msg);
    },
    showToastMessage: function(skin, img, msg) {
        var scope = this;
        this.view.flxMainContainer.top = "60dp";
        this.view.flxPopup.isVisible = true;
        if (this.timerCounter === undefined || this.timerCounter === null) this.timerCounter = 0;
        this.timerCounter = parseInt(this.timerCounter) + 1;
        var timerId = "timerPopupSuccess" + this.timerCounter;
        this.view.flxPopup.skin = "" + skin;
        this.view.customPopup.imgPopup.src = "" + img;
        this.view.customPopup.lblPopup.text = msg;
        try {
            kony.print(timerId);
            kony.timer.schedule(timerId, function() {
                scope.view.flxPopup.setVisibility(false);
                scope.view.flxPopup.isVisible = false;
                scope.view.flxMainContainer.top = "56dp";
            }, 3, false);
        } catch (e) {
            kony.print(timerId);
            kony.print(e);
        }
    },
    goBack: function() {
        var navManager = applicationManager.getNavigationManager();
        navManager.setCustomInfo("dateOfBirth", "");
        navManager.goBack();
    },
    onCancel: function() {
        var navManager = applicationManager.getNavigationManager();
        navManager.clearStack();
        var i18n = applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.enroll.cancelPopup");
        kony.ui.Alert(i18n, this.alertCB, constants.ALERT_TYPE_CONFIRMATION, "Yes", "No", "");
    },
    alertCB: function(response) {
        if (response) {
            var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
            authModule.presentationController.navigateToLogin();
        }
    }
});
define("AuthModule/frmForgotEnterDOBControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for btnOne **/
    AS_Button_i7f9f5abb3664e5fb694af8feb4212a6: function AS_Button_i7f9f5abb3664e5fb694af8feb4212a6(eventobject) {
        var self = this;
        this.setKeypadChar(1);
    },
    /** onClick defined for btnTwo **/
    AS_Button_h3ea77cf91fc4f4d9ff45deb3685f828: function AS_Button_h3ea77cf91fc4f4d9ff45deb3685f828(eventobject) {
        var self = this;
        this.setKeypadChar(2);
    },
    /** onClick defined for btnThree **/
    AS_Button_i93a76d23b8647f791038f00a879241a: function AS_Button_i93a76d23b8647f791038f00a879241a(eventobject) {
        var self = this;
        this.setKeypadChar(3);
    },
    /** onClick defined for btnFour **/
    AS_Button_f1671d383324483c86104e017fa68a98: function AS_Button_f1671d383324483c86104e017fa68a98(eventobject) {
        var self = this;
        this.setKeypadChar(4);
    },
    /** onClick defined for btnSix **/
    AS_Button_a0158c26af96430caa4ed3bfd9e600dd: function AS_Button_a0158c26af96430caa4ed3bfd9e600dd(eventobject) {
        var self = this;
        this.setKeypadChar(6);
    },
    /** onClick defined for btnSeven **/
    AS_Button_ie3bcc135a944dd8baa8de8a0961937b: function AS_Button_ie3bcc135a944dd8baa8de8a0961937b(eventobject) {
        var self = this;
        this.setKeypadChar(7);
    },
    /** onClick defined for btnEight **/
    AS_Button_d995428196c94d1992d34e7680ead2d7: function AS_Button_d995428196c94d1992d34e7680ead2d7(eventobject) {
        var self = this;
        this.setKeypadChar(8);
    },
    /** onClick defined for btnNine **/
    AS_Button_ee1f4c4192904e4f934349eb25ff3430: function AS_Button_ee1f4c4192904e4f934349eb25ff3430(eventobject) {
        var self = this;
        this.setKeypadChar(9);
    },
    /** onClick defined for btnZero **/
    AS_Button_jd2f9ffdd6924b03bb0175998407e8d3: function AS_Button_jd2f9ffdd6924b03bb0175998407e8d3(eventobject) {
        var self = this;
        this.setKeypadChar(0);
    },
    /** onTouchEnd defined for imgClearKeypad **/
    AS_Image_b8275a188bb543bda67ceee35f4a8969: function AS_Image_b8275a188bb543bda67ceee35f4a8969(eventobject, x, y) {
        var self = this;
        this.clearKeypadChar();
    },
    /** onClick defined for flxBack **/
    AS_FlexContainer_f392eaa783094c9386e76dffed39a3e1: function AS_FlexContainer_f392eaa783094c9386e76dffed39a3e1(eventobject) {
        var self = this;
        var authMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
        authMod.presentationController.commonFunctionForNavigation("frmForgotEnterSSN");
    },
    /** onClick defined for btnFive **/
    AS_Button_jeaf1c244412436086fe88aca660028c: function AS_Button_jeaf1c244412436086fe88aca660028c(eventobject) {
        var self = this;
        this.setKeypadChar(5);
    },
    /** onClick defined for undefined **/
    AS_BarButtonItem_f9d5801cb8764b678861b34f27658253: function AS_BarButtonItem_f9d5801cb8764b678861b34f27658253(eventobject) {
        var self = this;
        this.onCancel();
    },
    /** init defined for frmForgotEnterDOB **/
    AS_Form_f400c071ec4e44898268b8d4bb73f98d: function AS_Form_f400c071ec4e44898268b8d4bb73f98d(eventobject) {
        var self = this;
        this.init();
    },
    /** preShow defined for frmForgotEnterDOB **/
    AS_Form_i727c94db9924ff1910384bdcd323404: function AS_Form_i727c94db9924ff1910384bdcd323404(eventobject) {
        var self = this;
        this.preShow();
    }
});
define("AuthModule/frmForgotEnterDOBController", ["AuthModule/userfrmForgotEnterDOBController", "AuthModule/frmForgotEnterDOBControllerActions"], function() {
    var controller = require("AuthModule/userfrmForgotEnterDOBController");
    var controllerActions = ["AuthModule/frmForgotEnterDOBControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
