define("AuthModule/frmForgotAccountPrimaryLinking", function() {
    return function(controller) {
        function addWidgetsfrmForgotAccountPrimaryLinking() {
            this.setDefaultUnit(kony.flex.DP);
            var flxHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "56dp",
                "id": "flxHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxffffffShadow",
                "top": "0%",
                "width": "100%",
                "zIndex": 100
            }, {}, {});
            flxHeader.setDefaultUnit(kony.flex.DP);
            var customHeader = new com.kmb.common.customHeader({
                "clipBounds": false,
                "height": "100%",
                "id": "customHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "btnRight": {
                        "text": kony.i18n.getLocalizedString("kony.mb.common.CancelSmall"),
                        "isVisible": true
                    },
                    "flxBack": {
                        "isVisible": true
                    },
                    "flxSearch": {
                        "isVisible": false
                    },
                    "imgBack": {
                        "isVisible": false
                    },
                    "lblLocateUs": {
                        "text": kony.i18n.getLocalizedString("kony.mb.cantLogin.cantLogin")
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxHeader.add(customHeader);
            var flxPopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "10.50%",
                "id": "flxPopup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "sknFlxf54b5e",
                "top": "0dp",
                "width": "100%",
                "zIndex": 200
            }, {}, {});
            flxPopup.setDefaultUnit(kony.flex.DP);
            var customPopup = new com.kmb.common.customPopup({
                "clipBounds": true,
                "height": "100%",
                "id": "customPopup",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {}
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxPopup.add(customPopup);
            var flxMainContainer = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": "0%",
                "bounces": false,
                "clipBounds": false,
                "enableScrolling": true,
                "horizontalScrollIndicator": true,
                "id": "flxMainContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "sknScrFlxffffff",
                "top": "56dp",
                "verticalScrollIndicator": true,
                "width": "100%",
                "zIndex": 30
            }, {}, {});
            flxMainContainer.setDefaultUnit(kony.flex.DP);
            var lblText = new kony.ui.Label({
                "centerX": "50%",
                "id": "lblText",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl54565aSSP30",
                "text": kony.i18n.getLocalizedString("kony.mb.cantLogin.selectAccount"),
                "textStyle": {
                    "letterSpacing": 0,
                    "strikeThrough": false
                },
                "top": "3%",
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false
            });
            var lblTextdesc = new kony.ui.Label({
                "centerX": "50%",
                "id": "lblTextdesc",
                "isVisible": true,
                "left": "127dp",
                "skin": "sknLbl54565aSSPReg26",
                "text": kony.i18n.getLocalizedString("kony.mb.cantLogin.primAccDesc"),
                "textStyle": {
                    "letterSpacing": 0,
                    "strikeThrough": false
                },
                "top": "5%",
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false
            });
            var lblAccTextDesc = new kony.ui.Label({
                "centerX": "50.02%",
                "id": "lblAccTextDesc",
                "isVisible": true,
                "left": "127dp",
                "skin": "sknLbl54565aSSPReg26",
                "text": kony.i18n.getLocalizedString("kony.mb.cantLogin.slctPrimAcc"),
                "textStyle": {
                    "letterSpacing": 0,
                    "strikeThrough": false
                },
                "top": "3%",
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false
            });
            var flxAccLink = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAccLink",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10%",
                "isModalContainer": false,
                "top": "5%",
                "width": "80%",
                "zIndex": 1
            }, {}, {});
            flxAccLink.setDefaultUnit(kony.flex.DP);
            var segAccLink = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "data": [{
                    "imgIndicator": "radio_inactive.png",
                    "lblAccName": kony.i18n.getLocalizedString("kony.mb.enroll.primary"),
                    "lblAccNameValue": "David Cooper",
                    "lblAccNum": "1234567890",
                    "lblAccType": kony.i18n.getLocalizedString("kony.mb.enroll.accType"),
                    "lblAccTypeValue": "Business"
                }, {
                    "imgIndicator": "radio_inactive.png",
                    "lblAccName": kony.i18n.getLocalizedString("kony.mb.enroll.primary"),
                    "lblAccNameValue": "David Cooper",
                    "lblAccNum": "1234567890",
                    "lblAccType": kony.i18n.getLocalizedString("kony.mb.enroll.accType"),
                    "lblAccTypeValue": "Business"
                }, {
                    "imgIndicator": "radio_inactive.png",
                    "lblAccName": kony.i18n.getLocalizedString("kony.mb.enroll.primary"),
                    "lblAccNameValue": "David Cooper",
                    "lblAccNum": "1234567890",
                    "lblAccType": kony.i18n.getLocalizedString("kony.mb.enroll.accType"),
                    "lblAccTypeValue": "Business"
                }],
                "groupCells": false,
                "id": "segAccLink",
                "isVisible": true,
                "left": 0,
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowTemplate": "flxAccPrimaryLinking",
                "scrollingEvents": {},
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorColor": "ddddde00",
                "separatorRequired": true,
                "separatorThickness": 1,
                "showScrollbars": false,
                "top": 0,
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxAccDetails": "flxAccDetails",
                    "flxAccPrimaryLinking": "flxAccPrimaryLinking",
                    "flxAccTypeDetails": "flxAccTypeDetails",
                    "flxImgIndicator": "flxImgIndicator",
                    "imgIndicator": "imgIndicator",
                    "lblAccName": "lblAccName",
                    "lblAccNameValue": "lblAccNameValue",
                    "lblAccNum": "lblAccNum",
                    "lblAccType": "lblAccType",
                    "lblAccTypeValue": "lblAccTypeValue"
                },
                "width": "100%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 1, 0],
                "paddingInPixel": false
            }, {});
            flxAccLink.add(segAccLink);
            var btnContinue = new kony.ui.Button({
                "bottom": "5%",
                "centerX": "50%",
                "height": "50dp",
                "id": "btnContinue",
                "isVisible": true,
                "skin": "skinButtonDisabled",
                "text": kony.i18n.getLocalizedString("kony.mb.common.continue"),
                "top": "15%",
                "width": "90%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxMainContainer.add(lblText, lblTextdesc, lblAccTextDesc, flxAccLink, btnContinue);
            var flxMaincontainer1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxMaincontainer1",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxWhite",
                "top": "56dp",
                "width": "100%",
                "zIndex": 30
            }, {}, {});
            flxMaincontainer1.setDefaultUnit(kony.flex.DP);
            var lblText1 = new kony.ui.Label({
                "centerX": "50%",
                "id": "lblText1",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl54565aSSP30",
                "text": kony.i18n.getLocalizedString("kony.mb.cantLogin.selectAccount"),
                "textStyle": {
                    "letterSpacing": 0,
                    "strikeThrough": false
                },
                "top": "3%",
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false
            });
            var lblTextdesc1 = new kony.ui.Label({
                "centerX": "50%",
                "id": "lblTextdesc1",
                "isVisible": true,
                "left": "127dp",
                "skin": "sknLbl54565aSSPReg26",
                "text": kony.i18n.getLocalizedString("kony.mb.cantLogin.primAccDesc"),
                "textStyle": {
                    "letterSpacing": 0,
                    "strikeThrough": false
                },
                "top": "5%",
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false
            });
            var lblAccTextDesc1 = new kony.ui.Label({
                "centerX": "50.02%",
                "id": "lblAccTextDesc1",
                "isVisible": true,
                "left": "127dp",
                "skin": "sknLbl54565aSSPReg26",
                "text": kony.i18n.getLocalizedString("kony.mb.cantLogin.slctPrimAcc"),
                "textStyle": {
                    "letterSpacing": 0,
                    "strikeThrough": false
                },
                "top": "3%",
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false
            });
            var flxAccLink1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAccLink1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10%",
                "isModalContainer": false,
                "top": "5%",
                "width": "80%",
                "zIndex": 1
            }, {}, {});
            flxAccLink1.setDefaultUnit(kony.flex.DP);
            var segAccLink1 = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "data": [{
                    "imgIndicator": "radio_inactive.png",
                    "lblAccName": kony.i18n.getLocalizedString("kony.mb.enroll.primary"),
                    "lblAccNameValue": "David Cooper",
                    "lblAccNum": "1234567890",
                    "lblAccType": kony.i18n.getLocalizedString("kony.mb.enroll.accType"),
                    "lblAccTypeValue": "Business"
                }, {
                    "imgIndicator": "radio_inactive.png",
                    "lblAccName": kony.i18n.getLocalizedString("kony.mb.enroll.primary"),
                    "lblAccNameValue": "David Cooper",
                    "lblAccNum": "1234567890",
                    "lblAccType": kony.i18n.getLocalizedString("kony.mb.enroll.accType"),
                    "lblAccTypeValue": "Business"
                }, {
                    "imgIndicator": "radio_inactive.png",
                    "lblAccName": kony.i18n.getLocalizedString("kony.mb.enroll.primary"),
                    "lblAccNameValue": "David Cooper",
                    "lblAccNum": "1234567890",
                    "lblAccType": kony.i18n.getLocalizedString("kony.mb.enroll.accType"),
                    "lblAccTypeValue": "Business"
                }],
                "groupCells": false,
                "id": "segAccLink1",
                "isVisible": true,
                "left": 0,
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowTemplate": "flxAccPrimaryLinking",
                "scrollingEvents": {},
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorColor": "ddddde00",
                "separatorRequired": true,
                "separatorThickness": 1,
                "showScrollbars": false,
                "top": 0,
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxAccDetails": "flxAccDetails",
                    "flxAccPrimaryLinking": "flxAccPrimaryLinking",
                    "flxAccTypeDetails": "flxAccTypeDetails",
                    "flxImgIndicator": "flxImgIndicator",
                    "imgIndicator": "imgIndicator",
                    "lblAccName": "lblAccName",
                    "lblAccNameValue": "lblAccNameValue",
                    "lblAccNum": "lblAccNum",
                    "lblAccType": "lblAccType",
                    "lblAccTypeValue": "lblAccTypeValue"
                },
                "width": "100%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 1, 0],
                "paddingInPixel": false
            }, {});
            flxAccLink1.add(segAccLink1);
            var btnContinue1 = new kony.ui.Button({
                "bottom": "2%",
                "centerX": "50%",
                "height": "50dp",
                "id": "btnContinue1",
                "isVisible": true,
                "skin": "skinButtonDisabled",
                "text": kony.i18n.getLocalizedString("kony.mb.common.continue"),
                "top": "18%",
                "width": "90%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxMaincontainer1.add(lblText1, lblTextdesc1, lblAccTextDesc1, flxAccLink1, btnContinue1);
            this.add(flxHeader, flxPopup, flxMainContainer, flxMaincontainer1);
        };
        return [{
            "addWidgets": addWidgetsfrmForgotAccountPrimaryLinking,
            "enableScrolling": false,
            "enabledForIdleTimeout": false,
            "id": "frmForgotAccountPrimaryLinking",
            "init": controller.AS_Form_eb77d8fab92a42ed9518a31caa9250a3,
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "preShow": function(eventobject) {
                controller.AS_Form_e9c123dde48c4819a2ae00258c3f1d1c(eventobject);
            },
            "skin": "sknFormBckgrndGrey"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "footerOverlap": false,
            "headerOverlap": false,
            "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
            "retainScrollPosition": false,
            "titleBar": true,
            "titleBarSkin": "sknTitle1a98ffffffff30px",
            "windowSoftInputMode": constants.FORM_ADJUST_PAN
        }]
    }
});