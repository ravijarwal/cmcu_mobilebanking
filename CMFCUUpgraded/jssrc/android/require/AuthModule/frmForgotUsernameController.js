define("AuthModule/userfrmForgotUsernameController", {
    //Type your controller code here 
});
define("AuthModule/frmForgotUsernameControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for btnRight **/
    AS_Button_j4fd09b548f54d51bb0108e43eae39be: function AS_Button_j4fd09b548f54d51bb0108e43eae39be(eventobject) {
        var self = this;
        var authMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
        authMod.presentationController.commonFunctionForNavigation("frmLogin");
    },
});
define("AuthModule/frmForgotUsernameController", ["AuthModule/userfrmForgotUsernameController", "AuthModule/frmForgotUsernameControllerActions"], function() {
    var controller = require("AuthModule/userfrmForgotUsernameController");
    var controllerActions = ["AuthModule/frmForgotUsernameControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
