define({
    isLoginSuccess: function() {
        var configManager = applicationManager.getConfigurationManager();
        var main_user = applicationManager.getUserPreferencesManager().getUserName();
        var accountManager = applicationManager.getAccountManager();
        scope_AuthPresenter.asyncManager.initiateAsyncProcess(this.numberOfAsyncForLogin);
        var userPreferencesManager = applicationManager.getUserPreferencesManager();
        scope_AuthPresenter.getDeviceRegistrationStatus();
        scope_AuthPresenter.getAllAccounts();
    },
    presentationDeviceRegistrationSuccess: function(resDeviceSuc) {
        var configManager = applicationManager.getConfigurationManager();
        scope_AuthPresenter.asyncManager.setSuccessStatus(0, resDeviceSuc);
        if (resDeviceSuc[0].status !== "false") scope_AuthPresenter.setDeviceRegisterflag(true);
        else scope_AuthPresenter.setDeviceRegisterflag(false);
        if (scope_AuthPresenter.asyncManager.areAllservicesDone(this.numberOfAsyncForLogin)) {
            scope_AuthPresenter.navigationAfterLogin();
        }
    },
    presentationDeviceRegistrationError: function(resDeviceErr) {
        scope_AuthPresenter.asyncManager.setErrorStatus(0, resDeviceErr);
        applicationManager.getPresentationUtility().dismissLoadingScreen();
        if (resDeviceErr["isServerUnreachable"]) applicationManager.getPresentationInterruptHandler().showErrorMessage("postLogin", resDeviceErr);
    },
    presentationAccountsSucc: function(resAccountSuc) {
        var navManager = applicationManager.getNavigationManager();
        var configManager = applicationManager.getConfigurationManager();
        scope_AuthPresenter.logger.log(resAccountSuc);
        scope_AuthPresenter.asyncManager.setSuccessStatus(1, resAccountSuc);
        var accountObj = applicationManager.getAccountManager();
        var accountData = accountObj.getInternalAccounts();
        var custominfo = navManager.getCustomInfo("frmDashboard");
        if (!custominfo) {
            custominfo = {};
        }
        custominfo.accountData = accountData;
        navManager.setCustomInfo("frmDashboard", custominfo);
        this.numberOfAsyncForLogin = 2;
        if (scope_AuthPresenter.asyncManager.areAllservicesDone(this.numberOfAsyncForLogin)) {
            scope_AuthPresenter.navigationAfterLogin();
        }
    },
    presentationAccountsErr: function(resAccountErr) {
        scope_AuthPresenter.asyncManager.setErrorStatus(1, resAccountErr);
        scope_AuthPresenter.logger.log(resAccountErr);
        if (resAccountErr["isServerUnreachable"]) {
            applicationManager.getPresentationInterruptHandler().showErrorMessage("postLogin", resAccountErr);
        }
        applicationManager.getPresentationUtility().dismissLoadingScreen();
        var controller = applicationManager.getPresentationUtility().getController('frmLogin', true);
        var specificAccount = "not found";
        var invalidPassword = "invalid password";
        var frozenAccount = "frozen";
        var notAuthorized = "Not authorized";
        if (resAccountErr["errorMessage"].indexOf(specificAccount) !== -1) {
            controller.bindLoginErrorMessage(applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.login.invalidCredentials"));
        } else if (resAccountErr["errorMessage"].indexOf(invalidPassword) !== -1 || resAccountErr["errorMessage"].indexOf(notAuthorized) !== -1) {
            this.loginAttempt++;
            if (parseInt(this.loginAttempt) == 2 || parseInt(this.loginAttempt) == 3) {
                kony.ui.Alert({
                    "alertType": constants.ALERT_TYPE_INFO,
                    "yesLabel": "OK",
                    "message": "Can`t Login?",
                    "alertHandler": this.navigateToForgetFlow
                }, {
                    "iconPosition": constants.ALERT_ICON_POSITION_LEFT
                });
            }
            controller.bindLoginErrorMessage(applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.login.invalidCredentials"));
        } else if (resAccountErr["errorMessage"].indexOf(frozenAccount) !== -1) {
            //controller.bindLoginErrorMessage(applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.login.accountFrozen"));
            var i18nString = applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.login.accountFrozen");
            kony.ui.Alert(i18nString, this.ContactHandler, constants.ALERT_TYPE_CONFIRMATION, "Contact", "Ok", "");
        }
    },
    presentationLoginError: function(resError) {
        if (resError.opstatus == 1011) {
            kony.ui.Alert({
                "alertType": constants.ALERT_TYPE_INFO,
                "yesLabel": "Ok",
                "message": "You don't seem to have an active internet connection. Please try again later.",
                "alertHandler": this.handleInternetError
            }, {
                "iconPosition": constants.ALERT_ICON_POSITION_LEFT
            });
            applicationManager.getPresentationUtility().dismissLoadingScreen();
            return;
        }
        scope_AuthPresenter.isLoginSuccess();
    },
    navigationAfterLogin: function() {
        var userPreferencesManager = applicationManager.getUserPreferencesManager();
        var navManager = applicationManager.getNavigationManager();
        if (userPreferencesManager.isRememberMeOn() == false) scope_AuthPresenter.goToAccounts();
        else {
            var reg = scope_AuthPresenter.asyncManager.getData(0)[0];
            if (reg.status === "false") {
                scope_AuthPresenter.setDeviceRegisterflag(false);
                navManager.navigateTo("frmDevRegLanding");
            } else {
                scope_AuthPresenter.setDeviceRegisterflag(true);
                if (userPreferencesManager.isFirstTimeLogin()) {
                    var keys = scope_AuthPresenter.getAuthFlags();
                    keys.popUpMsg = "";
                    navManager.setCustomInfo("frmDevRegLoginType", keys);
                    var controller = applicationManager.getPresentationUtility().getController('frmDevRegLoginType', true);
                    controller.tempLoginMode = "password";
                    navManager.navigateTo("frmDevRegLoginType");
                } else scope_AuthPresenter.goToAccounts();
            }
        }
    },
    //invoking custom identity service of CMCU
    invokeIdentity: function() {
        scope = this;
        var authManger = applicationManager.getAuthManager();
        authManger.invokeIdentityService(scope.identitySuccess, scope.identityFailure);
    },
    identitySuccess: function(resSuccess) {
        scope = this;
        scope.sendOTP();
    },
    identityFailure: function(resError) {
        applicationManager.getPresentationUtility().dismissLoadingScreen();
        var controller = applicationManager.getPresentationUtility().getController('frmEnrollSecurityCheck', true);
    },
    //code for fetching otp from service when user requests otp through email,call,text,etc
    sendOTP: function() {
        scope = this;
        var navManager = applicationManager.getNavigationManager();
        var customInfo = navManager.getCustomInfo("frmDevRegLanding");
        var destination = customInfo.dest;
        var type = customInfo.type;
        var params = {
            "destination": destination,
            "userName": "JYOTHIVIJU",
            "type": type,
            "deviceInfo": ""
        };
        var authManager = applicationManager.getAuthManager();
        authManager.sendOTP(params, scope.sendOTPSuccess, scope.sendOTPFailure);
    },
    //navigating to security code screen to enter otp
    sendOTPSuccess: function(response) {
        applicationManager.getPresentationUtility().dismissLoadingScreen();
        var navManager = applicationManager.getNavigationManager();
        navManager.navigateTo("frmDevRegSecCode");
    },
    sendOTPFailure: function(error) {
        //enrollPresentationScope.logger.log("#### request OTP service call failed ####");
        applicationManager.getPresentationUtility().dismissLoadingScreen();
        if (error["isServerUnreachable"]) applicationManager.getPresentationInterruptHandler().showErrorMessage("preLogin", err);
        else {
            var controller = applicationManager.getPresentationUtility().getController('frmEnrollSecurityController', true);
            var errorMsg = applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.enroll.SomethingWrong");
            controller.bindViewError(errorMsg);
            //generic error callback
        }
    },
    //code for calling resend otp service
    resendOTP: function(type) {
        scope = this;
        var newUserManager = applicationManager.getNewUserBusinessManager();
        var params = {
            "userName": "JYOTHIVIJU",
        };
        var authManager = applicationManager.getAuthManager();
        authManager.resendOTP(params, scope.resendOTPSuccess, scope.resendOTPFailure);
        // newUserManager.sendOTP(params,scope.sendOTPSuccess, scope.sendOTPFailure);
    },
    resendOTPSuccess: function(response) {
        applicationManager.getPresentationUtility().dismissLoadingScreen();
        var controller = applicationManager.getPresentationUtility().getController('frmDevRegSecCode', true);
        controller.showToast();
    },
    //code for validating otp after user clicks verify btn in otp screen
    validateOTP: function(otp) {
        scope = this;
        var validationUtilManager = applicationManager.getValidationUtilManager();
        if (validationUtilManager.isValidOTP(otp)) {
            var newUserManager = applicationManager.getNewUserBusinessManager();
            //var username = "";
            var verifyOTPJSON = {
                "otp": otp,
                "userName": "JYOTHIVIJU",
                "deviceInfo": "",
                "screenFlow": "device registration"
            };
            var authManager = applicationManager.getAuthManager();
            authManager.verifyOTP(verifyOTPJSON, scope.validateOTPSuccess, scope.validateOTPFailure);
        } else {
            applicationManager.getPresentationUtility().dismissLoadingScreen();
            var controller = applicationManager.getPresentationUtility().getController('frmDevRegSecCodeController', true);
            var errormsg = applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.common.enterSecurityCode");
            controller.bindGenericError(errormsg);
        }
    },
    //validates otp,if its success then it populates account list and user list and navigate to account selection list screen
    validateOTPSuccess: function(response) {
        scope = this;
        // enrollPresentationScope.logger.log("####  OTP validation success call back ####");
        applicationManager.getPresentationUtility().dismissLoadingScreen();
        if (response.isSuccessful && response.isSuccessful !== "") {
            if (response.isSuccessful === "true") {
                this.updateDeviceRegistration();
            } else {
                var controller = applicationManager.getPresentationUtility().getController('frmDevRegSecCodeController', true);
                var errorMsg = applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.common.invalidSecurityCode");
                controller.bindGenericError(errorMsg);
            }
        }
    },
    /* For Forgot Module */
    naviagteToDOB: function(firstName, lastName) {
        var newUserManager = applicationManager.getNewUserBusinessManager();
        newUserManager.setEnrollAttribute("userlastname", lastName);
        newUserManager.setEnrollAttribute("userfirstname", firstName);
        this.authManger.setForgotAttribute("userlastname", lastName);
        this.commonFunctionForNavigation("frmForgotEnterDOB");
    },
    validateDOB: function(dob) {
        var validationManager = applicationManager.getValidationUtilManager();
        var res = validationManager.isDOBValid(dob);
        if (res === true) {
            var newUserManager = applicationManager.getNewUserBusinessManager();
            newUserManager.setEnrollAttribute("dateOfBirth", dob);
            this.authManger.setForgotAttribute("dateOfBirth", dob);
            this.commonFunctionForNavigation("frmForgotEnterSSN");
        } else {
            var controller = applicationManager.getPresentationUtility().getController('frmForgotEnterDOB', true);
            controller.bindViewError(applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.common.validDOB"));
        }
    },
    validateSSN: function(ssn) {
        var validationManager = applicationManager.getValidationUtilManager();
        var res = validationManager.isValidSSNNumber(SSN);
        if (res === true) {
            this.authManger.setForgotAttribute("ssn", SSN);
            this.commonFunctionForNavigation("frmForgotEnterDOB");
        } else {
            var controller = applicationManager.getPresentationUtility().getController('frmForgotEnterSSN', true);
            controller.bindViewError(applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.enroll.enterSSN"));
        }
    },
    // to change password 
    updatePassword: function(newpassword) {
        applicationManager.getPresentationUtility().showLoadingScreen();
        this.showDownTimeMessage();
        var userManager = applicationManager.getUserPreferencesManager();
        var navManager = applicationManager.getNavigationManager();
        var userData = navManager.getCustomInfo("accPrimLinkData");
        var params = {
            "userName": userData[0].lblAccNameValue,
            "newUserPassword": newpassword,
            "accountNumber": userData[0].accNumHidden
        };
        userManager.updateUserPassword(params, scope_AuthPresenter.presentationUpdatePasswordSuccess, scope_AuthPresenter.presentationUpdatePasswordError);
    },
    presentationUpdatePasswordSuccess: function(resSuccess) {
        var navManager = applicationManager.getNavigationManager();
        var userData = navManager.getCustomInfo("accPrimLinkData");
        if (resSuccess.isSuccessful === "true") {
            var username = userData[0].lblAccNameValue;
            scope_AuthPresenter.navigateToLoginAfterPasswordUpdate(username);
        }
    },
    navigateToLoginAfterPasswordUpdate: function(UserName) {
        var navManager = applicationManager.getNavigationManager();
        if (UserName && (UserName !== undefined || UserName !== "")) {
            var loginData = navManager.getCustomInfo("frmLogin");
            loginData.usernameFromForgotUsername = UserName;
            loginData.showPasswordUpdatedSuccessMessage = true;
            navManager.setCustomInfo("frmLogin", loginData);
        }
        applicationManager.getPresentationUtility().dismissLoadingScreen();
        scope_AuthPresenter.commonFunctionForNavigation("frmLogin");
    },
    showDownTimeMessage: function() {
        var authManager = applicationManager.getAuthManager();
        authManager.getDownTimeMessage(this.downTimesuccessCB, this.downTimeErrorCB);
    },
    downTimesuccessCB: function(resp) {
        var navMan = applicationManager.getNavigationManager();
        navMan.setCustomInfo("frmLoginDownTimeMessage", resp);
        var userObj = applicationManager.getUserPreferencesManager();
        var loginFlag = userObj.isUserLoggedin();
        var controller = applicationManager.getPresentationUtility().getController('frmLogin', true);
        controller.showdownTimeMessage(loginFlag);
        // navMan.navigateTo("frmLogin");
    },
    downTimeErrorCB: function(resErr) {
        applicationManager.getPresentationUtility().dismissLoadingScreen();
        if (resErr["isServerUnreachable"]) applicationManager.getPresentationInterruptHandler().showErrorMessage("preLogin", resErr);
    },
    Auth_PresentationController: function() {
        scope_AuthPresenter = this;
        kony.mvc.Presentation.BasePresenter.call(this);
        this.asyncManager = new AsyncManager();
        this.authManger = applicationManager.getAuthManager();
        this.logger = applicationManager.getLoggerManager();
        this.dmManager = applicationManager.getDirectMarketingManager();
        this.flowType = "";
        this.currentAuthMode = "";
        this.checkAppinit = false;
        this.cardsDataForCvv = {};
        this.usernameRules = [];
        this.passwordRules = [];
        this.loginAttempt = 0;
        /**   numberOfAsyncForLogin
         *  1.getUser
         *  2.getRefreshAccountsFromDB
         *  3.getDeviceRegistration
         *  4.getAccountsPostLogin
         *  5.getPFMPieChartData
         *  6.getPFMBarGraphData
         *  7.getAllEntitlements
         */
        this.numberOfAsyncForLogin = 2;
    },
    //       inheritsFrom(Auth_PresentationController, kony.mvc.Presentation.BasePresenter);
    //    initializePresentationController :function() {},
    navigateToForgetFlow: function() {
        applicationManager.getPresentationUtility().showLoadingScreen();
        var authMode = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
        authMode.presentationController.checkAppinit = true;
        var controller = applicationManager.getPresentationUtility().getController('frmLogin', true);
        authMode.presentationController.forgotNavigation(controller.usernameText);
    },
    ContactHandler: function(response) {
        if (response === true) {
            var number = "7043750183";
            kony.phone.dial(number);
        } else {
            var controller = applicationManager.getPresentationUtility().getController('frmLogin', true);
            controller.clearUnameAndPwdForFrozenAccnt();
        }
    },
    handleInternetError: function() {
        var controller = applicationManager.getPresentationUtility().getController('frmLogin', true);
        controller.clearUserNameAndPwd();
    },
    onLogin: function(UsernamePasswordJSON, formContext) {
        applicationManager.getPresentationUtility().showLoadingScreen();
        var validationManager = applicationManager.getValidationUtilManager();
        var navManager = applicationManager.getNavigationManager();
        this.Auth_PresentationController.UsernamePasswordJSON = UsernamePasswordJSON;
        if (validationManager.isValidUserName(UsernamePasswordJSON.username) && validationManager.isValidPassword(UsernamePasswordJSON.password)) {
            // var userPreferencesManager = applicationManager.getUserPreferencesManager();
            // if (userPreferencesManager.isNewUser(UsernamePasswordJSON.username))
            //     userPreferencesManager.clearUserData();
            var authManger = applicationManager.getAuthManager();
            authManger.login(UsernamePasswordJSON, this.presentationLoginSuccess.bind(formContext), this.presentationLoginError);
        } else {
            applicationManager.getPresentationUtility().dismissLoadingScreen();
            var controller = applicationManager.getPresentationUtility().getController('frmLogin', true);
            controller.bindLoginErrorMessage(applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.Invalid.Username.or.Password"));
        }
    },
    presentationLoginSuccess: function(resSuccess) {
        applicationManager.getPresentationUtility().dismissLoadingScreen();
        applicationManager.getPresentationUtility().showLoadingScreen();
        var userPreferencesManager = applicationManager.getUserPreferencesManager();
        var configManager = applicationManager.getConfigurationManager();
        var navManager = applicationManager.getNavigationManager();
        var loggerManager = applicationManager.getLoggerManager();
        var userMan = applicationManager.getUserPreferencesManager();
        var userName = userMan.getUserName();
        loggerManager.setUserID(userName);
        userPreferencesManager.isLoggedIn = true;
        kony.application.registerForIdleTimeout(10, scope_AuthPresenter.idleIimeOutCallback);
        if (scope_AuthPresenter.currentAuthMode == "password") {
            scope_AuthPresenter.currentAuthMode = "";
            if (userPreferencesManager.isNewUser(this.Auth_PresentationController.UsernamePasswordJSON.username)) userPreferencesManager.clearUserData(this);
        }
        var tempLoginData = navManager.getCustomInfo("frmLogin");
        scope_AuthPresenter.setRememberMeFlag(tempLoginData.isRememberMeOn);
        if (this.Auth_PresentationController.UsernamePasswordJSON) {
            userPreferencesManager.saveUserName(this.Auth_PresentationController.UsernamePasswordJSON.username);
            userPreferencesManager.savePassword(this.Auth_PresentationController.UsernamePasswordJSON.password);
            userPreferencesManager.savetempUserName(this.Auth_PresentationController.UsernamePasswordJSON.username);
        }
        scope_AuthPresenter.isLoginSuccess();
        var regManager = applicationManager.getRegistrationManager();
        regManager.registerForPushNotifications();
    }
});