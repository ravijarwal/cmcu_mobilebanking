define("AuthModule/userfrmDevRegLandingController", {
    init: function() {
        var navManager = applicationManager.getNavigationManager();
        var currentForm = navManager.getCurrentForm();
        applicationManager.getPresentationFormUtility().initCommonActions(this, "YES", currentForm);
    },
    preShow: function() {
        if (kony.os.deviceInfo().name !== "iPhone") {
            this.view.flxHeader.isVisible = true;
        } else this.view.flxHeader.isVisible = false;
        var navManager = applicationManager.getNavigationManager();
        var currentForm = navManager.getCurrentForm();
        applicationManager.getPresentationFormUtility().logFormName(currentForm);
        applicationManager.getPresentationUtility().dismissLoadingScreen();
        this.view.btnNoThanks.onClick = this.onNoThanksClick.bind(this);
        this.view.btnRegNow.onClick = this.regNow.bind(this);
        this.setDetails();
    },
    onNoThanksClick: function() {
        applicationManager.getPresentationUtility().showLoadingScreen();
        var authMode = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
        authMode.presentationController.defaultLoginToAccounts();
        authMode.presentationController.setDeviceRegisterflag(false);
    },
    setDetails: function() {
        var authMode = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
        var Phoneno = authMode.presentationController.getMobileNo();
        this.view.lblMobNoValue.text = applicationManager.getDataProcessorUtility().maskAccountNumber(Phoneno);
        var userEmail = authMode.presentationController.getEmail();
        this.view.lblEmailIdValue.text = "ravijarwal10@gmail.com"; //applicationManager.getDataProcessorUtility().maskAccountEmail(userEmail);
    },
    regNow: function() {
        var authMode = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
        applicationManager.getPresentationUtility().showLoadingScreen();
        var destination;
        var type;
        var mailText = this.view.lblEmailIdValue.text;
        var phoneText = this.view.lblMobNoValue.text;
        var navManager = applicationManager.getNavigationManager();
        if (mailText !== undefined && mailText !== ' ') {
            destination = mailText;
            type = "m";
        } else if (phoneText !== undefined && phoneText !== ' ') {
            destination = phoneText;
            type = "t";
        }
        var otpInfo = {
            "dest": destination,
            "type": type
        };
        navManager.setCustomInfo("frmDevRegLanding", otpInfo);
        authMode.presentationController.invokeIdentity();
    }
});
define("AuthModule/frmDevRegLandingControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** init defined for frmDevRegLanding **/
    AS_Form_edc932272eae4b4d8b357d4bf53d518f: function AS_Form_edc932272eae4b4d8b357d4bf53d518f(eventobject) {
        var self = this;
        this.init();
    },
    /** preShow defined for frmDevRegLanding **/
    AS_Form_dcf023c0b2754db8929b53a224e6e948: function AS_Form_dcf023c0b2754db8929b53a224e6e948(eventobject) {
        var self = this;
        this.preShow();
    }
});
define("AuthModule/frmDevRegLandingController", ["AuthModule/userfrmDevRegLandingController", "AuthModule/frmDevRegLandingControllerActions"], function() {
    var controller = require("AuthModule/userfrmDevRegLandingController");
    var controllerActions = ["AuthModule/frmDevRegLandingControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
