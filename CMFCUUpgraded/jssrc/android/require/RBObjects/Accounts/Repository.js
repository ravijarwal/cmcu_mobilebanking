define([], function() {
    var BaseRepository = kony.mvc.Data.BaseRepository;
    //Create the Repository Class
    function AccountsRepository(modelDefinition, config, defaultAppMode, dataSourceFactory, injectedDataSource) {
        BaseRepository.call(this, modelDefinition, config, defaultAppMode, dataSourceFactory, injectedDataSource);
    };
    //Setting BaseRepository as Parent to this Repository
    AccountsRepository.prototype = Object.create(BaseRepository.prototype);
    AccountsRepository.prototype.constructor = AccountsRepository;
    //For Operation 'getAllAccounts' with service id 'getAllAccounts8755'
    AccountsRepository.prototype.getAllAccounts = function(params, onCompletion) {
        return AccountsRepository.prototype.customVerb('getAllAccounts', params, onCompletion);
    };
    //For Operation 'getCustomerAccounts' with service id 'getCustomerAccounts7724'
    AccountsRepository.prototype.getCustomerAccounts = function(params, onCompletion) {
        return AccountsRepository.prototype.customVerb('getCustomerAccounts', params, onCompletion);
    };
    //For Operation 'getOrganizationAccounts' with service id 'getOrganisationAccounts7215'
    AccountsRepository.prototype.getOrganizationAccounts = function(params, onCompletion) {
        return AccountsRepository.prototype.customVerb('getOrganizationAccounts', params, onCompletion);
    };
    //For Operation 'newAccountOpening' with service id 'newAccountOpening2573'
    AccountsRepository.prototype.newAccountOpening = function(params, onCompletion) {
        return AccountsRepository.prototype.customVerb('newAccountOpening', params, onCompletion);
    };
    //For Operation 'updateUserAccountSettingsForAdmin' with service id 'UpdateUserAccountSettingsForAdmin4569'
    AccountsRepository.prototype.updateUserAccountSettingsForAdmin = function(params, onCompletion) {
        return AccountsRepository.prototype.customVerb('updateUserAccountSettingsForAdmin', params, onCompletion);
    };
    //For Operation 'getAccountsForDashboardLooping' with service id 'getAccountsDashboardLoopingConnector6767'
    AccountsRepository.prototype.getAccountsForDashboardLooping = function(params, onCompletion) {
        return AccountsRepository.prototype.customVerb('getAccountsForDashboardLooping', params, onCompletion);
    };
    //For Operation 'updateShareNickName' with service id 'updateShareNickname4109'
    AccountsRepository.prototype.updateShareNickName = function(params, onCompletion) {
        return AccountsRepository.prototype.customVerb('updateShareNickName', params, onCompletion);
    };
    //For Operation 'getPreferenceList' with service id 'getPreferenceAccessList6431'
    AccountsRepository.prototype.getPreferenceList = function(params, onCompletion) {
        return AccountsRepository.prototype.customVerb('getPreferenceList', params, onCompletion);
    };
    //For Operation 'updateAccountPreference' with service id 'updateAccountPreference6645'
    AccountsRepository.prototype.updateAccountPreference = function(params, onCompletion) {
        return AccountsRepository.prototype.customVerb('updateAccountPreference', params, onCompletion);
    };
    //For Operation 'fetchBankDetails' with service id 'fetchBankDetails9439'
    AccountsRepository.prototype.fetchBankDetails = function(params, onCompletion) {
        return AccountsRepository.prototype.customVerb('fetchBankDetails', params, onCompletion);
    };
    //For Operation 'updateFavouriteStatus' with service id 'updateFavouriteStatus8642'
    AccountsRepository.prototype.updateFavouriteStatus = function(params, onCompletion) {
        return AccountsRepository.prototype.customVerb('updateFavouriteStatus', params, onCompletion);
    };
    //For Operation 'getRecentAccounts' with service id 'getRecentAccounts9643'
    AccountsRepository.prototype.getRecentAccounts = function(params, onCompletion) {
        return AccountsRepository.prototype.customVerb('getRecentAccounts', params, onCompletion);
    };
    //For Operation 'updateAccountPhoneNumber' with service id 'updateAccountPhoneNumber9695'
    AccountsRepository.prototype.updateAccountPhoneNumber = function(params, onCompletion) {
        return AccountsRepository.prototype.customVerb('updateAccountPhoneNumber', params, onCompletion);
    };
    //For Operation 'getAccountsPostLogin' with service id 'getAccountSelectFields1549'
    AccountsRepository.prototype.getAccountsPostLogin = function(params, onCompletion) {
        return AccountsRepository.prototype.customVerb('getAccountsPostLogin', params, onCompletion);
    };
    //For Operation 'updateUserAccountSettings' with service id 'updateUserAccountSettings9732'
    AccountsRepository.prototype.updateUserAccountSettings = function(params, onCompletion) {
        return AccountsRepository.prototype.customVerb('updateUserAccountSettings', params, onCompletion);
    };
    //For Operation 'updateLoanNickName' with service id 'updateLoanNickname2609'
    AccountsRepository.prototype.updateLoanNickName = function(params, onCompletion) {
        return AccountsRepository.prototype.customVerb('updateLoanNickName', params, onCompletion);
    };
    //For Operation 'unLinkOrgAccounts' with service id 'unLinkOrgAccounts8710'
    AccountsRepository.prototype.unLinkOrgAccounts = function(params, onCompletion) {
        return AccountsRepository.prototype.customVerb('unLinkOrgAccounts', params, onCompletion);
    };
    //For Operation 'getAccountsForAdmin' with service id 'GetAccountsForAdmin7035'
    AccountsRepository.prototype.getAccountsForAdmin = function(params, onCompletion) {
        return AccountsRepository.prototype.customVerb('getAccountsForAdmin', params, onCompletion);
    };
    //For Operation 'getAllAccountsForDashboard' with service id 'getDashboardAccounts8016'
    AccountsRepository.prototype.getAllAccountsForDashboard = function(params, onCompletion) {
        return AccountsRepository.prototype.customVerb('getAllAccountsForDashboard', params, onCompletion);
    };
    //For Operation 'getMembershipAccounts' with service id 'getAccountsbyTINorMembership9871'
    AccountsRepository.prototype.getMembershipAccounts = function(params, onCompletion) {
        return AccountsRepository.prototype.customVerb('getMembershipAccounts', params, onCompletion);
    };
    return AccountsRepository;
})