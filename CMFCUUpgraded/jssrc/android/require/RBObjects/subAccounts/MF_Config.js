define([], function() {
    var mappings = {
        "accountNumber": "accountNumber",
        "idType": "idType",
        "recordType": "recordType",
        "id": "id",
    };
    Object.freeze(mappings);
    var typings = {
        "accountNumber": "string",
        "idType": "string",
        "recordType": "string",
        "id": "string",
    }
    Object.freeze(typings);
    var primaryKeys = ["id", ];
    Object.freeze(primaryKeys);
    var config = {
        mappings: mappings,
        typings: typings,
        primaryKeys: primaryKeys,
        serviceName: "RBObjects",
        tableName: "subAccounts"
    };
    Object.freeze(config);
    return config;
})