define("flxAccPrimaryLinking", function() {
    return function(controller) {
        var flxAccPrimaryLinking = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "focusSkin": "f9f9",
            "height": "65dp",
            "id": "flxAccPrimaryLinking",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "f9f9",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxAccPrimaryLinking.setDefaultUnit(kony.flex.DP);
        var lblAccNum = new kony.ui.Label({
            "id": "lblAccNum",
            "isVisible": true,
            "left": "5%",
            "maxNumberOfLines": 1,
            "skin": "sknLbl424242SSP26px",
            "text": "1234567890",
            "textStyle": {
                "letterSpacing": 0,
                "strikeThrough": false
            },
            "textTruncatePosition": constants.TEXT_TRUNCATE_END,
            "top": "5%",
            "width": "55%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "textCopyable": false
        });
        var flxAccDetails = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": 0,
            "clipBounds": false,
            "height": "15dp",
            "id": "flxAccDetails",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "5%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "2%",
            "width": "90%",
            "zIndex": 1
        }, {}, {});
        flxAccDetails.setDefaultUnit(kony.flex.DP);
        var lblAccName = new kony.ui.Label({
            "bottom": "0dp",
            "id": "lblAccName",
            "isVisible": true,
            "left": "1dp",
            "skin": "sknLblBlack98",
            "text": kony.i18n.getLocalizedString("kony.mb.enroll.primary"),
            "textStyle": {
                "letterSpacing": 0,
                "strikeThrough": false
            },
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "textCopyable": false
        });
        var lblAccNameValue = new kony.ui.Label({
            "centerY": "50.00%",
            "id": "lblAccNameValue",
            "isVisible": true,
            "left": "4dp",
            "skin": "sknLblBlack98",
            "text": "David Cooper",
            "textStyle": {
                "letterSpacing": 0,
                "strikeThrough": false
            },
            "width": "60%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "textCopyable": false
        });
        var flxImgIndicator = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "100%",
            "id": "flxImgIndicator",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": 0,
            "isModalContainer": false,
            "right": "10%",
            "skin": "slFbox",
            "top": 0,
            "width": "8%",
            "zIndex": 1
        }, {}, {});
        flxImgIndicator.setDefaultUnit(kony.flex.DP);
        var imgIndicator = new kony.ui.Image2({
            "bottom": "0dp",
            "height": "100%",
            "id": "imgIndicator",
            "isVisible": true,
            "right": "0dp",
            "skin": "slImage",
            "src": "radio_inactive.png",
            "width": "100%",
            "zIndex": 1
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxImgIndicator.add(imgIndicator);
        flxAccDetails.add(lblAccName, lblAccNameValue, flxImgIndicator);
        var flxAccTypeDetails = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": 0,
            "clipBounds": false,
            "height": "15dp",
            "id": "flxAccTypeDetails",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "5.00%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "2%",
            "width": "90%",
            "zIndex": 1
        }, {}, {});
        flxAccTypeDetails.setDefaultUnit(kony.flex.DP);
        var lblAccType = new kony.ui.Label({
            "bottom": "0dp",
            "id": "lblAccType",
            "isVisible": true,
            "left": "1dp",
            "skin": "sknLblBlack98",
            "text": kony.i18n.getLocalizedString("kony.mb.enroll.accType"),
            "textStyle": {
                "letterSpacing": 0,
                "strikeThrough": false
            },
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "textCopyable": false
        });
        var lblAccTypeValue = new kony.ui.Label({
            "centerY": "50.00%",
            "id": "lblAccTypeValue",
            "isVisible": true,
            "left": "4dp",
            "skin": "sknLblBlack98",
            "text": "Business",
            "textStyle": {
                "letterSpacing": 0,
                "strikeThrough": false
            },
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "textCopyable": false
        });
        flxAccTypeDetails.add(lblAccType, lblAccTypeValue);
        var flxSeparator = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0%",
            "clipBounds": true,
            "height": "1dp",
            "id": "flxSeparator",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0%",
            "isModalContainer": false,
            "skin": "sknFlxSeprtrGrey",
            "top": "10%",
            "width": "100%",
            "zIndex": 1
        }, {}, {});
        flxSeparator.setDefaultUnit(kony.flex.DP);
        flxSeparator.add();
        flxAccPrimaryLinking.add(lblAccNum, flxAccDetails, flxAccTypeDetails, flxSeparator);
        return flxAccPrimaryLinking;
    }
})