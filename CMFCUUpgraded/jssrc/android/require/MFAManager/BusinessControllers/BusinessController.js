define([], function() {
    function MFAManager() {
        kony.mvc.Business.Controller.call(this);
        scope_MFAManager = this;
        this.MFAResponse = "";
        this.flowType = "";
        this.servicekey = "";
        this.MFAType = "";
        this.communicationType = "";
        this.serviceId = "";
        this.operationType = "";
    }
    inheritsFrom(MFAManager, kony.mvc.Business.Controller);
    MFAManager.prototype.initMFAFlow = function(mfaJSON) {
        scope_MFAManager.setMFAResponse(mfaJSON.response);
        scope_MFAManager.setMFAFlowType(mfaJSON.flowType);
        scope_MFAManager.setServicekey(mfaJSON.response.MFAAttributes.serviceKey);
        scope_MFAManager.setMFAType(mfaJSON.response.MFAAttributes.MFAType);
        scope_MFAManager.setCommunicationType(mfaJSON.response.MFAAttributes.communicationType);
        applicationManager.getPresentationUtility().MFA.navigateBasedOnMFAType();
    };
    MFAManager.prototype.setMFAResponse = function(MFAResponse) {
        this.MFAResponse = MFAResponse;
    };
    MFAManager.prototype.getMFAResponse = function() {
        return this.MFAResponse;
    };
    MFAManager.prototype.setMFAFlowType = function(flowType) {
        this.flowType = flowType;
    };
    MFAManager.prototype.getMFAFlowType = function() {
        return this.flowType;
    };
    MFAManager.prototype.setMFAOperationType = function(operationType) {
        this.operationType = operationType;
    };
    MFAManager.prototype.getMFAOperationType = function() {
        return this.flowType;
    };
    MFAManager.prototype.setServicekey = function(servicekey) {
        this.servicekey = servicekey;
    };
    MFAManager.prototype.getServicekey = function() {
        return this.servicekey;
    };
    MFAManager.prototype.setMFAType = function(MFAType) {
        this.MFAType = MFAType;
    };
    MFAManager.prototype.getMFAType = function() {
        return this.MFAType;
    };
    MFAManager.prototype.setCommunicationType = function(communicationType) {
        this.communicationType = communicationType;
    };
    MFAManager.prototype.getCommunicationType = function() {
        return this.communicationType;
    };
    MFAManager.prototype.setServiceId = function(serviceId) {
        this.serviceId = serviceId;
    };
    MFAManager.prototype.getServiceId = function(serviceId) {
        return this.serviceId;
    };
    MFAManager.prototype.requestOTP = function(params) {
        var transactionManager = applicationManager.getTransactionManager();
        if (this.flowType === "BULK_BILL_PAY") {
            applicationManager.getTransactionManager().createBulkBillPayPayement(params, this.requestOTPSuccess.bind(this), this.requestOTPFailure.bind(this));
        } else if (this.operationType === "CREATE") {
            transactionManager.createTransaction(params, this.requestOTPSuccess.bind(this), this.requestOTPFailure.bind(this));
        } else if (this.operationType === "UPDATE") {
            transactionManager.updateTransaction(params, this.requestOTPSuccess.bind(this), this.requestOTPFailure.bind(this));
        }
    };
    MFAManager.prototype.requestOTPSuccess = function(response) {
        this.setMFAResponse(response);
        this.MFAResponse.MFAAttributes.remainingResendAttempts = response.MFAAttributes.remainingResendAttempts;
        this.MFAResponse.MFAAttributes.securityKey = response.MFAAttributes.securityKey;
        applicationManager.getPresentationUtility().MFA.showMFAOTPScreen();
    };
    MFAManager.prototype.requestOTPFailure = function(error) {
        applicationManager.getPresentationUtility().MFA.mfaOTPError(error.serverErrorRes);
    };
    MFAManager.prototype.verifyOTP = function(params) {
        var transactionManager = applicationManager.getTransactionManager();
        if (this.flowType === "BULK_BILL_PAY") {
            transactionManager.createBulkBillPayPayement(params, this.verifyOTPSuccess.bind(this), this.verifyOTPFailure.bind(this));
        } else if (this.operationType === "CREATE") {
            transactionManager.createTransaction(params, this.verifyOTPSuccess.bind(this), this.verifyOTPFailure.bind(this));
        } else if (this.operationType === "UPDATE") {
            transactionManager.updateTransaction(params, this.verifyOTPSuccess.bind(this), this.verifyOTPFailure.bind(this));
        }
    };
    MFAManager.prototype.verifyOTPSuccess = function(response) {
        if (response.MFAAttributes) {
            if (response.MFAAttributes.securityKey) {
                this.MFAResponse.MFAAttributes.remainingResendAttempts = response.MFAAttributes.remainingResendAttempts;
                this.MFAResponse.MFAAttributes.securityKey = response.MFAAttributes.securityKey;
                this.MFAResponse.MFAAttributes.communicationType = this.getCommunicationType();
                this.MFAResponse.MFAAttributes.isOTPExpired = false;
            } else if (response.MFAAttributes.isOTPExpired) {
                this.MFAResponse.MFAAttributes.remainingResendAttempts = response.MFAAttributes.remainingResendAttempts;
                this.MFAResponse.MFAAttributes.isOTPExpired = response.MFAAttributes.isOTPExpired;
                this.MFAResponse.MFAAttributes.communicationType = this.getCommunicationType();
            }
            applicationManager.getPresentationUtility().MFA.setSecureCodeScreen(response);
        } else {
            applicationManager.getPresentationUtility().MFA.navigateToAckScreen(response);
        }
    };
    MFAManager.prototype.isTransactionalError = function(error) {
        if (error.serverErrorRes.dbpErrCode) {
            return false;
        } else {
            return true;
        }
    };
    MFAManager.prototype.verifyOTPFailure = function(error) {
        if (this.isTransactionalError(error)) {
            applicationManager.getPresentationUtility().MFA.navigateToTransactionScreen(error);
        } else {
            applicationManager.getPresentationUtility().MFA.enteredIncorrectOTP(error.serverErrorRes);
        }
    };
    MFAManager.prototype.verifySecurityQuestions = function(params) {
        var transactionManager = applicationManager.getTransactionManager();
        if (this.flowType === "BULK_BILL_PAY") {
            applicationManager.getTransactionManager().createBulkBillPayPayement(params, this.verifyAnswersSuccess.bind(this), this.verifyAnswersFailure.bind(this));
        } else if (this.operationType === "CREATE") {
            transactionManager.createTransaction(params, this.verifyAnswersSuccess.bind(this), this.verifyAnswersFailure.bind(this));
        } else if (this.operationType === "UPDATE") {
            transactionManager.updateTransaction(params, this.verifyAnswersSuccess.bind(this), this.verifyAnswersFailure.bind(this));
        }
    };
    MFAManager.prototype.verifyAnswersSuccess = function(response) {
        if (response.MFAAttributes) {
            this.setMFAResponse(response);
            applicationManager.getPresentationUtility().MFA.navigateToSecurityQuestion();
        } else {
            applicationManager.getPresentationUtility().MFA.navigateToAckScreen(response);
        }
    };
    MFAManager.prototype.verifyAnswersFailure = function(error) {
        if (this.isTransactionalError(error)) {
            applicationManager.getPresentationUtility().MFA.navigateToTransactionScreen(error);
        } else {
            applicationManager.getPresentationUtility().MFA.enteredIncorrectAnswer(error.serverErrorRes);
        }
    };
    return MFAManager;
});