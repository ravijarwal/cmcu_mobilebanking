define({
    fetchAccountPendingTransactions: function(criteria, presentationSuccessCallback, presentationErrorCallback) {
        var  pendTran  =  kony.mvc.MDAApplication.getSharedInstance().getRepoManager().getRepository("Transactions");
        pendTran.customVerb("getSharesPendingPostedtrans", criteria, getAllCompletionCallback);

        function  getAllCompletionCallback(status,  data,  error) {
            var srh = applicationManager.getServiceResponseHandler();
            var obj = srh.manageResponse(status,  data,  error);
            if (obj["status"] === true) {
                presentationSuccessCallback(obj["data"]);
            } else {
                presentationErrorCallback(obj["errmsg"]);
            }
        }
    },
    //this method is for loan accountType
    fetchLoanAccountTransactions: function(criteria, presentationSuccessCallback, presentationErrorCallback) {
        var  pendTran  =  kony.mvc.MDAApplication.getSharedInstance().getRepoManager().getRepository("Transactions");
        pendTran.customVerb("getLoanAccountPendingPostedtrans", criteria, getAllCompletionCallback);

        function  getAllCompletionCallback(status,  data,  error) {
            var srh = applicationManager.getServiceResponseHandler();
            var obj = srh.manageResponse(status,  data,  error);
            if (obj["status"] === true) {
                presentationSuccessCallback(obj["data"]);
            } else {
                presentationErrorCallback(obj["errmsg"]);
            }
        }
    },
    /*
    This method is used to create an internal transfer

    */
    createTransaction: function(criteria, presentationSuccessCallback, presentationErrorCallback) {
        var  transacObj  =  kony.mvc.MDAApplication.getSharedInstance().getRepoManager().getRepository("Transactions");
        transacObj.customVerb("InternalTransfer", criteria, saveCompletionCallback);
        // var  transacObj  =  kony.mvc.MDAApplication.getSharedInstance().getRepoManager().getRepository("Transactions");
        // var transObj = this.convertDateFormat(tranObj);
        function  saveCompletionCallback(status,  data,  error) {
            var srh = applicationManager.getServiceResponseHandler();
            var obj = srh.manageResponse(status,  data,  error);
            if (obj["status"] === true) {
                presentationSuccessCallback(obj["data"]);
            } else {
                presentationErrorCallback(obj["errmsg"]);
            }
        }
    },
    getDonorReceipientAccountsList: function(PSCBFordonorRecipient, PECBFordonorRecipient) {
        var  transacObj  =  kony.mvc.MDAApplication.getSharedInstance().getRepoManager().getRepository("Transactions");
        transacObj.customVerb("getDonorReceipientAccounts", {}, saveCompletionCallback);
        // var  transacObj  =  kony.mvc.MDAApplication.getSharedInstance().getRepoManager().getRepository("Transactions");
        // var transObj = this.convertDateFormat(tranObj);
        function  saveCompletionCallback(status,  data,  error) {
            // alert("data"+JSON.stringify(data));
            var srh = applicationManager.getServiceResponseHandler();
            var obj = srh.manageResponse(status,  data,  error);
            if (obj["status"] === true) {
                PSCBFordonorRecipient(obj["data"]);
            } else {
                PECBFordonorRecipient(obj["errmsg"]);
            }
        }
    },
    fetchUserScheduledTransactions: function(PSCBForSchedTrns, PECBForSchedTrns) {
        var   getUsrScheduledTransactions  =  kony.mvc.MDAApplication.getSharedInstance().getRepoManager().getRepository("Transactions");
        getUsrScheduledTransactions.customVerb("getScheduledTransactions", {}, getAllCompletionCallback);

        function  getAllCompletionCallback(status,  data,  error) {
            var srh = applicationManager.getServiceResponseHandler();
            var obj = srh.manageResponse(status,  data,  error);
            if (obj["status"] === true) {
                PSCBForSchedTrns(obj["data"]);
            } else {
                PECBForSchedTrns(obj["errmsg"]);
            }
        }
    },
    fetchLoanDataTransactions: function(criteria, presentationSuccessCallback, presentationErrorCallback) {
        var  intrestType  =  kony.mvc.MDAApplication.getSharedInstance().getRepoManager().getRepository("Transactions");
        intrestType.customVerb("getloanTypeSelectFields", criteria, getAllCompletionCallback);

        function  getAllCompletionCallback(status,  data,  error) {
            var srh = applicationManager.getServiceResponseHandler();
            var obj = srh.manageResponse(status,  data,  error);
            if (obj["status"] === true) {
                presentationSuccessCallback(obj["data"]);
            } else {
                presentationErrorCallback(obj["errmsg"]);
            }
        }
    },
    deleteTransaction: function(record, presentationSuccessCallback, presentationErrorCallback) {
        var transactionsRepo = kony.mvc.MDAApplication.getSharedInstance().getRepoManager().getRepository("Transactions");
        transactionsRepo.customVerb("updateShareTransfer", record, deleteCompletionCallback);

        function  deleteCompletionCallback(status,  data,  error) {
            var srh = applicationManager.getServiceResponseHandler();
            var obj = srh.manageResponse(status,  data,  error);
            if (obj["status"] === true) {
                presentationSuccessCallback(obj["data"]);
            } else {
                presentationErrorCallback(obj["errmsg"]);
            }
        }
    },
    deleteLoanScheduledTransaction: function(record, presentationSuccessCallback, presentationErrorCallback) {
        var transactionsRepo = kony.mvc.MDAApplication.getSharedInstance().getRepoManager().getRepository("Transactions");
        transactionsRepo.customVerb("updateLoanScheduleTransfer", record, deleteCompletionCallback);

        function  deleteCompletionCallback(status,  data,  error) {
            var srh = applicationManager.getServiceResponseHandler();
            var obj = srh.manageResponse(status,  data,  error);
            if (obj["status"] === true) {
                presentationSuccessCallback(obj["data"]);
            } else {
                presentationErrorCallback(obj["errmsg"]);
            }
        }
    },
    updateEftScheduledTransaction: function(record, presentationSuccessCallback, presentationErrorCallback) {
        var transactionsRepo = kony.mvc.MDAApplication.getSharedInstance().getRepoManager().getRepository("Transactions");
        transactionsRepo.customVerb("updateEftScheduledTransfer", record, updateCompletionCallback);

        function  updateCompletionCallback(status,  data,  error) {
            var srh = applicationManager.getServiceResponseHandler();
            var obj = srh.manageResponse(status,  data,  error);
            if (obj["status"] === true) {
                presentationSuccessCallback(obj["data"]);
            } else {
                presentationErrorCallback(obj["errmsg"]);
            }
        }
    },
    deleteEftScheduledTransaction: function(record, presentationSuccessCallback, presentationErrorCallback) {
        var transactionsRepo = kony.mvc.MDAApplication.getSharedInstance().getRepoManager().getRepository("Transactions");
        transactionsRepo.customVerb("deleteEftScheduledTransfer", record, updateCompletionCallback);

        function  updateCompletionCallback(status,  data,  error) {
            var srh = applicationManager.getServiceResponseHandler();
            var obj = srh.manageResponse(status,  data,  error);
            if (obj["status"] === true) {
                // alert("success resp");
                presentationSuccessCallback(obj["data"]);
            } else {
                presentationErrorCallback(obj["errmsg"]);
            }
        }
    },
    fetchPostedTransactions: function(searchOptions, presentationSuccessCallback, presentationErrorCallback) {
        var  getPostedUserTransactions  =  kony.mvc.MDAApplication.getSharedInstance().getRepoManager().getRepository("Transactions");
        getPostedUserTransactions.customVerb("getSearchShareFeilds", searchOptions, getAllCompletionCallback);

        function  getAllCompletionCallback(status,  data,  error) {
            var srh = applicationManager.getServiceResponseHandler();
            var obj = srh.manageResponse(status,  data,  error);
            if (obj["status"] === true) {
                presentationSuccessCallback(obj["data"]);
            } else {
                presentationErrorCallback(obj["errmsg"]);
            }
        }
    },
    fetchPostedLoanTransactions: function(searchOptions, presentationSuccessCallback, presentationErrorCallback) {
        var  getPostedUserTransactions  =  kony.mvc.MDAApplication.getSharedInstance().getRepoManager().getRepository("Transactions");
        getPostedUserTransactions.customVerb("getSearchLoanFeilds", searchOptions, getAllCompletionCallback);

        function  getAllCompletionCallback(status,  data,  error) {
            var srh = applicationManager.getServiceResponseHandler();
            var obj = srh.manageResponse(status,  data,  error);
            if (obj["status"] === true) {
                presentationSuccessCallback(obj["data"]);
            } else {
                presentationErrorCallback(obj["errmsg"]);
            }
        }
    }
});