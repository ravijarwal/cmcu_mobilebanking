define({
    getListViewIcon: function(type) {
        if (type === "ATM" || type.toLowerCase() === "atm") {
            //return "grouptwo.png";
            if (kony.os.deviceInfo().name === "iPhone") {
                return "atm@2x.png";
            } else {
                return "atm.png";
            }
        } else if (type === "BRANCH" || type === "MainBranch" || type.toLowerCase() === "branch") {
            // return "group.png";
            if (kony.os.deviceInfo().name === "iPhone") {
                return "branche@2x.png";
            } else {
                return "branche.png";
            }
        }
    },
    ProcessDataForView: function(locationList) {
        locateUsPresentationScope.logger.log("#### start LocateUs_PresentationController ProcessDataForView ####");
        locateUsPresentationScope.logger.log("#### data to be processed is " + JSON.stringify(locationList) + " ####");
        try {
            var tempLocaterData = [];
            var locatorResultSet = [];
            var LocationData = [];
            for (var i = 0; i < locationList.length; i++) {
                tempLocaterData = {
                    lat: locationList[i].latitude,
                    lon: locationList[i].longitude,
                    name: locationList[i].addressLine1, //informationTitle,//addressLine1
                    desc: locationList[i].addressLine2, //addressLine1,//addressLine2
                    locationId: locationList[i].locationId,
                    image: locateUsPresentationScope.getMapPinIcon(locationList[i].type),
                    city: locationList[i].city,
                    zipCode: locationList[i].zipCode,
                    phone: locationList[i].phone,
                    email: locationList[i].email,
                    services: locationList[i].services,
                    workingHours: locationList[i].workingHours,
                    type: locationList[i].type,
                    showcallout: false,
                    distanceLabel: this.getDistance(locationList[i].latitude, locationList[i].longitude) + " Miles",
                    status: locationList[i].status,
                    calloutStatus: {
                        "text": locationList[i].status,
                        "skin": locateUsPresentationScope.getCallOutStatusSkin(locationList[i].status)
                    },
                    listViewStatus: {
                        "text": locationList[i].status,
                        "skin": locateUsPresentationScope.getListViewStatusSkin(locationList[i].status)
                    },
                    listViewImage: locateUsPresentationScope.getListViewIcon(locationList[i].type)
                };
                LocationData.push(tempLocaterData);
                tempLocaterData = [];
            }
            var currentLocation = {
                lat: locateUsPresentationScope.currentLatitude,
                lon: locateUsPresentationScope.currentLongitude,
                image: locateUsPresentationScope.getMapPinIcon("currentLocation"),
                showcallout: false
            };
            LocationData.push(currentLocation);
            locateUsPresentationScope.logger.log("#### after data processed " + JSON.stringify(LocationData));
            return LocationData;
        } catch (e) {
            locateUsPresentationScope.logger.log("#### in catch " + JSON.stringify(e) + " ####");
        }
    },
    getDistance: function(lat, long) {
        var navMgr = applicationManager.getNavigationManager();
        var latLongObj = navMgr.getCustomInfo("locationLatLong");
        var currentLatitude = latLongObj.latitude;
        var currentLongitude = latLongObj.longitude;
        var Radius = 6378137; // Earth’s mean radius in meter
        var dLat = this.rad(lat - currentLatitude);
        var dLong = this.rad(long - currentLongitude);
        var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(this.rad(currentLatitude)) * Math.cos(this.rad(lat)) * Math.sin(dLong / 2) * Math.sin(dLong / 2);
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        var distance = Radius * c;
        //converting to miles
        distance = distance * 0.00062137;
        distance = Math.round(distance, 2);
        return distance; // returns the distance in meter
    },
    rad: function(x) {
        return x * Math.PI / 180;
    }
});