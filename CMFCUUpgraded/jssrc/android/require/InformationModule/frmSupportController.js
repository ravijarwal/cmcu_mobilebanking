define("InformationModule/userfrmSupportController", {
    preShow: function() {
        this.setSegmentData();
        this.initActions();
        this.view.customHeader.flxBack.onClick = this.backIcon;
        if (kony.os.deviceInfo().name === "iPhone") {
            this.view.flxHeader.isVisible  =  true;
        } else {
            this.view.flxHeader.isVisible  =  true;
        }
        //this.view.lblAppVersion.text= kony.i18n.getLocalizedString("kony.mb.Support.AppVersion")+" "+appConfig.appVersion;
        //  this.enableOrDisableHamburger();
        var navManager = applicationManager.getNavigationManager();
        var configManager = applicationManager.getConfigurationManager();
        var MenuHandler = applicationManager.getMenuHandler();
        MenuHandler.setUpHamburgerForForm(this, configManager.constants.MENUCONTACT);
        this.enableOrDisableHamburger();
        var currentForm = navManager.getCurrentForm();
        applicationManager.getPresentationFormUtility().logFormName(currentForm);
        var ContactUs = applicationManager.getLoggerManager();
        ContactUs.setCustomMetrics(this, false, "Support");
    },
    init: function() {
        var navManager = applicationManager.getNavigationManager();
        var currentForm = navManager.getCurrentForm();
        applicationManager.getPresentationFormUtility().initCommonActions(this, "YES", currentForm);
    },
    initActions: function() {
        var scope = this;
        this.view.btnCallBranch.onClick = function() {
            //applicationManager.getPresentationUtility().showLoadingScreen(); 
            // var infoCall = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("InformationModule");
            //infoCall.presentationController.onClickCallUs();     
            kony.phone.dial("7043750183");
        };
        this.view.btnMapIcon.onClick = this.openLocation;
        this.view.btnMail.onClick = this.openEmail;
        /*this.view.btnMail.onClick = function() {
          // applicationManager.getPresentationUtility().showLoadingScreen();        
          var cc = [""];
          var bcc = [""];
          var sub = " ";
          var msgbody = "";
          var to = ["info@cmcu.org"];
          kony.phone.openEmail(to, cc, bcc, sub, msgbody);       
        };*/
    },
    openEmail: function() {
        try {
            var to = ["info@cmcu.org"];
            var cc = [""];
            var bcc = [""];
            var sub = "";
            var msgbody = "";
            kony.phone.openEmail(to, cc, bcc, sub, msgbody, false, []);
        } catch (err) {
            kony.ui.Alert("error in opening Email:: " + err);
        }
    },
    openLocation: function() {
        var loginCntrlr = applicationManager.getPresentationUtility().getController('frmLogin', true);
        loginCntrlr.onLocateUSClick();
    },
    enableOrDisableHamburger: function() {
        var userObj = applicationManager.getUserPreferencesManager();
        var Login = userObj.isUserLoggedin();
        if (Login === true) {
            this.view.customHeader.flxBack.imgBack.src = "hamburger.png";
            if (kony.os.deviceInfo().name === "iPhone") {
                this.view.flxSupportMain.bottom = "60dp";
                this.view.flxFooter.isVisible = true;
            } else {
                this.view.flxSupportMain.bottom = "0dp";
                this.view.flxFooter.isVisible = false;
            }
        } else {
            var scope = this;
            this.view.flxFooter.isVisible = false;
            this.view.flxSupportMain.bottom = "0dp";
            this.view.customHeader.flxBack.imgBack.src = "back_icon.png";
            this.view.customHeader.flxBack.onClick = function() {
                scope.backIcon();
            };
        }
    },
    showDial: function(phoneNumber) {
        applicationManager.getPresentationUtility().dismissLoadingScreen();
        kony.phone.dial(phoneNumber);
    },
    backIcon: function() {
        if (kony.os.deviceInfo().name === "iPhone") {
            var userObj = applicationManager.getUserPreferencesManager();
            var Login = userObj.isUserLoggedin();
            if (Login === true) {
                var navManager = applicationManager.getNavigationManager();
                navManager.goBack();
            } else {
                var informationPC = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("InformationModule");
                informationPC.presentationController.commonFunctionForNavigation("frmLogin");
            }
        } else {
            var informationPC = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("InformationModule");
            informationPC.presentationController.commonFunctionForNavigation("frmLogin");
        }
    },
    setSegmentData: function() {
        var scope = this;
        var configManager = applicationManager.getConfigurationManager();
        var data = [{
            "imgArrow": "chevronright.png",
            "lblTitle": "Holidays"
        }, {
            "imgArrow": "chevronright.png",
            "lblTitle": configManager.constants.TERMS
        }, {
            "imgArrow": "chevronright.png",
            "lblTitle": configManager.constants.PRIVACY
        }];
        this.view.segSupport.setData(data);
        this.view.segSupport.onRowClick = function() {
            var selectedvalue = scope.view.segSupport.selectedItems[0].lblTitle;
            if (selectedvalue === configManager.constants.PRIVACY) {
                // applicationManager.getPresentationUtility().showLoadingScreen(); 
                // var informationPC = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("InformationModule");
                //informationPC.presentationController.onClickPrivacyPolicy(selectedvalue); 
                var navUrl = "https://www.cmcu.org/wp-content/uploads/2015/09/Privacy-Policy.pdf";
                kony.application.openURL(navUrl);
            } else if (selectedvalue === configManager.constants.TERMS) {
                // applicationManager.getPresentationUtility().showLoadingScreen();  
                // var informationPC = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("InformationModule");
                //informationPC.presentationController.onClickTermsAndConditions(selectedvalue);  
                var navUrl = "https://www.cmcu.org/terms-conditions/";
                kony.application.openURL(navUrl);
            } else if (selectedvalue === configManager.constants.ABOUT) {
                applicationManager.getPresentationUtility().showLoadingScreen();
                var informationPC = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("InformationModule");
                informationPC.presentationController.onClickAboutUs(selectedvalue);
            } else if (selectedvalue === configManager.constants.FAQ) {
                applicationManager.getPresentationUtility().showLoadingScreen();
                var informationPC = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("InformationModule");
                informationPC.presentationController.onClickFAQs(selectedvalue);
            }
        }
    }
});
define("InformationModule/frmSupportControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for undefined **/
    AS_BarButtonItem_bc8a949af94c4a469a52b4ee901c284e: function AS_BarButtonItem_bc8a949af94c4a469a52b4ee901c284e(eventobject) {
        var self = this;
        this.backIcon();
    },
    /** init defined for frmSupport **/
    AS_Form_hdedb5c748b04643a3fa32f841560020: function AS_Form_hdedb5c748b04643a3fa32f841560020(eventobject) {
        var self = this;
        this.init();
    },
    /** preShow defined for frmSupport **/
    AS_Form_a9b17a108586468d98ee88f097af2c2f: function AS_Form_a9b17a108586468d98ee88f097af2c2f(eventobject) {
        var self = this;
        this.preShow();
    }
});
define("InformationModule/frmSupportController", ["InformationModule/userfrmSupportController", "InformationModule/frmSupportControllerActions"], function() {
    var controller = require("InformationModule/userfrmSupportController");
    var controllerActions = ["InformationModule/frmSupportControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
