define("frmAlreadyEnrolled", function() {
    return function(controller) {
        function addWidgetsfrmAlreadyEnrolled() {
            this.setDefaultUnit(kony.flex.DP);
            var flxSecurityCheckMain = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxSecurityCheckMain",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "isModalContainer": false,
                "top": "0%",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSecurityCheckMain.setDefaultUnit(kony.flex.DP);
            var flxSecurityCheck = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "200dp",
                "id": "flxSecurityCheck",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlx1a98ff",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSecurityCheck.setDefaultUnit(kony.flex.DP);
            var imgUser = new kony.ui.Image2({
                "centerX": "50%",
                "height": "40dp",
                "id": "imgUser",
                "isVisible": true,
                "skin": "slImage",
                "src": "usericon.png",
                "top": "45dp",
                "width": "40dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblDesc = new kony.ui.Label({
                "centerX": "50%",
                "id": "lblDesc",
                "isVisible": true,
                "left": "0dp",
                "skin": "CopyslLabel0a0f152c56f1f42",
                "text": "Hello,John!",
                "textStyle": {
                    "letterSpacing": 0,
                    "strikeThrough": false
                },
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false
            });
            var lblDescription = new kony.ui.Label({
                "bottom": "30dp",
                "centerX": "50%",
                "id": "lblDescription",
                "isVisible": true,
                "skin": "sknLblffffffSSP30px",
                "text": "Our records indicate that you have already enrolled for online banking.",
                "textStyle": {
                    "letterSpacing": 0,
                    "strikeThrough": false
                },
                "top": "10dp",
                "width": "89%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false
            });
            flxSecurityCheck.add(imgUser, lblDesc, lblDescription);
            var flxSecurityCheckOptions = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "440dp",
                "id": "flxSecurityCheckOptions",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "f9f9",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxSecurityCheckOptions.setDefaultUnit(kony.flex.DP);
            var lblText = new kony.ui.Label({
                "centerX": "50%",
                "id": "lblText",
                "isVisible": true,
                "left": "0dp",
                "skin": "CopyslLabel0cbe09323455244",
                "text": "Reset your login credentials in case you have forgotten your login detail",
                "textStyle": {
                    "letterSpacing": 0,
                    "strikeThrough": false
                },
                "top": "50dp",
                "width": "90%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false
            });
            var btnLoginHere = new kony.ui.Button({
                "focusSkin": "sknBtn055BAF26px",
                "height": "40dp",
                "id": "btnLoginHere",
                "isVisible": true,
                "left": "20dp",
                "right": "20dp",
                "skin": "sknBtn0095e4RoundedffffffSSP26px",
                "text": kony.i18n.getLocalizedString("kony.mb.AlreadyEnrolled.LoginHere"),
                "top": "85dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnForgotLogin = new kony.ui.Button({
                "centerX": "50%",
                "focusSkin": "slButtonGlossRed",
                "height": "50dp",
                "id": "btnForgotLogin",
                "isVisible": true,
                "left": "0dp",
                "skin": "skinBtnwhite",
                "text": "forgot login",
                "top": "15dp",
                "width": "90%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnRegDevice = new kony.ui.Button({
                "centerX": "50%",
                "focusSkin": "slButtonGlossRed",
                "height": "50dp",
                "id": "btnRegDevice",
                "isVisible": true,
                "left": "0dp",
                "skin": "skinBtnwhite",
                "text": kony.i18n.getLocalizedString("kony.mb.preferences.RegisterDeviceSmall"),
                "top": "15dp",
                "width": "90%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSecurityCheckOptions.add(lblText, btnLoginHere, btnForgotLogin, btnRegDevice);
            flxSecurityCheckMain.add(flxSecurityCheck, flxSecurityCheckOptions);
            this.add(flxSecurityCheckMain);
        };
        return [{
            "addWidgets": addWidgetsfrmAlreadyEnrolled,
            "enabledForIdleTimeout": true,
            "id": "frmAlreadyEnrolled",
            "init": controller.AS_Form_g377ee87228b4966a960d71b31ace021,
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "preShow": function(eventobject) {
                controller.AS_Form_e6c07ed8004b4feea886e08fa99e5423(eventobject);
            },
            "skin": "sknFrm1a98ffGradient10"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "footerOverlap": false,
            "headerOverlap": false,
            "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
            "retainScrollPosition": false,
            "titleBar": true,
            "titleBarSkin": "slTitleBar",
            "windowSoftInputMode": constants.FORM_ADJUST_PAN
        }]
    }
});