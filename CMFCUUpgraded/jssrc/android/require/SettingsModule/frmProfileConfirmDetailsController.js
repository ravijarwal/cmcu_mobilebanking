define("SettingsModule/userfrmProfileConfirmDetailsController", {
    init: function() {
        var navManager = applicationManager.getNavigationManager();
        var currentForm = navManager.getCurrentForm();
        applicationManager.getPresentationFormUtility().initCommonActions(this, "YES", currentForm);
    },
    frmPreShow: function() {
        this.setPreshowData();
        this.setFlowActions();
        this.setData();
        var navManager = applicationManager.getNavigationManager();
        var currentForm = navManager.getCurrentForm();
        applicationManager.getPresentationFormUtility().logFormName(currentForm);
        applicationManager.getPresentationUtility().dismissLoadingScreen();
    },
    setPreshowData: function() {
        if (kony.os.deviceInfo().name !== "iPhone") {
            this.view.flxHeader.isVisible = true;
            this.view.flxMainContainer.top = "56dp";
        } else {
            this.view.flxHeader.isVisible = false;
            this.view.flxMainContainer.top = "0dp";
        }
    },
    setData: function() {
        var nav = applicationManager.getNavigationManager();
        var data = nav.getCustomInfo('frmProfileConfirmDetails');
        this.view.lblPhoneNumberValue.text = data.phoneCountryCode + " " + data.phoneNumber;
        this.view.lblContantTypeValue.text = data.type;
        this.view.lblCountryValue.text = data.countryType;
        this.view.lblMarkasPrimaryValue.text = data.isPrimary === "true" ? "Yes" : "No";
    },
    setFlowActions: function() {
        var scope = this;
        this.view.btnUpdateChanges.onClick = function() {
            //scope.navToList();
            var settingsMode = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("SettingsModule");
            settingsMode.presentationController.addUserPhoneNumber();
        };
        this.view.customHeader.btnRight.onClick = function() {
            var settingsMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("SettingsModule");
            settingsMod.presentationController.commonFunctionForNavigation("frmProfilePersonalDetails");
        };
        this.view.customHeader.flxBack.onClick = function() {
            var navManager = applicationManager.getNavigationManager();
            navManager.goBack();
        };
    },
    navToList: function() {}
});
define("SettingsModule/frmProfileConfirmDetailsControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for undefined **/
    AS_BarButtonItem_f22a9ea3c19c4330bdf5b7e333e2a886: function AS_BarButtonItem_f22a9ea3c19c4330bdf5b7e333e2a886(eventobject) {
        var self = this;
        var settingsMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("SettingsModule");
        settingsMod.presentationController.commonFunctionForNavigation("frmProfilePersonalDetails");
    },
    /** init defined for frmProfileConfirmDetails **/
    AS_Form_id0439c2d044414cb546bb1d29f1725e: function AS_Form_id0439c2d044414cb546bb1d29f1725e(eventobject) {
        var self = this;
        this.init();
    },
    /** preShow defined for frmProfileConfirmDetails **/
    AS_Form_feeb946510a64b318acb262bb4490168: function AS_Form_feeb946510a64b318acb262bb4490168(eventobject) {
        var self = this;
        this.frmPreShow();
    }
});
define("SettingsModule/frmProfileConfirmDetailsController", ["SettingsModule/userfrmProfileConfirmDetailsController", "SettingsModule/frmProfileConfirmDetailsControllerActions"], function() {
    var controller = require("SettingsModule/userfrmProfileConfirmDetailsController");
    var controllerActions = ["SettingsModule/frmProfileConfirmDetailsControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
