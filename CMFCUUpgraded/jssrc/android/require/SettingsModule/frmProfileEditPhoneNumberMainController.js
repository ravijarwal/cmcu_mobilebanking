define("SettingsModule/userfrmProfileEditPhoneNumberMainController", {
    init: function() {
        var navManager = applicationManager.getNavigationManager();
        var currentForm = navManager.getCurrentForm();
        applicationManager.getPresentationFormUtility().initCommonActions(this, "YES", currentForm);
    },
    onNavigate: function(param) {
        var self = this;
        self.view.flxDropdowm.isVisible = false;
        if (param === "add") {
            self.setAddFlow();
        } else if (param === "edit") {
            self.setEditFlow();
        }
    },
    setAddFlow: function() {
        var scopeObj = this;
        this.view.btnVerifyPhoneNumber.text = kony.i18n.getLocalizedString("kony.mb.common.continue");
        this.view.btnVerifyPhoneNumber.setVisibility(true);
        this.view.imgCheckboxPrimary.src = "checkboxempty.png";
        this.view.imgCheckboxInternational.src = "checkboxempty.png";
        this.view.btnDelete.setVisibility(false);
        this.view.flxPrimary.isVisible = true;
        this.view.lblCode.text = this.setCountryCode();
        var settingsMode = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("SettingsModule");
        var data = settingsMode.presentationController.getPhoneBuilderObject();
        this.view.lblEnterPhoneNumberHeader.text = "Type(" + data.type + ")";
        for (var i = 0; i < 10; i++) {
            scopeObj.clearKeypadChar("");
        }
        this.view.lblPhoneNumber.text = scopeObj.keypadString;
        this.view.btnVerifyPhoneNumber.onClick = function() {
            var number = scopeObj.keypadString;
            number = number.replace('(', '');
            number = number.replace(')', '');
            number = number.replace('-', '');
            var isPrimary = "false";
            if (scopeObj.view.imgCheckboxPrimary.src === "checkbox.png") {
                isPrimary = "true";
            }
            var phoneCountryCode = scopeObj.view.lblCode.text;
            settingsMode.presentationController.createOrUpdatePhoneBuilderObject('phoneNumber', number);
            settingsMode.presentationController.createOrUpdatePhoneBuilderObject('phoneCountryCode', phoneCountryCode);
            settingsMode.presentationController.createOrUpdatePhoneBuilderObject('isPrimary', isPrimary);
            //settingsMode.addUserPhoneNumber();
            settingsMode.presentationController.navigateToAddPhoneNumberConfirmPage();
        };
    },
    setEditFlow: function() {
        var scopeObj = this;
        this.view.btnVerifyPhoneNumber.text = kony.i18n.getLocalizedString("kony.mb.Profile.UpdateChanges");
        this.view.btnVerifyPhoneNumber.setVisibility(true);
        this.view.btnDelete.setVisibility(true);
        this.view.btnDelete.onClick = function() {
            scopeObj.onBtnDeleteClick();
        };
        this.view.btnVerifyPhoneNumber.onClick = function() {
            //scopeObj.navToEditPhoneList("edit");
            scopeObj.onUpdateButtonClick();
        };
        var nav = applicationManager.getNavigationManager();
        var data1 = nav.getCustomInfo('frmProfileEditPhoneNumberMain');
        data1 = data1.data;
        //     var type = data1.type;
        //     var phoneNumber = data1.phoneNumber;
        var type = data1.Extension;
        var phoneNumber = data1.phoneNumber;
        //Temp fix;
        if (phoneNumber === undefined) {
            phoneNumber = data1.Value;
        }
        var isPrimary = data1.isPrimary;
        var countryType = data1.countryType;
        this.selectedData = data1;
        this.view.lblEnterPhoneNumberHeader.text = "Type (" + type + ")";
        //if(isPrimary === "1"){
        if (isPrimary === "true") {
            this.view.imgCheckboxPrimary.src = "checkbox.png";
        } else {
            this.view.imgCheckboxPrimary.src = "checkboxempty.png";
        }
        //if(isPrimary === "1"){
        if (isPrimary === "true") {
            this.view.flxPrimary.isVisible = false;
            this.view.btnDelete.isVisible = false;
        } else {
            this.view.flxPrimary.isVisible = true;
            this.view.btnDelete.isVisible = true;
        }
        if (countryType && countryType.toLowerCase() === "international") {
            this.view.imgCheckboxInternational.src = "checkbox.png";
        } else {
            this.view.imgCheckboxInternational.src = "checkboxempty.png";
        }
        scopeObj.keypadString = "";
        scopeObj.keypadString = phoneNumber;
        this.view.lblCode.text = this.setCountryCode();
        this.view.lblPhoneNumber.text = scopeObj.keypadString;
    },
    keypadString: '',
    timerCounter: 0,
    currentInputModule: 'phonenumber',
    preShow: function() {
        var scopeObj = this;
        this.view.flxPhoneType.isVisible = false;
        if (kony.os.deviceInfo().name !== "iPhone") {
            this.view.flxHeader.isVisible = true;
            this.view.flxMainContainer.top = "56dp";
        } else {
            this.view.flxHeader.isVisible = false;
            this.view.flxMainContainer.top = "0dp";
        }
        this.view.keypad.btnOne.onClick = function() {
            scopeObj.setKeypadChar("1");
        };
        this.view.keypad.btnTwo.onClick = function() {
            scopeObj.setKeypadChar("2");
        };
        this.view.keypad.btnThree.onClick = function() {
            scopeObj.setKeypadChar("3");
        };
        this.view.keypad.btnFour.onClick = function() {
            scopeObj.setKeypadChar("4");
        };
        this.view.keypad.btnFive.onClick = function() {
            scopeObj.setKeypadChar("5");
        };
        this.view.keypad.btnSix.onClick = function() {
            scopeObj.setKeypadChar("6");
        };
        this.view.keypad.btnSeven.onClick = function() {
            scopeObj.setKeypadChar("7");
        };
        this.view.keypad.btnEight.onClick = function() {
            scopeObj.setKeypadChar("8");
        };
        this.view.keypad.btnNine.onClick = function() {
            scopeObj.setKeypadChar("9");
        };
        this.view.keypad.btnZero.onClick = function() {
            scopeObj.setKeypadChar("0");
        };
        this.view.keypad.imgClearKeypad.onTouchEnd = function() {
            scopeObj.clearKeypadChar("");
        };
        this.view.flxCheckboxPrimary.onClick = function() {
            scopeObj.toggle(scopeObj.view.imgCheckboxPrimary);
            if (scopeObj.validatePhone()) scopeObj.enterPostAction();
            else scopeObj.incompleteView();
        };
        this.view.flxCheckboxInternational.onClick = function() {
            scopeObj.toggle(scopeObj.view.imgCheckboxInternational);
            if (scopeObj.validatePhone()) scopeObj.enterPostAction();
            else scopeObj.incompleteView();
        };
        this.view.customHeader.flxBack.onClick = function() {
            var navManager = applicationManager.getNavigationManager();
            navManager.goBack();
        };
        this.view.customHeader.btnRight.onClick = function() {
            var settingsMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("SettingsModule");
            settingsMod.presentationController.commonFunctionForNavigation("frmProfilePersonalDetails");
        };
        this.view.flxCode.onTouchEnd = this.showCountriesList;
        var navManager = applicationManager.getNavigationManager();
        var jsonValue = navManager.getCustomInfo("frmProfileEditPhoneNumberMain");
        this.onNavigate(jsonValue.flow);
        var currentForm = navManager.getCurrentForm();
        applicationManager.getPresentationFormUtility().logFormName(currentForm);
        applicationManager.getPresentationUtility().dismissLoadingScreen();
    },
    toggle: function(img) {
        if (img.src === "checkbox.png") {
            img.src = "checkboxempty.png";
        } else img.src = "checkbox.png"
    },
    updateInputBullets: function() {
        var scope = this;
        scope.updateInputBulletsOf('(___)___-____', "flxInputPhoneNumber");
        //updateBullets[this.currentInputModule]();
    },
    updateInputBulletsOf: function(dummyString, inputFlx) {
        var widgets = this.view[inputFlx].widgets();
        for (var i = 0; i < this.keypadString.length; i++) {
            widgets[i].skin = "sknLbl979797SSP60px";
            widgets[i].text = this.keypadString[i];
        }
        for (var i = this.keypadString.length; i < widgets.length; i++) {
            widgets[i].skin = "sknLble3e3e3SSP60px";
            widgets[i].text = dummyString[i];
        }
        this.view.forceLayout();
    },
    showCountriesList: function() {
        var settingsMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("SettingsModule");
        settingsMod.presentationController.getCountryCodes();
    },
    setKeypadChar: function(char) {
        this.keypadString = this.keypadString + char;
        this.view.lblPhoneNumber.text = this.keypadString;
        //if valid
        if (this.validatePhone()) {
            this.enterPostAction();
        } else {
            this.incompleteView();
        }
        this.view.forceLayout();
        return;
    },
    clearKeypadChar: function() {
        if (this.keypadString.length === 0) return;
        this.keypadString = this.keypadString.substr(0, this.keypadString.length - 1);
        this.view.lblPhoneNumber.text = this.keypadString;
        if (this.validatePhone()) {
            this.enterPostAction();
        } else {
            this.incompleteView();
        }
        this.view.forceLayout();
        return;
    },
    validatePhone: function() {
        //       if(this.keypadString.length <= 15 && this.keypadString.length >= 1)
        //       {
        //         return true;
        //       }
        //       return false;    
        var validationMan = applicationManager.getValidationUtilManager();
        return validationMan.isValidPhoneNumber(this.keypadString);
    },
    enterPostAction: function() {
        this.view.btnVerifyPhoneNumber.setEnabled(true);
        this.view.btnVerifyPhoneNumber.skin = "sknBtn0095e4RoundedffffffSSP26px";
    },
    incompleteView: function() {
        this.view.btnVerifyPhoneNumber.skin = "sknBtna0a0a0SSPReg26px";
        this.view.btnVerifyPhoneNumber.setEnabled(false);
    },
    showPopupIncorrectCredentials: function(par) {
        var scopeObj = this;
        if (par === "invalidphoneno") {
            this.view.customPopup.lblPopup.text = kony.i18n.getLocalizedString("kony.mb.OnBoarding.InvaliPhoneno");
        }
        this.timerCounter = parseInt(this.timerCounter) + 1;
        var timerId = "timerPopupError" + this.timerCounter;
        this.view.flxPopup.skin = "sknFlxf54b5e";
        this.view.customPopup.imgPopup.src = "errormessage.png";
        this.view.flxPopup.setVisibility(true);
        kony.timer.schedule(timerId, function() {
            scopeObj.view.flxPopup.setVisibility(false);
            scopeObj.keypadString = '';
            scopeObj.updateInputBullets();
        }, 1.5, false);
    },
    navToEditPhoneList: function(param) {},
    navToConfirmDetails: function() {
        var settingsMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("SettingsModule");
        settingsMod.presentationController.commonFunctionForNavigation("frmProfileConfirmDetails");
    },
    setCountryCode: function() {
        var nav = applicationManager.getNavigationManager();
        var phoneData = nav.getCustomInfo('frmProfileEditPhoneNumberMain');
        if (!kony.sdk.isNullOrUndefined(phoneData)) {
            if (!kony.sdk.isNullOrUndefined(phoneData.data.phoneCountryCode)) {
                return phoneData.data.phoneCountryCode;
            } else if (!kony.sdk.isNullOrUndefined(phoneData.phoneCountryCode)) {
                return phoneData.phoneCountryCode;
            }
        }
        return applicationManager.getConfigurationManager().getConfigurationValue("defaultCountryISDCode");
    },
    onUpdateButtonClick: function() {
        var number = this.keypadString;
        number = number.replace('(', '');
        number = number.replace(')', '');
        number = number.replace('-', '');
        var isPrimary = "true";
        if (this.view.imgCheckboxPrimary.src === "checkboxempty.png") {
            isPrimary = "false";
        }
        var countryType = "domestic";
        if (this.view.imgCheckboxInternational.src === "checkbox.png") {
            countryType = 'International';
        }
        //     if(number && number.length < 10){
        //       return;
        //     }
        var data = this.selectedData;
        data.isPrimary = isPrimary;
        data.countryType = countryType;
        data.phoneNumber = number;
        data.phoneCountryCode = this.view.lblCode.text;
        var settingsMode = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("SettingsModule");
        settingsMode.presentationController.updateUserPhoneNumber(data);
    },
    onBtnDeleteClick: function() {
        var data = this.selectedData;
        var id = data.id;
        var settingsMode = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("SettingsModule");
        settingsMode.presentationController.deleteUserPhoneNumber(id);
    }
});
define("SettingsModule/frmProfileEditPhoneNumberMainControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for btnOne **/
    AS_Button_b600ad00331a44adad502fe1f21a821e: function AS_Button_b600ad00331a44adad502fe1f21a821e(eventobject) {
        var self = this;
    },
    /** onClick defined for btnTwo **/
    AS_Button_e5930f70c45143c3a6f5df643220ac49: function AS_Button_e5930f70c45143c3a6f5df643220ac49(eventobject) {
        var self = this;
        this.setChar(2);
    },
    /** onClick defined for btnThree **/
    AS_Button_a5d6ad3993b143bcab0b68f04055971e: function AS_Button_a5d6ad3993b143bcab0b68f04055971e(eventobject) {
        var self = this;
        this.setChar(3);
    },
    /** onClick defined for btnFour **/
    AS_Button_j17cc5ddc7d64209a500109135dbafb1: function AS_Button_j17cc5ddc7d64209a500109135dbafb1(eventobject) {
        var self = this;
        this.setChar(4);
    },
    /** onClick defined for btnFive **/
    AS_Button_b81f10d76b9c49b489f80447d90eaf59: function AS_Button_b81f10d76b9c49b489f80447d90eaf59(eventobject) {
        var self = this;
        this.setChar(5);
    },
    /** onClick defined for btnSix **/
    AS_Button_fb3682f759a6453a816a59ab285ee2f5: function AS_Button_fb3682f759a6453a816a59ab285ee2f5(eventobject) {
        var self = this;
        this.setChar(6);
    },
    /** onClick defined for btnSeven **/
    AS_Button_h8875e2b7c9547c7a74675abc0b59396: function AS_Button_h8875e2b7c9547c7a74675abc0b59396(eventobject) {
        var self = this;
        this.setChar(7);
    },
    /** onClick defined for btnEight **/
    AS_Button_c095a38b7a464014af0b22bb663e72d0: function AS_Button_c095a38b7a464014af0b22bb663e72d0(eventobject) {
        var self = this;
        this.setChar(8);
    },
    /** onClick defined for btnNine **/
    AS_Button_cfdb950230e04132a3b710a756627684: function AS_Button_cfdb950230e04132a3b710a756627684(eventobject) {
        var self = this;
        this.setChar(9);
    },
    /** onClick defined for btnZero **/
    AS_Button_a33fcaf49c4742deb085f317be8e8db5: function AS_Button_a33fcaf49c4742deb085f317be8e8db5(eventobject) {
        var self = this;
        this.setChar(0);
    },
    /** onClick defined for undefined **/
    AS_BarButtonItem_i3e856b61534448e810c1a27477d2b7a: function AS_BarButtonItem_i3e856b61534448e810c1a27477d2b7a(eventobject) {
        var self = this;
        var settingsMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("SettingsModule");
        settingsMod.presentationController.commonFunctionForNavigation("frmProfilePersonalDetails");
    },
    /** init defined for frmProfileEditPhoneNumberMain **/
    AS_Form_b6e0cbe80fb942d484f3b30a6a3686be: function AS_Form_b6e0cbe80fb942d484f3b30a6a3686be(eventobject) {
        var self = this;
        this.init();
    },
    /** preShow defined for frmProfileEditPhoneNumberMain **/
    AS_Form_de72fc54f6484408848879f4aeb90ca3: function AS_Form_de72fc54f6484408848879f4aeb90ca3(eventobject) {
        var self = this;
        this.preShow();
    }
});
define("SettingsModule/frmProfileEditPhoneNumberMainController", ["SettingsModule/userfrmProfileEditPhoneNumberMainController", "SettingsModule/frmProfileEditPhoneNumberMainControllerActions"], function() {
    var controller = require("SettingsModule/userfrmProfileEditPhoneNumberMainController");
    var controllerActions = ["SettingsModule/frmProfileEditPhoneNumberMainControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
