define("SettingsModule/userfrmPreferencesTouchIdController", {
    init: function() {
        var navManager = applicationManager.getNavigationManager();
        var currentForm = navManager.getCurrentForm();
        applicationManager.getPresentationFormUtility().initCommonActions(this, "YES", currentForm);
    },
    preShow: function() {
        if (kony.os.deviceInfo().name === "iPhone") {
            this.view.flxHeader.isVisible = false;
        } else {
            this.view.flxHeader.isVisible = true;
        }
        this.initActions();
        var navManager = applicationManager.getNavigationManager();
        var currentForm = navManager.getCurrentForm();
        applicationManager.getPresentationFormUtility().logFormName(currentForm);
        applicationManager.getPresentationUtility().dismissLoadingScreen();
    },
    initActions: function() {
        var scope = this;
        this.view.customHeader.flxBack.onClick = function() {
            var navManager = applicationManager.getNavigationManager();
            navManager.goBack();
        };
        this.view.btnSetAsDefault.onClick = this.setTouchIdDefaultMode;
    },
    setTouchIdDefaultMode: function() {
        applicationManager.getPresentationUtility().showLoadingScreen();
        var navManager = applicationManager.getNavigationManager();
        var authMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
        authMod.presentationController.setTouchIdflag(true);
        authMod.presentationController.setDefaultMode("touchid");
        var settingsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("SettingsModule");
        settingsModule.presentationController.getDevDetails();
        var tempData = settingsModule.presentationController.getAuthModeData();
        tempData.popUpMsg = kony.i18n.getLocalizedString("kony.mb.Touch.Id.is.set.a.Default.Login");
        navManager.setCustomInfo("frmPreferencesDefaultLogin", tempData);
        settingsModule.presentationController.commonFunctionForNavigation("frmPreferencesDefaultLogin");
    }
});
define("SettingsModule/frmPreferencesTouchIdControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for undefined **/
    AS_BarButtonItem_b6a38b969b7941d09eb9dfeada81baf1: function AS_BarButtonItem_b6a38b969b7941d09eb9dfeada81baf1(eventobject) {
        var self = this;
        var settingsMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("SettingsModule");
        settingsMod.presentationController.commonFunctionForNavigation("frmTransfers");
    },
    /** preShow defined for frmPreferencesTouchId **/
    AS_Form_cba77b2ee45b4ed0a03f30d88de63afd: function AS_Form_cba77b2ee45b4ed0a03f30d88de63afd(eventobject) {
        var self = this;
        this.preShow();
    }
});
define("SettingsModule/frmPreferencesTouchIdController", ["SettingsModule/userfrmPreferencesTouchIdController", "SettingsModule/frmPreferencesTouchIdControllerActions"], function() {
    var controller = require("SettingsModule/userfrmPreferencesTouchIdController");
    var controllerActions = ["SettingsModule/frmPreferencesTouchIdControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
