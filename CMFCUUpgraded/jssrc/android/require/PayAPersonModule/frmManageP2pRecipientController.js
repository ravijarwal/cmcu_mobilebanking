define("PayAPersonModule/userfrmManageP2pRecipientController", {
    init: function() {
        var navManager = applicationManager.getNavigationManager();
        var currentForm = navManager.getCurrentForm();
        applicationManager.getPresentationFormUtility().initCommonActions(this, "YES", currentForm);
    },
    preShow: function() {
        if (kony.os.deviceInfo().name === "iPhone") {
            this.view.flxHeader.isVisible = false;
        }
        this.initActions();
        this.setDataToForm();
        applicationManager.getPresentationUtility().dismissLoadingScreen();
        var navManager = applicationManager.getNavigationManager();
        var currentForm = navManager.getCurrentForm();
        applicationManager.getPresentationFormUtility().logFormName(currentForm);
    },
    initActions: function() {
        var scope = this;
        this.view.customHeader.flxBack.onClick = function() {
            var navMan = applicationManager.getNavigationManager();
            navMan.goBack();
        }
        this.view.customHeader.btnRight.onClick = function() {
            scope.onClickEdit();
        }
        this.view.btnDeleteRecipient.onClick = function() {
            //kony.ui.Alert("Are You sure do you want to delete this user permanently", confirmDelete, constants.ALERT_TYPE_CONFIRMATION, "Yes", "No", "");
            var basicConfig = {
                "alertType": constants.ALERT_TYPE_CONFIRMATION,
                "yesLabel": applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.common.AlertYes"),
                "noLabel": applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.common.AlertNo"),
                "message": applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.common.deleteRecipient", "Do you want to delete the recipient"),
                "alertHandler": scope.confirmDelete
            };
            applicationManager.getPresentationUtility().showAlertMessage(basicConfig, {});;
        }
        this.view.btnPayAPerson.onClick = function() {
            var payeeMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule");
            var data = payeeMod.presentationController.getP2PPayeeDetails();
            var navMan = applicationManager.getNavigationManager();
            navMan.setEntryPoint("payaperson", "frmManageP2pRecipient");
            payeeMod.presentationController.getP2pAccounts(data);
        }
    },
    setDataToForm: function() {
        var scope = this;
        var payeeMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule");
        var recipientData = payeeMod.presentationController.getP2PPayeeDetails();
        if (recipientData) {
            scope.view.lblRecipientNameValue.text = recipientData.name;
            scope.view.lblRecipientContact.text = recipientData.phone;
            scope.view.lblRecipientNickNameValue.text = recipientData.nickName;
        }
    },
    confirmDelete: function(response) {
        if (response === true) {
            applicationManager.getPresentationUtility().showLoadingScreen();
            var p2pMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule");
            p2pMod.presentationController.deleteP2PRecipient();
        } else {
            kony.print("don't delete");
        }
    },
    onClickEdit: function() {
        applicationManager.getPresentationUtility().showLoadingScreen();
        var p2pMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule");
        var accountDetails = p2pMod.presentationController.getP2PPayeeDetails();
        p2pMod.presentationController.editBenificiaryNickName(accountDetails);
    }
});
define("PayAPersonModule/frmManageP2pRecipientControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for undefined **/
    AS_BarButtonItem_f1e52fbc8e364eb7b7810db2ef1cdac7: function AS_BarButtonItem_f1e52fbc8e364eb7b7810db2ef1cdac7(eventobject) {
        var self = this;
        this.onClickEdit();
    },
    /** init defined for frmManageP2pRecipient **/
    AS_Form_aa94a46d2a754201bc4ead18d838a622: function AS_Form_aa94a46d2a754201bc4ead18d838a622(eventobject) {
        var self = this;
        this.init();
    },
    /** preShow defined for frmManageP2pRecipient **/
    AS_Form_h5a445d7dab84d8790af30b592bb9df2: function AS_Form_h5a445d7dab84d8790af30b592bb9df2(eventobject) {
        var self = this;
        this.preShow();
    }
});
define("PayAPersonModule/frmManageP2pRecipientController", ["PayAPersonModule/userfrmManageP2pRecipientController", "PayAPersonModule/frmManageP2pRecipientControllerActions"], function() {
    var controller = require("PayAPersonModule/userfrmManageP2pRecipientController");
    var controllerActions = ["PayAPersonModule/frmManageP2pRecipientControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
