/**
 *@module ServiceResponseHandler
 */
define([], function() {
    /**
     * ServiceResponseHandler used to get a proper Success or Error backend response
     *@alias module:ServiceResponseHandler
     *@class
     */
    ServiceResponseHandler = function() {
        /**@member {object} serviceResponseHandler Contains backend response*/
        this.serviceResponseHandler = null;
    };
    ServiceResponseHandler.prototype.validatingResponse = function(response) {
        var errcode, resError, res, errMsg;
        var errorMapping = {
            "10001": kony.i18n.getLocalizedString("kony.mb.10001"),
            "10003": kony.i18n.getLocalizedString("kony.mb.10003"),
            "10008": kony.i18n.getLocalizedString("kony.mb.10008"),
            "10009": kony.i18n.getLocalizedString("kony.mb.10009"),
            "10010": kony.i18n.getLocalizedString("kony.mb.10010"),
            "10011": kony.i18n.getLocalizedString("kony.mb.10011"),
            "10012": kony.i18n.getLocalizedString("kony.mb.10012"),
            "10013": kony.i18n.getLocalizedString("kony.mb.10013"),
            "10014": kony.i18n.getLocalizedString("kony.mb.10014"),
            "10015": kony.i18n.getLocalizedString("kony.mb.10015"),
            "10016": kony.i18n.getLocalizedString("kony.mb.10016"),
            "10017": kony.i18n.getLocalizedString("kony.mb.10017"),
            "10018": kony.i18n.getLocalizedString("kony.mb.10018"),
            "10019": kony.i18n.getLocalizedString("kony.mb.10019"),
            "10022": kony.i18n.getLocalizedString("kony.mb.10022"),
            "10023": kony.i18n.getLocalizedString("kony.mb.10023"),
            "10024": kony.i18n.getLocalizedString("kony.mb.10024"),
            "10026": kony.i18n.getLocalizedString("kony.mb.10026"),
            "10027": kony.i18n.getLocalizedString("kony.mb.10027"),
            "10046": kony.i18n.getLocalizedString("kony.mb.10046"),
            "10047": kony.i18n.getLocalizedString("kony.mb.10047"),
            "10048": kony.i18n.getLocalizedString("kony.mb.10048"),
            "10049": kony.i18n.getLocalizedString("kony.mb.10049"),
            "10050": kony.i18n.getLocalizedString("kony.mb.10050"),
            "10051": kony.i18n.getLocalizedString("kony.mb.10051"),
            "10052": kony.i18n.getLocalizedString("kony.mb.10052"),
            "10053": kony.i18n.getLocalizedString("kony.mb.10053"),
            "10054": kony.i18n.getLocalizedString("kony.mb.10054"),
            "10055": kony.i18n.getLocalizedString("kony.mb.10055"),
            "10057": kony.i18n.getLocalizedString("kony.mb.10057"),
            "10058": kony.i18n.getLocalizedString("kony.mb.10058"),
            "10059": kony.i18n.getLocalizedString("kony.mb.10059"),
            "10060": kony.i18n.getLocalizedString("kony.mb.10060"),
            "10063": kony.i18n.getLocalizedString("kony.mb.10063"),
            "10072": kony.i18n.getLocalizedString("kony.mb.10072"),
            "10073": kony.i18n.getLocalizedString("kony.mb.10073"),
            "10074": kony.i18n.getLocalizedString("kony.mb.10074"),
            "10075": kony.i18n.getLocalizedString("kony.mb.10075"),
            "10083": kony.i18n.getLocalizedString("kony.mb.10083"),
            "10084": kony.i18n.getLocalizedString("kony.mb.10084"),
            "10086": kony.i18n.getLocalizedString("kony.mb.10086"),
            "10087": kony.i18n.getLocalizedString("kony.mb.10087"),
            "10088": kony.i18n.getLocalizedString("kony.mb.10088"),
            "10089": kony.i18n.getLocalizedString("kony.mb.10089"),
            "10090": kony.i18n.getLocalizedString("kony.mb.10090"),
            "10091": kony.i18n.getLocalizedString("kony.mb.10091"),
            "10092": kony.i18n.getLocalizedString("kony.mb.10092"),
            "10093": kony.i18n.getLocalizedString("kony.mb.10093"),
            "10094": kony.i18n.getLocalizedString("kony.mb.10094"),
            "10095": kony.i18n.getLocalizedString("kony.mb.10095"),
            "10096": kony.i18n.getLocalizedString("kony.mb.10096"),
            "10097": kony.i18n.getLocalizedString("kony.mb.10097"),
            "10098": kony.i18n.getLocalizedString("kony.mb.10098"),
            "10117": kony.i18n.getLocalizedString("kony.mb.10117"),
            "10118": kony.i18n.getLocalizedString("kony.mb.10118"),
            "10119": kony.i18n.getLocalizedString("kony.mb.10119"),
            "10135": kony.i18n.getLocalizedString("kony.mb.10119"),
            "11017": kony.i18n.getLocalizedString("kony.mb.11017"),
            "11018": kony.i18n.getLocalizedString("kony.mb.11018"),
            "11019": kony.i18n.getLocalizedString("kony.mb.11019"),
            "11020": kony.i18n.getLocalizedString("kony.mb.11020"),
            "11021": kony.i18n.getLocalizedString("kony.mb.11021"),
            "11022": kony.i18n.getLocalizedString("kony.mb.11022"),
            "11024": kony.i18n.getLocalizedString("kony.mb.11024"),
            "11025": kony.i18n.getLocalizedString("kony.mb.11025"),
            "11027": kony.i18n.getLocalizedString("kony.mb.11027")
        };
        if (response.hasOwnProperty("errcode") || response.hasOwnProperty("dbpErrCode")) {
            errcode = response.errcode ? response.errcode : response.dbpErrCode;
            if (errorMapping[errcode] !== undefined) resError = {
                "errorMessage": errorMapping[errcode],
                "isServerUnreachable": false,
                "serverErrorRes": response
            };
            else {
                if (response.errmsg) {
                    errMsg = response.errmsg;
                } else if (response.dbpErrMsg) errMsg = response.dbpErrMsg;
                else {
                    errMsg = kony.i18n.getLocalizedString("kony.mb.An.error.occurred.while.making.the.request.");
                }
                resError = {
                    "errorMessage": errMsg,
                    "isServerUnreachable": false,
                    "serverErrorRes": response
                };
            }
            res = {
                "status": false,
                "errmsg": resError
            };
        } else res = {
            "status": true,
            "data": response
        };
        return res;
    };
    ServiceResponseHandler.prototype.manageLoginResponse = function(response) {
        var resError, res;
        if (response.opstatus == 1011) {
            if (kony.os.deviceInfo().name === "thinclient" && kony.net.isNetworkAvailable(constants.NETWORK_TYPE_ANY) === false) location.reload(); //todo later so that it can be in sync with RB
            else {
                isServiceFailure = true;
                if (response.errmsg) {
                    errMsg = response.errmsg;
                } else if (response.dbpErrMsg) errMsg = response.dbpErrMsg;
                else {
                    errMsg = kony.i18n.getLocalizedString("kony.mb.An.error.occurred.while.making.the.request.");
                }
            }
            resError = {
                "errorMessage": errMsg,
                "isServerUnreachable": isServiceFailure,
                "serverErrorRes": response
            };
            res = {
                "status": false,
                "errmsg": resError
            };
        } else {
            res = this.validatingResponse(response.details);
        }
        return res;
    };
    /**
     * ManageResponse method is used to format the backend reponse based on the success and error
     * @param {string} status , which contains kony.mvc.constants of success or error 
     * @param {object} response , if status is success the response consists of success response of that service call 
     * @param {object} error , if status is error the error consists of error response of that service call 
     * @return {object} res, returns entire reponse of manageResponse based on the success or error
     */
    ServiceResponseHandler.prototype.manageResponse = function(status, response, error) {
        /**@member {object} res Contains formatted backend response*/
        var res;
        if (status == kony.mvc.constants.STATUS_SUCCESS) {
            kony.print("response:" + JSON.stringify(response));
            res = this.validatingResponse(response);
        } else {
            if (error.opstatus == 1011) {
                if (kony.os.deviceInfo().name === "thinclient" && kony.net.isNetworkAvailable(constants.NETWORK_TYPE_ANY) === false) {
                    location.reload(); //todo later so that it can be in sync with RB
                } else {
                    isServiceFailure = true;
                    if (error.errmsg) {
                        errMsg = error.errmsg;
                    } else if (error.dbpErrMsg) errMsg = error.dbpErrMsg;
                    else {
                        errMsg = kony.i18n.getLocalizedString("kony.mb.An.error.occurred.while.making.the.request.");
                    }
                }
            } else {
                isServiceFailure = false;
                if (error.errmsg) errMsg = error.errmsg;
                else if (error.dbpErrMsg) errMsg = error.dbpErrMsg;
                else errMsg = kony.i18n.getLocalizedString("kony.mb.An.Internal.Error.occured.Please.try.after.sometime.");
            }
            resError = {
                "errorMessage": errMsg,
                "isServerUnreachable": isServiceFailure,
                "serverErrorRes": error
            };
            res = {
                "status": false,
                "errmsg": resError
            };
        }
        return res;
    };
    return ServiceResponseHandler;
});