define("userfrmProfileCountryController", {
    init: function() {
        var navManager = applicationManager.getNavigationManager();
        var currentForm = navManager.getCurrentForm();
        applicationManager.getPresentationFormUtility().initCommonActions(this, "YES", currentForm);
    },
    preShow: function() {
        this.view.flxSearch.setVisibility(false);
        if (kony.os.deviceInfo().name !== "iPhone") {
            this.view.flxHeader.isVisible = true;
        } else {
            this.view.flxHeader.isVisible = false;
        }
        this.initActions();
    },
    initActions: function() {
        var scope = this;
        this.view.segCountry.onRowClick = function() {
            scope.segmentRowClick();
        };
        this.view.customHeader.flxBack.onClick = function() {
            var navManager = applicationManager.getNavigationManager();
            navManager.goBack();
        };
        this.view.customHeader.btnRight.onClick = function() {
            var settingsMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("SettingsModule");
            settingsMod.presentationController.commonFunctionForNavigation("frmProfilePersonalDetails");
        };
    },
    setFormUI: function(data) {
        var widgetDataMap = {
            lblFrequency: "Name"
        };
        this.view.segCountry.widgetDataMap = widgetDataMap;
        this.view.segCountry.setData(data);
        applicationManager.getPresentationUtility().dismissLoadingScreen();
    },
    segmentRowClick: function() {
        var settingsMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("SettingsModule");
        var phoneData = {};
        var navManager = applicationManager.getNavigationManager();
        phoneData = navManager.getCustomInfo("frmProfileEditPhoneNumberMain");
        if (phoneData && phoneData.data) {
            phoneData.data.phoneCountryCode = this.view.segCountry.selectedRowItems[0].ISDCode;
        } else {
            phoneData.phoneCountryCode = this.view.segCountry.selectedRowItems[0].ISDCode;
        }
        navManager.setCustomInfo("frmProfileEditPhoneNumberMain", phoneData);
        settingsMod.presentationController.commonFunctionForNavigation("frmProfileEditPhoneNumberMain");
    },
});
define("frmProfileCountryControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for undefined **/
    AS_BarButtonItem_bb75c69733284ae78d3c9a875e6aeebf: function AS_BarButtonItem_bb75c69733284ae78d3c9a875e6aeebf(eventobject) {
        var self = this;
        var settingsMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("SettingsModule");
        settingsMod.presentationController.commonFunctionForNavigation("frmProfilePersonalDetails");
    },
    /** init defined for frmProfileCountry **/
    AS_Form_e575315514bb41979bb5d1576ae597e0: function AS_Form_e575315514bb41979bb5d1576ae597e0(eventobject) {
        var self = this;
        this.init();
    },
    /** preShow defined for frmProfileCountry **/
    AS_Form_a9209fa9f99046eb92df436f301c697c: function AS_Form_a9209fa9f99046eb92df436f301c697c(eventobject) {
        var self = this;
        this.preShow();
    }
});
define("frmProfileCountryController", ["userfrmProfileCountryController", "frmProfileCountryControllerActions"], function() {
    var controller = require("userfrmProfileCountryController");
    var controllerActions = ["frmProfileCountryControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
