define(function() {
    return {
        "properties": [{
            "name": "optionTitle",
            "enumerable": true,
            "configurable": false,
            "writable": true
        }, {
            "name": "data",
            "enumerable": true,
            "configurable": false,
            "writable": true
        }, {
            "name": "icon",
            "enumerable": true,
            "configurable": false,
            "writable": true
        }, {
            "name": "rowTemplate",
            "enumerable": true,
            "configurable": false,
            "writable": true
        }, {
            "name": "widgetDataMap",
            "enumerable": true,
            "configurable": false,
            "writable": true
        }],
        "apis": ["setData", "setSegmentData", "selectedRowIndex", "selectedMobileRowIndex"],
        "events": ["onRowClick"]
    }
});