define(function() {
    return function(controller) {
        var dropdown = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "dropdown",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20dp",
            "isModalContainer": false,
            "right": "20dp",
            "skin": "sknflxffffffBorderf1f1f13px",
            "top": "0dp",
            "zIndex": 1
        }, controller.args[0], "dropdown"), extendConfig({}, controller.args[1], "dropdown"), extendConfig({}, controller.args[2], "dropdown"));
        dropdown.setDefaultUnit(kony.flex.DP);
        var flxDropdownHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "55dp",
            "id": "flxDropdownHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "CopyslFbox0aee127faeffd4c",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxDropdownHeader"), extendConfig({}, controller.args[1], "flxDropdownHeader"), extendConfig({}, controller.args[2], "flxDropdownHeader"));
        flxDropdownHeader.setDefaultUnit(kony.flex.DP);
        var richTxtOptionTitle = new kony.ui.RichText(extendConfig({
            "centerY": "50%",
            "height": "55dp",
            "id": "richTxtOptionTitle",
            "isVisible": true,
            "left": "53dp",
            "linkSkin": "defRichTextLink",
            "right": "50dp",
            "skin": "sknRtx424242SSP26px",
            "text": "Enter <b>CVV Code</b> <br>of your Credit/debit card",
            "zIndex": 1
        }, controller.args[0], "richTxtOptionTitle"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "richTxtOptionTitle"), extendConfig({}, controller.args[2], "richTxtOptionTitle"));
        var flxArrow = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxArrow",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "20dp",
            "skin": "slFbox",
            "top": "0dp",
            "width": "20dp",
            "zIndex": 1
        }, controller.args[0], "flxArrow"), extendConfig({}, controller.args[1], "flxArrow"), extendConfig({}, controller.args[2], "flxArrow"));
        flxArrow.setDefaultUnit(kony.flex.DP);
        var imgArrow = new kony.ui.Image2(extendConfig({
            "centerY": "50%",
            "height": "14dp",
            "id": "imgArrow",
            "isVisible": true,
            "right": "0dp",
            "skin": "slImage",
            "src": "calenderarrowright.png",
            "width": "14dp",
            "zIndex": 1
        }, controller.args[0], "imgArrow"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgArrow"), extendConfig({}, controller.args[2], "imgArrow"));
        flxArrow.add(imgArrow);
        var flxImg = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxImg",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "45dp",
            "zIndex": 1
        }, controller.args[0], "flxImg"), extendConfig({}, controller.args[1], "flxImg"), extendConfig({}, controller.args[2], "flxImg"));
        flxImg.setDefaultUnit(kony.flex.DP);
        var imgIcon = new kony.ui.Image2(extendConfig({
            "centerY": "50%",
            "height": "24dp",
            "id": "imgIcon",
            "isVisible": true,
            "left": "10dp",
            "skin": "slImage",
            "src": "entercvvicon.png",
            "width": "22dp",
            "zIndex": 1
        }, controller.args[0], "imgIcon"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgIcon"), extendConfig({}, controller.args[2], "imgIcon"));
        var flxSeparator = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxSeparator",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "0dp",
            "skin": "sknFlxSeprtrGrey",
            "top": "0dp",
            "width": "1dp",
            "zIndex": 1
        }, controller.args[0], "flxSeparator"), extendConfig({}, controller.args[1], "flxSeparator"), extendConfig({}, controller.args[2], "flxSeparator"));
        flxSeparator.setDefaultUnit(kony.flex.DP);
        flxSeparator.add();
        flxImg.add(imgIcon, flxSeparator);
        flxDropdownHeader.add(richTxtOptionTitle, flxArrow, flxImg);
        var sgmOptions = new kony.ui.SegmentedUI2(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "data": [{
                "imgArrow": "calenderarrowright.png",
                "imgRadio": "radio_inactive.png",
                "lblHomeEmailHiddenVal": "si***n1@gmail.com",
                "lblHomePhnHiddenVal": "si***n1@gmail.com",
                "lblMobilePhnHiddenVal": "si***n1@gmail.com",
                "lblOption": "si***n1@gmail.com",
                "lblPrsnlEmailHiddenVal": "si***n1@gmail.com",
                "lblWrkEmailHiddenVal": "si***n1@gmail.com",
                "lblWrkPhnHiddenVal": "si***n1@gmail.com"
            }, {
                "imgArrow": "calenderarrowright.png",
                "imgRadio": "radio_inactive.png",
                "lblHomeEmailHiddenVal": "si***n1@gmail.com",
                "lblHomePhnHiddenVal": "si***n1@gmail.com",
                "lblMobilePhnHiddenVal": "si***n1@gmail.com",
                "lblOption": "si***n1@gmail.com",
                "lblPrsnlEmailHiddenVal": "si***n1@gmail.com",
                "lblWrkEmailHiddenVal": "si***n1@gmail.com",
                "lblWrkPhnHiddenVal": "si***n1@gmail.com"
            }, {
                "imgArrow": "calenderarrowright.png",
                "imgRadio": "radio_inactive.png",
                "lblHomeEmailHiddenVal": "si***n1@gmail.com",
                "lblHomePhnHiddenVal": "si***n1@gmail.com",
                "lblMobilePhnHiddenVal": "si***n1@gmail.com",
                "lblOption": "si***n1@gmail.com",
                "lblPrsnlEmailHiddenVal": "si***n1@gmail.com",
                "lblWrkEmailHiddenVal": "si***n1@gmail.com",
                "lblWrkPhnHiddenVal": "si***n1@gmail.com"
            }],
            "groupCells": false,
            "id": "sgmOptions",
            "isVisible": false,
            "left": "0dp",
            "needPageIndicator": true,
            "onRowClick": controller.AS_onRowClick_c0843ebb1de845f28c732635f4e0a710,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": false,
            "rowFocusSkin": "seg2Focus",
            "rowSkin": "seg2Normal",
            "rowTemplate": "flxDropdownOption",
            "scrollingEvents": {},
            "sectionHeaderSkin": "sliPhoneSegmentHeader",
            "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
            "separatorColor": "f1f1f100",
            "separatorRequired": true,
            "separatorThickness": 1,
            "showScrollbars": false,
            "top": "55dp",
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {
                "flxArrow": "flxArrow",
                "flxDropdownOption": "flxDropdownOption",
                "flxRadio": "flxRadio",
                "imgArrow": "imgArrow",
                "imgRadio": "imgRadio",
                "lblHomeEmailHiddenVal": "lblHomeEmailHiddenVal",
                "lblHomePhnHiddenVal": "lblHomePhnHiddenVal",
                "lblMobilePhnHiddenVal": "lblMobilePhnHiddenVal",
                "lblOption": "lblOption",
                "lblPrsnlEmailHiddenVal": "lblPrsnlEmailHiddenVal",
                "lblWrkEmailHiddenVal": "lblWrkEmailHiddenVal",
                "lblWrkPhnHiddenVal": "lblWrkPhnHiddenVal"
            },
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "sgmOptions"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "sgmOptions"), extendConfig({}, controller.args[2], "sgmOptions"));
        dropdown.add(flxDropdownHeader, sgmOptions);
        return dropdown;
    }
})