define("CheckDepositModule/userfrmCheckDepositToController", {
    segData: [],
    searchText: "",
    checkDepositToInit: function() {
        var navManager = applicationManager.getNavigationManager();
        var currentForm = navManager.getCurrentForm();
        applicationManager.getPresentationFormUtility().initCommonActions(this, "YES", currentForm);
    },
    preShow: function() {
        this.view.tbxSearch.text = "";
        this.renderTitleBar();
        this.setSegmentData();
        this.view.customHeader.flxBack.onClick = this.flxBackOnClick;
        this.view.customHeader.btnRight.onClick = this.onCancelClick;
        this.view.btnAddAccount.onClick = this.btnAddAccountOnClick;
        this.view.tbxSearch.onTextChange = this.onSearchTextChange;
        applicationManager.getPresentationUtility().dismissLoadingScreen();
        var navManager = applicationManager.getNavigationManager();
        var currentForm = navManager.getCurrentForm();
        applicationManager.getPresentationFormUtility().logFormName(currentForm);
    },
    renderTitleBar: function() {
        var deviceUtilManager = applicationManager.getDeviceUtilManager();
        var isIphone = deviceUtilManager.isIPhone();
        if (isIphone) {
            this.view.flxHeader.setVisibility(false);
        }
    },
    flxBackOnClick: function() {
        var navManager = applicationManager.getNavigationManager();
        navManager.goBack();
    },
    onCancelClick: function() {
        var navManager = applicationManager.getNavigationManager();
        var prevForm = navManager.getPreviousForm();
        var checkDepositModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CheckDepositModule");
        checkDepositModule.presentationController.commonFunctionForNavigation(prevForm);
    },
    getWidgetDataMap: function() {
        var dataMap = {
            "lblAccountNumber": "lblAccountNumber",
            "lblAccountName": "nickName",
            "lblBankName": "lblBankName",
            "lblAccountBalValue": "lblAccountBalValue",
            "lblAccountBal": "AvailableBalStaticLabel",
            "imgBank": "imgBank"
        };
        return dataMap;
    },
    segToAccountOnClick: function() {
        var selectedToAccountData = this.view.segToAccount.selectedRowItems[0];
        var checkDepositModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CheckDepositModule");
        checkDepositModule.presentationController.setSelectedAccountData(selectedToAccountData);
    },
    setSegmentData: function(segData) {
        var navManager = applicationManager.getNavigationManager();
        var dataToSet = [];
        if (!segData) {
            this.segData = navManager.getCustomInfo("frmCheckDepositTo");
            dataToSet = this.segData;
        } else {
            dataToSet = segData;
        }
        if (dataToSet && dataToSet.length !== 0) {
            var dataMap = this.getWidgetDataMap();
            this.view.segToAccount.widgetDataMap = dataMap;
            this.view.segToAccount.setData(dataToSet);
            this.view.segToAccount.setVisibility(true);
            this.view.flxNoTransactions.setVisibility(false);
        } else {
            this.view.segToAccount.setVisibility(false);
            this.view.flxNoTransactions.setVisibility(true);
        }
    },
    onSearchTextChange: function() {
        var searchText = this.view.tbxSearch.text.toLowerCase();
        var navManager = applicationManager.getNavigationManager();
        if (searchText && this.segData.length !== 0) {
            this.view.segToAccount.removeAll();
            var data = this.segData;
            var searchSegData = applicationManager.getDataProcessorUtility().commonSegmentSearch("lblAccountName", searchText, data);
            this.setSegmentData(searchSegData);
        } else if (!searchText && this.segData.length !== 0) {
            this.setSegmentData(this.segData);
        }
        applicationManager.getPresentationUtility().dismissLoadingScreen();
    }
});
define("CheckDepositModule/frmCheckDepositToControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onRowClick defined for segToAccount **/
    AS_Segment_ba7add50a71c4631a5ddec4a5fe1bbb3: function AS_Segment_ba7add50a71c4631a5ddec4a5fe1bbb3(eventobject, sectionNumber, rowNumber) {
        var self = this;
        this.segToAccountOnClick();
    },
    /** onClick defined for undefined **/
    AS_BarButtonItem_efd0f97d04714895844b595780483c12: function AS_BarButtonItem_efd0f97d04714895844b595780483c12(eventobject) {
        var self = this;
        this.onCancelClick();
    },
    /** init defined for frmCheckDepositTo **/
    AS_Form_a346d929df954279884198710fa59322: function AS_Form_a346d929df954279884198710fa59322(eventobject) {
        var self = this;
        this.checkDepositToInit();
    },
    /** preShow defined for frmCheckDepositTo **/
    AS_Form_j9576dfd3bbe4f959e842e0db8a27807: function AS_Form_j9576dfd3bbe4f959e842e0db8a27807(eventobject) {
        var self = this;
        this.preShow();
    }
});
define("CheckDepositModule/frmCheckDepositToController", ["CheckDepositModule/userfrmCheckDepositToController", "CheckDepositModule/frmCheckDepositToControllerActions"], function() {
    var controller = require("CheckDepositModule/userfrmCheckDepositToController");
    var controllerActions = ["CheckDepositModule/frmCheckDepositToControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
