define({
    ValidationUtilManager: function() {
        this.phoneNumberRegex = /^([0-9]){7,15}$/;
        this.passwordRegex = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).*$/;
        this.invalidChar = "@\/\\";
        //allowed characters.... !"#$%&()+,-/;<=>?[\]^_`{|}*'
        this.zipRegex = /^[0-9a-zA-Z]*$/;
        this.urlRegex = /^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/|www\.)[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/;
    },
    isValidUserName: function(username) {
        if (username === null || username === undefined || username === "") return false;
        else if (username.indexOf(" ") != -1) {
            return false;
        } else if (username.length < 6 || username.length > 20) {
            return false;
        } else if (!this.isInvalidCharacterPresentUserName(username)) {
            return false;
        }
        return true;
    },
    isInvalidCharacterPresentUserName: function(username) {
        var regexp = this.zipRegex;
        if (!(username.match(regexp))) {
            return false;
        }
        return true;
    },
    isValidPassword: function(password) {
        if (password === null || password === undefined || password === "") return false;
        else if (password.indexOf(" ") != -1) {
            return false;
        } else if (password.length < 8 || password.length > 20) {
            return false;
        } else if (!this.isInvalidCharacterPresent(password)) {
            return false;
        } else if (!this.passwordExpressionMatch(password)) {
            return false;
        }
        return true;
    },
    passwordExpressionMatch: function(val) {
        if (!val.match(this.passwordRegex)) {
            return false;
        }
        return true;
    },
    isInvalidCharacterPresent: function(value) {
        var invalidChar = "@~. :"; //allowed characters............ !"#$%&()+,-/;<=>?[\]^_`{|}*'
        var regexp = invalidChar;
        for (var i = 0; i < regexp.length; i++) {
            if (value.indexOf(regexp[i]) != -1) {
                return false;
            }
        }
        return true;
    }
});