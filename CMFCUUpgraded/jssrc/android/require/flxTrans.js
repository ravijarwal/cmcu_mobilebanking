define("flxTrans", function() {
    return function(controller) {
        var flxTrans = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "focusSkin": "f9f9",
            "height": "70dp",
            "id": "flxTrans",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "f9f9",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxTrans.setDefaultUnit(kony.flex.DP);
        var lblTransactions = new kony.ui.Label({
            "id": "lblTransactions",
            "isVisible": true,
            "left": "19dp",
            "maxNumberOfLines": 1,
            "skin": "sknLbl424242SSP26px",
            "textStyle": {
                "letterSpacing": 0,
                "strikeThrough": false
            },
            "textTruncatePosition": constants.TEXT_TRUNCATE_END,
            "top": "15dp",
            "width": "55%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "textCopyable": false
        });
        var lblTransactionsAmount = new kony.ui.Label({
            "bottom": "10dp",
            "centerY": "32dp",
            "id": "lblTransactionsAmount",
            "isVisible": true,
            "right": "10dp",
            "skin": "sknLbl424242SSP32px",
            "textStyle": {
                "letterSpacing": 0,
                "strikeThrough": false
            },
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "textCopyable": false
        });
        var lblRunningBalance = new kony.ui.Label({
            "bottom": "10dp",
            "centerY": "73.57%",
            "id": "lblRunningBalance",
            "isVisible": true,
            "right": "10dp",
            "skin": "sknBlackLbl",
            "textStyle": {
                "letterSpacing": 0,
                "strikeThrough": false
            },
            "top": 5,
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "textCopyable": false
        });
        var flxSeparator = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0dp",
            "clipBounds": true,
            "height": "1dp",
            "id": "flxSeparator",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxf1f1f1",
            "width": "100%",
            "zIndex": 1
        }, {}, {});
        flxSeparator.setDefaultUnit(kony.flex.DP);
        flxSeparator.add();
        var flxWrapper = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "15dp",
            "clipBounds": false,
            "height": "15dp",
            "id": "flxWrapper",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "20dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "65%",
            "zIndex": 1
        }, {}, {});
        flxWrapper.setDefaultUnit(kony.flex.DP);
        var imgIndicator = new kony.ui.Image2({
            "centerY": "50%",
            "height": "15dp",
            "id": "imgIndicator",
            "isVisible": false,
            "left": "0dp",
            "skin": "slImage",
            "src": "recurrenceindication.png",
            "width": "15dp",
            "zIndex": 1
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblDate = new kony.ui.Label({
            "bottom": "0dp",
            "id": "lblDate",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknLbla0a0a022px",
            "textStyle": {
                "letterSpacing": 0,
                "strikeThrough": false
            },
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "textCopyable": false
        });
        flxWrapper.add(imgIndicator, lblDate);
        flxTrans.add(lblTransactions, lblTransactionsAmount, lblRunningBalance, flxSeparator, flxWrapper);
        return flxTrans;
    }
})