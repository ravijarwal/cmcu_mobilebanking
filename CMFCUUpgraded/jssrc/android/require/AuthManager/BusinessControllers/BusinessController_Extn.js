define({
    //Invoking CMCUIdentityService in fabric 
    invokeIdentityService: function(presentationSuccess, presentationError) {
        var authParams = {
            "loginOptions": {
                "isOfflineEnabled": false
            }
        };
        authClient = KNYMobileFabric.getIdentityService("CMCUIdentityService");
        authClient.login(authParams, successCallback, errorCallback);

        function successCallback(resSuccess) {
            presentationSuccess(resSuccess);
        }

        function errorCallback(resError) {
            presentationError(resError);
        }
    },
    //code for fetching otp
    sendOTP: function(params, presentationSuccessCallback, presentationErrorCallback) {
        var self = this;
        var reqotp =  kony.mvc.MDAApplication.getSharedInstance().getRepoManager().getRepository("User");
        reqotp.customVerb('sendOTP', params, OTPCompletionCallback);

        function  OTPCompletionCallback(status,  data,  error) {
            var srh = applicationManager.getServiceResponseHandler();
            var obj = srh.manageResponse(status, data, error);
            kony.print("Data from OTP Enrollment... " + JSON.stringify(obj));
            if (obj["status"] === true) {
                presentationSuccessCallback(obj["data"]);
            } else {
                presentationErrorCallback(obj["errmsg"]);
            }
        }
    },
    //code for calling resend otp service
    resendOTP: function(params, presentationSuccessCallback, presentationErrorCallback) {
        var self = this;
        var resendotp =  kony.mvc.MDAApplication.getSharedInstance().getRepoManager().getRepository("User");
        resendotp.customVerb('resendOTP', params, resendOTPCompletionCallback);

        function  resendOTPCompletionCallback(status,  data,  error) {
            var srh = applicationManager.getServiceResponseHandler();
            var obj = srh.manageResponse(status, data, error);
            kony.print("Data from resend OTP Enrollment... " + JSON.stringify(obj));
            if (obj["status"] === true) {
                presentationSuccessCallback(obj["data"]);
            } else {
                presentationErrorCallback(obj["errmsg"]);
            }
        }
    },
    getDownTimeMessage: function(downTimesuccessCB, downTimeErrorCB) {
        var downTimeService =  kony.mvc.MDAApplication.getSharedInstance().getRepoManager().getRepository("Messages");
        downTimeService.customVerb('DownTimeMessaeService', {}, downTimeServiceCompletionCallback);

        function  downTimeServiceCompletionCallback(status,  data,  error) {
            var srh = applicationManager.getServiceResponseHandler();
            var obj = srh.manageResponse(status, data, error);
            if (obj["status"] === true) {
                downTimesuccessCB(obj["data"]);
            } else {
                downTimeErrorCB(obj["errmsg"]);
            }
        }
    },
    login: function(UsernamePasswordJSON, presentationSuccess, presentationError) {
        var authParams = {
            "username": UsernamePasswordJSON.username,
            "password": UsernamePasswordJSON.password,
            "loginOptions": {
                "isOfflineEnabled": false
            }
        };
        var configManager = applicationManager.getConfigurationManager();
        authClient = KNYMobileFabric.getIdentityService("CMCUIdentityService");
        authClient.login(authParams, successCallback, errorCallback);

        function successCallback(resSuccess) {
            presentationSuccess(resSuccess);
        }

        function errorCallback(resError) {
            presentationError(resError);
        }
    }
});