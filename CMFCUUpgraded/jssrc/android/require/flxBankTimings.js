define("flxBankTimings", function() {
    return function(controller) {
        var flxBankTimings = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxBankTimings",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknLightGreyFlx",
            "top": "5dp",
            "width": "100%",
            "zIndex": 1
        }, {}, {});
        flxBankTimings.setDefaultUnit(kony.flex.DP);
        var lblTimingTitle = new kony.ui.Label({
            "centerY": "20%",
            "id": "lblTimingTitle",
            "isVisible": true,
            "left": "6%",
            "skin": "sknLbl3e4f56SSPBold26px",
            "text": "Monday - Thursday",
            "textStyle": {
                "letterSpacing": 0,
                "strikeThrough": false
            },
            "top": "5%",
            "width": "50%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "textCopyable": false
        });
        var lblTimingValue = new kony.ui.Label({
            "id": "lblTimingValue",
            "isVisible": false,
            "skin": "sknLbl424242SSP26px",
            "textStyle": {
                "letterSpacing": 0,
                "strikeThrough": false
            },
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "textCopyable": false
        });
        var lblTimeZone = new kony.ui.Label({
            "id": "lblTimeZone",
            "isVisible": true,
            "left": "6%",
            "skin": "sknLbl424242SSP26px",
            "text": "Label",
            "textStyle": {
                "letterSpacing": 0,
                "strikeThrough": false
            },
            "top": "0%",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "textCopyable": false
        });
        flxBankTimings.add(lblTimingTitle, lblTimingValue, lblTimeZone);
        return flxBankTimings;
    }
})