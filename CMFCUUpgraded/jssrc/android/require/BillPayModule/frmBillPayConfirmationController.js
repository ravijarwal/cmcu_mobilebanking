define("BillPayModule/userfrmBillPayConfirmationController", {
    init: function() {
        var navManager = applicationManager.getNavigationManager();
        var currentForm = navManager.getCurrentForm();
        applicationManager.getPresentationFormUtility().initCommonActions(this, "YES", currentForm);
    },
    preShow: function() {
        if (kony.os.deviceInfo().name === "iPhone") {
            this.view.flxHeader.isVisible = false;
        }
        this.populateData();
        this.initActions();
        var navManager = applicationManager.getNavigationManager();
        var currentForm = navManager.getCurrentForm();
        applicationManager.getPresentationFormUtility().logFormName(currentForm);
    },
    initActions: function() {
        this.view.customHeader.flxBack.onClick = function() {
            var navMan = applicationManager.getNavigationManager();
            navMan.goBack();
        }
        this.view.customHeader.btnRight.onClick = function() {
            var billPayModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule");
            billPayModule.presentationController.cancelCommon();
        }
        this.view.btnContinue.onClick = this.continueOnClick;
    },
    continueOnClick: function() {
        var description = this.view.txtDescription.text;
        var billPayModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule");
        billPayModule.presentationController.makeATransfer(description);
    },
    populateData: function() {
        var  billPayMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule");
        var transObj = billPayMod.presentationController.getTransObject();
        var forUtility = applicationManager.getFormatUtilManager();
        var amount = forUtility.formatAmountandAppendCurrencySymbol(transObj.amount, transObj.transactionCurrency);
        var dataobj;
        this.view.lblPaymentAmountValue.text = amount;
        this.view.lblFromAccountValue.text = transObj.fromAccNickName;
        this.view.lblBank.text = transObj.fromBankName;
        this.view.lblToPayeeValue.text = transObj.payeeNickName;
        this.view.lblPayeeAddress.text = transObj.payeeAdress;
        this.view.segDetails.widgetDataMap = {
            lblKey: "key",
            lblValue: "value",
        };
        if (transObj.transactionsNotes) this.view.txtDescription.text = transObj.transactionsNotes;
        else this.view.txtDescription.text = "";
        if (transObj.isScheduled == "0") {
            dataobj = [{
                "key": kony.i18n.getLocalizedString("kony.mb.transaction.frequency"),
                "value": kony.i18n.getLocalizedString("kony.mb.frequency.TransferNow")
            }, {
                "key": kony.i18n.getLocalizedString("kony.mb.Transfers.transfersDate"),
                "value": forUtility.getFormattedCalendarDate(transObj.scheduledDate)
            }]
        } else {
            if (transObj.frequencyType == "Once") {
                dataobj = [{
                    "key": kony.i18n.getLocalizedString("kony.mb.transaction.frequency"),
                    "value": kony.i18n.getLocalizedString("kony.mb.frequency.OneTime")
                }, {
                    "key": kony.i18n.getLocalizedString("kony.mb.Transfers.transfersDate"),
                    "value": forUtility.getFormattedCalendarDate(transObj.scheduledDate)
                }]
            } else if (scope_BillPayPresentationController.getDuration() == applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.frequency.DateRange")) {
                dataobj = [{
                    "key": kony.i18n.getLocalizedString("kony.mb.transaction.frequency"),
                    "value": transObj.frequencyType
                }, {
                    "key": kony.i18n.getLocalizedString("kony.mb.Transfers.Duration"),
                    "value": kony.i18n.getLocalizedString("kony.mb.frequency.DateRange")
                }, {
                    "key": kony.i18n.getLocalizedString("kony.mb.Transfers.StartDate"),
                    "value": forUtility.getFormattedCalendarDate(transObj.frequencyStartDate)
                }, {
                    "key": kony.i18n.getLocalizedString("kony.mb.Transfers.EndDate"),
                    "value": forUtility.getFormattedCalendarDate(transObj.frequencyEndDate)
                }]
            } else if (scope_BillPayPresentationController.getDuration() == applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.Transfers.RecurrenceNo")) {
                dataobj = [{
                    "key": kony.i18n.getLocalizedString("kony.mb.transaction.frequency"),
                    "value": transObj.frequencyType
                }, {
                    "key": kony.i18n.getLocalizedString("kony.mb.Transfers.Duration"),
                    "value": kony.i18n.getLocalizedString("kony.mb.Transfers.RecurrenceNo")
                }, {
                    "key": kony.i18n.getLocalizedString("kony.mb.frequency.NumberofRecurrence"),
                    "value": transObj.numberOfRecurrences
                }, {
                    "key": kony.i18n.getLocalizedString("kony.mb.Transfers.StartDate"),
                    "value": forUtility.getFormattedCalendarDate(transObj.scheduledDate)
                }]
            }
        }
        this.view.segDetails.setData(dataobj);
    }
});
define("BillPayModule/frmBillPayConfirmationControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for btnContinue **/
    AS_Button_jf9de2a245f04ad79bce9b4de67dc9c9: function AS_Button_jf9de2a245f04ad79bce9b4de67dc9c9(eventobject) {
        var self = this;
        var billPayMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule");
        billPayMod.presentationController.commonFunctionForNavigation("frmBillPay");
    },
    /** onClick defined for undefined **/
    AS_BarButtonItem_f1c6ee9e787b4f82bdeecab60260c466: function AS_BarButtonItem_f1c6ee9e787b4f82bdeecab60260c466(eventobject) {
        var self = this;
        var transferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule");
        transferModule.presentationController.cancelCommon();
    },
    /** init defined for frmBillPayConfirmation **/
    AS_Form_f58adfd38d30494a833dc7970d5049c7: function AS_Form_f58adfd38d30494a833dc7970d5049c7(eventobject) {
        var self = this;
        this.init();
    },
    /** preShow defined for frmBillPayConfirmation **/
    AS_Form_fc9fa43e93794226be55b07e7d5811c4: function AS_Form_fc9fa43e93794226be55b07e7d5811c4(eventobject) {
        var self = this;
        this.preShow();
    }
});
define("BillPayModule/frmBillPayConfirmationController", ["BillPayModule/userfrmBillPayConfirmationController", "BillPayModule/frmBillPayConfirmationControllerActions"], function() {
    var controller = require("BillPayModule/userfrmBillPayConfirmationController");
    var controllerActions = ["BillPayModule/frmBillPayConfirmationControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
