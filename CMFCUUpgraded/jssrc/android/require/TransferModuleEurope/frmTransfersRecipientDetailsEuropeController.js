define("TransferModuleEurope/userfrmTransfersRecipientDetailsEuropeController", {
    init: function() {
        var navManager = applicationManager.getNavigationManager();
        var currentForm = navManager.getCurrentForm();
        applicationManager.getPresentationFormUtility().initCommonActions(this, "YES", currentForm);
    },
    frmPreShow: function() {
        this.view.txtRecipientName.onTextChange = this.btnContinueHandler;
        this.view.txtLastName.onTextChange = this.btnContinueHandler;
        this.view.customHeader.flxBack.onClick = this.flxBackOnClick;
        this.view.customHeader.btnRight.onClick = this.flxBackOnClick;
        this.view.btnContinue.onClick = this.btnContinueOnClick;
        this.view.customHeader.btnRight.onClick = this.onClickCancel;
        this.renderTitleBar();
        applicationManager.getPresentationUtility().dismissLoadingScreen();
        var navManager = applicationManager.getNavigationManager();
        var currentForm = navManager.getCurrentForm();
        applicationManager.getPresentationFormUtility().logFormName(currentForm);
    },
    btnRightOnClick: function() {},
    renderTitleBar: function() {
        if (kony.os.deviceInfo().name === 'iPhone') {
            this.view.flxHeader.setVisibility(false);
        }
    },
});
define("TransferModuleEurope/frmTransfersRecipientDetailsEuropeControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for undefined **/
    AS_BarButtonItem_ef0049161b6d40d18e84be786a54fcf6: function AS_BarButtonItem_ef0049161b6d40d18e84be786a54fcf6(eventobject) {
        var self = this;
        this.onClickCancel();
    },
    /** init defined for frmTransfersRecipientDetailsEurope **/
    AS_Form_eafca79a20b94407b5207b40d934da21: function AS_Form_eafca79a20b94407b5207b40d934da21(eventobject) {
        var self = this;
        this.init();
    },
    /** preShow defined for frmTransfersRecipientDetailsEurope **/
    AS_Form_d92a3e16687f43fb8488cd6ec6858832: function AS_Form_d92a3e16687f43fb8488cd6ec6858832(eventobject) {
        var self = this;
        this.frmPreShow();
    }
});
define("TransferModuleEurope/frmTransfersRecipientDetailsEuropeController", ["TransferModuleEurope/userfrmTransfersRecipientDetailsEuropeController", "TransferModuleEurope/frmTransfersRecipientDetailsEuropeControllerActions"], function() {
    var controller = require("TransferModuleEurope/userfrmTransfersRecipientDetailsEuropeController");
    var controllerActions = ["TransferModuleEurope/frmTransfersRecipientDetailsEuropeControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
