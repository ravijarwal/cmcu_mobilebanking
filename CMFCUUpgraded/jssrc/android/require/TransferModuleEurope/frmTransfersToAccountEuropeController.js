define("TransferModuleEurope/userfrmTransfersToAccountEuropeController", {
    transferType: '',
    init: function() {
        var navManager = applicationManager.getNavigationManager();
        var currentForm = navManager.getCurrentForm();
        applicationManager.getPresentationFormUtility().initCommonActions(this, "YES", currentForm);
    },
    preShow: function() {
        //  applicationManager.getPresentationUtility().dismissLoadingScreen();
        if (kony.os.deviceInfo().name === "iPhone") {
            this.view.flxHeader.isVisible = false;
            this.view.flxMainContainer.top = "0dp";
        }
        this.clearTextBox();
        this.initActions();
        applicationManager.getPresentationUtility().dismissLoadingScreen();
        var navManager = applicationManager.getNavigationManager();
        var currentForm = navManager.getCurrentForm();
        applicationManager.getPresentationFormUtility().logFormName(currentForm);
    },
    initActions: function() {
        var scope = this;
        this.view.customHeader.flxBack.onClick = function() {
            var navMan = applicationManager.getNavigationManager();
            navMan.goBack();
        }
        this.view.customHeader.btnRight.onClick = this.cancelOnClick;
        this.view.flxClose.onTouchStart = this.clearTextBox;
        this.view.tbxTo.onTextChange = this.showRecepSuggestions;
        this.view.segToAccount.onRowClick = this.chooseRecep;
        this.view.btnAddRecipient.onClick = this.onClickContinue;
    },
    chooseRecep: function() {
        var rowindex = Math.floor(this.view.segToAccount.selectedRowIndex[1]);
        var selectedAccountData = this.view.segToAccount.data[rowindex];
        this.view.tbxTo.text = selectedAccountData.IBAN;
        this.view.flxClose.setVisibility(true);
        this.view.flxToAccountHints.setVisibility(false);
        this.view.flxNewRecipient.setVisibility(false);
        this.view.flxNoTransactions.setVisibility(false);
        this.enableContinueButton();
    },
    clearTextBox: function() {
        this.view.tbxTo.text = "";
        this.view.flxClose.setVisibility(false);
        this.view.flxToAccountHints.setVisibility(false);
        this.view.flxNewRecipient.setVisibility(false);
        this.view.flxNoTransactions.setVisibility(false);
        this.disableContinueButton();
        var navMan = applicationManager.getNavigationManager();
        var accdata = navMan.getCustomInfo("frmTransfersToAccount");
        accdata.selectedAccountData = {}
        navMan.setCustomInfo("frmTransfersToAccount", accdata);
    },
    showRecepSuggestions: function() {
        var data = this.view.tbxTo.text;
        var navMan = applicationManager.getNavigationManager();
        var accdata = navMan.getCustomInfo("frmTransfersToAccount");
        if (data.length) {
            this.view.flxClose.setVisibility(true);
            var TransModPresentationController = applicationManager.getModulesPresentationController("TransferModule");
            TransModPresentationController.fetchExternalAccountsByIbanOrName(data);
        } else this.clearTextBox();
    },
    cancelOnClick: function() {
        var TransModPresentationController = applicationManager.getModulesPresentationController("TransferModule");
        TransModPresentationController.cancelCommon();
    },
    segmentDataSet: function(recepList) {
        if (recepList.length) {
            this.view.segToAccount.widgetDataMap = {
                lblNickNameValue: "beneficiaryName",
                lblIBANValue: "IBAN"
            };
            this.view.segToAccount.setData(recepList);
            this.view.flxToAccountHints.setVisibility(true);
            this.view.flxNewRecipient.setVisibility(false);
            this.view.flxNoTransactions.setVisibility(false);
            this.enableContinueButton();
        } else {
            this.view.flxToAccountHints.setVisibility(false);
            this.view.flxNoTransactions.setVisibility(true);
            if (applicationManager.getValidationUtilManager().isValidIBAN(this.view.tbxTo.text)) {
                this.enableContinueButton();
                this.view.flxNewRecipient.setVisibility(true);
            } else {
                this.view.flxNewRecipient.setVisibility(false);
                this.disableContinueButton();
            }
        }
    },
    enableContinueButton: function() {
        this.view.btnAddRecipient.setEnabled(true);
        this.view.btnAddRecipient.skin = "sknBtn0095e4RoundedffffffSSP26px";
    },
    disableContinueButton: function() {
        this.view.btnAddRecipient.setEnabled(false);
        this.view.btnAddRecipient.skin = "sknBtna0a0a0SSPReg26px";
    },
    onClickContinue: function() {
        applicationManager.getPresentationUtility().showLoadingScreen();
        var IBAN = this.view.tbxTo.text;
        if (IBAN != "" && !kony.sdk.isNullOrUndefined(IBAN) && applicationManager.getValidationUtilManager().isValidIBAN(IBAN)) {
            var navMan = applicationManager.getNavigationManager();
            var accdata = navMan.getCustomInfo("frmTransfersToAccount");
            if (kony.sdk.isNullOrUndefined(accdata.selectedAccountData)) accdata.selectedAccountData = {}
            accdata.selectedAccountData.IBAN = applicationManager.getFormatUtilManager().deFormatIBAN(IBAN);
            navMan.setCustomInfo("frmTransfersToAccount", accdata);
            var TransModPresentationController = applicationManager.getModulesPresentationController("TransferModule");
            TransModPresentationController.checkExistingAccountwithIBAN(IBAN);
            //navMan.navigateTo("frmTransfersRecipientNameEurope");
        } else {
            this.bindGenericError(applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.TransfersEurope.InvalidIBAN"));
        }
    },
    bindGenericError: function(msg) {
        applicationManager.getPresentationUtility().dismissLoadingScreen();
        applicationManager.getDataProcessorUtility().showToastMessageError(this, msg);
    }
});
define("TransferModuleEurope/frmTransfersToAccountEuropeControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for flxBack **/
    AS_FlexContainer_c4b25eb5d2f9417198a943f3da548521: function AS_FlexContainer_c4b25eb5d2f9417198a943f3da548521(eventobject) {
        var self = this;
        var transModPresentationController = applicationManager.getModulesPresentationController("TransferModule");
        transModPresentationController.commonFunctionForNavigation("frmTransactionMode");
    },
    /** onClick defined for flxClose **/
    AS_FlexContainer_b01abfe9dc8f432390264517d3247adc: function AS_FlexContainer_b01abfe9dc8f432390264517d3247adc(eventobject) {
        var self = this;
        self.view.tbxTo.text = "";
    },
    /** onRowClick defined for segToAccount **/
    AS_Segment_b2ea93c408774da5a0c350b379bb7ef8: function AS_Segment_b2ea93c408774da5a0c350b379bb7ef8(eventobject, sectionNumber, rowNumber) {
        var self = this;
        self.view.segToAccount.isVisible = false;
        self.view.flxNewRecipient.isVisible = true;
        self.view.tbxTo.text = "FR 4567 7456 3425 4532 1423 4356 664 ";
    },
    /** onClick defined for undefined **/
    AS_BarButtonItem_c5f2fd79d433482fb530200ddb09e3dc: function AS_BarButtonItem_c5f2fd79d433482fb530200ddb09e3dc(eventobject) {
        var self = this;
        this.cancelOnClick();
    },
    /** init defined for frmTransfersToAccountEurope **/
    AS_Form_dd71ec169af548c99e8208c990ffe06f: function AS_Form_dd71ec169af548c99e8208c990ffe06f(eventobject) {
        var self = this;
        this.init();
    },
    /** preShow defined for frmTransfersToAccountEurope **/
    AS_Form_h9341b312f3f4fedbc0857b76b7167cd: function AS_Form_h9341b312f3f4fedbc0857b76b7167cd(eventobject) {
        var self = this;
        this.preShow();
    }
});
define("TransferModuleEurope/frmTransfersToAccountEuropeController", ["TransferModuleEurope/userfrmTransfersToAccountEuropeController", "TransferModuleEurope/frmTransfersToAccountEuropeControllerActions"], function() {
    var controller = require("TransferModuleEurope/userfrmTransfersToAccountEuropeController");
    var controllerActions = ["TransferModuleEurope/frmTransfersToAccountEuropeControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
