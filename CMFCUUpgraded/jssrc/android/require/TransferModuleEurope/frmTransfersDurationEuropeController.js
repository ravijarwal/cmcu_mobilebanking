define("TransferModuleEurope/userfrmTransfersDurationEuropeController", {
    init: function() {
        var navManager = applicationManager.getNavigationManager();
        var currentForm = navManager.getCurrentForm();
        applicationManager.getPresentationFormUtility().initCommonActions(this, "YES", currentForm);
    },
    preShow: function() {
        if (kony.os.deviceInfo().name === "iPhone") {
            this.view.flxHeader.isVisible = false;
        }
        var navMan = applicationManager.getNavigationManager();
        var  transMod = applicationManager.getModulesPresentationController("TransferModule");
        var index = transMod.getIndexForDuration();
        this.view.segDuration.rowFocusSkin = "";
        this.view.segDuration.retainSelection = false;
        if (index !== null && index !== undefined && index !== "") {
            this.view.segDuration.rowFocusSkin = "sknFlxf9f9f9";
            this.view.segDuration.retainSelection = true;
            this.view.segDuration.selectedRowIndex = [0, index];
        }
        this.initActions();
        applicationManager.getPresentationUtility().dismissLoadingScreen();
        var navManager = applicationManager.getNavigationManager();
        var currentForm = navManager.getCurrentForm();
        applicationManager.getPresentationFormUtility().logFormName(currentForm);
    },
    initActions: function() {
        var scope = this;
        this.view.segDuration.onRowClick = function() {
            scope.segmentRowClick();
        }
        this.view.customHeader.flxBack.onClick = function() {
            var navMan = applicationManager.getNavigationManager();
            navMan.goBack();
        }
        this.view.customHeader.btnRight.onClick = function() {
            scope.cancelOnClick();
        }
    },
    cancelOnClick: function() {
        var transferModule = applicationManager.getModulesPresentationController("TransferModule");
        transferModule.cancelCommon();
    },
    segmentRowClick: function() {
        var navMan = applicationManager.getNavigationManager();
        var index = this.view.segDuration.data[this.view.segDuration.selectedIndex[1]].lblFrequency;
        navMan.setCustomInfo("frmTransfersDuration", {
            "duration": index
        });
        var transferModule = applicationManager.getModulesPresentationController("TransferModule");
        transferModule.switchDurationType(index);
    }
});
define("TransferModuleEurope/frmTransfersDurationEuropeControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for flxBack **/
    AS_FlexContainer_gc71ad4a6abe490e8f114a170e83e64b: function AS_FlexContainer_gc71ad4a6abe490e8f114a170e83e64b(eventobject) {
        var self = this;
        var transModPresentationController = applicationManager.getModulesPresentationController("TransferModule");
        transModPresentationController.commonFunctionForNavigation("frmTransferFrequency");
    },
    /** onClick defined for undefined **/
    AS_BarButtonItem_i6aa4c8c58d144b4a60d390fb17fc676: function AS_BarButtonItem_i6aa4c8c58d144b4a60d390fb17fc676(eventobject) {
        var self = this;
        this.cancelOnClick();
    },
    /** init defined for frmTransfersDurationEurope **/
    AS_Form_c99de74c06c4457ea7a4b4064e7a77b7: function AS_Form_c99de74c06c4457ea7a4b4064e7a77b7(eventobject) {
        var self = this;
        this.init();
    },
    /** preShow defined for frmTransfersDurationEurope **/
    AS_Form_d998f65670154703aa375bca9dfddfc1: function AS_Form_d998f65670154703aa375bca9dfddfc1(eventobject) {
        var self = this;
        this.preShow();
    }
});
define("TransferModuleEurope/frmTransfersDurationEuropeController", ["TransferModuleEurope/userfrmTransfersDurationEuropeController", "TransferModuleEurope/frmTransfersDurationEuropeControllerActions"], function() {
    var controller = require("TransferModuleEurope/userfrmTransfersDurationEuropeController");
    var controllerActions = ["TransferModuleEurope/frmTransfersDurationEuropeControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
