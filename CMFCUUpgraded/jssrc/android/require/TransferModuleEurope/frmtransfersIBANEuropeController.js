define("TransferModuleEurope/userfrmtransfersIBANEuropeController", {
    keypadString: '',
    init: function() {
        this.initActions();
    },
    preShow: function() {
        if (kony.os.deviceInfo().name === "iPhone") {
            this.view.flxHeader.isVisible = false;
        }
        var transferModulePresentationController = applicationManager.getModulesPresentationController("TransferModule");
        var IBAN = transferModulePresentationController.getIBAN();
        if (IBAN) {
            this.view.tbxIBAN.text = IBAN;
        } else {
            this.view.tbxIBAN.text = "";
            this.disableContinueButton();
        }
        var navManager = applicationManager.getNavigationManager();
        var currentForm = navManager.getCurrentForm();
        applicationManager.getPresentationUtility().dismissLoadingScreen();
        applicationManager.getPresentationFormUtility().logFormName(currentForm);
    },
    initActions: function() {
        var scope = this;
        var navManager = applicationManager.getNavigationManager();
        var currentForm = navManager.getCurrentForm();
        applicationManager.getPresentationFormUtility().initCommonActions(this, "YES", currentForm);
        this.view.tbxIBAN.onTextChange = scope.navigateToAccountNumber;
        this.view.customHeader.flxBack.onClick = scope.flxBackOnClick;
        this.view.btnContinue.onClick = function() {
            var IBAN = scope.view.tbxIBAN.text;
            var transferModule = applicationManager.getModulesPresentationController("TransferModule");
            var isValidIBAN = transferModule.isValidIBAN(IBAN, "frmtransfersIBANEurope");
            if (isValidIBAN) {
                //transferModule.navigateToEnterBenificiaryNameFromIBAN(IBAN);
                scope.fetchBankDetailsForDomestic();
            }
        };
        this.view.customHeader.btnRight.onClick = scope.onClickCancel;
    },
    getServiceName:   function(displayName) {
        var  serviceName;
        if (displayName  ===  "InternationalAccountsTransfer") {
            serviceName  =  "International Account to Account Fund Transfer";
        } 
        else  if  (displayName  ===  "OtherBankAccountsTransfer") {
            serviceName  =  "Interbank Account to Account Fund Transfer";
        }
        var  servicesForUser  =  applicationManager.getConfigurationManager().getServicesListForUser();
        if  (servicesForUser) {
            serviceName  =  servicesForUser.filter(function(dataItem) {
                if (dataItem.displayName  ===  displayName)  return  true;
            });
            if (serviceName  &&  serviceName.length  >  0) {
                serviceName  =  serviceName[0].serviceName;
            }
        }
        return  serviceName
    },
    /** Fetches bank details of international bank
     */
    fetchBankDetailsForDomestic: function() {
        applicationManager.getPresentationUtility().showLoadingScreen();
        var scope = this;
        var InternationalBankServiceName = "Internation Account to Account Fund Transfer";
        var IBAN = scope.view.tbxIBAN.text;
        var serviceName = scope.getServiceName("InternationalAccountsTransfer");
        if (IBAN !== "") {
            var transferModulePresentationController = applicationManager.getModulesPresentationController("TransferModule");
            transferModulePresentationController.fetchBankDetailsForDomesticTransfer(IBAN, serviceName);
        }
    },
    validateIBAN: function(response) {
        applicationManager.getPresentationUtility().dismissLoadingScreen();
        var scope = this;
        if (response !== "") {
            var transferModulePresentationController = applicationManager.getModulesPresentationController("TransferModule");
            var IBAN = scope.view.tbxIBAN.text;
            transferModulePresentationController.navigateToEnterBenificiaryNameFromIBAN(IBAN, response);
        } else {
            var transferModulePresentationController = applicationManager.getModulesPresentationController("TransferModule");
            var IBAN = scope.view.tbxIBAN.text;
            response = "KonyBank";
            transferModulePresentationController.navigateToEnterBenificiaryNameFromIBAN(IBAN, response);
        }
    },
    onClickCancel: function() {
        applicationManager.getPresentationUtility().showLoadingScreen();
        var navManager = applicationManager.getNavigationManager();
        var navigateToForm = navManager.getEntryPoint("createInternalBankBenificiary");
        var transferModPresentationController = applicationManager.getModulesPresentationController("TransferModule");
        transferModPresentationController.commonFunctionForNavigation(navigateToForm);
    },
    flxBackOnClick: function() {
        var navMan = applicationManager.getNavigationManager();
        navMan.goBack();
    },
    navigateToAccountNumber: function() {
        var IBAN = this.view.tbxIBAN.text;
        if (IBAN.length > 0) {
            this.enableContinueButton();
        } else {
            this.disableContinueButton();
        }
    },
    enableContinueButton: function() {
        this.view.btnContinue.setEnabled(true);
        this.view.btnContinue.skin = "sknBtn0095e4RoundedffffffSSP26px";
    },
    disableContinueButton: function() {
        this.view.btnContinue.setEnabled(false);
        this.view.btnContinue.skin = "sknBtna0a0a0SSPReg26px";
    },
    bindGenericError: function(errorMsg) {
        applicationManager.getPresentationUtility().dismissLoadingScreen();
        var scopeObj = this;
        applicationManager.getDataProcessorUtility().showToastMessageError(scopeObj, errorMsg);
    }
});
define("TransferModuleEurope/frmtransfersIBANEuropeControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for btnContinue **/
    AS_Button_hbfc0d68a6784cd18e567f09afebcb78: function AS_Button_hbfc0d68a6784cd18e567f09afebcb78(eventobject) {
        var self = this;
        var ntf = new kony.mvc.Navigation("frmBenNameEurope");
        ntf.navigate();
    },
    /** onClick defined for undefined **/
    AS_BarButtonItem_a4ad93bbacfe4e9eae91c08dd95187f3: function AS_BarButtonItem_a4ad93bbacfe4e9eae91c08dd95187f3(eventobject) {
        var self = this;
        this.cancelOnClick();
    },
    /** init defined for frmtransfersIBANEurope **/
    AS_Form_aa217f7297e54858a9fbfbcbcc0099b9: function AS_Form_aa217f7297e54858a9fbfbcbcc0099b9(eventobject) {
        var self = this;
        this.init();
    },
    /** preShow defined for frmtransfersIBANEurope **/
    AS_Form_hdd9bd35abc04f7aa33b1c84ae187ef2: function AS_Form_hdd9bd35abc04f7aa33b1c84ae187ef2(eventobject) {
        var self = this;
        this.preShow();
    }
});
define("TransferModuleEurope/frmtransfersIBANEuropeController", ["TransferModuleEurope/userfrmtransfersIBANEuropeController", "TransferModuleEurope/frmtransfersIBANEuropeControllerActions"], function() {
    var controller = require("TransferModuleEurope/userfrmtransfersIBANEuropeController");
    var controllerActions = ["TransferModuleEurope/frmtransfersIBANEuropeControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
