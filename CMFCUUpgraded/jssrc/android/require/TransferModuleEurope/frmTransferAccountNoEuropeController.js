define("TransferModuleEurope/userfrmTransferAccountNoEuropeController", {
    timerCounter: 0,
    keypadString: '',
    init: function() {},
    frmPreshow: function() {
        this.keypadString = '';
        if (kony.os.deviceInfo().name !== "iPhone") {
            this.view.flxHeader.isVisible = true;
        } else {
            this.view.flxHeader.isVisible = false;
        }
        this.view.lblAccountNo.text = "";
    },
    setKeypadChar: function(char) {
        this.keypadString = this.keypadString + char;
        if (this.keypadString.length > 0 && this.keypadString.length < 17) {
            this.enterCodePostAction();
        } else if (this.keypadString.length < 1) {
            this.incompleteCodeView();
        } else if (this.keypadString.length > 16) {
            this.keypadString = this.keypadString.slice(0, 16);
            return;
        }
        //this.updateInputBullets("flxInputAccNo");
        this.view.lblAccountNo.text = this.keypadString;
    },
    clearKeypadChar: function() {
        if (this.keypadString.length === 1) {
            this.keypadString = '';
            //   this.updateInputBullets("flxInputAccNo");
        }
        if (this.keypadString.length !== 0) {
            this.keypadString = this.keypadString.substr(0, this.keypadString.length - 1);
            if (this.keypadString.length < 1) {
                this.incompleteCodeView();
            }
            // this.updateInputBullets("flxInputAccNo");
        }
        if (this.keypadString.length < 1) {
            this.incompleteCodeView();
        }
        this.view.lblAccountNo.text = this.keypadString;
    },
    updateInputBullets: function(inputFlx) {
        var widgets = this.view[inputFlx].widgets();
        for (var i = 0; i < this.keypadString.length; i++) {
            // widgets[i].skin = "sknLbl484848sspReg50px";
            widgets[i].text = this.keypadString[i];
        }
        for (var i = this.keypadString.length; i < widgets.length; i++) {
            //widgets[i].skin = "sknLble3e3e3SSP60px";
            widgets[i].text = '_';
        }
        this.view.forceLayout();
    },
    enterCodePostAction: function() {
        this.view.btnContinue.setEnabled(true);
        this.view.btnContinue.skin = "sknBtn0095e4RoundedffffffSSP26px";
    },
    incompleteCodeView: function() {
        this.view.btnContinue.skin = "sknBtna0a0a0SSPReg26px";
        this.view.btnContinue.setEnabled(false);
    },
});
define("TransferModuleEurope/frmTransferAccountNoEuropeControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for btnOne **/
    AS_Button_db5b046542ab45a785108e5c0436a964: function AS_Button_db5b046542ab45a785108e5c0436a964(eventobject) {
        var self = this;
        this.setKeypadChar(1);
    },
    /** onClick defined for btnTwo **/
    AS_Button_cfadd208ba3c45d1b97ced31935d0666: function AS_Button_cfadd208ba3c45d1b97ced31935d0666(eventobject) {
        var self = this;
        this.setKeypadChar(2);
    },
    /** onClick defined for btnThree **/
    AS_Button_de5bfb24366e47a592ca397708797a0c: function AS_Button_de5bfb24366e47a592ca397708797a0c(eventobject) {
        var self = this;
        this.setKeypadChar(3);
    },
    /** onClick defined for btnFour **/
    AS_Button_d86d5843ca77442d91d6bb98409a4a42: function AS_Button_d86d5843ca77442d91d6bb98409a4a42(eventobject) {
        var self = this;
        this.setKeypadChar(4);
    },
    /** onClick defined for btnFive **/
    AS_Button_f9c0fce2b5eb41259e8ad81e93e75122: function AS_Button_f9c0fce2b5eb41259e8ad81e93e75122(eventobject) {
        var self = this;
        this.setKeypadChar(5);
    },
    /** onClick defined for btnSix **/
    AS_Button_ae8ab674eebb4b3eaa7727721c2c34f4: function AS_Button_ae8ab674eebb4b3eaa7727721c2c34f4(eventobject) {
        var self = this;
        this.setKeypadChar(6);
    },
    /** onClick defined for btnSeven **/
    AS_Button_caa1ad56b14e44d18e4d535afbb88b31: function AS_Button_caa1ad56b14e44d18e4d535afbb88b31(eventobject) {
        var self = this;
        this.setKeypadChar(7);
    },
    /** onClick defined for btnEight **/
    AS_Button_b8383835d5af4177b3f6c282fb6d2cad: function AS_Button_b8383835d5af4177b3f6c282fb6d2cad(eventobject) {
        var self = this;
        this.setKeypadChar(8);
    },
    /** onClick defined for btnNine **/
    AS_Button_d0960484e0764780b4c2affb7a4f9a70: function AS_Button_d0960484e0764780b4c2affb7a4f9a70(eventobject) {
        var self = this;
        this.setKeypadChar(9);
    },
    /** onClick defined for btnZero **/
    AS_Button_e5e986cd1a7d420187bec01ab8d7ebe0: function AS_Button_e5e986cd1a7d420187bec01ab8d7ebe0(eventobject) {
        var self = this;
        this.setKeypadChar(0);
    },
    /** onTouchEnd defined for imgClearKeypad **/
    AS_Image_b5ff7c6be9ee4659b5bbf9eb9dcfb326: function AS_Image_b5ff7c6be9ee4659b5bbf9eb9dcfb326(eventobject, x, y) {
        var self = this;
        this.clearKeypadChar();
    },
    /** init defined for frmTransferAccountNoEurope **/
    AS_Form_da03db176fd04aa0a0846f4d63812478: function AS_Form_da03db176fd04aa0a0846f4d63812478(eventobject) {
        var self = this;
        this.init();
    },
    /** preShow defined for frmTransferAccountNoEurope **/
    AS_Form_iedd8fdeeea44393956a12918b923582: function AS_Form_iedd8fdeeea44393956a12918b923582(eventobject) {
        var self = this;
        return self.frmPreshow.call(this);
    }
});
define("TransferModuleEurope/frmTransferAccountNoEuropeController", ["TransferModuleEurope/userfrmTransferAccountNoEuropeController", "TransferModuleEurope/frmTransferAccountNoEuropeControllerActions"], function() {
    var controller = require("TransferModuleEurope/userfrmTransferAccountNoEuropeController");
    var controllerActions = ["TransferModuleEurope/frmTransferAccountNoEuropeControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
