define("TransferModuleEurope/userfrmTransfersEndDateEuropeController", {
    freq: '',
    init: function() {
        var navManager = applicationManager.getNavigationManager();
        var currentForm = navManager.getCurrentForm();
        applicationManager.getPresentationFormUtility().initCommonActions(this, "YES", currentForm);
    },
    preShow: function() {
        if (kony.os.deviceInfo().name === "iPhone") {
            this.view.flxHeader.isVisible = false;
        }
        if (this.view.customCalendar.selectedDate === '') {
            this.view.btnContinue.setEnabled(false);
        } else {
            this.view.btnContinue.setEnabled(true);
        }
        this.view.customCalendar.preShow();
        this.view.customCalendar.selectedDate = '';
        //  this.view.customCalendar.isCalendarEndDateFrm = true;
        this.view.customCalendar.triggerContinueAction = false;
        //   this.view.customCalendar.isOnceTransaction = false;
        this.view.customCalendar.updateDateBullets();
        //  this.view.customCalendar.unHighlightAllDays(); 
        var  transMod = applicationManager.getModulesPresentationController("TransferModule");
        var startdate = transMod.getTransObject();
        this.view.customCalendar.setFirstEnabledDate(startdate.scheduledDate);
        var forUtility = applicationManager.getFormatUtilManager();
        if (startdate.endCalendarDate !== null && startdate.endCalendarDate !== undefined && startdate.endCalendarDate !== "") {
            this.setDateToCalendar(startdate.endCalendarDate);
        } else if (startdate.frequencyEndDate !== null && startdate.frequencyEndDate !== undefined && startdate.frequencyEndDate !== "") {
            this.setDateToCalendar(startdate.endCalendarDate);
        } else {
            this.view.customCalendar.setSelectedDate(startdate.scheduledDate);
        }
        this.initActions();
        this.view.customCalendar.resetCal();
        applicationManager.getPresentationUtility().dismissLoadingScreen();
        var navManager = applicationManager.getNavigationManager();
        var currentForm = navManager.getCurrentForm();
        applicationManager.getPresentationFormUtility().logFormName(currentForm);
    },
    initActions: function() {
        var scope = this;
        this.view.btnContinue.onClick = this.continueAction;
        this.view.customHeader.flxBack.onClick = function() {
            var navMan = applicationManager.getNavigationManager();
            navMan.goBack();
        }
        this.view.customHeader.btnRight.onClick = function() {
            scope.cancelOnClick();
        }
    },
    setDateToCalendar: function(dateString) {
        var forUtility = applicationManager.getFormatUtilManager();
        var configManager = applicationManager.getConfigurationManager();
        var frequencyEndDate = forUtility.getDateObjectFromCalendarString(dateString, configManager.getCalendarDateFormat());
        frequencyEndDate = forUtility.getFormattedSelectedDate(frequencyEndDate);
        this.view.customCalendar.setSelectedDate(frequencyEndDate);
    },
    cancelOnClick: function() {
        var transferModule = applicationManager.getModulesPresentationController("TransferModule");
        transferModule.cancelCommon();
    },
    continueAction: function() {
        var transferModule = applicationManager.getModulesPresentationController("TransferModule");
        transferModule.transferScheduledEndDate(this.view.customCalendar.getSelectedDate());
    }
});
define("TransferModuleEurope/frmTransfersEndDateEuropeControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for btnContinue **/
    AS_Button_c1c43725c0a94e619fd05968713eca0e: function AS_Button_c1c43725c0a94e619fd05968713eca0e(eventobject) {
        var self = this;
        // var transModPresentationController = applicationManager.getModulesPresentationController("TransferModule");
        // transModPresentationController.commonFunctionForNavigation("frmTransferConfirmation");
        var ntf = new kony.mvc.Navigation("frmTransferConfirmationEurope");
        ntf.navigate();
    },
    /** onClick defined for undefined **/
    AS_BarButtonItem_c31fa734fdee40129534f89de3f5a0f4: function AS_BarButtonItem_c31fa734fdee40129534f89de3f5a0f4(eventobject) {
        var self = this;
        this.cancelOnClick();
    },
    /** init defined for frmTransfersEndDateEurope **/
    AS_Form_i619b43ed99c4f50952c447759d7e7f7: function AS_Form_i619b43ed99c4f50952c447759d7e7f7(eventobject) {
        var self = this;
        this.init();
    },
    /** preShow defined for frmTransfersEndDateEurope **/
    AS_Form_ee086ccf0f7b49249bf1d37f3970e8bf: function AS_Form_ee086ccf0f7b49249bf1d37f3970e8bf(eventobject) {
        var self = this;
        this.preShow();
    }
});
define("TransferModuleEurope/frmTransfersEndDateEuropeController", ["TransferModuleEurope/userfrmTransfersEndDateEuropeController", "TransferModuleEurope/frmTransfersEndDateEuropeControllerActions"], function() {
    var controller = require("TransferModuleEurope/userfrmTransfersEndDateEuropeController");
    var controllerActions = ["TransferModuleEurope/frmTransfersEndDateEuropeControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
