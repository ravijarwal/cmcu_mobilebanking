define("TransferModuleEurope/userfrmTransfersStartDateEuropeController", {
    freq: '',
    startDateKey: '',
    onNavigate: function(obj) {
        if (obj == undefined) {
            return;
        }
        this.startDateKey = obj;
        if (this.startDateKey === "oneTime") {
            this.view.customHeader.lblLocateUs.text = "Send Date";
        } else {
            this.view.customHeader.lblLocateUs.text = "Start Date";
        }
    },
    init: function() {
        var navManager = applicationManager.getNavigationManager();
        var currentForm = navManager.getCurrentForm();
        applicationManager.getPresentationFormUtility().initCommonActions(this, "YES", currentForm);
    },
    preShow: function() {
        if (kony.os.deviceInfo().name === "iPhone") {
            this.view.flxHeader.isVisible = false;
        }
        if (this.view.customCalendar.selectedDate === '') {
            this.view.btnContinue.setEnabled(false);
        } else {
            this.view.btnContinue.setEnabled(true);
        }
        this.initActions();
        this.view.customCalendar.preShow();
        this.view.customCalendar.selectedDate = '';
        // this.view.customCalendar.isCalendarEndDateFrm = false;
        this.view.customCalendar.triggerContinueAction = true;
        //  this.view.customCalendar.isOnceTransaction = false;
        this.view.customCalendar.updateDateBullets();
        //  this.view.customCalendar.unHighlightAllDays(); 
        this.view.customCalendar.setFirstEnabledDate();
        var  transMod = applicationManager.getModulesPresentationController("TransferModule");
        var startdate = transMod.getTransObject();
        var navMan = applicationManager.getNavigationManager();
        var data = navMan.getCustomInfo("frmTransfersStartDate");
        this.freq = data.freq;
        var forUtility = applicationManager.getFormatUtilManager();
        //       if(this.freq==="Once"){
        //         this.view.customCalendar.isOnceTransaction = true;
        //       }
        var info = navMan.getCustomInfo("frmTransfersDuration");
        if (info)
            if (startdate.scheduledCalendarDate !== null && startdate.scheduledCalendarDate !== undefined && startdate.scheduledCalendarDate !== "") {
                this.setDateToCalendar(startdate.scheduledCalendarDate);
            } else if (startdate.scheduledDate !== null && startdate.scheduledDate !== undefined && startdate.scheduledDate !== "") {
            this.setDateToCalendar(startdate.scheduledDate);
        } else {
            var startDate = new Date();
            var startDateFeed1 = (startDate.getMonth() + 1) + "/" + startDate.getDate() + "/" + startDate.getFullYear();
            this.view.customCalendar.setFirstEnabledDate(startDateFeed1);
            //   this.view.customCalendar.setSelectedDateValue('');
        }
        if (this.freq === "Once") {
            // this.view.customHeader.lblLocateUs.text = applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.frequency.sendDateCaps");
            this.view.customHeader.lblLocateUs.text = "Send Date";
            this.view.btnContinue.isVisible = true;
            this.view.customCalendar.triggerContinueAction = false;
        } else {
            // this.view.customHeader.lblLocateUs.text = applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.frequency.startDateCaps");
            this.view.customHeader.lblLocateUs.text = "Start Date";
            this.view.btnContinue.isVisible = false;
            this.view.customCalendar.triggerContinueAction = true;
        }
        this.view.customCalendar.resetCal();
        applicationManager.getPresentationUtility().dismissLoadingScreen();
        var navManager = applicationManager.getNavigationManager();
        var currentForm = navManager.getCurrentForm();
        applicationManager.getPresentationFormUtility().logFormName(currentForm);
    },
    initActions: function() {
        var scope = this;
        this.view.customHeader.flxBack.onClick = function() {
            var navMan = applicationManager.getNavigationManager();
            navMan.goBack();
        }
        this.view.btnContinue.onClick = this.continueAction;
        this.view.customHeader.btnRight.onClick = function() {
            scope.cancelOnClick();
        }
    },
    setDateToCalendar: function(dateString) {
        var forUtility = applicationManager.getFormatUtilManager();
        var configManager = applicationManager.getConfigurationManager();
        var scheduledDate = forUtility.getDateObjectFromCalendarString(dateString, configManager.getCalendarDateFormat());
        scheduledDate = forUtility.getFormattedSelectedDate(scheduledDate);
        this.view.customCalendar.setSelectedDate(scheduledDate);
    },
    cancelOnClick: function() {
        var transferModule = applicationManager.getModulesPresentationController("TransferModule");
        transferModule.cancelCommon();
    },
    backAction: function() {
        var transferModule = applicationManager.getModulesPresentationController("TransferModule");
        if (this.startDateKey === "oneTime") {
            transferModule.commonFunctionForNavigation("frmTransferFrequency");
        } else if (this.startDateKey === "recurrence") {
            transferModule.commonFunctionForNavigation("frmTransfersRecurrence");
        } else {
            transferModule.commonFunctionForNavigation("frmTransfersDuration");
        }
    },
    continueAction: function() {
        var transferModule = applicationManager.getModulesPresentationController("TransferModule");
        if (this.freq === "Once" || this.freq === "NofRR") {
            transferModule.transferScheduledDate(this.view.customCalendar.getSelectedDate());
        } else {
            transferModule.transferScheduledStrtDate(this.view.customCalendar.getSelectedDate());
        }
    }
});
define("TransferModuleEurope/frmTransfersStartDateEuropeControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for btnContinue **/
    AS_Button_ed7b851da4f44619b16faebe734997c0: function AS_Button_ed7b851da4f44619b16faebe734997c0(eventobject) {
        var self = this;
        var ntf = new kony.mvc.Navigation("frmTransfersEndDateEurope");
        ntf.navigate();
    },
    /** onClick defined for undefined **/
    AS_BarButtonItem_bdd3b46820a84c10b995a744d830368b: function AS_BarButtonItem_bdd3b46820a84c10b995a744d830368b(eventobject) {
        var self = this;
        this.cancelOnClick();
    },
    /** init defined for frmTransfersStartDateEurope **/
    AS_Form_aad4dc71a28944b3ab3cb4306e0ffeb6: function AS_Form_aad4dc71a28944b3ab3cb4306e0ffeb6(eventobject) {
        var self = this;
        this.init();
    },
    /** preShow defined for frmTransfersStartDateEurope **/
    AS_Form_ddb3eda8aacf4a0b9e460fd6895c773a: function AS_Form_ddb3eda8aacf4a0b9e460fd6895c773a(eventobject) {
        var self = this;
        this.preShow();
    }
});
define("TransferModuleEurope/frmTransfersStartDateEuropeController", ["TransferModuleEurope/userfrmTransfersStartDateEuropeController", "TransferModuleEurope/frmTransfersStartDateEuropeControllerActions"], function() {
    var controller = require("TransferModuleEurope/userfrmTransfersStartDateEuropeController");
    var controllerActions = ["TransferModuleEurope/frmTransfersStartDateEuropeControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
