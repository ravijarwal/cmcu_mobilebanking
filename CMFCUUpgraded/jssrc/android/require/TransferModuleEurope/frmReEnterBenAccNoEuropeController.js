define("TransferModuleEurope/userfrmReEnterBenAccNoEuropeController", {
    timerCounter: 0,
    keypadString: '',
    init: function() {
        this.initActions();
    },
    frmPreshow: function() {
        this.keypadString = '';
        this.view.lblAccNo.text = "";
        var transferModulePresentationController = applicationManager.getModulesPresentationController("TransferModule");
        var accountNumber = transferModulePresentationController.getReEnteredAccountNumber();
        if (accountNumber) {
            this.keypadString = accountNumber;
            this.enterCodePostAction();
        } else {
            this.incompleteCodeView();
        }
        // this.updateInputBullets("flxReInputAccNo");
        var navManager = applicationManager.getNavigationManager();
        var currentForm = navManager.getCurrentForm();
        applicationManager.getPresentationUtility().dismissLoadingScreen();
        applicationManager.getPresentationFormUtility().logFormName(currentForm);
        this.renderTitleBar();
        //this.view.customHeader.btnRight.onClick=this.flxBackOnClick;
    },
    initActions: function() {
        var scope = this;
        var navManager = applicationManager.getNavigationManager();
        var currentForm = navManager.getCurrentForm();
        applicationManager.getPresentationFormUtility().initCommonActions(this, "YES", currentForm);
        this.view.btnContinue.onClick = scope.btnContinueOnClick;
        this.view.customHeader.flxBack.onClick = scope.flxBackOnClick;
        this.view.customHeader.btnRight.onClick = scope.onClickCancel;
    },
    btnRightOnClick: function() {},
    renderTitleBar: function() {
        if (kony.os.deviceInfo().name === 'iPhone') {
            this.view.flxHeader.setVisibility(false);
        }
    },
    btnContinueOnClick: function() {
        var match = false;
        var reEneterdAccountNumber = this.keypadString;
        var transferModulePresentationController = applicationManager.getModulesPresentationController("TransferModule");
        var benificiaryData = transferModulePresentationController.getBenificiaryData();
        var initialAccountNumber = benificiaryData.accountNumber;
        if (initialAccountNumber === reEneterdAccountNumber) {
            match = true;
        } else {
            match = false;
        }
        if (match) {
            var transferModulePresentationController = applicationManager.getModulesPresentationController("TransferModule");
            var isValidAccNo = transferModulePresentationController.isValidAccNum(reEneterdAccountNumber, "frmReEnterBenAccNoEurope");
            if (isValidAccNo) {
                transferModulePresentationController.navigateToBenificiaryName(reEneterdAccountNumber);
            }
        } else {
            this.showErrorPopup(applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.cardless.wrongAccountNumberMessage"));
            this.keypadString = '';
            //	this.updateInputBullets("flxReInputAccNo");
            this.incompleteCodeView();
        }
    },
    flxBackOnClick: function() {
        var navMan = applicationManager.getNavigationManager();
        navMan.goBack();
    },
    setKeypadChar: function(char) {
        this.keypadString = this.keypadString + char;
        if (this.keypadString.length > 0 && this.keypadString.length < 17) {
            this.enterCodePostAction();
        } else if (this.keypadString.length < 1) {
            this.incompleteCodeView();
        } else if (this.keypadString.length > 16) {
            this.keypadString = this.keypadString.slice(0, 16);
            return;
        }
        // this.updateInputBullets("flxReInputAccNo");
        this.view.lblAccNo.text = this.keypadString;
    },
    showErrorPopup: function(errorMsg) {
        applicationManager.getPresentationUtility().dismissLoadingScreen();
        var scopeObj = this;
        applicationManager.getDataProcessorUtility().showToastMessageError(scopeObj, errorMsg);
    },
    clearKeypadChar: function() {
        if (this.keypadString.length === 1) {
            this.keypadString = '';
            //  this.updateInputBullets("flxReInputAccNo");
        }
        if (this.keypadString.length !== 0) {
            this.keypadString = this.keypadString.substr(0, this.keypadString.length - 1);
            if (this.keypadString.length < 1) {
                this.incompleteCodeView();
            }
            //   this.updateInputBullets("flxReInputAccNo");
        }
        if (this.keypadString.length < 1) {
            this.incompleteCodeView();
        }
        this.view.lblAccNo.text = this.keypadString;
    },
    updateInputBullets: function(inputFlx) {
        var widgets = this.view[inputFlx].widgets();
        for (var i = 0; i < this.keypadString.length; i++) {
            // widgets[i].skin = "sknLbl484848sspReg50px";
            widgets[i].text = this.keypadString[i];
        }
        for (var i = this.keypadString.length; i < widgets.length; i++) {
            //widgets[i].skin = "sknLble3e3e3SSP60px";
            widgets[i].text = '_';
        }
        this.view.forceLayout();
    },
    enterCodePostAction: function() {
        this.view.btnContinue.setEnabled(true);
        this.view.btnContinue.skin = "sknBtn0095e4RoundedffffffSSP26px";
    },
    incompleteCodeView: function() {
        this.view.btnContinue.skin = "sknBtna0a0a0SSPReg26px";
        this.view.btnContinue.setEnabled(false);
    },
    onClickCancel: function() {
        applicationManager.getPresentationUtility().showLoadingScreen();
        var navManager = applicationManager.getNavigationManager();
        var navigateToForm = navManager.getEntryPoint("createInternalBankBenificiary");
        var transferModPresentationController = applicationManager.getModulesPresentationController("TransferModule");
        transferModPresentationController.commonFunctionForNavigation(navigateToForm);
    },
    bindGenericError: function(errorMsg) {
        applicationManager.getPresentationUtility().dismissLoadingScreen();
        var scopeObj = this;
        applicationManager.getDataProcessorUtility().showToastMessageError(scopeObj, errorMsg);
    }
});
define("TransferModuleEurope/frmReEnterBenAccNoEuropeControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for btnOne **/
    AS_Button_ad60f3012cd141b598af55e808a56314: function AS_Button_ad60f3012cd141b598af55e808a56314(eventobject) {
        var self = this;
        this.setKeypadChar(1);
    },
    /** onClick defined for btnTwo **/
    AS_Button_h31fe2d010fd4901ae9079f8e76f16e0: function AS_Button_h31fe2d010fd4901ae9079f8e76f16e0(eventobject) {
        var self = this;
        this.setKeypadChar(2);
    },
    /** onClick defined for btnThree **/
    AS_Button_h9b5554ea0bf40ad801ffbe96f4b7b25: function AS_Button_h9b5554ea0bf40ad801ffbe96f4b7b25(eventobject) {
        var self = this;
        this.setKeypadChar(3);
    },
    /** onClick defined for btnFour **/
    AS_Button_ce77106e7bde4536ae616ff2fc0ccf57: function AS_Button_ce77106e7bde4536ae616ff2fc0ccf57(eventobject) {
        var self = this;
        this.setKeypadChar(4);
    },
    /** onClick defined for btnFive **/
    AS_Button_a666fb5dbfa64cd98ca4f7f2feac9927: function AS_Button_a666fb5dbfa64cd98ca4f7f2feac9927(eventobject) {
        var self = this;
        this.setKeypadChar(5);
    },
    /** onClick defined for btnSix **/
    AS_Button_cf9cc641a605491d83fcd14e2009debc: function AS_Button_cf9cc641a605491d83fcd14e2009debc(eventobject) {
        var self = this;
        this.setKeypadChar(6);
    },
    /** onClick defined for btnSeven **/
    AS_Button_jd0c047d04c9465eaaf8f207385ab344: function AS_Button_jd0c047d04c9465eaaf8f207385ab344(eventobject) {
        var self = this;
        this.setKeypadChar(7);
    },
    /** onClick defined for btnEight **/
    AS_Button_c23cc454af564055942359e5453eeab7: function AS_Button_c23cc454af564055942359e5453eeab7(eventobject) {
        var self = this;
        this.setKeypadChar(8);
    },
    /** onClick defined for btnNine **/
    AS_Button_i6d7e93b9b514df69e369cebf2f46726: function AS_Button_i6d7e93b9b514df69e369cebf2f46726(eventobject) {
        var self = this;
        this.setKeypadChar(9);
    },
    /** onClick defined for btnZero **/
    AS_Button_cc6a0ab26ded475787b891ab7fcd1488: function AS_Button_cc6a0ab26ded475787b891ab7fcd1488(eventobject) {
        var self = this;
        this.setKeypadChar(0);
    },
    /** onTouchEnd defined for imgClearKeypad **/
    AS_Image_e79994a483214a7e96edd635ff3f9c1c: function AS_Image_e79994a483214a7e96edd635ff3f9c1c(eventobject, x, y) {
        var self = this;
        this.clearKeypadChar();
    },
    /** onClick defined for undefined **/
    AS_BarButtonItem_a59b0f592ae64def8385ffa2292de82d: function AS_BarButtonItem_a59b0f592ae64def8385ffa2292de82d(eventobject) {
        var self = this;
        this.onClickCancel();
    },
    /** init defined for frmReEnterBenAccNoEurope **/
    AS_Form_i4640369e2a340cfb98e315e9137cadc: function AS_Form_i4640369e2a340cfb98e315e9137cadc(eventobject) {
        var self = this;
        this.init();
    },
    /** preShow defined for frmReEnterBenAccNoEurope **/
    AS_Form_a29b266f3477482abab36260d2e91390: function AS_Form_a29b266f3477482abab36260d2e91390(eventobject) {
        var self = this;
        this.frmPreshow();
    }
});
define("TransferModuleEurope/frmReEnterBenAccNoEuropeController", ["TransferModuleEurope/userfrmReEnterBenAccNoEuropeController", "TransferModuleEurope/frmReEnterBenAccNoEuropeControllerActions"], function() {
    var controller = require("TransferModuleEurope/userfrmReEnterBenAccNoEuropeController");
    var controllerActions = ["TransferModuleEurope/frmReEnterBenAccNoEuropeControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
