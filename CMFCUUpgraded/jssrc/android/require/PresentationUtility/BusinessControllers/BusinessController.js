/**
 *@module PresentationUtility
 */
define([], function() {
    /**
     * PresentationUtility consists of all utilities anf wrapper functions related to Presentation 
     *@alias module:PresentationUtility
     *@class
     */
    function PresentationUtility() {
        /*
  A variable maintained to store row index globally on swipe 
  Note:It is maintained to delete on swipe till platform fix issue related to segment
*/
        /**@member {integer}  number to maintain index for swipe*/
        this.rowIndexforSwipe = -1;
    }
    inheritsFrom(PresentationUtility, kony.mvc.Business.Delegator);
    PresentationUtility.prototype.initializeBusinessController = function() {};
    /**
     * A wrapper on kony alert message for further use
     * @param {JSON} basicConfig - same as basicConfig in kony.ui.Alert
     * @param {JSON} pspConfig - same as pspConfig in kony.ui.Alert
     */
    PresentationUtility.prototype.showAlertMessage = function(basicConfig, pspConfig) {
            if (kony.os.deviceInfo().name === "android") {
                basicConfig.alertIcon = "transparentbox.png";
            }
            kony.ui.Alert(basicConfig, pspConfig);
        }
        /**
         * Returns value of given i18n key in device's locale
         * @param {String} i18n Key - an i18n key to look out for
         * @param {String} noKeyValue(optonal) - returns this when lookout failed 
         * @returns {String} - value associated to that key if its not there noKeyValue is returned
         */
    PresentationUtility.prototype.getStringFromi18n = function(stringValue, noKeyValue) {
            return kony.i18n.getLocalizedString(stringValue) ? kony.i18n.getLocalizedString(stringValue) : noKeyValue;
        }
        /**
         * A UI function to show loading indicator
         */
    PresentationUtility.prototype.showLoadingScreen = function() {
            kony.application.showLoadingScreen(null, "", constants.LOADING_SCREEN_POSITION_ONLY_CENTER, true, true, null);
        }
        /**
         * A UI function to dismiss loading indicator
         */
    PresentationUtility.prototype.dismissLoadingScreen = function() {
            kony.application.dismissLoadingScreen();
        }
        /**
         * Returns the controller of the requested form
         * @param {String} formname - Name of the form for which the controller is required
         * @param {Boolean} isForm - expects true if the requested controller is of a form 
         * @returns {object} - returns the requested controller(kony.mvc.MDAFormController)
         */
    PresentationUtility.prototype.getController = function(formname, isForm) {
        var controller = _kony.mvc.GetController(formname, isForm);
        return controller;
    };
    PresentationUtility.prototype.MFA = {
        phoneAndEmail: {
            "phone": "",
            "email": ""
        },
        setPhoneAndEmail: function(phoneAndEmail) {
            this.phoneAndEmail = phoneAndEmail;
        },
        getPhoneAndEmail: function() {
            return this.phoneAndEmail;
        },
        navigateBasedOnMFAType: function() {
            var mfaManager = applicationManager.getMFAManager();
            switch (mfaManager.getMFAType()) {
                case "SECURE_ACCESS_CODE":
                    this.navigateToOtpScreen();
                    break;
                case "SECURITY_QUESTIONS":
                    this.navigateAndSetSecurityQuestions();
                    break;
            }
        },
        navigateToOtpScreen: function() {
            var mfaManager = applicationManager.getMFAManager();
            switch (mfaManager.getCommunicationType()) {
                case "DISPLAY_ALL":
                    this.navigateToPhoneEmailScreen();
                    break;
                case "DISPLAY_NO_VALUE":
                    this.navigateToSecureCodeScreen();
                    break;
                case "DISPLAY_PRIMARY":
                    this.navigateToSecureCodeScreen();
                    break;
            }
        },
        navigateToPhoneEmailScreen: function() {
            applicationManager.getPresentationUtility().showLoadingScreen();
            var navigationManager = applicationManager.getNavigationManager();
            navigationManager.navigateTo("frmMFAOption3");
            var mfaManager = applicationManager.getMFAManager();
            var mfaResponse = mfaManager.getMFAResponse();
            var flowType = mfaManager.getMFAFlowType();
            var controller = applicationManager.getPresentationUtility().getController('frmMFAOption3', true);
            controller.setFormUI(mfaResponse, flowType);
        },
        navigateToSecureCodeScreen: function() {
            applicationManager.getPresentationUtility().showLoadingScreen();
            var navigationManager = applicationManager.getNavigationManager();
            navigationManager.navigateTo("frmMFASecurityCode");
            var mfaManager = applicationManager.getMFAManager();
            var mfaResponse = mfaManager.getMFAResponse();
            var controller = applicationManager.getPresentationUtility().getController('frmMFASecurityCode', true);
            controller.setFormUI(mfaResponse);
        },
        setSecureCodeScreen: function() {
            applicationManager.getPresentationUtility().showLoadingScreen();
            var mfaManager = applicationManager.getMFAManager();
            var mfaResponse = mfaManager.getMFAResponse();
            var controller = applicationManager.getPresentationUtility().getController('frmMFASecurityCode', true);
            controller.setFormUI(mfaResponse);
        },
        showMFAOTPScreen: function() {
            applicationManager.getPresentationUtility().showLoadingScreen();
            var navigationManager = applicationManager.getNavigationManager();
            navigationManager.navigateTo("frmMFASecurityCode");
            var mfaManager = applicationManager.getMFAManager();
            var mfaResponse = mfaManager.getMFAResponse();
            var controller = applicationManager.getPresentationUtility().getController('frmMFASecurityCode', true);
            controller.setFormUI(mfaResponse);
        },
        navigateAndSetSecurityQuestions: function() {
            applicationManager.getPresentationUtility().showLoadingScreen();
            var navManager = applicationManager.getNavigationManager();
            navManager.navigateTo("frmSecurityQuestions");
            var controller = applicationManager.getPresentationUtility().getController('frmSecurityQuestions', true);
            var mfaManager = applicationManager.getMFAManager();
            var mfaAttributes = mfaManager.getMFAResponse().MFAAttributes;
            controller.setFormUI(mfaAttributes);
        },
        navigateToSecurityQuestion: function() {
            var controller = applicationManager.getPresentationUtility().getController('frmSecurityQuestions', true);
            var mfaManager = applicationManager.getMFAManager();
            var mfaAttributes = mfaManager.getMFAResponse().MFAAttributes;
            controller.setFormUI(mfaAttributes);
        },
        setSecurityQuestions: function() {
            applicationManager.getPresentationUtility().showLoadingScreen();
            var controller = applicationManager.getPresentationUtility().getController('frmSecurityQuestions', true);
            var mfaManager = applicationManager.getMFAManager();
            var mfaAttributes = mfaManager.getMFAResponse().MFAAttributes;
            controller.setFormUI(mfaAttributes);
        },
        cancelMFAFlow: function() {
            var mfaManager = applicationManager.getMFAManager();
            var flowType = mfaManager.getMFAFlowType();
            switch (flowType) {
                case "TRANSFERS":
                    var transferModule = applicationManager.getModulesPresentationController("TransferModule");
                    transferModule.cancelCommon();
                    break;
                case "PAYAPERSON":
                    var  p2pMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule");
                    p2pMod.presentationController.cancelCommon();
                    break;
                case "BILLPAY":
                    var billPayMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule");
                    billPayMod.presentationController.cancelCommon();
                    break;
            }
        },
        verifyOTP: function(data) {
            var mfaManager = applicationManager.getMFAManager();
            var data = {
                "MFAAttributes": {
                    "serviceName": mfaManager.getServiceId(),
                    "serviceKey": mfaManager.getServicekey(),
                    "OTP": data
                }
            };
            mfaManager.verifyOTP(data);
        },
        enteredIncorrectOTP: function(error) {
            var controller = applicationManager.getPresentationUtility().getController('frmMFASecurityCode', true);
            controller.showIncorrectOTPError(error);
        },
        navigateToAckScreen: function(response) {
            var mfaManager = applicationManager.getMFAManager();
            var flowType = mfaManager.getMFAFlowType();
            switch (flowType) {
                case "TRANSFERS":
                    var transferModule = applicationManager.getModulesPresentationController("TransferModule");
                    transferModule.presentationMakeATransferSuccess(response);
                    break;
                case "PAYAPERSON":
                    var  p2pMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule");
                    p2pMod.presentationController.createP2pSuccCallback(response);
                    break;
                case "BILLPAY":
                    var billPayMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule");
                    billPayMod.presentationController.presentationMakeATransferSuccess(response);
                    break;
            }
        },
        navigateToTransactionScreen: function(response) {
            this.onMFAError(response);
        },
        onMFAError: function(error) {
            var mfaManager = applicationManager.getMFAManager();
            var flowType = mfaManager.getMFAFlowType();
            switch (flowType) {
                case "TRANSFERS":
                    var transferModule = applicationManager.getModulesPresentationController("TransferModule");
                    transferModule.presentationMakeATransferError(error);
                    break;
                case "PAYAPERSON":
                    var  p2pMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule");
                    p2pMod.presentationController.createP2pErrCallback(error);
                    break;
                case "BILLPAY":
                    var billPayMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule");
                    billPayMod.presentationController.presentationMakeATransferError(error);
                    break;
            }
        },
        requestOTP: function(params) {
            var mfaManager = applicationManager.getMFAManager();
            var data = {
                "MFAAttributes": {
                    "serviceName": mfaManager.getServiceId(),
                    "serviceKey": mfaManager.getServicekey(),
                    "OTP": params
                }
            };
            mfaManager.requestOTP(data);
        },
        mfaOTPError: function(err) {
            applicationManager.getPresentationUtility().dismissLoadingScreen();
            if (err["isServerUnreachable"]) {
                applicationManager.getPresentationInterruptHandler().showErrorMessage("postLogin", err);
            } else {
                var currentForm = kony.application.getCurrentForm().id;
                var controller = applicationManager.getPresentationUtility().getController(currentForm, true);
                controller.bindError(err.errorMessage);
            }
        },
        verifySecurityQuestions: function(data) {
            var mfaManager = applicationManager.getMFAManager();
            var inputparams = {
                "MFAAttributes": {
                    "serviceName": mfaManager.getServiceId(),
                    "serviceKey": mfaManager.getServicekey(),
                    "securityQuestions": data
                }
            };
            mfaManager.verifySecurityQuestions(inputparams);
        },
        getMFAResponse: function() {
            var mfaManager = applicationManager.getMFAManager();
            return mfaManager.getMFAResponse();
        },
        enteredIncorrectAnswer: function(err) {
            var controller = applicationManager.getPresentationUtility().getController('frmSecurityQuestions', true);
            controller.showIncorrectSecurityAnswerError(err);
        },
        getMFAFlowType: function() {
            var mfaManager = applicationManager.getMFAManager();
            return mfaManager.getMFAFlowType();
        },
        getServiceIdBasedOnDisplayName: function(displayName) {
            var configManager = applicationManager.getConfigurationManager();
            var mfaManager = applicationManager.getMFAManager();
            var services = configManager.getServicesListForUser();
            for (var i = 0; i < services.length; i++) {
                if (services[i].displayName === displayName) {
                    mfaManager.setServiceId(services[i].serviceId);
                }
            }
        },
        getDisplayNameBasedOnTransactionMode: function() {
            var transferModulePresentationController = applicationManager.getModulesPresentationController("TransferModule");
            var mfaManager = applicationManager.getMFAManager();
            var displayName = "";
            switch (transferModulePresentationController.transactionMode) {
                case applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.transfer.MyKonyAccounts"):
                    displayName = "KonyBankAccountsTransfer";
                    break;
                case applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.transfer.OtherKonyBankMembers"):
                    displayName = "OtherKonyAccountsTransfer";
                    break;
                case applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.transfer.OtherBankAccounts"):
                    displayName = "OtherBankAccountsTransfer";
                    break;
                case applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.transfer.InternationalTransfer"):
                    displayName = "InternationalAccountsTransfer";
                    break;
            }
            return displayName;
        }
    };
    return PresentationUtility;
});