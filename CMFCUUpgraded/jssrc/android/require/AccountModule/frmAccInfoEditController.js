define("AccountModule/userfrmAccInfoEditController", {
    objRec: '',
    init: function() {
        var navManager = applicationManager.getNavigationManager();
        var currentForm = navManager.getCurrentForm();
        applicationManager.getPresentationFormUtility().initCommonActions(this, "YES", currentForm);
    },
    onNavigate: function(obj) {
        if (obj === undefined) {
            var newObj = {
                "view": "familyCheckingAcc"
            };
            obj = newObj;
        }
        this.objRec = obj;
    },
    txtNickNameTextChange: function() {
        var format = /^[a-zA-Z0-9 ]*$/gm;
        var nckNme = format.test(this.view.txtNickName.text);
        var nickname = this.view.txtNickName.text;
        if (!nckNme) {
            this.view.btnSave.skin = "sknBtnOnBoardingInactive";
            this.view.btnSave.setEnabled(false);
        } else {
            if (nickname === "") {
                this.view.btnSave.skin = "sknBtnOnBoardingInactive";
                this.view.btnSave.setEnabled(false);
            } else {
                this.view.btnSave.skin = "sknBtn0095e426pxEnabled";
                this.view.btnSave.setEnabled(true);
            }
        }
    },
    frmEditNickNamePreShow: function() {
        this.view.customHeader.lblLocateUs.centerX = "50%";
        this.view.customHeader.lblLocateUs.text = "EDIT";
        this.view.customHeader.btnRight.text = kony.i18n.getLocalizedString("kony.mb.common.CancelSmall");
        this.view.customHeader.flxHeader.btnRight.skin = "sknLblb8dcffSSP32px";
        this.view.customHeader.flxHeader.btnRight.focusSkin = "sknLblb8dcffSSP32px";
        this.view.lblNickName.text = kony.i18n.getLocalizedString("kony.mb.accNickName.nickName");
        this.view.btnSave.onClick = this.btnSaveOnClick;
        this.view.customHeader.flxBack.onClick = this.flxBackOnClick;
        this.view.customHeader.btnRight.onClick = this.flxBackOnClick;
        this.view.txtNickName.onTextChange = this.txtNickNameTextChange;
        this.view.txtNickName.onDone = this.btnSaveOnClick;
        applicationManager.getPresentationUtility().dismissLoadingScreen();
        //         if (kony.os.deviceInfo().name !== "iPhone") {
        //             this.view.flxHeader.isVisible = true;
        //         } else {
        //             this.view.flxHeader.isVisible = false;
        //         }
        this.view.flxHeader.setVisibility(true);
        var navigationManager = applicationManager.getNavigationManager();
        var nickName = navigationManager.getCustomInfo("frmAccInfoEdit");
        this.view.txtNickName.placeholder = nickName;
        if (nickName !== null && nickName !== undefined && nickName !== "") {
            this.view.txtNickName.text = nickName;
        } else {
            this.view.txtNickName.text = "";
        }
        this.txtNickNameTextChange();
        var currentForm = navigationManager.getCurrentForm();
        applicationManager.getPresentationFormUtility().logFormName(currentForm);
        applicationManager.getPresentationUtility().dismissLoadingScreen();
        this.view.txtNickName.setEnabled(true);
        this.view.txtNickName.setFocus(true);
    },
    btnSaveOnClick: function() {
        var navMngr = applicationManager.getNavigationManager();
        var nickName = this.view.txtNickName.text.toUpperCase();
        this.view.txtNickName.text = nickName;
        navMngr.setCustomInfo("frmAccInfoEdit", nickName);
        var accMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountModule");
        var data = navMngr.getCustomInfo("accountCode");
        var loanCode = data.loanCode;
        var shareCode = data.shareCode;
        var irsCode = data.irsCode;
        if (this.view.txtNickName.text !== "" && this.view.txtNickName.text !== null && this.view.txtNickName.text !== undefined) {
            applicationManager.getPresentationUtility().showLoadingScreen();
            if (loanCode !== null && typeof loanCode !== "undefined") {
                //account is loanType
                var loanID = data.accountID;
                accMod.presentationController.updateLoanNickName(loanID, nickName);
            } else if ((shareCode !== null && typeof shareCode !== "undefined") || (irsCode !== "undefined" && irsCode !== null && irsCode !== 0)) {
                //account is shareType
                var shareID = data.accountID;
                accMod.presentationController.updateShareNickName(shareID, nickName);
            }
        }
    },
    flxBackOnClick: function() {
        var navMan = applicationManager.getNavigationManager();
        this.accDetails = navMan.goBack();
    },
});
define("AccountModule/frmAccInfoEditControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for undefined **/
    AS_BarButtonItem_dae885c7ddbc44cb946de9bec0b568a2: function AS_BarButtonItem_dae885c7ddbc44cb946de9bec0b568a2(eventobject) {
        var self = this;
        var navMan = applicationManager.getNavigationManager();
        this.accDetails = navMan.goBack();
    },
    /** init defined for frmAccInfoEdit **/
    AS_Form_gf9df2332ade4a2a99bc037bbaa18927: function AS_Form_gf9df2332ade4a2a99bc037bbaa18927(eventobject) {
        var self = this;
        this.init();
    },
    /** preShow defined for frmAccInfoEdit **/
    AS_Form_bd5f43b966b644b399683095b3b21d4f: function AS_Form_bd5f43b966b644b399683095b3b21d4f(eventobject) {
        var self = this;
        this.frmEditNickNamePreShow();
    },
    /** postShow defined for frmAccInfoEdit **/
    AS_Form_bee2858cb2d9483aadb2207ce57c3ade: function AS_Form_bee2858cb2d9483aadb2207ce57c3ade(eventobject) {
        var self = this;
        this.view.txtNickName.setEnabled(true);
        this.view.txtNickName.setFocus(true);
    }
});
define("AccountModule/frmAccInfoEditController", ["AccountModule/userfrmAccInfoEditController", "AccountModule/frmAccInfoEditControllerActions"], function() {
    var controller = require("AccountModule/userfrmAccInfoEditController");
    var controllerActions = ["AccountModule/frmAccInfoEditControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
