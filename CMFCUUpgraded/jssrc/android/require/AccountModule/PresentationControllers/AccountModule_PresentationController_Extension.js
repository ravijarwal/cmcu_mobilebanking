define({
    getTotalAvailableBalance: function(data) {
        var forUtility = applicationManager.getFormatUtilManager();
        var configManager = applicationManager.getConfigurationManager();
        var totalBalance = 0;
        for (i = 0; i < data.length; i++) {
            if (data[i].accountType == configManager.constants.CHECKING && data[i].shareCode == "1" && data[i].irsCode == "0") {
                totalBalance = totalBalance + parseInt(data[i]["availableBalance"]);
            }
        }
        return forUtility.formatAmountandAppendCurrencySymbol(totalBalance);
    },
    getTotalDebtBalance: function(data) {
        var forUtility = applicationManager.getFormatUtilManager();
        var configManager = applicationManager.getConfigurationManager();
        var totalDebt = 0;
        for (i = 0; i < data.length; i++) {
            if (data[i].accountType == configManager.constants.SAVINGS && data[i].shareCode == "0" && data[i].irsCode == "0") {
                totalDebt = totalDebt + parseInt(data[i]["availableBalance"]);
            }
        }
        return forUtility.formatAmountandAppendCurrencySymbol(totalDebt);
    },
    processAccountsData: function(data) {
        var accProcessedData = [];
        for (var i = 0; i < data.length; i++) {
            accProcessedData[i] = {};
            if (i === 0) {
                this.primaryAccntNumber = data[i].primaryAccntNumber;
            }
            if (data[i].accountName !== null && data[i].accountName !== " ") {
                accProcessedData[i].accountName = data[i].accountName;
            }
            // alert("data[i].availableBalance"+data[i].availableBalance);
            if (data[i].availableBalance !== null && data[i].availableBalance !== " ") {
                accProcessedData[i].availableBalance = this.getAvailableBalanceCurrencyString(data[i]);
                accProcessedData[i].accountBalanceType = this.getAvailableBalanceType(data[i]);
            }
            // accProcessedData[i].availableBalance = this.getAvailableBalanceCurrencyString(data[i]);
            // accProcessedData[i].accountID = data[i].accountID;
            //accProcessedData[i].bankName = data[i].bankName;
            // accProcessedData[i].accountType=data[i].accountType;
            accProcessedData[i].accountID = data[i].accountID;
            accProcessedData[i].bankName = data[i].bankName;
            accProcessedData[i].primaryAccntNumber = this.primaryAccntNumber;
            accProcessedData[i].accountType = data[i].accountType;
            accProcessedData[i].nickName = data[i].nickName;
            accProcessedData[i].shareCode = data[i].shareCode;
            accProcessedData[i].irsCode = data[i].irsCode;
            accProcessedData[i].shareType = data[i].shareType;
            accProcessedData[i].loanCode = data[i].loanCode;
            accProcessedData[i].loanType = data[i].loanType;
            accProcessedData[i].currentBalance = data[i].currentBalance;
            accProcessedData[i].heldAt = data[i].heldAt;
            accProcessedData[i].currentAmountDue = data[i].currentAmountDue;
            accProcessedData[i].dueDate = data[i].dueDate;
            accProcessedData[i].url = data[i].url;
            if (accProcessedData[i].irsCode !== null && accProcessedData[i].irsCode !== undefined) {
                if (accProcessedData[i].irsCode !== "0") {
                    accProcessedData[i].accountBalanceType = kony.i18n.getLocalizedString("kony.mb.accdetails.currBal");
                    accProcessedData[i].accountName = "S" + accProcessedData[i].accountID + ":" + accProcessedData[i].accountName;
                } else if (accProcessedData[i].shareCode !== null && accProcessedData[i].shareCode !== undefined) {
                    if (accProcessedData[i].shareCode === "1" || accProcessedData[i].shareCode === "0") {
                        accProcessedData[i].accountBalanceType = kony.i18n.getLocalizedString("kony.mb.accdetails.availBal");
                        accProcessedData[i].accountName = "S" + accProcessedData[i].accountID + ":" + accProcessedData[i].accountName;
                    } else if (accProcessedData[i].shareCode === "2") {
                        accProcessedData[i].accountBalanceType = kony.i18n.getLocalizedString("kony.mb.accdetails.currBal");
                        accProcessedData[i].accountName = "S" + accProcessedData[i].accountID + ":" + accProcessedData[i].accountName;
                    }
                }
            } else if (accProcessedData[i].loanCode !== null && typeof accProcessedData[i].loanCode !== "undefined") {
                accProcessedData[i].accountBalanceType = kony.i18n.getLocalizedString("kony.mb.accdetails.currBal");
                accProcessedData[i].accountName = "L" + accProcessedData[i].accountID + ":" + accProcessedData[i].accountName;
            }
        }
        return accProcessedData;
    },
    getTotalDepositBalance: function(data) {
        var forUtility = applicationManager.getFormatUtilManager();
        var configManager = applicationManager.getConfigurationManager();
        var totalDebt = 0;
        for (i = 0; i < data.length; i++) {
            if (data[i].accountType == configManager.constants.SAVINGS || data[i].accountType == configManager.constants.CHECKING) totalDebt = totalDebt + parseInt(data[i]["availableBalance"]);
        }
        return forUtility.formatAmountandAppendCurrencySymbol(totalDebt);
    },
    fetchAccountTransactions: function(shareID) {
        var shareId = shareID;
        var navMan = applicationManager.getNavigationManager();
        var selectedAccount = navMan.getCustomInfo("frmAccountDetails");
        var accountManager = applicationManager.getAccountManager();
        if (selectedAccount === undefined || selectedAccount === null) selectedAccount = {};
        selectedAccount.selectedAccountData = accountManager.getInternalShareAccountByID(shareId);
        selectedAccount.selectedAccountData.type = "internal";
        navMan.setCustomInfo("frmAccountDetails", selectedAccount);
        this.selectedAccType();
        var transactionObj = applicationManager.getTransactionManager();
        var criteria = {
            "shareId": shareId
        };
        var authMode = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
        authMode.presentationController.showDownTimeMessage();
        transactionObj.fetchAccountPendingTransactions(criteria, scope_Acc_Pres.fetchAccountPenTranPresSucCallback, scope_Acc_Pres.fetchAccountPenTranPreErrCallback);
    },
    fetchLoanAccountTransactions: function(loanid) {
        var LoanId = loanid;
        var navMan = applicationManager.getNavigationManager();
        var selectedAccount = navMan.getCustomInfo("frmAccountDetails");
        var accountManager = applicationManager.getAccountManager();
        if (selectedAccount === undefined || selectedAccount === null) selectedAccount = {};
        selectedAccount.selectedAccountData = accountManager.getInternalLoanAccountByID(LoanId);
        selectedAccount.selectedAccountData.type = "internal";
        navMan.setCustomInfo("frmAccountDetails", selectedAccount);
        this.selectedAccType();
        var transactionObj = applicationManager.getTransactionManager();
        var criteria = {
            "LoanId": LoanId
        };
        var authMode = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
        authMode.presentationController.showDownTimeMessage();
        transactionObj.fetchLoanAccountTransactions(criteria, scope_Acc_Pres.fetchAccountLoanPenTranPresSucCallback, scope_Acc_Pres.fetchAccountPenTranPreErrCallback);
    },
    fetchAccountPenTranPresSucCallback: function(resTransPend) {
        applicationManager.getPresentationUtility().dismissLoadingScreen();
        scope_Acc_Pres.navigateToAccountDetails(resTransPend);
    },
    fetchAccountLoanPenTranPresSucCallback: function(resTransPend) {
        applicationManager.getPresentationUtility().dismissLoadingScreen();
        var navMan = applicationManager.getNavigationManager();
        navMan.setCustomInfo("loanInfo", resTransPend);
        var loanData = navMan.getCustomInfo("loanInfo");
        var interestType = loanData["InterestType"];
        scope_Acc_Pres.fetchGetCCFields(interestType);
        scope_Acc_Pres.navigateToAccountDetails(resTransPend);
    },
    fetchAccountPenTranPreErrCallback: function(resTransPendErr) {
        applicationManager.getPresentationUtility().dismissLoadingScreen();
        if (resTransPendErr["isServerUnreachable"]) applicationManager.getPresentationInterruptHandler().showErrorMessage("postLogin", resTransPendErr);
    },
    navigateToAccountDetails: function(resTransPend) {
        var navMan = applicationManager.getNavigationManager();
        var accountManager = applicationManager.getAccountManager();
        var selectedAccount = navMan.getCustomInfo("frmAccountDetails");
        selectedAccount.Transactions = resTransPend;
        navMan.setCustomInfo("frmAccountDetails", selectedAccount);
        navMan.navigateTo("frmAccountDetails");
    },
    getAvailableBalanceType: function(data) {
        var configManager = applicationManager.getConfigurationManager();
        switch (data.accountType) {
            case configManager.constants.SAVINGS:
                return kony.i18n.getLocalizedString("kony.mb.accdetails.availBal");
            case configManager.constants.CHECKING:
                return kony.i18n.getLocalizedString("kony.mb.accdetails.availBal");
            case configManager.constants.CREDITCARD:
                return kony.i18n.getLocalizedString("kony.mb.accdetails.outstandingBal");
            case configManager.constants.DEPOSIT:
                return kony.i18n.getLocalizedString("kony.mb.accdetails.currBal");
            case configManager.constants.MORTGAGE:
                return kony.i18n.getLocalizedString("kony.mb.accdetails.outstandingBal");
            case configManager.constants.LOAN:
                return kony.i18n.getLocalizedString("kony.mb.accdetails.outstandingBal");
            default:
                return kony.i18n.getLocalizedString("kony.mb.accdetails.availBal");
        }
    },
    fetchGetCCFields: function(interestType) {
        var criteria = {
            "InterestType": interestType
        };
        var authMode = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
        authMode.presentationController.showDownTimeMessage();
        var transactionObj = applicationManager.getTransactionManager();
        transactionObj.fetchLoanDataTransactions(criteria, scope_Acc_Pres.fetchGetCCFieldsSucCallback, scope_Acc_Pres.fetchAccountPenTranPreErrCallback);
    },
    fetchGetCCFieldsSucCallback: function(resLoan) {
        var navMan = applicationManager.getNavigationManager();
        var accountManager = applicationManager.getAccountManager();
        var selectedAccount = navMan.getCustomInfo("frmAccountDetails");
        selectedAccount.loanData = resLoan;
        navMan.setCustomInfo("frmAccountDetails", selectedAccount);
    },
    // Differenting the account Type
    selectedAccType: function() {
        var navigationManager = applicationManager.getNavigationManager();
        this.accDetails = navigationManager.getCustomInfo("frmAccountDetails");
        this.accTypeCode = navigationManager.getCustomInfo("accountCode");
        this.loanCode = this.accTypeCode.loanCode;
        this.shareCode = this.accTypeCode.shareCode;
        this.trackCode = this.accTypeCode.trackingType;
        this.irsCode = this.accTypeCode.irsCode;
        if (this.irsCode !== null && typeof this.irsCode !== "undefined") {
            if (this.irsCode !== "0") {
                this.accDetails.selectedAccountData.accountType = "IRA";
                this.accDetails.selectedAccountData.primaryAccntNumber = this.accTypeCode.primaryAccntNumber;
                this.accDetails.selectedAccountData.shareID = this.accTypeCode.accountID;
            }
        }
        if (this.shareCode !== null && typeof this.shareCode !== "undefined") {
            if (this.shareCode === "0") {
                this.accDetails.selectedAccountData.accountType = "Savings";
            } else if (this.shareCode === "1") {
                this.accDetails.selectedAccountData.accountType = "Checking";
            } else if (this.shareCode === "2") {
                this.accDetails.selectedAccountData.accountType = "CD";
                this.accDetails.selectedAccountData.primaryAccntNumber = this.accTypeCode.primaryAccntNumber;
                this.accDetails.selectedAccountData.shareID = this.accTypeCode.accountID;
            }
        }
        if (this.loanCode !== null && typeof this.loanCode !== "undefined") {
            if (this.loanCode !== "2" && this.loanCode !== "3" && this.loanCode !== "6") {
                this.accDetails.selectedAccountData.accountType = "Loan";
            }
            if (this.loanCode === "3") {
                this.accDetails.selectedAccountData.accountType = "CreditCard";
            }
            if (this.loanCode === "2" || this.loanCode === "6") {
                this.accDetails.selectedAccountData.accountType = "LOC";
            }
            if (this.trackCode !== null && typeof this.trackCode !== "undefined") {
                if (this.trackCode === "1") {
                    this.accDetails.selectedAccountData.accountType = "Mortgage";
                }
            }
        }
    },
    navigateToInvestments: function(investmentsUrl) {
        var navManager = applicationManager.getNavigationManager();
        navManager.setCustomInfo("frmInvestmentAccounts", investmentsUrl);
        navManager.navigateTo("frmInvestmentAccounts");
    },
    //Update NickName for Shares
    updateShareNickName: function(ShareId, NickName) {
        var shareId = ShareId;
        var nickName = NickName;
        var criteria = {
            "ShareId": shareId,
            "nickName": nickName
        };
        var authMode = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
        authMode.presentationController.showDownTimeMessage();
        var acntMngrObj = applicationManager.getAccountManager();
        acntMngrObj.updateShareNickName(criteria, scope_Acc_Pres.updateNickNameSucCallback, scope_Acc_Pres.updateNickNamePreErrCallback);
    },
    //Update NickName for Loans
    updateLoanNickName: function(LoanId, NickName) {
        var loanId = LoanId;
        var nickName = NickName;
        var criteria = {
            "LoanId": loanId,
            "nickName": nickName
        };
        var authMode = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
        authMode.presentationController.showDownTimeMessage();
        var acntMngrObj = applicationManager.getAccountManager();
        acntMngrObj.updateLoanNickName(criteria, scope_Acc_Pres.updateNickNameSucCallback, scope_Acc_Pres.updateNickNamePreErrCallback);
    },
    updateNickNameSucCallback: function(nickNameResp) {
        applicationManager.getPresentationUtility().dismissLoadingScreen();
        scope_Acc_Pres.navigateToAccountInfo(nickNameResp);
    },
    navigateToAccountInfo: function(nickNameResp) {
        var navMan = applicationManager.getNavigationManager();
        var accDetails = navMan.getCustomInfo("frmAccountDetails");
        var nickName = navMan.getCustomInfo("frmAccInfoEdit");
        accDetails.selectedAccountData.nickName = nickName;
        accDetails.Transactions.Nickname = nickName;
        navMan.setCustomInfo("frmAccountDetails", accDetails);
        navMan.navigateTo("frmAccountInfo");
    },
    updateNickNamePreErrCallback: function(updateNickNameErr) {
        applicationManager.getPresentationUtility().dismissLoadingScreen();
        if (updateNickNameErr.isServerUnreachable) applicationManager.getPresentationInterruptHandler().showErrorMessage("postLogin", updateNickNameErr);
    },
    Account_PresentationController: function() {
        scope_Acc_Pres = this;
        kony.mvc.Presentation.BasePresenter.call(this);
        this.asyncManager = new AsyncManager();
        /**   numberOfAsyncForTransactions
         *  1.getAccountPendingTransactions
         *  2.getAccountPostedTransactions
         */
        scope_Acc_Pres.numberOfAsyncForTransactions = 2;
        /**   numberOfAsyncForPFMGraph
         *  1.getPFMBarGraph
         *  2.getPFMBudgetGraph
         */
        scope_Acc_Pres.numberOfAsyncForPFMGraph = 2;
        this.directMarketingManager = applicationManager.getDirectMarketingManager();
        this.primaryAccntNumber = null;
    }
});