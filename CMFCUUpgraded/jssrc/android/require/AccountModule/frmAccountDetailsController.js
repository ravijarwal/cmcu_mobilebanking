define("AccountModule/userfrmAccountDetailsController", {
    pendingaccounts: null,
    postedaccounts: null,
    segmentData: null,
    objRec: '',
    init: function() {
        var navManager = applicationManager.getNavigationManager();
        var currentForm = navManager.getCurrentForm();
        onNavigate = false;
        applicationManager.getPresentationFormUtility().initCommonActions(this, "YES", currentForm);
    },
    onNavigate: function(obj) {
        if (obj === undefined) {
            var newObj = {
                "view": "familyCheckingAcc"
            };
            obj = newObj;
        }
        this.objRec = obj;
    },
    frmAccountDetailsPreshow: function() {
        var self = this;
        search = false;
        if (search === false) {
            this.view.imgPosteddropdown.isVisible = true;
            this.view.imgPendingdropdown.isVisible = true;
        }
        if (onNavigate === false) {
            arrowPosted = "up";
            arrowPending = "up";
            //initially setting pending and posted trans as visible
            this.view.segPosted.isVisible = true;
            this.view.segPending.isVisible = true;
            this.view.flxTypePosted.isVisible = true;
            this.view.flxTypePending.isVisible = true;
            this.view.imgPosteddropdown.src = "arrowupblue.png";
            this.view.imgPendingdropdown.src = "arrowupblue.png";
        } else {
            onNavigate = false;
        }
        if (arrowPending === "down") {
            this.view.segPending.isVisible = false;
        }
        if (arrowPosted === "down") {
            this.view.segPosted.isVisible = false;
        }
        counterPosted = 0;
        counterPending = 0;
        flagEnd = false;
        counterSearchPosted = 0;
        counterSearchPending = 0;
        //scrollend 
        this.view.flxMainContainer.onScrollEnd = this.scrollEnd;
        //footer 
        this.view.customFooter.imgTransfer.src = "transferfooter.png";
        this.view.customFooter.imgAccounts.src = "accountsactive.png";
        this.view.customFooter.flxTransferSel.isVisible = false;
        this.view.customFooter.flxAccounts.onClick = this.onClickFlxAccounts;
        this.view.customFooter.flxTransfer.onClick = this.onClickFlxTransfers;
        this.view.flxPendingDropDown.onClick = this.flxPendingHide;
        this.view.flxPostedDropDown.onClick = this.flxPostedHide;
        this.view.flxUp.onTouchEnd = this.hideLinks;
        //this.view.ImgUpArrow.onTouchEnd=this.hideLinks;
        this.view.flxSearch.isVisible = true;
        this.view.customSearchbox.tbxSearch.text = "";
        this.view.flxBalance.isVisible = true;
        // this.view.segTransactions.isVisible = true;
        // this.view.flxNoTransactions.isVisible = false;
        this.view.lblDueDate.setVisibility(false);
        // this.setFooter();
        this.view.flxSearchMain.setVisibility(true);
        this.view.flxTransactions.setVisibility(true);
        this.view.flxTransactions.top = "340Dp";
        var navManager = applicationManager.getNavigationManager();
        if (kony.os.deviceInfo().name !== "iPhone") {
            this.view.flxHeader.isVisible = true;
        } else {
            this.view.flxHeader.isVisible = true;
        }
        this.view.flxFooter.isVisible = true;
        this.view.btnChatbot.onClick = function() {
            var chatBotMode = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ChatBotModule");
            chatBotMode.presentationController.handleFirstTimeOpen();
        };
        this.showcardlessPopUp();
        var configManager = applicationManager.getConfigurationManager();
        var navManager = applicationManager.getNavigationManager();
        var accountsDetails = navManager.getCustomInfo("frmAccountDetails");
        if (accountsDetails.res !== undefined && accountsDetails.res !== null) {
            if (accountsDetails.type == "error") this.showErrorPopup(accountsDetails.res);
            else this.showSuccessPopup(accountsDetails.res, accountsDetails.typeOfTransaction);
        }
        accountsDetails.res = null;
        navManager.setCustomInfo("frmAccountDetails", accountsDetails);
        var type = accountsDetails.selectedAccountData["type"];
        this.view.customSearchbox.tbxSearch.placeholder = kony.i18n.getLocalizedString("kony.mb.SearchByDescription");
        var MenuHandler = applicationManager.getMenuHandler();
        MenuHandler.setUpHamburgerForForm(this, configManager.constants.MENUACCOUNTS);
        if (type.toLowerCase().trim() === "internal") {
            this.view.flxAdvSearch.isVisible = true;
            this.view.imgAdvSearch.isVisible = true;
            this.view.customHeader.imgSearch.isVisible = true;
            this.view.flxOptions.isVisible = true;
            //getting accountType
            var navMan = applicationManager.getNavigationManager();
            var selectedAccount = navMan.getCustomInfo("frmAccountDetails");
            var accountType = selectedAccount.selectedAccountData.accountType;
            this.setFlxOptionsBasedOnType(accountType);
            // this.setFlxOptionsBasedOnType(accountsDetails.selectedAccountData["accountType"]);
            this.setFlxOptionsBasedOnType()
            this.setSegmentData();
            this.setBalanceData();
        } else {
            this.view.customHeader.imgSearch.isVisible = true;
            this.view.flxAdvSearch.isVisible = false;
            this.view.imgAdvSearch.isVisible = false;
            this.view.flxOptions.isVisible = false;
            this.setTransactionsDataforAggregated();
        }
        this.view.flxHeaderSearchbox.setVisibility(false);
        // this.view.flxMainContainer.onScrolling = this.flxMainContainerOnScrolling;
        // this.view.segTransactions.onTouchEnd = this.segTransactionsOnTouchEnd;
        this.view.tbxSearch.onTouchEnd = this.flxSearchOnTouchEnd;
        this.view.customHeader.flxBack.onClick = this.flxBackOnClick;
        this.view.customSearchbox.btnCancel.onClick = this.btnCancelOnClick;
        this.view.tbxSearch.onTextChange = this.tbxSearchOnTextChange;
        //this.view.tbxSearch.text="";
        //     if (this.objRec.view === "familyCheckingAcc") {
        //       //family chekcing account view 
        //       this.createViewForFamilyCheckingAccount();
        //     }
        //     if (this.objRec.view === "creditCard") {
        //       // credit card view
        //       this.createViewForCreditCard();
        //     }
        //     if (this.objRec.view === "homeLoanAcc") {
        //       // home loan account view
        //       this.createViewForHomeLoanAccount();
        //     }
        //     if (this.objRec.view === "depositAccount") {
        //       //deposit account view
        //       this.createViewForDepositAccount();
        //     }
        //  this.view.segTransactions.onRowClick = this.segTransactionsOnRowClick;
        this.view.segPosted.onRowClick = this.segTransactionsOnRowClick;
        /*this.view.customHeader.flxSearch.onClick = function() {
                var navManager = applicationManager.getNavigationManager();
                var accountsDetails = navManager.getCustomInfo("frmAccountDetails");
                var accountModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountModule");
                if(String(accountsDetails.selectedAccountData.type).toLowerCase().trim() === "external") {
                    accountModule.presentationController.fetchInfoForExternalBankAccount();
                } else {
                    navManager.setCustomInfo("frmAccountInfo", accountsDetails);
         		accountModule.presentationController.commonFunctionForNavigation("frmAccountInfo");
                }
            };*/
        var navManager = applicationManager.getNavigationManager();
        var currentForm = navManager.getCurrentForm();
        applicationManager.getPresentationFormUtility().logFormName(currentForm);
        applicationManager.getPresentationUtility().dismissLoadingScreen();
    },
    showcardlessPopUp: function() {
        var cardlessModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardLessModule");
        if (cardlessModule.presentationController.qrSuccessFlag) {
            var transactionID = cardlessModule.presentationController.getTransactionId();
            applicationManager.getDataProcessorUtility().showToastMessageSuccess(this, applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.cardLess.transactionMessage") + transactionID);
            cardlessModule.presentationController.setTransactionId();
            cardlessModule.presentationController.qrSuccessFlag = false;
        }
    },
    setFlxOptionsBasedOnType: function(accountType) {
        switch (accountType) {
            case "Checking":
                this.checkingAccountsOptions();
                break;
            case "Savings":
                this.savingAccountsOptions();
                break;
            case "Deposit":
                this.depositRelatedOptions();
                break;
            case "CreditCard":
                this.creditCardAccountsOptions();
                break;
            case "LOC":
                this.locAccountsOptions();
                break;
            case "IRA":
                this.iraAccountsOptions();
                break;
            case "CD":
                this.cdAccountsOptions();
        }
    },
    checkingAccountsOptions: function() {
        var navMan = applicationManager.getNavigationManager();
        var accountsDetails = navMan.getCustomInfo("frmAccountDetails");
        //displaying buttons
        this.view.btnOption1.text = applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.checkings.alerts");
        this.view.btnOption2.text = applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.checkings.billpay");
        this.view.imgArrow.isVisible = true;
        //dispalying label content
        var accountpendingPostTransaction = accountsDetails.Transactions;
        var overdraftBalance = accountpendingPostTransaction.OverdraftTolerance;
        var accountsData = accountsDetails.selectedAccountData;
        var currentBalance = accountsData.currentBalance;
        var courtseyPayAvailable;
        this.view.imgArrow.onTouchEnd = this.checkingAccountsLinks;
        if (currentBalance > 0) courtseyPayAvailable = overdraftBalance;
        else if (currentBalance < 0) {
            var absBal = Math.abs(currentBalance);
            courtseyPayAvailable = overdraftBalance - absBal;
        } else {
            courtseyPayAvailable = 0.0;
        }
        if (courtseyPayAvailable !== 'undefined' && courtseyPayAvailable !== "") {
            this.view.lblOption1Txt.isVisible = true;
            this.view.lblOption1Txt.text = "COURTESY PAY AVAILABLE $" + courtseyPayAvailable;
            this.view.lblOption1Txt.skin = sknBlueLbl;
            if (overdraftBalance !== 'undefined' && overdraftBalance !== "") {
                this.view.lblOption1Txt.text = "COURTESY PAY AVAILABLE $" + courtseyPayAvailable + " | COURTESY PAY LIMIT $" + overdraftBalance;
            }
        } else {
            if (overdraftBalance !== 'undefined' && overdraftBalance !== "") {
                this.view.lblOption1Txt.isVisible = true;
                this.view.lblOption1Txt.text = "COURTESY PAY LIMIT $" + overdraftBalance;
                this.view.lblOption1Txt.skin = sknBlueLbl;
            } else {
                this.view.lblOption1Txt.isVisible = false;
            }
        }
    },
    savingAccountsOptions: function() {
        this.view.btnOption1.text = applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.savings.addProduct");
        this.view.btnOption2.text = applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.savings.documents");
        //this.view.flxOptionLabels.isVisible=false;
        this.view.lblOption1Txt.isVisible = false;
        this.view.imgArrow.isVisible = true;
        this.view.imgArrow.onTouchEnd = this.savingAccountsLinks;
    },
    creditCardAccountsOptions: function() {
        this.view.btnOption1.text = applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.accdetails.makeAPayment");
        this.view.btnOption2.text = applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.creditcard.balanceTransfer");
        this.view.imgArrow.isVisible = true;
        //dispalying label content
        var navMan = applicationManager.getNavigationManager();
        var accountsDetails = navMan.getCustomInfo("frmAccountDetails");
        var dueDate = accountsDetails.Transactions.DueDate;
        this.view.imgArrow.onTouchEnd = this.creditCardAccountsLinks;
        var paymentAmount = accountsDetails.Transactions.PaymentDue;
        if (paymentAmount !== 'undefined' && paymentAmount !== "") {
            this.view.lblOption1Txt.isVisible = true;
            this.view.lblOption1Txt.text = "PAYMENT AMOUNT $" + paymentAmount;
            this.view.lblOption1Txt.skin = sknBlueLbl;
            if (dueDate !== 'undefined' && dueDate !== "") {
                this.view.lblOption1Txt.text = "PAYMENT AMOUNT $" + paymentAmount + " | DUE DATE " + dueDate;
            }
        } else {
            if (dueDate !== 'undefined' && dueDate !== "") {
                this.view.lblOption1Txt.text = "DUE DATE " + dueDate;
                this.view.lblOption1Txt.skin = sknBlueLbl;
            } else {
                this.view.lblOption1Txt.isVisible = false;
            }
        }
    },
    locAccountsOptions: function() {
        this.view.btnOption1.text = applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.accdetails.makeAPayment");
        this.view.btnOption2.text = applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.accdetails.cashAdv");
        this.view.imgArrow.isVisible = true;
        this.view.imgArrow.onTouchEnd = this.locAccountLinks;
        //dispalying label content
        var navMan = applicationManager.getNavigationManager();
        var accountsDetails = navMan.getCustomInfo("frmAccountDetails");
        var dueDate = accountsDetails.Transactions.DueDate;
        var paymentAmount = accountsDetails.Transactions.PaymentDue;
        if (paymentAmount !== 'undefined' && paymentAmount !== "") {
            this.view.lblOption1Txt.isVisible = true;
            this.view.lblOption1Txt.text = "PAYMENT AMOUNT $" + paymentAmount;
            this.view.lblOption1Txt.skin = sknBlueLbl;
            if (dueDate !== 'undefined' && dueDate !== "") {
                this.view.lblOption1Txt.text = "PAYMENT AMOUNT $" + paymentAmount + "| DUE DATE " + dueDate;
            }
        } else {
            if (dueDate !== 'undefined' && dueDate !== "") {
                this.view.lblOption1Txt.isVisible = true;
                this.view.lblOption1Txt.text = "DUE DATE " + dueDate;
                this.view.lblOption1Txt.skin = sknBlueLbl;
            } else {
                this.view.lblOption1Txt.isVisible = false;
            }
        }
    },
    cdAccountsOptions: function() {
        this.view.btnOption1.text = applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.common.details");
        this.view.btnOption2.text = applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.common.accountDocument");
        this.view.lblOption1Txt.isVisible = false;
        this.view.imgArrow.isVisible = false;
        var navMan = applicationManager.getNavigationManager();
        var accountsDetails = navMan.getCustomInfo("frmAccountDetails");
        var maturityDate = accountsDetails.Transactions.MaturityDate;
        if (maturityDate !== null || maturityDate !== 'undefined') {
            this.view.lblOption1Txt.text = "MATURITY DATE " + maturityDate;
            this.view.lblOption1Txt.isVisible = true;
        }
    },
    iraAccountsOptions: function() {
        this.view.btnOption1.text = applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.common.details");
        this.view.btnOption2.text = applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.common.statements");
        this.view.imgArrow.isVisible = false;
        this.view.lblOption1Txt.isVisible = false;
    },
    commonOptions: function() {
        this.view.btnWithdrawCash.text = applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.accdetails.withdrawCash");
        this.view.btnWithdrawCash.onClick = function() {
            var navManager = applicationManager.getNavigationManager();
            var accountsDetails = navManager.getCustomInfo("frmAccountDetails");
            var cardlessModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardLessModule");
            cardlessModule.presentationController.clearTransactionObject();
            cardlessModule.presentationController.navigateToCashRecipientForm(accountsDetails.selectedAccountData);
            //cardlessModule.presentationController.navigateToQRCashWithdrawForm(accountsDetails.selectedAccountData);  	
        };
    },
    depositRelatedOptions: function() {
        this.view.btnWithdrawCash.text = applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.accounts.NEWDEPOSIT");
        this.view.btnWithdrawCash.onClick = function() {
            var navManager = applicationManager.getNavigationManager();
            navManager.setEntryPoint("Deposit", "frmAccountDetails");
            var accountsDetails = navManager.getCustomInfo("frmAccountDetails");
            var checkDepositModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CheckDepositModule");
            checkDepositModule.presentationController.navigateFromAccountDetails(accountsDetails.selectedAccountData);
        };
    },
    creditCardRelatedOptions: function() {
        this.view.btnWithdrawCash.text = applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.accounts.MAKEAPAYMENT");
        this.view.btnWithdrawCash.onClick = function() {
            var navManager = applicationManager.getNavigationManager();
            var navigateToForm = navManager.setEntryPoint("makeatransfer", "frmAccountDetails");
            var accountsDetails = navManager.getCustomInfo("frmAccountDetails");
            var transferModulePresentationController = applicationManager.getModulesPresentationController("TransferModule");
            transferModulePresentationController.transactionMode = applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.transfer.MyKonyAccounts");
            transferModulePresentationController.navigateToTransfers(accountsDetails.selectedAccountData);
        };
    },
    loanRelatedOptions: function() {
        var accountsDetails = navMan.getCustomInfo("frmAccountDetails");
        var accountpendingPostTransaction = accountsDetails.Transactions;
        this.view.btnOption1.text = applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.accdetails.makeAPayment");
        this.view.btnOption2.text = applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.common.statements");
        var accountpendingPostTransaction = accountsDetails.Transactions;
        // this.view.btnWithdrawCash.text=applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.accounts.PAYDUEAMOUNT");
        // this.view.btnWithdrawCash.onClick=function()
        {
            var navManager = applicationManager.getNavigationManager();
            var navigateToForm = navManager.setEntryPoint("makeatransfer", "frmAccountDetails");
            var accountsDetails = navManager.getCustomInfo("frmAccountDetails");
            var transferModulePresentationController = applicationManager.getModulesPresentationController("TransferModule");
            transferModulePresentationController.transactionMode = applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.transfer.MyKonyAccounts");
            transferModulePresentationController.navigateToTransfers(accountsDetails.selectedAccountData);
        };
    },
    setTransactionsDataforAggregated: function() {
        var postedTransactionsdata = [],
            pendingTransactiondata = [];
        var navMan = applicationManager.getNavigationManager();
        var forUtility = applicationManager.getFormatUtilManager();
        var accountsDetails = navMan.getCustomInfo("frmAccountDetails");
        var postedTransactions = accountsDetails.externalPostedTransactions;
        var pendingTransaction = accountsDetails.externalPendingTransactions;
        this.view.lblBalanceValue.text = accountsDetails.selectedAccountData["availableBalance"];
        this.view.lblCurrBalValue.text = accountsDetails.selectedAccountData["availableBalance"];
        //this.view.customHeader.lblLocateUs.text = accountsDetails.selectedAccountData["nickName"];
        //this.view.title = accountsDetails.selectedAccountData["nickName"];
        var navMan = applicationManager.getNavigationManager();
        var accountsDetails = navMan.getCustomInfo("frmAccountDetails");
        var nickname = accountsDetails.Nickname;
        this.view.customHeader.lblLocateUs.text = nickname;
        //  this.view.title =nickname;
        this.view.lblDueDate.text = "";
        this.view.segTransactions.widgetDataMap = {
            lblTransaction: "description",
            lblDate: "TransactionDate",
            lblTransactionAmount: "Amount",
            transactionId: "TransactionId",
            lblHeader: "lblHeader"
        };
        if (pendingTransaction.length > 0 && postedTransactions.length > 0) {
            var data = [
                [{
                    "lblHeader": "Pending Transactions"
                }, pendingTransaction],
                [{
                    "lblHeader": "Posted Transactions"
                }, postedTransactions]
            ];
            this.segmentData = data;
            this.view.segTransactions.setData(data);
            this.pendingaccounts = this.view.segTransactions.data[0][1];
            this.postedaccounts = this.view.segTransactions.data[1][1];
        } else if (pendingTransaction.length > 0) {
            var data = [
                [{
                    "lblHeader": "Pending Transactions"
                }, pendingTransaction]
            ];
            this.segmentData = data;
            this.view.segTransactions.setData(data);
            this.pendingaccounts = this.view.segTransactions.data[0][1];
            this.postedaccounts = [];
        } else if (postedTransactions.length > 0) {
            var data = [
                [{
                    "lblHeader": "Posted Transactions"
                }, postedTransactions]
            ];
            this.segmentData = data;
            this.view.segTransactions.setData(data);
            this.postedaccounts = this.view.segTransactions.data[0][1];
            this.pendingaccounts = [];
        } else {
            this.segmentData = [];
            this.pendingaccounts = [];
            this.postedaccounts = [];
            this.view.segTransactions.isVisible = false;
            this.view.flxNoTransactions.isVisible = true;
        }
    },
    setprevSegmentData: function() {
        var postedTransactionsdata = [],
            pendingTransactiondata = [];
        var navMan = applicationManager.getNavigationManager();
        // var configManager = applicationManager.getConfigurationManager();
        var forUtility = applicationManager.getFormatUtilManager();
        var accountsDetails = navMan.getCustomInfo("frmAccountDetails");
        var accountsData = accountsDetails.selectedAccountData;
        var postedTransactions = accountsDetails.postedTransaction;
        var pendingTransaction = accountsDetails.pendingTransactions;
        this.view.segTransactions.widgetDataMap = {
            lblTransaction: "description",
            lblDate: "scheduledDate",
            lblTransactionAmount: "amount",
            transactionId: "transactionId",
            lblHeader: "lblHeader"
        };
        if (pendingTransaction.length > 0 && postedTransactions.length > 0) {
            var data = [
                [{
                    "lblHeader": "Pending Transactions"
                }, pendingTransaction],
                [{
                    "lblHeader": "Posted Transactions"
                }, postedTransactions]
            ];
            this.segmentData = data;
            this.view.segTransactions.setData(data);
            this.pendingaccounts = this.view.segTransactions.data[0][1];
            this.postedaccounts = this.view.segTransactions.data[1][1];
        } else if (pendingTransaction.length > 0) {
            var data = [
                [{
                    "lblHeader": "Pending Transactions"
                }, pendingTransaction]
            ];
            this.segmentData = data;
            this.view.segTransactions.setData(data);
            this.pendingaccounts = this.view.segTransactions.data[0][1];
            this.postedaccounts = [];
        } else if (postedTransactions.length > 0) {
            var data = [
                [{
                    "lblHeader": "Posted Transactions"
                }, postedTransactions]
            ];
            this.segmentData = data;
            this.view.segTransactions.setData(data);
            this.postedaccounts = this.view.segTransactions.data[0][1];
            this.pendingaccounts = [];
        } else {
            this.segmentData = [];
            this.pendingaccounts = [];
            this.postedaccounts = [];
            this.view.segTransactions.isVisible = false;
            this.view.flxNoTransactions.isVisible = true;
        }
    },
    setSegmentData: function() {
        this.view.segTransactions.isVisible = false;
        var postedTransactiondata = [],
            pendingTransactiondata = [];
        var navMan = applicationManager.getNavigationManager();
        var accountsDetails = navMan.getCustomInfo("frmAccountDetails");
        var accountpendingPostTransaction = accountsDetails.Transactions.accountTypeList;
        var formatUtil = applicationManager.getFormatUtilManager();
        //differentating pending and posted transactions based on id and idType
        for (var i in accountpendingPostTransaction) {
            var idType = accountpendingPostTransaction[i].IdType;
            var id = accountpendingPostTransaction[i].Id;
            if ((idType !== null && id !== null) && (typeof idType !== "undefined" && typeof id !== "undefined")) {
                postedTransactiondata.push(accountpendingPostTransaction[i]);
            } else {
                pendingTransactiondata.push(accountpendingPostTransaction[i]);
            }
        }
        this.pendingTransactiondata = pendingTransactiondata;
        this.postedTransactiondata = postedTransactiondata;
        if (pendingTransactiondata.length > 0) {
            var newPendingList = pendingTransactiondata;
            this.showPendingList(newPendingList);
        } else {
            //if there are no pending record from backend
            this.pendingTransactiondata = [];
            this.view.segPending.isVisible = false;
            this.view.flxTypePending.isVisible = false;
        }
        if (postedTransactiondata.length > 0) {
            //showing only 100 records
            var newPostedList = postedTransactiondata.slice(0, counterPosted + 21);
            if (counterPosted <= postedTransactiondata.length) {
                counterPosted = counterPosted + 20;
                this.showPostedList(newPostedList);
            } else {
                //adding remaining records
                flagEnd = true;
                newPostedList = newPostedList.slice(0, postedTransactiondata.length + 1);
                this.showPostedList(newPostedList);
            }
        } else {
            //if there are no posted record 
            this.postedTransactiondata = [];
            this.view.flxTypePosted.isVisible = false;
            this.view.segPosted.isVisible = false;
        }
    },
    setBalanceData: function() {
        var navMan = applicationManager.getNavigationManager();
        // var configManager = applicationManager.getConfigurationManager();
        var forUtility = applicationManager.getFormatUtilManager();
        var accountsDetails = navMan.getCustomInfo("frmAccountDetails");
        var accountsData = accountsDetails.selectedAccountData;
        var nickname = accountsDetails.Transactions.Nickname;
        var description = accountsDetails.Transactions.description;
        if (nickname !== "" && nickname !== null && typeof nickname !== "undefined") {
            this.view.customHeader.lblLocateUs.text = nickname;
            // this.view.title= nickname;
        } else if (description !== "" && description !== null && typeof description !== "undefined") {
            this.view.customHeader.lblLocateUs.text = description;
            // this.view.title= description;
        }
        var configManager = applicationManager.getConfigurationManager();
        if (accountsData.accountType === configManager.constants.CHECKING || accountsData.accountType === configManager.constants.SAVINGS) {
            this.view.lblAvailableBalance.text = applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.accounts.AvailableBalance");
            this.view.lblCurrentBalance.text = applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.accounts.CurrentBalance");
            this.view.lblBalanceValue.text = forUtility.formatAmountandAppendCurrencySymbol(accountsData.availableBalance);
            this.view.lblCurrBalValue.text = forUtility.formatAmountandAppendCurrencySymbol(accountsData.currentBalance);
            this.view.lblOption1Txt.isVisible = true;
            this.view.lblDueDate.text = "";
        }
        if (accountsData.accountType === configManager.constants.SAVINGS) {
            this.view.lblAvailableBalance.text = applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.accounts.AvailableBalance");
            this.view.lblCurrentBalance.text = applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.accounts.CurrentBalance");
            this.view.lblBalanceValue.text = forUtility.formatAmountandAppendCurrencySymbol(accountsData.availableBalance);
            this.view.lblCurrBalValue.text = forUtility.formatAmountandAppendCurrencySymbol(accountsData.currentBalance);
            this.view.lblOption1Txt.isVisible = false;
            this.view.lblDueDate.text = "";
        }
        if (accountsData.accountType === configManager.constants.CREDITCARD) {
            this.view.lblAvailableBalance.text = applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.accdetails.availCred");
            this.view.lblCurrentBalance.text = applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.accounts.CurrentBalance");
            this.view.lblBalanceValue.text = forUtility.formatAmountandAppendCurrencySymbol(accountsDetails.Transactions.AvailableCredit);
            if (accountsDetails.Transactions.AvailableCredit === null) {
                accountsDetails.Transactions.AvailableCredit = 0.00;
            }
            this.view.lblCurrBalValue.text = forUtility.formatAmountandAppendCurrencySymbol(accountsData.currentBalance);
            // this.view.lblDueDate.text= "Reward Balance"+forUtility.formatAmountandAppendCurrencySymbol(accountsData.
            var dateobj = forUtility.getDateObjectfromString(accountsData.dueDate, "YYYY-MM-DD");
            this.view.lblDueDate.text = applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.accountdetails.dueon") + " " + forUtility.getFormatedDateString(dateobj, forUtility.APPLICATION_DATE_FORMAT);
        }
        if (accountsData.accountType === configManager.constants.LOAN || accountsData.accountType === configManager.constants.MORTGAGE) {
            this.view.lblAvailableBalance.text = applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.accounts.OutstandingPrincipleBalance");
            this.view.lblCurrentBalance.text = applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.accounts.CurrentDue");
            this.view.lblBalanceValue.text = forUtility.formatAmountandAppendCurrencySymbol(accountsData.availableBalance);
            this.view.lblCurrBalValue.text = forUtility.formatAmountandAppendCurrencySymbol(accountsData.currentBalance);
            var dateobj = forUtility.getDateObjectfromString(accountsData.dueDate, "YYYY-MM-DD");
            this.view.lblDueDate.text = applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.accountdetails.dueon") + " " + forUtility.getFormatedDateString(dateobj, forUtility.APPLICATION_DATE_FORMAT);
        }
        if (accountsData.accountType === configManager.constants.DEPOSIT) {
            this.view.lblAvailableBalance.text = applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.accounts.AvailableBalance");
            this.view.lblCurrentBalance.text = applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.accounts.MaturityDate");
            this.view.lblBalanceValue.text = forUtility.formatAmountandAppendCurrencySymbol(accountsData.availableBalance);
            var dateobj = forUtility.getDateObjectfromString(accountsData.maturityDate, "YYYY-MM-DD");
            this.view.lblCurrBalValue.text = forUtility.getFormatedDateString(dateobj, forUtility.APPLICATION_DATE_FORMAT);
        }
        if (accountsData.accountType === "LOC") {
            this.view.lblAvailableBalance.text = applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.accdetails.availCred");
            this.view.lblCurrentBalance.text = applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.accounts.CurrentBalance");
            this.view.lblBalanceValue.text = forUtility.formatAmountandAppendCurrencySymbol(accountsDetails.Transactions.AvailableCredit);
            this.view.lblCurrBalValue.text = forUtility.formatAmountandAppendCurrencySymbol(accountsData.currentBalance);
        }
        if (accountsData.accountType === "CD") {
            this.view.lblCurrentBalance.text = "";
            this.view.lblCurrBalValue.text = "";
            this.view.lblAvailableBalance.text = applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.accounts.CurrentBalance");
            this.view.lblBalanceValue.text = forUtility.formatAmountandAppendCurrencySymbol(accountsData.currentBalance);
        }
        if (accountsData.accountType === "IRA") {
            this.view.lblCurrentBalance.text = "";
            this.view.lblCurrBalValue.text = "";
            this.view.lblAvailableBalance.text = applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.accounts.CurrentBalance");
            this.view.lblBalanceValue.text = forUtility.formatAmountandAppendCurrencySymbol(accountsData.currentBalance);
        }
    },
    tbxSearchOnTextChange: function() {
        var navObj = applicationManager.getNavigationManager();
        var searchtext = this.view.customSearchbox.tbxSearch.text.toLowerCase();
        //when some value is entered for search
        if (searchtext !== null && searchtext !== "") {
            var data = [],
                headers = [];
            headers.push(applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.accdetails.pendingTransactions"));
            headers.push(applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.accdetails.postedTransactions"));
            if (typeof dataPending !== 'undefined' && dataPending !== null) {
                data.push(dataPending);
            } else {
                //if pending list from bakend is empty
                data.push("");
            }
            if (typeof dataPosted !== 'undefined' && dataPosted !== null) {
                data.push(dataPosted);
            } else {
                //if posted list from backend is empty
                data.push("");
            }
            var searchobj = applicationManager.getDataProcessorUtility().commonSectionSegmentSearch("lblTransactions", searchtext, data, headers);
            if (searchobj.length === 0) {
                this.view.flxTransactions.setVisibility(false);
            }
            //if pending and posted transactions are empty records
            this.view.flxTypePending.isVisible = false;
            this.view.segPending.isVisible = false;
            this.view.flxTypePosted.isVisible = false;
            this.view.segPosted.isVisible = false;
            for (var i = 0; i < searchobj.length; i++) {
                for (var j = 0; j < searchobj[i].length; j++) {
                    if (searchobj[i][j].lblHeader == "Pending Transactions") {
                        var newPendingList = searchobj[i][j + 1];
                        this.view.flxTransactions.setVisibility(true);
                        this.view.flxTypePending.isVisible = true;
                        this.view.segPending.isVisible = true;
                        this.view.segPending.setData(newPendingList);
                    } else if (searchobj[i][j].lblHeader == "Posted Transactions") {
                        var newPostedList = searchobj[i][j + 1].slice(0, 21);
                        this.view.flxTypePosted.isVisible = true;
                        this.view.flxTransactions.setVisibility(true);
                        this.view.segPosted.isVisible = true;
                        this.view.segPosted.setData(newPostedList);
                    }
                }
            }
        }
        //when value entered is empty then displaying list
        else {
            if (this.postedTransactiondata.length > 0) {
                this.view.flxTypePosted.isVisible = true;
                this.view.flxTransactions.isVisible = true;
                this.view.segPosted.isVisible = true;
                this.view.segPosted.setData(dataPosted);
            }
            if (this.pendingTransactiondata.length > 0) {
                this.view.flxTypePending.isVisible = true;
                this.view.segPending.isVisible = true;
                this.view.flxTransactions.setVisibility(true);
                this.view.segPending.setData(dataPending);
            }
        }
    },
    //  setFooter: function() {
    //         this.view.customFooter.lblAccounts.skin = "sknLbl424242SSP20px";
    //         this.view.customFooter.flxAccSelect.setVisibility(true);
    //         this.view.customFooter.lblTransfer.skin = "sknLblA0A0A0SSP20px";
    //         this.view.customFooter.flxTransferSel.setVisibility(false);
    //         this.view.customFooter.lblBillPay.skin = "sknLblA0A0A0SSP20px";
    //         this.view.customFooter.flxBillSelected.setVisibility(false);
    //         this.view.customFooter.lblMore.skin = "sknLblA0A0A0SSP20px";
    //         this.view.customFooter.flxMoreSelect.setVisibility(false);
    //     },
    flxMainContainerOnScrolling: function() {
        if (this.view.flxMainContainer.contentOffsetMeasured.y >= 165) {
            //alert("fixed");
            this.view.segTransactions.height = "100%";
            this.view.flxMainContainer.forceLayout();
        }
    },
    segTransactionsOnTouchEnd: function() {
        if ((this.view.segTransactions.height !== 'preferred') && (this.view.flxHeaderSearchbox.isVisible === false)) {
            if (this.view.segTransactions.contentOffsetMeasured.y <= 1) {
                this.view.segTransactions.height = "preferred";
                this.view.flxMainContainer.forceLayout();
            }
        }
    },
    createViewForFamilyCheckingAccount: function() {
        this.view.customHeader.lblLocateUs.text = "FAMILY CHECKING ACCOUNT";
        this.view.lblDueDate.setVisibility(false);
        this.view.btnWithdrawCash.text = kony.i18n.getLocalizedString("kony.mb.accdetails.withdrawCash");
        this.view.lblAvailableBalance.text = kony.i18n.getLocalizedString("kony.mb.accdetails.availBal");
    },
    createViewForCreditCard: function() {
        this.view.customHeader.lblLocateUs.text = "MY CREDIT CARD";
        this.view.lblDueDate.setVisibility(true);
        this.view.btnWithdrawCash.text = kony.i18n.getLocalizedString("kony.mb.accdetails.makeAPayment");
        this.view.lblAvailableBalance.text = kony.i18n.getLocalizedString("kony.mb.accdetails.availBal");
    },
    createViewForHomeLoanAccount: function() {
        this.view.customHeader.lblLocateUs.text = "HOME LOAN ACCOUNT";
        this.view.lblDueDate.setVisibility(true);
        this.view.btnWithdrawCash.text = kony.i18n.getLocalizedString("kony.mb.accdetails.payDueAmount");
        this.view.lblAvailableBalance.text = kony.i18n.getLocalizedString("kony.mb.accdetails.outstandingPrincipalBal");
    },
    createViewForDepositAccount: function() {
        this.view.customHeader.lblLocateUs.text = "MY DEPOSIT ACCOUNT";
        this.view.lblDueDate.setVisibility(false);
        this.view.lblCurrentBalance.text = kony.i18n.getLocalizedString("kony.mb.accdetails.maturityDate");
        this.view.lblCurrBalValue.text = "12/06/2017";
        this.view.btnWithdrawCash.text = kony.i18n.getLocalizedString("kony.mb.accdetails.newDeposit");
        this.view.lblAvailableBalance.text = kony.i18n.getLocalizedString("kony.mb.accdetails.availBal");
    },
    segTransactionsOnRowClick: function() {
        try {
            var navMan = applicationManager.getNavigationManager();
            var accountsDetails = navMan.getCustomInfo("frmAccountDetails");
            var type = accountsDetails.selectedAccountData["type"];
            if (type.toLowerCase().trim() === "external") {
                return;
            }
            var selectedSectionItems = this.view.segPosted.selectedRowItems[0];
            // var fullsegList=accountsDetails;
            var trnsmodule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransactionModule");
            // var res=[{rowData:selectedSectionItems}, {fulldata:fullsegList}]
            navMan.setCustomInfo("frmTransactions", selectedSectionItems);
            navMan.setEntryPoint("Transactions", "frmTransactions");
            onNavigate = true;
            trnsmodule.presentationController.commonFunctionForNavigation("frmTransactions");
        } catch (e) {
            kony.print("Exception occurred at segTransactionsOnRowClick : " + e);
        }
    },
    flxSearchOnTouchEnd: function() {
        search = true;
        //setting visiblity of arrows in visible
        this.view.imgPosteddropdown.isVisible = false;
        this.view.imgPendingdropdown.isVisible = false;
        this.view.flxHeaderSearchbox.setVisibility(true);
        this.view.flxHeader.setVisibility(false);
        this.view.flxSearch.setVisibility(false);
        this.view.flxShadow.setVisibility(false);
        this.view.flxBalance.setVisibility(false);
        this.view.flxSeperator2.setVisibility(false);
        this.view.flxOptions.setVisibility(false);
        this.view.flxTransactions.top = "39Dp";
        this.view.flxTransactions.forceLayout();
        this.view.customSearchbox.tbxSearch.setFocus(true);
        //if pending and posted transactions are empty records
        this.view.flxTypePending.isVisible = false;
        this.view.segPending.isVisible = false;
        this.view.flxTypePosted.isVisible = false;
        this.view.segPosted.isVisible = false;
        this.setSearchData();
    },
    setSearchData: function() {
        var newPendingList = this.pendingTransactiondata;
        var newPostedList = this.postedTransactiondata;
        if (newPendingList.length > 0) {
            this.showPendingSearchList(newPendingList);
        } else {
            this.view.flxTypePending.isVisible = false;
            this.view.segPending.isVisible = false;
        }
        if (newPostedList.length > 0) {
            var newPostedSearchList = newPostedList.slice(0, counterSearchPosted + 21);
            flagEnd = false;
            if (counterSearchPosted <= newPostedSearchList.length) {
                counterSearchPosted = counterSearchPosted + 20;
                this.showPostedSearchList(newPostedSearchList);
            } else {
                //add remaining records and stop
                newPostedSearchList = newPostedList.slice(0, newPostedList.length);
                flagEnd = true;
                this.showPostedSearchList(newPostedSearchList);
            }
        } else {
            this.view.flxTypePosted.isVisible = false;
            this.view.segPosted.isVisible = false;
        }
        this.view.customSearchbox.tbxSearch.onTextChange = this.tbxSearchOnTextChange;
        this.view.flxMainContainer.forceLayout();
    },
    btnCancelOnClick: function() {
        search = false;
        this.view.flxMainContainer.top = "58Dp";
        this.view.flxTransactions.setVisibility(true);
        this.view.flxTypePending.isVisible = true;
        this.view.flxTypePosted.isVisible = true;
        if (arrowPending == "down") {
            this.view.segPending.isVisible = false;
        } else {
            this.view.segPending.isVisible = true;
        }
        if (arrowPosted === "down") {
            this.view.segPosted.isVisible = false;
        } else {
            this.view.segPosted.isVisible = true;
        }
        this.view.imgPosteddropdown.isVisible = true;
        this.view.imgPendingdropdown.isVisible = true;
        this.view.flxHeaderSearchbox.setVisibility(false);
        if (kony.os.deviceInfo().name !== "iPhone") {
            this.view.flxHeader.isVisible = true;
        } else {
            this.view.flxHeader.isVisible = true;
        }
        this.view.flxSearch.setVisibility(true);
        this.view.flxShadow.setVisibility(false);
        this.view.flxBalance.setVisibility(true);
        this.view.flxSeperator2.setVisibility(true);
        this.view.customSearchbox.tbxSearch.text = "";
        var navManager = applicationManager.getNavigationManager();
        var accountsDetails = navManager.getCustomInfo("frmAccountDetails");
        var type = accountsDetails.selectedAccountData["type"];
        if (type.toLowerCase().trim() === "internal") {
            this.view.flxOptions.isVisible = true;
        }
        this.view.flxTransactions.top = "340Dp";
        this.setSegmentData();
        this.view.flxMainContainer.forceLayout();
    },
    flxBackOnClick: function() {
        search = false;
        this.view.imgPosteddropdown.isVisible = true;
        this.view.imgPendingdropdown.isVisible = true;
        if (arrowPending === "down") {
            this.view.segPending.isVisible = false;
        }
        if (arrowPosted === "down") {
            this.view.segPosted.isVisible = false;
        }
        var navMan = applicationManager.getNavigationManager();
        navMan.goBack();
    },
    getStatements: function() {
        applicationManager.getPresentationUtility().showLoadingScreen();
        var accMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountModule");
        accMod.presentationController.fetchAccountStataments();
    },
    navigateToAdvanceSearch: function() {
        var accountMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountModule");
        onNavigate = true;
        accountMod.presentationController.commonFunctionForNavigation("frmAdvanceSearch");
        //custom metric API to generate Reports
        KNYMetricsService.sendCustomMetrics("frmAccountDetails", {
            "Search Transactions": "Initial Search"
        });
    },
    gotoAccountInfo: function() {
        var navManager = applicationManager.getNavigationManager();
        var accountsDetails = navManager.getCustomInfo("frmAccountDetails");
        var accountModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountModule");
        if (String(accountsDetails.selectedAccountData.type).toLowerCase().trim() === "external") {
            accountModule.presentationController.fetchInfoForExternalBankAccount();
        } else {
            navManager.setCustomInfo("frmAccountInfo", accountsDetails);
            accountModule.presentationController.commonFunctionForNavigation("frmAccountInfo");
        }
    },
    appendEmptySection: function(dataParam) {
        var data = dataParam;
        var emptySection = [{
                "template": "flxEmptyHeader",
            },
            [{
                "template": "flxAccountDetailsEmptyRow"
            }]
        ];
        data.unshift(emptySection);
        return data;
    },
    showSuccessPopup: function(refID, type) {
        // TO DO i18n's
        var msg;
        if (type === "delete") {
            msg = "Transaction was cancelled successfully with reference ID : " + (refID.transactionId || refID.refernceId);
        } else {
            if (refID.referenceId) msg = "Transaction was done successfully with transaction ID : " + refID.referenceId;
            else msg = "Transaction was edited successfully with reference ID : " + refID.transactionId;
        }
        applicationManager.getDataProcessorUtility().showToastMessageSuccess(this, msg);
    },
    showErrorPopup: function(err) {
        applicationManager.getDataProcessorUtility().showToastMessageError(this, JSON.stringify(err));
    },
    flxPendingHide: function() {
        if (this.view.segPending.isVisible === true) {
            this.view.segPending.setVisibility(false);
            this.view.imgPendingdropdown.src = "arrowdownblue.png";
            arrowPending = "down";
        } else {
            arrowPending = "up";
            this.view.segPending.setVisibility(true);
            this.view.imgPendingdropdown.src = "arrowupblue.png";
        }
    },
    flxPostedHide: function() {
        if (this.view.segPosted.isVisible === true) {
            this.view.segPosted.setVisibility(false);
            this.view.imgPosteddropdown.src = "arrowdownblue.png";
            arrowPosted = "down";
        } else {
            arrowPosted = "up";
            this.view.segPosted.setVisibility(true);
            this.view.imgPosteddropdown.src = "arrowupblue.png";
        }
    },
    checkingAccountsLinks: function() {
        this.view.imgArrow.setVisibility(false);
        this.view.flxLinks.setVisibility(true);
        this.view.lblLink1.text = "account documents (statements)";
        this.view.flxLink1.setVisibility(true);
        this.view.lblLink2.text = "ach history";
        this.view.flxLink2.setVisibility(true);
        this.view.lblLink3.text = "apple pay";
        this.view.flxLink3.setVisibility(true);
        this.view.lblLink4.text = "courtesy pay";
        this.view.flxLink4.setVisibility(false);
        this.view.lblLink5.text = "courtesy pay debit";
        this.view.flxLink5.setVisibility(true);
        this.view.lblLink6.text = "order checks";
        this.view.flxLink6.setVisibility(true);
        this.view.lblLink7.text = "stop payment";
        this.view.flxLink7.setVisibility(true);
        this.view.lblLink8.text = "send me a check";
        this.view.flxLink8.setVisibility(true);
        this.view.lblLink9.text = "details";
        this.view.flxLink9.setVisibility(true);
        this.view.flxLink10.setVisibility(true);
    },
    savingAccountsLinks: function() {
        this.view.imgArrow.setVisibility(false);
        this.view.flxLinks.setVisibility(true);
        this.view.lblLink1.text = "ach history";
        this.view.flxLink1.setVisibility(true);
        this.view.lblLink2.text = "send me a check";
        this.view.flxLink2.setVisibility(true);
        this.view.lblLink3.text = "details";
        this.view.flxLink3.setVisibility(true);
        this.view.flxLink4.setVisibility(false);
        this.view.flxLink6.setVisibility(false);
        this.view.flxLink5.setVisibility(false);
        this.view.flxLink7.setVisibility(false);
        this.view.flxLink8.setVisibility(false);
        this.view.flxLink9.setVisibility(false);
        this.view.flxLink10.setVisibility(true);
    },
    creditCardAccountsLinks: function() {
        this.view.imgArrow.setVisibility(false);
        this.view.flxLinks.setVisibility(true);
        this.view.lblLink1.text = "rewards";
        this.view.flxLink1.setVisibility(true);
        this.view.lblLink2.text = "alerts";
        this.view.flxLink2.setVisibility(true);
        this.view.lblLink3.text = "cash advance";
        this.view.flxLink3.setVisibility(true);
        this.view.lblLink4.text = "cleared checks";
        this.view.flxLink4.setVisibility(true);
        this.view.lblLink5.text = "credit card statements";
        this.view.flxLink5.setVisibility(true);
        this.view.lblLink6.text = "stop payment";
        this.view.flxLink6.setVisibility(true);
        this.view.lblLink7.text = "details";
        this.view.flxLink7.setVisibility(true);
        this.view.flxLink8.setVisibility(false);
        this.view.flxLink9.setVisibility(false);
        this.view.flxLink10.setVisibility(true);
    },
    locAccountLinks: function() {
        this.view.imgArrow.setVisibility(false);
        this.view.flxLinks.setVisibility(true);
        this.view.lblLink1.setVisibility(true);
        this.view.lblLink1.text = "skip a pay";
        this.view.flxLink2.setVisibility(true);
        this.view.lblLink2.text = "alerts";
        this.view.flxLink3.setVisibility(true);
        this.view.lblLink3.text = "statements";
        this.view.flxLink4.setVisibility(true);
        this.view.lblLink4.text = "tax forms";
        this.view.flxLink5.setVisibility(true);
        this.view.lblLink5.text = "payoff inquiry";
        this.view.flxLink6.setVisibility(true);
        this.view.lblLink6.text = "order check";
        this.view.flxLink7.setVisibility(true);
        this.view.lblLink7.text = "details";
        this.view.flxLink8.setVisibility(false);
        this.view.flxLink9.setVisibility(false);
        this.view.flxLink10.setVisibility(true);
    },
    hideLinks: function() {
        this.view.flxLinks.setVisibility(false);
        this.view.imgArrow.setVisibility(true);
    },
    onClickFlxAccounts: function() {
        var navManager = applicationManager.getNavigationManager();
        var accountObj = applicationManager.getAccountManager();
        var configManager = applicationManager.getConfigurationManager();
        var accountData = "";
        accountData = accountObj.getInternalAccounts();
        var custominfo = navManager.getCustomInfo("frmDashboard");
        if (!custominfo) {
            custominfo = {};
        }
        custominfo.accountData = accountData;
        navManager.setCustomInfo("frmDashboard", custominfo);
        navManager.navigateTo("frmDashboardAggregated");
    },
    onClickFlxTransfers: function() {
        var transMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransactionModule");
        transMod.presentationController.getTransactions();
    },
    scrollEnd: function() {
        var deviceHeight = kony.os.deviceInfo().screenHeight;
        var contentSize = this.view.flxMainContainer.contentOffsetMeasured;
        if (search === true) {
            flxScrollContainerHeight = this.view.flxTransactions.frame.height;
        } else {
            flxScrollContainerHeight = this.view.flxTransactions.frame.height + 340;
        }
        flxScrollContainerEnd = flxScrollContainerHeight - deviceHeight;
        //if(contentSize.y >=pendingListEnd && counterPending<=pendingTransactiondata.length){
        //   alert("Pending  Loading" );
        //this.paginationPosted();
        // }
        // else
        if (contentSize.y >= flxScrollContainerEnd && flagEnd === false) {
            //calling timmer
            applicationManager.getPresentationUtility().showLoadingScreen();
            try {
                kony.timer.schedule("mytimer12", this.paginationPosted, 3, false);
            } catch (Exception) {}
        }
    },
    paginationPosted: function() {
        try {
            kony.timer.cancel("mytimer12");
        } catch (Exception) {}
        applicationManager.getPresentationUtility().dismissLoadingScreen();
        if (search === true) {
            this.setSearchData();
        } else {
            this.setSegmentData();
        }
    },
    showPendingList: function(newPendingList) {
        var temp = {};
        var data = [];
        var lblTransactionAmountMod;
        var lblDateModified;
        for (var l = 0; l < newPendingList.length; l++) {
            var formatUtil = applicationManager.getFormatUtilManager();
            var pendingAmount = newPendingList[l]["amount"];
            if (pendingAmount > 0) {
                lblDateModified = {
                    text: newPendingList[l]["effectiveDate"],
                    isVisible: false
                }
                lblTransactionAmountMod = {
                    skin: "sknLblSuccessTrnsfr",
                    text: newPendingList[l]["amount"]
                };
            } else if (pendingAmount < 0) {
                lblDateModified = {
                    text: newPendingList[l]["effectiveDate"],
                    isVisible: false
                }
                lblTransactionAmountMod = {
                    skin: "sknlblAlertRed",
                    text: newPendingList[l]["amount"]
                };
            } else {
                lblDateModified = {
                    text: newPendingList[l]["effectiveDate"],
                    isVisible: false
                }
                lblTransactionAmountMod = {
                    text: newPendingList[l]["amount"]
                };
            }
            temp = {
                "lblTransactions": newPendingList[l]["payeeName"],
                "lblDate": lblDateModified,
                "lblTransactionsAmount": lblTransactionAmountMod
            };
            if (pendingAmount.indexOf('$') == -1) {
                temp.lblTransactionsAmount.text = formatUtil.formatAmountandAppendCurrencySymbol(newPendingList[l]["amount"]);
            }
            data.push(temp);
        }
        this.view.flxTransactions.setVisibility(true);
        this.view.segPending.setData(data);
        //  this.view.segPending.isVisible = true;
        //this.pendingTransactiondata= this.view.segPending.data;
        //  this.view.flxTypePending.isVisible=true;
    },
    showPostedList: function(newPostedList) {
        var formatUtil = applicationManager.getFormatUtilManager();
        var lblDateModified;
        var lblrunningBalanceModified;
        var temp = {};
        var data = [];
        var lblTransactionAmountMod;
        for (var l = 0; l < newPostedList.length; l++) {
            var amount = newPostedList[l]["TranAmount"];
            var runningBalance = newPostedList[l]["newBalance"];
            if (runningBalance === null || runningBalance === "" || runningBalance === undefined) {
                newPostedList[l]["newBalance"] = "";
            }
            var lblPrevAvailBalanceModified = newPostedList[l]["PrevAvailBalance"];
            if (amount > 0) {
                lblDateModified = {
                    text: newPostedList[l]["effectiveDate"],
                    isVisible: false
                }
                lblrunningBalanceModified = {
                    text: newPostedList[l]["newBalance"]
                }
                lblTransactionAmountMod = {
                    skin: "sknLblSuccessTrnsfr",
                    text: newPostedList[l]["TranAmount"]
                };
            } else if (amount < 0) {
                lblDateModified = {
                    text: newPostedList[l]["effectiveDate"],
                    isVisible: false
                }
                lblrunningBalanceModified = {
                    text: newPostedList[l]["newBalance"]
                }
                lblTransactionAmountMod = {
                    skin: "sknBlueLbl",
                    text: newPostedList[l]["TranAmount"]
                };
            } else {
                lblDateModified = {
                    text: newPostedList[l]["effectiveDate"],
                    isVisible: false
                }
                lblTransactionAmountMod = {
                    text: newPostedList[l]["TranAmount"]
                }
                lblrunningBalanceModified = {
                    text: newPostedList[l]["newBalance"]
                }
            }
            temp = {
                "lblTransactions": newPostedList[l]["StmtDesc"],
                "lblDate": lblDateModified,
                "lblRunningBalance": lblrunningBalanceModified,
                "lblTransactionsAmount": lblTransactionAmountMod,
                "lblPostedDate": newPostedList[l]["PostDate"],
                "lblSourceCode": newPostedList[l]["SourceCode"],
                "lblPrevAvailBalance": lblPrevAvailBalanceModified,
                "lblActionCode": newPostedList[l]["ActionCode"],
                "lblPostTime": newPostedList[l]["PostTime"],
                "lblCommentCode": newPostedList[l]["CommentCode"],
                "lblRegECode": newPostedList[l]["RegECode"]
            };
            if (amount.indexOf('$') == -1) {
                temp.lblTransactionsAmount.text = formatUtil.formatAmountandAppendCurrencySymbol(newPostedList[l]["TranAmount"]);
            }
            if (temp.lblRunningBalance.text !== undefined && temp.lblRunningBalance.text !== "" && temp.lblRunningBalance.text !== null) {
                temp.lblRunningBalance.text = formatUtil.formatAmountandAppendCurrencySymbol(temp.lblRunningBalance.text);
            }
            if (temp.lblPrevAvailBalance !== undefined && temp.lblPrevAvailBalance !== "" && temp.lblPrevAvailBalance !== null) {
                temp.lblPrevAvailBalance = formatUtil.formatAmountandAppendCurrencySymbol(newPostedList[l]["PrevAvailBalance"]);
            }
            data.push(temp);
        }
        this.view.flxTransactions.setVisibility(true);
        this.view.segPosted.setData(data);
        // this.view.segPosted.isVisible = true;
        //  this.view.flxTypePosted.isVisible=true;
    },
    showPendingSearchList: function(newPendingList) {
        var temp = {};
        var data = [];
        var lblTransactionAmountMod;
        var lblDateModified;
        for (var l = 0; l < newPendingList.length; l++) {
            var formatUtil = applicationManager.getFormatUtilManager();
            var pendingAmount = newPendingList[l]["amount"];
            if (pendingAmount > 0) {
                lblDateModified = {
                    text: newPendingList[l]["effectiveDate"],
                    isVisible: true
                }
                lblTransactionAmountMod = {
                    skin: "sknLblSuccessTrnsfr",
                    text: newPendingList[l]["amount"]
                };
            } else if (pendingAmount < 0) {
                lblDateModified = {
                    text: newPendingList[l]["effectiveDate"],
                    isVisible: true
                }
                lblTransactionAmountMod = {
                    skin: "sknlblAlertRed",
                    text: newPendingList[l]["amount"]
                };
            } else {
                lblDateModified = {
                    text: newPendingList[l]["effectiveDate"],
                    isVisible: true
                }
                lblTransactionAmountMod = {
                    text: newPendingList[l]["amount"]
                };
            }
            temp = {
                "lblTransactions": newPendingList[l]["payeeName"],
                "lblDate": lblDateModified,
                "lblTransactionsAmount": lblTransactionAmountMod
            };
            if (pendingAmount.indexOf('$') == -1) {
                temp.lblTransactionsAmount.text = formatUtil.formatAmountandAppendCurrencySymbol(newPendingList[l]["amount"]);
            }
            data.push(temp);
        }
        this.view.segPending.setData(data);
        this.view.segPending.isVisible = true;
        this.view.flxTypePending.isVisible = true;
        dataPending = this.view.segPending.data;
    },
    showPostedSearchList: function(newPostedList) {
        var formatUtil = applicationManager.getFormatUtilManager();
        var lblDateModified;
        var lblrunningBalanceModified;
        var temp = {};
        var data = [];
        var lblTransactionAmountMod;
        for (var l = 0; l < newPostedList.length; l++) {
            var amount = newPostedList[l]["TranAmount"];
            var runningBalance = newPostedList[l]["newBalance"];
            if (runningBalance === null || runningBalance === "" || runningBalance === undefined) {
                newPostedList[l]["newBalance"] = "";
            }
            var lblPrevAvailBalanceModified = newPostedList[l]["PrevAvailBalance"];
            if (amount > 0) {
                lblDateModified = {
                    text: newPostedList[l]["effectiveDate"],
                    isVisible: true
                }
                lblrunningBalanceModified = {
                    text: newPostedList[l]["newBalance"],
                    isVisible: false
                }
                lblTransactionAmountMod = {
                    skin: "sknLblSuccessTrnsfr",
                    text: newPostedList[l]["TranAmount"]
                };
            } else if (amount < 0) {
                lblDateModified = {
                    text: newPostedList[l]["effectiveDate"],
                    isVisible: true
                }
                lblrunningBalanceModified = {
                    text: newPostedList[l]["newBalance"],
                    isVisible: false
                }
                lblTransactionAmountMod = {
                    skin: "sknBlueLbl",
                    text: newPostedList[l]["TranAmount"]
                };
            } else {
                lblDateModified = {
                    text: newPostedList[l]["effectiveDate"],
                    isVisible: true
                }
                lblTransactionAmountMod = {
                    text: newPostedList[l]["TranAmount"]
                }
                lblrunningBalanceModified = {
                    text: newPostedList[l]["newBalance"],
                    isVisible: false
                }
            }
            temp = {
                "lblTransactions": newPostedList[l]["StmtDesc"],
                "lblDate": lblDateModified,
                "lblRunningBalance": lblrunningBalanceModified,
                "lblTransactionsAmount": lblTransactionAmountMod,
                "lblPostedDate": newPostedList[l]["PostDate"],
                "lblSourceCode": newPostedList[l]["SourceCode"],
                "lblPrevAvailBalance": lblPrevAvailBalanceModified,
                "lblActionCode": newPostedList[l]["ActionCode"],
                "lblPostTime": newPostedList[l]["PostTime"],
                "lblCommentCode": newPostedList[l]["CommentCode"],
                "lblRegECode": newPostedList[l]["RegECode"]
            };
            if (amount.indexOf('$') == -1) {
                temp.lblTransactionsAmount.text = formatUtil.formatAmountandAppendCurrencySymbol(newPostedList[l]["TranAmount"]);
            }
            if (temp.lblRunningBalance.text !== undefined && temp.lblRunningBalance.text !== "" && temp.lblRunningBalance.text !== null) {
                temp.lblRunningBalance.text = formatUtil.formatAmountandAppendCurrencySymbol(temp.lblRunningBalance.text);
            }
            if (temp.lblPrevAvailBalance !== undefined && temp.lblPrevAvailBalance !== "" && temp.lblPrevAvailBalance !== null) {
                temp.lblPrevAvailBalance = formatUtil.formatAmountandAppendCurrencySymbol(newPostedList[l]["PrevAvailBalance"]);
            }
            data.push(temp);
        }
        this.view.segPosted.setData(data);
        this.view.flxTypePosted.isVisible = true;
        this.view.flxTransactions.setVisibility(true);
        this.view.segPosted.isVisible = true;
        dataPosted = this.view.segPosted.data;
    }
});
define("AccountModule/frmAccountDetailsControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onTouchEnd defined for imgSearch **/
    AS_Image_i9ba471e1f334efdaf0bb9fe3b400962: function AS_Image_i9ba471e1f334efdaf0bb9fe3b400962(eventobject, x, y) {
        var self = this;
        this.gotoAccountInfo();
    },
    /** onClick defined for flxAdvSearch **/
    AS_FlexContainer_a33bd484b8db4bdd9298c1fc0b1515f6: function AS_FlexContainer_a33bd484b8db4bdd9298c1fc0b1515f6(eventobject) {
        var self = this;
        this.navigateToAdvanceSearch();
    },
    /** onClick defined for btnStatements **/
    AS_Button_a31ed350c6104381981c3c5b53be3c48: function AS_Button_a31ed350c6104381981c3c5b53be3c48(eventobject) {
        var self = this;
        this.getStatements();
    },
    /** onClick defined for undefined **/
    AS_BarButtonItem_f27e36a7e6aa471aa51a141ff7c319f0: function AS_BarButtonItem_f27e36a7e6aa471aa51a141ff7c319f0(eventobject) {
        var self = this;
        this.gotoAccountInfo();
    },
    /** init defined for frmAccountDetails **/
    AS_Form_e320ad1f929e4a3dad4efe079f2102b1: function AS_Form_e320ad1f929e4a3dad4efe079f2102b1(eventobject) {
        var self = this;
        this.init();
    },
    /** preShow defined for frmAccountDetails **/
    AS_Form_ab38005fe48c468c8516b24ac28c0519: function AS_Form_ab38005fe48c468c8516b24ac28c0519(eventobject) {
        var self = this;
        return self.frmAccountDetailsPreshow.call(this);
    }
});
define("AccountModule/frmAccountDetailsController", ["AccountModule/userfrmAccountDetailsController", "AccountModule/frmAccountDetailsControllerActions"], function() {
    var controller = require("AccountModule/userfrmAccountDetailsController");
    var controllerActions = ["AccountModule/frmAccountDetailsControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
