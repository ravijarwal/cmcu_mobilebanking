define("AccountModule/userfrmInvestmentAccountsController", {
    //Type your controller code here 
    onNavigate: function() {
            var navManager = applicationManager.getNavigationManager();
            var brwsrUrl = navManager.getCustomInfo("frmInvestmentAccounts");
            var headersData = {
                "Content-type": "application/x-www-form-urlencoded"
            };
            var urlConf = {
                URL: brwsrUrl,
                requestMethod: constants.BROWSER_REQUEST_METHOD_POST,
                headers: headersData
            };
            this.view.investmentsBrowser.requestURLConfig = urlConf;
        }
        //     this.view.investmentsBrowser.handleRequest = handleRequestCallback;
        //     var query = {
        //     };
        //     var params = {
        //      "originalURL": brwsrUrl,
        //       "queryParams":query,
        //       "requestMethod":Constants.BROWSER_REQUEST_METHOD_POST,
        //       "header":headersData    
        //     };
        //   },
        //    handleRequestCallback: function(browserWidget, params) {
        //     kony.print("handleRequest event triggered");
        //     kony.print("Original URL" + params["originalURL"]);
        //     kony.print("Request Method" + params["requestMethod"]);
        //     kony.print("Header" + JSON.stringify(params["header"]));
        // }
});
define("AccountModule/frmInvestmentAccountsControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
});
define("AccountModule/frmInvestmentAccountsController", ["AccountModule/userfrmInvestmentAccountsController", "AccountModule/frmInvestmentAccountsControllerActions"], function() {
    var controller = require("AccountModule/userfrmInvestmentAccountsController");
    var controllerActions = ["AccountModule/frmInvestmentAccountsControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
