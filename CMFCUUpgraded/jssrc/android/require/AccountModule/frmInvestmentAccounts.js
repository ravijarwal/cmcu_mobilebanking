define("AccountModule/frmInvestmentAccounts", function() {
    return function(controller) {
        function addWidgetsfrmInvestmentAccounts() {
            this.setDefaultUnit(kony.flex.DP);
            var investmentsBrowser = new kony.ui.Browser({
                "detectTelNumber": true,
                "enableZoom": false,
                "height": "100%",
                "htmlString": "Browser",
                "id": "investmentsBrowser",
                "isVisible": true,
                "left": "0dp",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            this.add(investmentsBrowser);
        };
        return [{
            "addWidgets": addWidgetsfrmInvestmentAccounts,
            "enabledForIdleTimeout": false,
            "id": "frmInvestmentAccounts",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "skin": "sknFlxGreyBg"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "footerOverlap": false,
            "headerOverlap": false,
            "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
            "retainScrollPosition": false,
            "titleBar": true,
            "titleBarSkin": "slTitleBar",
            "windowSoftInputMode": constants.FORM_ADJUST_PAN
        }]
    }
});