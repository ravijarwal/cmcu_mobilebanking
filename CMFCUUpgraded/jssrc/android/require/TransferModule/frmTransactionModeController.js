define("TransferModule/userfrmTransactionModeController", {
    preShow: function() {
        if (kony.os.deviceInfo().name === "iPhone") {
            this.view.flxHeader.isVisible = false;
        }
        this.setSegmentData();
        this.initActions();
        applicationManager.getPresentationUtility().dismissLoadingScreen();
        var navManager = applicationManager.getNavigationManager();
        var currentForm = navManager.getCurrentForm();
        applicationManager.getPresentationFormUtility().logFormName(currentForm);
    },
    init: function() {
        var navManager = applicationManager.getNavigationManager();
        var currentForm = navManager.getCurrentForm();
        applicationManager.getPresentationFormUtility().initCommonActions(this, "YES", currentForm);
    },
    initActions: function() {
        var scope = this;
        this.view.customHeader.imgBack.src = "back_icon.png";
        this.view.segTransactionMode.onRowClick = function() {
                scope.segmentRowClick();
            }
            // this.view.btnTemp.onClick = function(){
            // }
    },
    segmentRowClick: function() {
        var type = this.view.segTransactionMode.data[this.view.segTransactionMode.selectedIndex[1]].lblTransactionMode;
        applicationManager.getPresentationUtility().showLoadingScreen();
        var  transMod = applicationManager.getModulesPresentationController("TransactionModule");
        transMod.presentationController.transactionMode = type;
        transMod.presentationController.showAccounts("InternalTransfer");
    },
    backNavigation: function() {
        var navMan = applicationManager.getNavigationManager();
        navMan.goBack();
    },
    setSegmentData: function() {
        var data = [];
        var transferMod = applicationManager.getModulesPresentationController("TransferModule");
        data = [{
            lblTMode: "My Own CMCU Accounts",
            lblTModeDesc: "Transfer Money to your CMCU Accounts",
            imgArrow: "chevronright.png",
            template: "flxTransactionMode"
        }, {
            lblTMode: "Another CMCU Customer",
            lblTModeDesc: "Transfer money to another CMCU Customer",
            imgArrow: "chevronright.png",
            template: "flxTransactionMode"
        }, {
            lblTMode: "Other Bank Accounts",
            lblTModeDesc: "Transfer money to accounts at other banks",
            imgArrow: "chevronright.png",
            template: "flxTransactionMode"
        }];
        this.view.segTransactionMode.widgetDataMap = {
            lblTransactionMode: "lblTMode",
            lblTransactionModeDescription: "lblTModeDesc",
            imgArrow: "imgArrow",
            "flxTransactionMode": "flxTransactionMode"
        };
        this.view.segTransactionMode.setData(data);
    }
});
define("TransferModule/frmTransactionModeControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for flxBack **/
    AS_FlexContainer_fdcfc70f9b4847238077f8bb729ab3ee: function AS_FlexContainer_fdcfc70f9b4847238077f8bb729ab3ee(eventobject) {
        var self = this;
        this.backNavigation();
    },
    /** onClick defined for undefined **/
    AS_BarButtonItem_e445fa2e93204f1cb5979e885563b7fc: function AS_BarButtonItem_e445fa2e93204f1cb5979e885563b7fc(eventobject) {
        var self = this;
        var transModPresentationController = applicationManager.getModulesPresentationController("TransactionModule");
        transModPresentationController.commonFunctionForNavigation("frmTransfers");
    },
    /** init defined for frmTransactionMode **/
    AS_Form_aa0a7a6dccc64c8f9f478835b4388466: function AS_Form_aa0a7a6dccc64c8f9f478835b4388466(eventobject) {
        var self = this;
        this.init();
    },
    /** preShow defined for frmTransactionMode **/
    AS_Form_bdd1d73677564bdc8490ef814f126f2d: function AS_Form_bdd1d73677564bdc8490ef814f126f2d(eventobject) {
        var self = this;
        this.preShow();
    }
});
define("TransferModule/frmTransactionModeController", ["TransferModule/userfrmTransactionModeController", "TransferModule/frmTransactionModeControllerActions"], function() {
    var controller = require("TransferModule/userfrmTransactionModeController");
    var controllerActions = ["TransferModule/frmTransactionModeControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
