define("TransferModule/userfrmTransfersFromAccountController", {
    transferType: '',
    segmentData: null,
    //     onNavigate: function (obj) {
    //         if (obj == undefined) {
    //             return;
    //         }
    //         var scope = this;
    //         var setType = {
    //             "mykony": function () {
    //                 scope.myKonySegmentData();
    //             },
    //             "myother": function () {
    //                 scope.myOtherSegmentData();
    //             },
    //             "otherkony": function () {
    //                 scope.otherKonySegmentData();
    //             },
    //             "otherbank": function () {
    //                 scope.otherBankSegmentData();
    //             },
    //             "international": function () {
    //                 scope.internationalSegmentData();
    //             },
    //             "wire": function () {
    //                 scope.wireSegmentData();
    //             },
    //         };
    //         this.transferType = obj;
    //         setType[obj];
    //     },
    init: function() {
        var navManager = applicationManager.getNavigationManager();
        var currentForm = navManager.getCurrentForm();
        applicationManager.getPresentationFormUtility().initCommonActions(this, "YES", currentForm);
    },
    preShow: function() {
        // applicationManager.getPresentationUtility().dismissLoadingScreen();
        if (this.view.flxHeaderSearchbox.isVisible === true) {
            this.view.flxHeaderSearchbox.isVisible = false;
            this.view.flxSearch.isVisible = true;
            this.view.flxHeader.isVisible = true;
            this.view.flxMainContainer.top = "56dp";
        }
        if (kony.os.deviceInfo().name === "iPhone") {
            this.view.flxHeader.isVisible = false;
            this.view.flxMainContainer.top = "0dp";
        }
        this.initActions();
        applicationManager.getPresentationUtility().dismissLoadingScreen();
        var navManager = applicationManager.getNavigationManager();
        var currentForm = navManager.getCurrentForm();
        applicationManager.getPresentationFormUtility().logFormName(currentForm);
    },
    initActions: function() {
        var scope = this;
        this.view.flxNoTransactions.isVisible = false;
        // this.view.flxHeaderNT.isVisible=true;
        // this.view.flxSeperator3.isVisible=true;
        this.view.segAccounts.isVisible = true;
        this.view.tbxSearch.text = "";
        this.setSegmentData();
        this.view.customHeader.imgBack.src = "back_icon.png";
        this.view.customHeader.flxBack.onClick = function() {
            var navMan = applicationManager.getNavigationManager();
            navMan.goBack();
        }
        this.view.segAccounts.onRowClick = function() {
            scope.segmentRowClick();
        }
        this.view.customHeader.btnRight.skin = "sknFlxBorderPrimaryGrey";
        this.view.customHeader.btnRight.onClick = this.cancelOnClick;
        this.view.tbxSearch.onTouchEnd = this.showSearch;
        // this.view.tbxSearch.onTextChange = this.showSearch;
        this.view.customSearchbox.btnCancel.onClick = this.showSearch;
    },
    cancelOnClick: function() {
        //var transferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
        // transferModule.presentationController.cancelCommon();
        var navMan = applicationManager.getNavigationManager();
        navMan.navigateTo("frmTransfers");
    },
    segmentRowClick: function() {
        var selaccdata = [];
        var navMan = applicationManager.getNavigationManager();
        var rowindex = this.view.segAccounts.selectedRowIndex[1];
        var frmaccdata = this.view.segAccounts.data[rowindex];
        selaccdata.push(frmaccdata);
        var transferModule = applicationManager.getModulesPresentationController("TransferModule");
        var tranAmt = navMan.setCustomInfo("frmTransferAmount", selaccdata[0]);
        transferModule.presentationController.setFromAccountsForTransactions(selaccdata[0]);
        transferModule.presentationController.commonFunctionForNavigation("frmTransferAmount");
    },
    //     myKonySegmentData: function () {
    //         scope.myOtherSegmentData();
    //     },
    setSegmentData: function() {
        var frmaccdata = [];
        var navMan = applicationManager.getNavigationManager();
        var accdata = navMan.getCustomInfo("frmToFromAccounts");
        var internalAccounts = accdata.DonorList;
        // alert("accounts in from accnt ctrller"+JSON.stringify(internalAccounts)); 
        var transMod = applicationManager.getModulesPresentationController("TransactionModule");
        var processedAccountsData = transMod.presentationController.processAccountsData(internalAccounts);
        var slectedaccdata = navMan.getCustomInfo("frmTransfersFromAccount");
        slectedaccdata = slectedaccdata.selectedAccountData;
        //alert("selected data is ***"+slectedaccdata);
        var transaMan = applicationManager.getTransactionManager();
        frmaccdata = internalAccounts;
        frAccount = [];
        var leng = frmaccdata.length;
        //alert("length is+"+leng)
        var selectedString = slectedaccdata["accountID"];
        var selectedAccType = slectedaccdata["accountName"];
        var frmAccType = "";
        for (var i = 0; i < leng; i++) {
            //Selecting only those accounts which r not selected in To accnts
            if (frmaccdata[i]["accountID"] !== null && frmaccdata[i]["accountID"] !== selectedString) {
                var firstChar = selectedAccType.substr(0, 1);
                //alert("selectedAccType"+firstChar);
                //if selected To accnt is Loan,dont allow loan accounts in From Accnts
                if (firstChar === "L") {
                    frmAccType = frmaccdata[i]["fromAccountType"];
                    var fromfirstChar = frmAccType.substr(0, 1);
                    if (fromfirstChar !== firstChar) {
                        frAccount.push(frmaccdata[i]);
                    }
                }
                //Allowing all accounts in From if slected account is not Loan
                else {
                    frAccount.push(frmaccdata[i]);
                }
            }
        }
        frAccount.list = "DONORLIST";
        // var flxSeperatorTransModified = {skin : "sknFlxSeparatorSegTrnsfrs"};
        var transMod = applicationManager.getModulesPresentationController("TransactionModule");
        var processedAccountsData = transMod.presentationController.processAccountsData(frAccount);
        if (processedAccountsData.length > 0) {
            this.view.flxNoTransactions.isVisible = false;
            this.view.segAccounts.isVisible = true;
            this.view.segAccounts.widgetDataMap = {
                lblAccountName: "accountName",
                accountType: "accountType",
                lblAccountID: "accountID",
                lblBankName: "bankName",
                lblAccountBalValue: "availableBalance",
                lblAccountBal: "accountBalanceType",
                accountNumber: "accountNumber"
            };
            for (i = 0; i < processedAccountsData.length; i++) {
                var type = processedAccountsData[i]["fromAccountType"];
                var accId = processedAccountsData[i]["accountID"];
                // alert("type)))))"+type+"*****"+"accId****"+accId)
                var firstChar = "S";
                if (type === "Share") {
                    firstChar = "S";
                } else if (type === "Loan") {
                    firstChar = "L";
                }
                processedAccountsData[i]["accountName"] = firstChar + accId + ":" + processedAccountsData[i]["accountName"];
                this.view.segAccounts.setData(processedAccountsData);
            }
            this.segmentData = this.view.segAccounts.data;
        } else {
            this.segmentData = [];
            this.view.flxNoTransactions.isVisible = true;
            // this.view.flxHeaderNT.isVisible=false;
            // this.view.flxSeperator3.isVisible=false;
            this.view.segAccounts.isVisible = false;
        }
        // applicationManager.getPresentationUtility().dismissLoadingScreen();
    },
    //     otherKonySegmentData: function () {
    //         scope.myOtherSegmentData();
    //     },
    //     otherBankSegmentData: function () {
    //         scope.myOtherSegmentData();
    //     },
    //     internationalSegmentData: function () {
    //     },
    //     wireSegmentData: function () {
    //     },
    showSearch: function() {
        //     if (kony.os.deviceInfo().name === "iPhone") {
        //     } else {
        if (this.view.flxHeaderSearchbox.isVisible == true) {
            this.view.flxHeaderSearchbox.isVisible = false;
            this.view.flxSearch.isVisible = true;
            if (kony.os.deviceInfo().name === "iPhone") {
                this.view.flxHeader.isVisible = false;
                this.view.flxMainContainer.top = "0dp";
            } else {
                this.view.flxHeader.isVisible = true;
                this.view.flxMainContainer.top = "56dp";
            }
            if (this.segmentData.length > 0) {
                this.view.segAccounts.setData(this.segmentData);
                this.view.flxNoTransactions.isVisible = false;
                // this.view.flxHeaderNT.isVisible=true;
                // this.view.flxSeperator3.isVisible=true;
                this.view.segAccounts.isVisible = true;
            } else {
                this.view.flxNoTransactions.isVisible = true;
                // this.view.flxHeaderNT.isVisible=false;
                // this.view.flxSeperator3.isVisible=false;
                this.view.segAccounts.isVisible = false;
            }
        } else {
            this.view.flxSearch.isVisible = false;
            this.view.flxHeader.isVisible = false;
            this.view.flxMainContainer.top = "40dp";
            this.view.customSearchbox.tbxSearch.text = "";
            this.view.flxHeaderSearchbox.isVisible = true;
            this.view.customSearchbox.tbxSearch.setFocus(true);
            this.view.customSearchbox.tbxSearch.onTextChange = this.searchdata;
        }
        //}
    },
    searchdata: function() {
        var searchData;
        var navMan = applicationManager.getNavigationManager();
        var searchtext = this.view.customSearchbox.tbxSearch.text.toLowerCase();
        if (searchtext) {
            this.view.segAccounts.removeAll();
            var data = this.segmentData;
            searchData = applicationManager.getDataProcessorUtility().commonSegmentSearch("accountName", searchtext, data);
            if (searchData.length > 0) {
                this.view.segAccounts.setData(searchData);
                this.view.flxNoTransactions.isVisible = false;
                // this.view.flxHeaderNT.isVisible=true;
                // this.view.flxSeperator3.isVisible=true;
                this.view.segAccounts.isVisible = true;
            } else {
                this.view.segAccounts.isVisible = false;
                this.view.flxNoTransactions.isVisible = true;
                // this.view.flxHeaderNT.isVisible=false;
                // this.view.flxSeperator3.isVisible=false;
            }
        } else {
            if (this.segmentData.length > 0) {
                this.view.segAccounts.setData(this.segmentData);
                this.view.flxNoTransactions.isVisible = false;
                // this.view.flxHeaderNT.isVisible=true;
                // this.view.flxSeperator3.isVisible=true;
                this.view.segAccounts.isVisible = true;
            } else {
                this.view.flxNoTransactions.isVisible = true;
                // this.view.flxHeaderNT.isVisible=false;
                // this.view.flxSeperator3.isVisible=false;
                this.view.segAccounts.isVisible = false;
            }
        }
    }
});
define("TransferModule/frmTransfersFromAccountControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for undefined **/
    AS_BarButtonItem_g06da55ba03740b78ce8d43dbc568aa9: function AS_BarButtonItem_g06da55ba03740b78ce8d43dbc568aa9(eventobject) {
        var self = this;
        this.cancelOnClick();
    },
    /** init defined for frmTransfersFromAccount **/
    AS_Form_def3e4f4dc044d52ac7bd450efab43ff: function AS_Form_def3e4f4dc044d52ac7bd450efab43ff(eventobject) {
        var self = this;
        this.init();
    },
    /** preShow defined for frmTransfersFromAccount **/
    AS_Form_d17def117dbe4a45931a975e96d15180: function AS_Form_d17def117dbe4a45931a975e96d15180(eventobject) {
        var self = this;
        this.preShow();
    }
});
define("TransferModule/frmTransfersFromAccountController", ["TransferModule/userfrmTransfersFromAccountController", "TransferModule/frmTransfersFromAccountControllerActions"], function() {
    var controller = require("TransferModule/userfrmTransfersFromAccountController");
    var controllerActions = ["TransferModule/frmTransfersFromAccountControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
