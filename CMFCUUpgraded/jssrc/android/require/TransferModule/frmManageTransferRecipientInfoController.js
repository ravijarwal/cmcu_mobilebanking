define("TransferModule/userfrmManageTransferRecipientInfoController", {
    onNavigate: function(obj) {
        if (obj == undefined) {
            return;
        }
    },
    init: function() {
        this.initActions();
    },
    preShow: function() {
        if (kony.os.deviceInfo().name === "iPhone") {
            this.view.flxHeader.isVisible = false;
        }
        var transferModPresentationController = applicationManager.getModulesPresentationController("TransferModule");
        var benificiaryDetails = transferModPresentationController.getBenificiaryData();
        this.setDetails(benificiaryDetails);
        var navManager = applicationManager.getNavigationManager();
        var currentForm = navManager.getCurrentForm();
        applicationManager.getPresentationUtility().dismissLoadingScreen();
        applicationManager.getPresentationFormUtility().logFormName(currentForm);
    },
    initActions: function() {
        var scope = this;
        var navManager = applicationManager.getNavigationManager();
        var currentForm = navManager.getCurrentForm();
        applicationManager.getPresentationFormUtility().initCommonActions(this, "YES", currentForm);
        this.view.btnTransfer.onClick = function() {
            var transModPresentationController = applicationManager.getModulesPresentationController("TransferModule");
            transModPresentationController.setTransferToInfo();
            var navMan = applicationManager.getNavigationManager();
            navMan.setEntryPoint("makeatransfer", "frmManageTransferRecipientInfo");
        };
        this.view.customHeader.flxBack.onClick = function() {
            var navMan = applicationManager.getNavigationManager();
            navMan.goBack();
        };
        this.view.customHeader.btnRight.onClick = function() {
            scope.editBenificiaryName();
        };
        this.view.btnDeleteRecipient.onClick = function() {
            var basicConfig = {
                message: applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.transfers.deleteAlertMessage"),
                alertIcon: null,
                alertType: constants.ALERT_TYPE_CONFIRMATION,
                yesLabel: applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.common.AlertYes"),
                noLabel: applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.common.AlertNo"),
                alertHandler: scope.deleteHandler
            };
            var pspConfig = {};
            applicationManager.getPresentationUtility().showAlertMessage(basicConfig, pspConfig);
        };
    },
    editBenificiaryName: function() {
        applicationManager.getPresentationUtility().showLoadingScreen();
        var transferModulePresentationController = applicationManager.getModulesPresentationController("TransferModule");
        transferModulePresentationController.commonFunctionForNavigation("frmManageEditRecipient");
    },
    deleteHandler: function(response) {
        if (response === true) {
            applicationManager.getPresentationUtility().showLoadingScreen();
            var transferModulePresentationController = applicationManager.getModulesPresentationController("TransferModule");
            transferModulePresentationController.deleteSameBankBenificiary();
        }
    },
    setDetails: function(accountDetails) {
        this.view.lblRecipientNameValue.text = accountDetails.beneficiaryName;
        var maskedAccountNumber = applicationManager.getDataProcessorUtility().maskAccountNumber(JSON.stringify(JSON.parse(accountDetails.accountNumber)));
        this.view.lblAccountNumberValue.text = maskedAccountNumber;
        this.view.lblAccountTypeValue.text = accountDetails.accountType;
        this.view.lblNickNameValue.text = accountDetails.nickName;
        this.view.lblAccountBal.text = accountDetails.accountType;
        this.view.title = accountDetails.nickName;
        this.view.customHeader.lblLocateUs.text = accountDetails.nickName;
        var transferModulePresentationController = applicationManager.getModulesPresentationController("TransferModule");
        if (transferModulePresentationController.getFlowType() === "InternationalRecipients") {
            if (accountDetails.countryName) {
                this.view.lblBankName.text = accountDetails.bankName + "," + accountDetails.countryName;
                this.view.lblBankBranchValue.text = accountDetails.bankName + "," + accountDetails.countryName;
            } else {
                this.view.lblBankName.text = accountDetails.bankName;
                this.view.lblBankBranchValue.text = accountDetails.bankName;
            }
            this.view.flxAccounts.isVisible = true;
            this.view.lblRoutingNumberValue.isVisible = true;
            this.view.lblRoutingNumberValue.text = accountDetails.swiftCode;
            this.view.lblRoutingNumber.isVisible = true;
            this.view.lblRoutingNumber.text = applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.Manage.SwiftCode");
            this.view.imgBank.isVisible = true;
            this.view.imgBank.src = "externalbank.png";
        } else if (transferModulePresentationController.getFlowType() === "SameBankRecipients") {
            this.view.lblBankName.text = accountDetails.bankName;
            this.view.lblBankBranchValue.text = accountDetails.bankName;
            this.view.flxAccounts.isVisible = false;
            this.view.lblRoutingNumberValue.isVisible = false;
            this.view.lblRoutingNumber.isVisible = false;
            this.view.imgBank.isVisible = false;
        } else {
            this.view.lblBankName.text = accountDetails.bankName;
            this.view.lblBankBranchValue.text = accountDetails.bankName;
            this.view.flxAccounts.isVisible = true;
            this.view.lblRoutingNumberValue.isVisible = true;
            this.view.lblRoutingNumberValue.text = accountDetails.routingNumber;
            this.view.lblRoutingNumber.isVisible = true;
            this.view.lblRoutingNumber.text = applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.Manage.RoutingNumber");
            this.view.imgBank.isVisible = true;
            this.view.imgBank.src = "externalbank.png";
        }
    }
});
define("TransferModule/frmManageTransferRecipientInfoControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for undefined **/
    AS_BarButtonItem_je307569a36d4a0baa311e9646a59ec2: function AS_BarButtonItem_je307569a36d4a0baa311e9646a59ec2(eventobject) {
        var self = this;
        this.editBenificiaryName();
    },
    /** init defined for frmManageTransferRecipientInfo **/
    AS_Form_fcc1cb4f9c1e4b1b8d36360785dd6d8f: function AS_Form_fcc1cb4f9c1e4b1b8d36360785dd6d8f(eventobject) {
        var self = this;
        this.init();
    },
    /** preShow defined for frmManageTransferRecipientInfo **/
    AS_Form_b5c239a3f464495c961b0c9d9c4256c9: function AS_Form_b5c239a3f464495c961b0c9d9c4256c9(eventobject) {
        var self = this;
        this.preShow();
    }
});
define("TransferModule/frmManageTransferRecipientInfoController", ["TransferModule/userfrmManageTransferRecipientInfoController", "TransferModule/frmManageTransferRecipientInfoControllerActions"], function() {
    var controller = require("TransferModule/userfrmManageTransferRecipientInfoController");
    var controllerActions = ["TransferModule/frmManageTransferRecipientInfoControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
