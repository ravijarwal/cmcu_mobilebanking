NewUserBusinessManagerConfig = {
    "ModuleName": "NewUserBusinessManager",
    "BusinessControllerConfig": {
        "CommandHandler": [],
        "BusinessControllerClass": "NewUserBusinessManager/BusinessControllers/BusinessController",
          "BusinessExtensions": [
                "NewUserBusinessManager/BusinessControllers/BusinessController_Extn"
            ]
    }
};