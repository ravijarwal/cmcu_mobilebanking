AccountManagerConfig = {
    "ModuleName": "AccountManager",
    "BusinessControllerConfig": {
        "CommandHandler": [],
        "BusinessControllerClass": "AccountManager/BusinessControllers/BusinessController",
        "BusinessExtensions": [
                "AccountManager/BusinessControllers/BusinessController_Extension"
            ]
    }
};