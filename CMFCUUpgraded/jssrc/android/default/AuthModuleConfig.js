AuthModuleConfig = {
  "BusinessControllerConfig": {
    "BusinessControllerClass": "AuthModule/BusinessControllers/BusinessController",
    "CommandHandler": []
  },
  "Forms": {
    "mobile": {
      "frmDevRegFaceId": {
        "ControllerExtensions": [],
        "FormController": "kony.mvc.MDAFormController",
        "Controller": "AuthModule/frmDevRegFaceIdController",
        "FormName": "AuthModule/frmDevRegFaceId",
        "friendlyName": "frmDevRegFaceId"
      },
      "frmDevRegFaceIdSetAsDefault": {
        "ControllerExtensions": [],
        "FormController": "kony.mvc.MDAFormController",
        "Controller": "AuthModule/frmDevRegFaceIdSetAsDefaultController",
        "FormName": "AuthModule/frmDevRegFaceIdSetAsDefault",
        "friendlyName": "frmDevRegFaceIdSetAsDefault"
      },
      "frmDevRegLanding": {
        "ControllerExtensions": [],
        "FormController": "kony.mvc.MDAFormController",
        "Controller": "AuthModule/frmDevRegLandingController",
        "FormName": "AuthModule/frmDevRegLanding",
        "friendlyName": "frmDevRegLanding"
      },
      "frmDevRegLoginType": {
        "ControllerExtensions": [],
        "FormController": "kony.mvc.MDAFormController",
        "Controller": "AuthModule/frmDevRegLoginTypeController",
        "FormName": "AuthModule/frmDevRegLoginType",
        "friendlyName": "frmDevRegLoginType"
      },
      "frmDevRegPin": {
        "ControllerExtensions": [],
        "FormController": "kony.mvc.MDAFormController",
        "Controller": "AuthModule/frmDevRegPinController",
        "FormName": "AuthModule/frmDevRegPin",
        "friendlyName": "frmDevRegPin"
      },
      "frmDevRegPinConfirmation": {
        "ControllerExtensions": [],
        "FormController": "kony.mvc.MDAFormController",
        "Controller": "AuthModule/frmDevRegPinConfirmationController",
        "FormName": "AuthModule/frmDevRegPinConfirmation",
        "friendlyName": "frmDevRegPinConfirmation"
      },
      "frmDevRegSecCode": {
        "ControllerExtensions": [],
        "FormController": "kony.mvc.MDAFormController",
        "Controller": "AuthModule/frmDevRegSecCodeController",
        "FormName": "AuthModule/frmDevRegSecCode",
        "friendlyName": "frmDevRegSecCode"
      },
      "frmDevRegTouchId": {
        "ControllerExtensions": [],
        "FormController": "kony.mvc.MDAFormController",
        "Controller": "AuthModule/frmDevRegTouchIdController",
        "FormName": "AuthModule/frmDevRegTouchId",
        "friendlyName": "frmDevRegTouchId"
      },
      "frmExternalBankLogin": {
        "ControllerExtensions": [],
        "FormController": "kony.mvc.MDAFormController",
        "Controller": "AuthModule/frmExternalBankLoginController",
        "FormName": "AuthModule/frmExternalBankLogin",
        "friendlyName": "frmExternalBankLogin"
      },
      "frmForgotCreatePassword": {
        "ControllerExtensions": [],
        "FormController": "kony.mvc.MDAFormController",
        "Controller": "AuthModule/frmForgotCreatePasswordController",
        "FormName": "AuthModule/frmForgotCreatePassword",
        "friendlyName": "frmForgotCreatePassword"
      },
      "frmForgotEnterCVV": {
        "ControllerExtensions": [],
        "FormController": "kony.mvc.MDAFormController",
        "Controller": "AuthModule/frmForgotEnterCVVController",
        "FormName": "AuthModule/frmForgotEnterCVV",
        "friendlyName": "frmForgotEnterCVV"
      },
      "frmForgotEnterDOB": {
        "ControllerExtensions": [],
        "FormController": "kony.mvc.MDAFormController",
        "Controller": "AuthModule/frmForgotEnterDOBController",
        "FormName": "AuthModule/frmForgotEnterDOB",
        "friendlyName": "frmForgotEnterDOB"
      },
      "frmForgotEnterLastName": {
        "ControllerExtensions": [],
        "FormController": "kony.mvc.MDAFormController",
        "Controller": "AuthModule/frmForgotEnterLastNameController",
        "FormName": "AuthModule/frmForgotEnterLastName",
        "friendlyName": "frmForgotEnterLastName"
      },
      "frmForgotEnterSSN": {
        "ControllerExtensions": [],
        "FormController": "kony.mvc.MDAFormController",
        "Controller": "AuthModule/frmForgotEnterSSNController",
        "FormName": "AuthModule/frmForgotEnterSSN",
        "friendlyName": "frmForgotEnterSSN"
      },
      "frmForgotEnterSecurityCode": {
        "ControllerExtensions": [],
        "FormController": "kony.mvc.MDAFormController",
        "Controller": "AuthModule/frmForgotEnterSecurityCodeController",
        "FormName": "AuthModule/frmForgotEnterSecurityCode",
        "friendlyName": "frmForgotEnterSecurityCode"
      },
      "frmForgotMain": {
        "ControllerExtensions": [],
        "FormController": "kony.mvc.MDAFormController",
        "Controller": "AuthModule/frmForgotMainController",
        "FormName": "AuthModule/frmForgotMain",
        "friendlyName": "frmForgotMain"
      },
      "frmForgotSelectMethod": {
        "ControllerExtensions": [],
        "FormController": "kony.mvc.MDAFormController",
        "Controller": "AuthModule/frmForgotSelectMethodController",
        "FormName": "AuthModule/frmForgotSelectMethod",
        "friendlyName": "frmForgotSelectMethod"
      },
      "frmFullScreenAds": {
        "ControllerExtensions": [],
        "FormController": "kony.mvc.MDAFormController",
        "Controller": "AuthModule/frmFullScreenAdsController",
        "FormName": "AuthModule/frmFullScreenAds",
        "friendlyName": "frmFullScreenAds"
      },
      "frmLogin": {
        "ControllerExtensions": [],
        "FormController": "kony.mvc.MDAFormController",
        "Controller": "AuthModule/frmLoginController",
        "FormName": "AuthModule/frmLogin",
        "friendlyName": "frmLogin"
      },
      "frmForgotUsername": {
        "ControllerExtensions": [],
        "FormController": "kony.mvc.MDAFormController",
        "Controller": "AuthModule/frmForgotUsernameController",
        "FormName": "AuthModule/frmForgotUsername"
      },
      "frmForgotAccountPrimaryLinking": {
        "ControllerExtensions": [],
        "FormController": "kony.mvc.MDAFormController",
        "Controller": "AuthModule/frmForgotAccountPrimaryLinkingController",
        "FormName": "AuthModule/frmForgotAccountPrimaryLinking",
        "friendlyName": "frmForgotAccountPrimaryLinking"
      },
      "frmLanguageSelectionLoading": {
        "Controller": "AuthModule/frmLanguageSelectionLoadingController",
        "ControllerExtensions": [],
        "FormController": "kony.mvc.MDAFormController",
        "FormName": "AuthModule/frmLanguageSelectionLoading",
        "friendlyName": "frmLanguageSelectionLoading"
      }
    },
    "tablet": {
      "frmLogin": {
        "ControllerExtensions": [],
        "FormController": "kony.mvc.MDAFormController",
        "Controller": "AuthModule/frmLoginController",
        "FormName": "AuthModule/frmLogin",
        "friendlyName": "frmLogin"
      },
      "frmDevRegLanding": {
        "ControllerExtensions": [],
        "FormController": "kony.mvc.MDAFormController",
        "Controller": "AuthModule/frmDevRegLandingController",
        "FormName": "AuthModule/frmDevRegLanding",
        "friendlyName": "frmDevRegLanding"
      },
      "frmDevRegPinConfirmation": {
        "ControllerExtensions": [],
        "FormController": "kony.mvc.MDAFormController",
        "Controller": "AuthModule/frmDevRegPinConfirmationController",
        "FormName": "AuthModule/frmDevRegPinConfirmation",
        "friendlyName": "frmDevRegPinConfirmation"
      },
      "frmDevRegPin": {
        "ControllerExtensions": [],
        "FormController": "kony.mvc.MDAFormController",
        "Controller": "AuthModule/frmDevRegPinController",
        "FormName": "AuthModule/frmDevRegPin",
        "friendlyName": "frmDevRegPin"
      },
      "frmFullScreenAds": {
        "ControllerExtensions": [],
        "FormController": "kony.mvc.MDAFormController",
        "Controller": "AuthModule/frmFullScreenAdsController",
        "FormName": "AuthModule/frmFullScreenAds",
        "friendlyName": "frmFullScreenAds"
      },
      "frmDevRegLoginType": {
        "ControllerExtensions": [],
        "FormController": "kony.mvc.MDAFormController",
        "Controller": "AuthModule/frmDevRegLoginTypeController",
        "FormName": "AuthModule/frmDevRegLoginType",
        "friendlyName": "frmDevRegLoginType"
      },
      "frmDevRegSecCode": {
        "ControllerExtensions": [],
        "FormController": "kony.mvc.MDAFormController",
        "Controller": "AuthModule/frmDevRegSecCodeController",
        "FormName": "AuthModule/frmDevRegSecCode",
        "friendlyName": "frmDevRegSecCode"
      },
      "frmForgotMain": {
        "ControllerExtensions": [],
        "FormController": "kony.mvc.MDAFormController",
        "Controller": "AuthModule/frmForgotMainController",
        "FormName": "AuthModule/frmForgotMain",
        "friendlyName": "frmForgotMain"
      },
      "frmDefaultLogin": {
        "ControllerExtensions": [],
        "FormController": "kony.mvc.MDAFormController",
        "Controller": "AuthModule/frmDefaultLoginController",
        "FormName": "AuthModule/frmDefaultLogin",
        "friendlyName": "frmDefaultLogin"
      },
      "frmForgotEnterLastName": {
        "ControllerExtensions": [],
        "FormController": "kony.mvc.MDAFormController",
        "Controller": "AuthModule/frmForgotEnterLastNameController",
        "FormName": "AuthModule/frmForgotEnterLastName",
        "friendlyName": "frmForgotEnterLastName"
      },
      "frmForgotEnterSSN": {
        "ControllerExtensions": [],
        "FormController": "kony.mvc.MDAFormController",
        "Controller": "AuthModule/frmForgotEnterSSNController",
        "FormName": "AuthModule/frmForgotEnterSSN",
        "friendlyName": "frmForgotEnterSSN"
      },
      "frmForgotEnterDOB": {
        "ControllerExtensions": [],
        "FormController": "kony.mvc.MDAFormController",
        "Controller": "AuthModule/frmForgotEnterDOBController",
        "FormName": "AuthModule/frmForgotEnterDOB",
        "friendlyName": "frmForgotEnterDOB"
      },
      "frmForgotSelectMethod": {
        "ControllerExtensions": [],
        "FormController": "kony.mvc.MDAFormController",
        "Controller": "AuthModule/frmForgotSelectMethodController",
        "FormName": "AuthModule/frmForgotSelectMethod",
        "friendlyName": "frmForgotSelectMethod"
      },
      "frmForgotEnterCVV": {
        "ControllerExtensions": [],
        "FormController": "kony.mvc.MDAFormController",
        "Controller": "AuthModule/frmForgotEnterCVVController",
        "FormName": "AuthModule/frmForgotEnterCVV",
        "friendlyName": "frmForgotEnterCVV"
      },
      "frmForgotCreatePassword": {
        "ControllerExtensions": [],
        "FormController": "kony.mvc.MDAFormController",
        "Controller": "AuthModule/frmForgotCreatePasswordController",
        "FormName": "AuthModule/frmForgotCreatePassword",
        "friendlyName": "frmForgotCreatePassword"
      },
      "frmDevRegFaceId": {
        "ControllerExtensions": [],
        "FormController": "kony.mvc.MDAFormController",
        "Controller": "AuthModule/frmDevRegFaceIdController",
        "FormName": "AuthModule/frmDevRegFaceId",
        "friendlyName": "frmDevRegFaceId"
      },
      "frmForgotEnterSecurityCode": {
        "ControllerExtensions": [],
        "FormController": "kony.mvc.MDAFormController",
        "Controller": "AuthModule/frmForgotEnterSecurityCodeController",
        "FormName": "AuthModule/frmForgotEnterSecurityCode",
        "friendlyName": "frmForgotEnterSecurityCode"
      },
      "frmDevRegTouchId": {
        "ControllerExtensions": [],
        "FormController": "kony.mvc.MDAFormController",
        "Controller": "AuthModule/frmDevRegTouchIdController",
        "FormName": "AuthModule/frmDevRegTouchId",
        "friendlyName": "frmDevRegTouchId"
      },
      "frmDevRegFaceIdSetAsDefault": {
        "ControllerExtensions": [],
        "FormController": "kony.mvc.MDAFormController",
        "Controller": "AuthModule/frmDevRegFaceIdSetAsDefaultController",
        "FormName": "AuthModule/frmDevRegFaceIdSetAsDefault",
        "friendlyName": "frmDevRegFaceIdSetAsDefault"
      },
      "frmExternalBankLogin": {
        "ControllerExtensions": [],
        "FormController": "kony.mvc.MDAFormController",
        "Controller": "AuthModule/frmExternalBankLoginController",
        "FormName": "AuthModule/frmExternalBankLogin",
        "friendlyName": "frmExternalBankLogin"
      }
    }
  },
  "ModuleName": "AuthModule",
  "PresentationControllerConfig": {
    "Default": {
      "PresentationControllerClass": "AuthModule/PresentationControllers/PresentationController",
      "PresentationExtensions": [
        "AuthModule/PresentationControllers/AuthModule_PresentationController_Extension"
      ]
    },
    "Tablet": {
      "PresentationControllerClass": "AuthModule/PresentationControllers/PresentationController_Tablet",
      "PresentationExtensions": []
    }
  }
};