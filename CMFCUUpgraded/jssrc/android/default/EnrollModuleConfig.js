EnrollModuleConfig = {
  "BusinessControllerConfig": {
    "BusinessControllerClass": "EnrollModule/BusinessControllers/BusinessController",
    "CommandHandler": []
  },
  "Forms": {
    "mobile": {
      "frmEnrollAccinfo": {
        "ControllerExtensions": [],
        "FormController": "kony.mvc.MDAFormController",
        "Controller": "EnrollModule/frmEnrollAccinfoController",
        "FormName": "EnrollModule/frmEnrollAccinfo",
        "friendlyName": "frmEnrollAccinfo"
      },
      "frmEnrollCVV": {
        "ControllerExtensions": [],
        "FormController": "kony.mvc.MDAFormController",
        "Controller": "EnrollModule/frmEnrollCVVController",
        "FormName": "EnrollModule/frmEnrollCVV",
        "friendlyName": "frmEnrollCVV"
      },
      "frmEnrollDOB": {
        "ControllerExtensions": [],
        "FormController": "kony.mvc.MDAFormController",
        "Controller": "EnrollModule/frmEnrollDOBController",
        "FormName": "EnrollModule/frmEnrollDOB",
        "friendlyName": "frmEnrollDOB"
      },
      "frmEnrollLastName": {
        "ControllerExtensions": [],
        "FormController": "kony.mvc.MDAFormController",
        "Controller": "EnrollModule/frmEnrollLastNameController",
        "FormName": "EnrollModule/frmEnrollLastName",
        "friendlyName": "frmEnrollLastName"
      },
      "frmEnrollSSn": {
        "ControllerExtensions": [],
        "FormController": "kony.mvc.MDAFormController",
        "Controller": "EnrollModule/frmEnrollSSnController",
        "FormName": "EnrollModule/frmEnrollSSn",
        "friendlyName": "frmEnrollSSn"
      },
      "frmEnrollSecurity": {
        "ControllerExtensions": [],
        "FormController": "kony.mvc.MDAFormController",
        "Controller": "EnrollModule/frmEnrollSecurityController",
        "FormName": "EnrollModule/frmEnrollSecurity",
        "friendlyName": "frmEnrollSecurity"
      },
      "frmEnrollSecurityCheck": {
        "ControllerExtensions": [],
        "FormController": "kony.mvc.MDAFormController",
        "Controller": "EnrollModule/frmEnrollSecurityCheckController",
        "FormName": "EnrollModule/frmEnrollSecurityCheck",
        "friendlyName": "frmEnrollSecurityCheck"
      },
      "frmEnrollSignUp": {
        "ControllerExtensions": [],
        "FormController": "kony.mvc.MDAFormController",
        "Controller": "EnrollModule/frmEnrollSignUpController",
        "FormName": "EnrollModule/frmEnrollSignUp",
        "friendlyName": "frmEnrollSignUp"
      },
      "frmEmailCapture": {
        "ControllerExtensions": [],
        "FormController": "kony.mvc.MDAFormController",
        "Controller": "EnrollModule/frmEmailCaptureController",
        "FormName": "EnrollModule/frmEmailCapture",
        "friendlyName": "frmEmailCapture"
      },
      "frmAccountLinking": {
        "ControllerExtensions": [],
        "FormController": "kony.mvc.MDAFormController",
        "Controller": "EnrollModule/frmAccountLinkingController",
        "FormName": "EnrollModule/frmAccountLinking",
        "friendlyName": "frmAccountLinking"
      },
      "frmEnrollVerifyIdentity": {
        "ControllerExtensions": [],
        "FormController": "kony.mvc.MDAFormController",
        "Controller": "EnrollModule/frmEnrollVerifyIdentityController",
        "FormName": "EnrollModule/frmEnrollVerifyIdentity",
        "friendlyName": "frmEnrollVerifyIdentity"
      },
      "frmAccountPrimaryLinking": {
        "ControllerExtensions": [],
        "FormController": "kony.mvc.MDAFormController",
        "Controller": "EnrollModule/frmAccountPrimaryLinkingController",
        "FormName": "EnrollModule/frmAccountPrimaryLinking",
        "friendlyName": "frmAccountPrimaryLinking"
      }
    },
    "tablet": {
      "frmEnrollCVV": {
        "Controller": "EnrollModule/frmEnrollCVVController",
        "ControllerExtensions": [],
        "FormController": "kony.mvc.MDAFormController",
        "FormName": "EnrollModule/frmEnrollCVV",
        "friendlyName": "frmEnrollCVV"
      },
      "frmEnrollCVVNumber": {
        "Controller": "EnrollModule/frmEnrollCVVNumberController",
        "ControllerExtensions": [],
        "FormController": "kony.mvc.MDAFormController",
        "FormName": "EnrollModule/frmEnrollCVVNumber",
        "friendlyName": "frmEnrollCVVNumber"
      },
      "frmEnrollDOB": {
        "Controller": "EnrollModule/frmEnrollDOBController",
        "ControllerExtensions": [],
        "FormController": "kony.mvc.MDAFormController",
        "FormName": "EnrollModule/frmEnrollDOB",
        "friendlyName": "frmEnrollDOB"
      },
      "frmEnrollLastName": {
        "Controller": "EnrollModule/frmEnrollLastNameController",
        "ControllerExtensions": [],
        "FormController": "kony.mvc.MDAFormController",
        "FormName": "EnrollModule/frmEnrollLastName",
        "friendlyName": "frmEnrollLastName"
      },
      "frmEnrollSSn": {
        "Controller": "EnrollModule/frmEnrollSSnController",
        "ControllerExtensions": [],
        "FormController": "kony.mvc.MDAFormController",
        "FormName": "EnrollModule/frmEnrollSSn",
        "friendlyName": "frmEnrollSSn"
      },
      "frmEnrollSecurity": {
        "Controller": "EnrollModule/frmEnrollSecurityController",
        "ControllerExtensions": [],
        "FormController": "kony.mvc.MDAFormController",
        "FormName": "EnrollModule/frmEnrollSecurity",
        "friendlyName": "frmEnrollSecurity"
      },
      "frmEnrollSecurityCheck": {
        "Controller": "EnrollModule/frmEnrollSecurityCheckController",
        "ControllerExtensions": [],
        "FormController": "kony.mvc.MDAFormController",
        "FormName": "EnrollModule/frmEnrollSecurityCheck",
        "friendlyName": "frmEnrollSecurityCheck"
      },
      "frmEnrollSignUp": {
        "Controller": "EnrollModule/frmEnrollSignUpController",
        "ControllerExtensions": [],
        "FormController": "kony.mvc.MDAFormController",
        "FormName": "EnrollModule/frmEnrollSignUp",
        "friendlyName": "frmEnrollSignUp"
      },
      "frmAlreadyEnrolled": {
        "ControllerExtensions": [],
        "FormController": "kony.mvc.MDAFormController",
        "Controller": "EnrollModule/frmAlreadyEnrolledController",
        "FormName": "EnrollModule/frmAlreadyEnrolled",
        "friendlyName": "frmAlreadyEnrolled"
      }
    }
  },
  "ModuleName": "EnrollModule",
  "PresentationControllerConfig": {
    "Default": {
      "PresentationControllerClass": "EnrollModule/PresentationControllers/PresentationController",
      "PresentationExtensions": [
        "EnrollModule/PresentationControllers/PresentationController_Extn"
      ]
    },
    "Tablet": {
      "PresentationControllerClass": "EnrollModule/PresentationControllers/PresentationController_Tablet",
      "PresentationExtensions": []
    }
  }
};