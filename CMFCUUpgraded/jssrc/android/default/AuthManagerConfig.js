AuthManagerConfig = {
    "ModuleName": "AuthManager",
    "BusinessControllerConfig": {
        "CommandHandler": [],
        "BusinessControllerClass": "AuthManager/BusinessControllers/BusinessController",
           "BusinessExtensions": [
                "AuthManager/BusinessControllers/BusinessController_Extn"
            ]
    }
};