MenuHandlerConfig = 
{
    "ModuleName": "MenuHandler",
    "BusinessControllerConfig": {
        "CommandHandler": [],
        "BusinessControllerClass": "MenuHandler/BusinessControllers/BusinessController",
        "BusinessExtensions": [
                "MenuHandler/BusinessControllers/BusinessController_Extsn"
            ]
    }
};