ConfigurationManagerConfig = {
    "ModuleName": "ConfigurationManager",
    "BusinessControllerConfig": {
        "CommandHandler": [],
        "BusinessControllerClass": "ConfigurationManager/BusinessControllers/BusinessController",
        "BusinessExtensions": [
                "ConfigurationManager/BusinessControllers/BusinessController_Extsn"
            ]
    }
}
;